//
//  AboutUsViewController.m
//  Yewang
//
//  Created by everhelp on 16/4/14.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import "AboutUsViewController.h"
#import "MyMapViewController.h"

@interface AboutUsViewController ()

@end

@implementation AboutUsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"关于我们";
    
}
- (void)viewWillAppear:(BOOL)animated{
    self.tabBarController.tabBar.hidden = YES;
    self.navigationController.navigationBar.hidden = NO;
    [super viewWillAppear:animated];
}
- (IBAction)clickLocation:(id)sender {
    MyMapViewController *myMap = [self.storyboard instantiateViewControllerWithIdentifier:@"mymap"];
    myMap.latitude = 0;
    myMap.longitude = 0;
//    [self presentViewController:myMap animated:YES completion:nil];
    [self.navigationController pushViewController:myMap animated:YES];
}

- (IBAction)clickCall:(id)sender {
    NSMutableString *strPhone = [[NSMutableString alloc]initWithFormat:@"tel:4008507198"];
    UIWebView *callWebView = [[UIWebView alloc]init];
    [callWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:strPhone]]];
    [self .view addSubview:callWebView];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
