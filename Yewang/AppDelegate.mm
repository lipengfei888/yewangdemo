//
//  AppDelegate.m
//  Yewang
//
//  Created by everhelp on 16/4/7.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import "AppDelegate.h"
#import <AVOSCloudIM.h>
#import <AlipaySDK/AlipaySDK.h>
#import <ShareSDK/ShareSDK.h>
#import <ShareSDKConnector/ShareSDKConnector.h>
#import <TencentOpenAPI/TencentOAuth.h>
#import <TencentOpenAPI/QQApiInterface.h>
#import "WXApi.h"
#import "WeiboSDK.h"
//新浪微博SDK需要在项目Build Settings中的Other Linker Flags添加"-ObjC"
#import <CoreLocation/CoreLocation.h>
#import "ClubListInfo.h"
#import "FMDBHepler.h"
#import <BaiduMapAPI_Base/BMKBaseComponent.h>

@interface AppDelegate ()<WXApiDelegate>{
    BMKMapManager *_mapManager;
}
@property (nonatomic ,strong) FMDBHepler *helperManager;
@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    [self setAllNavigationBaeStyle];
    ///leancould
    [AVOSCloud setApplicationId:@"NmgGH9HnFh4PlESeYASfKjtG" clientKey:@"1D6RKNBTWMogrRfLKCb8CBAa"];
    //跟踪统计应用的打开情况
    [AVAnalytics trackAppOpenedWithLaunchOptions:launchOptions];
    
    //初始化第三方服务（分享，地图）
    [self initTheThirdService];
    //判断是否是第一次打开应用
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"firstStart"]) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"firstStart"];
        NSLog(@"第一次启动");
       
    } else {
        NSLog(@"不是第一次启动");
       
    }
    [self loadDataCache];
    return YES;
}

#pragma mark - 设置导航栏文字颜色
- (void)setAllNavigationBaeStyle{
    UINavigationBar *navBar = [UINavigationBar appearance];
    navBar.barTintColor = kRGBColor;
    navBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor whiteColor]};
    navBar.tintColor = [UIColor whiteColor];
    UIBarButtonItem *item = [UIBarButtonItem appearance];
    item.tintColor = [UIColor whiteColor];
}
#pragma mark - 加载分享
- (void)initTheThirdService{
    
    //初始化百度地图
    _mapManager = [[BMKMapManager alloc] init];
    BOOL ret = [_mapManager start:@"cSXI45X0yFcZzsoW7Hoxd4IaDAyoHITC" generalDelegate:nil];
    if (!ret) {
        NSLog(@"BaiduMapManager start failed");
    }
    
    /*//从 APNs 注册推送所需的 device token
     if (launchOptions) {
     // do something else
     
     
     [AVAnalytics trackAppOpenedWithLaunchOptions:launchOptions];
     }
     [AVOSCloudIM registerForRemoteNotification];*/
    
    /**
     *  设置ShareSDK的appKey，如果尚未在ShareSDK官网注册过App，请移步到http://mob.com/login 登录后台进行应用注册
     *  在将生成的AppKey传入到此方法中。
     *  方法中的第二个第三个参数为需要连接社交平台SDK时触发，
     *  在此事件中写入连接代码。第四个参数则为配置本地社交平台时触发，根据返回的平台类型来配置平台信息。
     *  如果您使用的时服务端托管平台信息时，第二、四项参数可以传入nil，第三项参数则根据服务端托管平台来决定要连接的社交SDK。
     */
    //向微信终端注册appID
    //    [WXApi registerApp:@"wx4868b35061f87885" withDescription:@"NightClub"];
    [WXApi registerApp:@"wx9cc86b555863397d" withDescription:@"NightNet"];
    [ShareSDK registerApp:@"iosv1101"
     
          activePlatforms:@[
                            //                            @(SSDKPlatformTypeSinaWeibo),
                            @(SSDKPlatformTypeSMS),
                            @(SSDKPlatformTypeWechat),
                            @(SSDKPlatformTypeQQ)]
                 onImport:^(SSDKPlatformType platformType)
     {
         switch (platformType)
         {
             case SSDKPlatformTypeWechat:
                 [ShareSDKConnector connectWeChat:[WXApi class]];
                 break;
             case SSDKPlatformTypeQQ:
                 [ShareSDKConnector connectQQ:[QQApiInterface class] tencentOAuthClass:[TencentOAuth class]];
                 break;
                 //             case SSDKPlatformTypeSinaWeibo:
                 //                 [ShareSDKConnector connectWeibo:[WeiboSDK class]];
                 //                 break;
             default:
                 break;
         }
     }
          onConfiguration:^(SSDKPlatformType platformType, NSMutableDictionary *appInfo)
     {
         
         switch (platformType)
         {
             case SSDKPlatformTypeSinaWeibo:
                 //设置新浪微博应用信息,其中authType设置为使用SSO＋Web形式授权
                 //                 [appInfo SSDKSetupSinaWeiboByAppKey:@"568898243"
                 //                                           appSecret:@"38a4f8204cc784f81f9f0daaf31e02e3"
                 //                                         redirectUri:@"http://www.sharesdk.cn"
                 //                                            authType:SSDKAuthTypeBoth];
                 //                 break;
             case SSDKPlatformTypeWechat:
                 [appInfo SSDKSetupWeChatByAppId:@"wx9cc86b555863397d"
                                       appSecret:@"64020361b8ec4c99936c0e3999a9f249"];
                 break;
             case SSDKPlatformTypeQQ:
                 [appInfo SSDKSetupQQByAppId:@"100371282"
                                      appKey:@"aed9b0303e3ed1e27bae87c33761161d"
                                    authType:SSDKAuthTypeBoth];
                 break;
             default:
                 break;
         }
     }];
}
#pragma mark - 缓存数据
- (void)loadDataCache{
    self.helperManager = [[FMDBHepler sharedInstance] init];
    NSArray *arr = [self.helperManager stepsFromSqlite];
    if (arr.count <1) {
        NSString *strUrl = [NSString stringWithFormat:@"%@Club/GetClubList?Id=6",BASICURL];
        [BingNetWorkConnection connectionWithUrl:strUrl type:BINGO_REQUEST_GET params:nil FinishBlock:^(id responseObject) {
            NSArray *array = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves error:nil];
            for (NSDictionary *dict in array) {
                ClubListInfo *clubInfo = [[ClubListInfo alloc] initWithDict:dict];
                NSLog(@"%@",clubInfo);
            }
            
        } FailedBlock:^(NSError *error) {
            NSLog(@"缓存数据：%@",error.userInfo);
        }];
    }
}

#pragma mark - 判断定位权限
- (void)getLocationManagerStatus{
    NSString *statusStr ;
    if ([CLLocationManager locationServicesEnabled] &&([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse || [CLLocationManager authorizationStatus] == kCLAuthorizationStatusNotDetermined)) {
        //定位功能可用，开始定位
        NSLog(@"定位功能可用，开始定位");
        statusStr = @"open";
    } else if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied){
        NSLog(@"定位功能不可用，提示用户或忽略");
        statusStr = @"close";
    }
    NSNotification * notice = [NSNotification notificationWithName:@"getLocationStatus" object:nil userInfo:@{@"status":statusStr}];
    [[NSNotificationCenter defaultCenter]postNotification:notice];
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}
/*
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken{
    
    //如果注册成功，APNs会返回给你设备的token，iOS系统会把它传递给app delegate代理——application:didRegisterForRemoteNotificationsWithDeviceToken:方法，你应该在这个方法里面把token保存到AVOS Cloud后台
    
    AVInstallation *currentInstallation = [AVInstallation currentInstallation];
    [currentInstallation setDeviceTokenFromData:deviceToken];
//    currentInstallation.badge
    [currentInstallation setObject:@(YES) forKey:@"您有新的订房,请及时处理."];
    [currentInstallation saveInBackground];
}
- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    //如果注册失败，application:didFailToRegisterForRemoteNotificationsWithError:方法会被调用，通过NSError参数你可以看到具体的出错信息
    NSLog(@"注册失败，无法获取设备ID, 具体错误: %@", error);
}
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    if (application.applicationState == UIApplicationStateActive) {
        // 转换成一个本地通知，显示到通知栏，你也可以直接显示出一个alertView，只是那样稍显aggressive：）
        UILocalNotification *localNotification = [[UILocalNotification alloc] init];
        localNotification.userInfo = userInfo;
        localNotification.soundName = UILocalNotificationDefaultSoundName;
        localNotification.alertBody = [[userInfo objectForKey:@"aps"] objectForKey:@"alert"];
        localNotification.fireDate = [NSDate date];
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    } else {
        [AVAnalytics trackAppOpenedWithRemoteNotificationPayload:userInfo];
    }
    NSLog(@"userInfo == %@",userInfo);
    NSString *message = [[userInfo objectForKey:@"aps"]objectForKey:@"alert"];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:message delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    
    [alert show];
}*/
- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    NSLog(@"=========================这是从前台，后台进入都经过的方法");
    [self getLocationManagerStatus];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    //如果极简开发包不可用,会跳转支付宝钱包进行支付,需要将支付宝钱包的支付结果回传给开 发包
    if ([url.host isEqualToString:@"safepay"])
    {
        [[AlipaySDK defaultService] processOrderWithPaymentResult:url standbyCallback:^(NSDictionary *resultDic) {
            
            NSNotification * notice = [NSNotification notificationWithName:@"payResult" object:nil userInfo:@{@"result":resultDic}];
            [[NSNotificationCenter defaultCenter]postNotification:notice];
            NSNotification * notice2 = [NSNotification notificationWithName:@"payAmountResult" object:nil userInfo:@{@"result":resultDic}];
            [[NSNotificationCenter defaultCenter]postNotification:notice2];
        }];
    }
    
    if ([url.host isEqualToString:@"platformapi"])//支付宝钱包快登授权返回 authCode
    {
        [[AlipaySDK defaultService] processAuthResult:url standbyCallback:^(NSDictionary *resultDic) {
           //payAmountResult
            NSNotification * notice = [NSNotification notificationWithName:@"payResult" object:nil userInfo:@{@"result":resultDic}];
            [[NSNotificationCenter defaultCenter]postNotification:notice];
            NSNotification * notice2 = [NSNotification notificationWithName:@"payAmountResult" object:nil userInfo:@{@"result":resultDic}];
            [[NSNotificationCenter defaultCenter]postNotification:notice2];
        }];
    }
    
    //微信支付的跳转处理
    if ([WXApi handleOpenURL:url delegate:self]) {
        return [WXApi handleOpenURL:url delegate:self];
    }
    
    return YES;
}

- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<NSString *,id> *)options{
    if ([url.host isEqualToString:@"safepay"]) {
        [[AlipaySDK defaultService] processOrderWithPaymentResult:url standbyCallback:^(NSDictionary *resultDic) {
            //【由于在跳转支付宝客户端支付的过程中,商户 app 在后台很可能被系统 kill 了,所以 pay 接口的 callback 就会失效,请商户对 standbyCallback 返回的回调结果进行处理,就是在这个方 法里面处理跟 callback 一样的逻辑】
            //注册一个通知，
            //创建一个消息对象
            NSNotification * notice = [NSNotification notificationWithName:@"payResult" object:nil userInfo:@{@"result":resultDic}];
            //发送消息
            [[NSNotificationCenter defaultCenter]postNotification:notice];
           
        }];
    }
    //微信支付的跳转处理
    if ([WXApi handleOpenURL:url delegate:self]) {
        return [WXApi handleOpenURL:url delegate:self];
    }
    
    return YES;
}

- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url{
    return  [WXApi handleOpenURL:url delegate:self];
}

// 微信支付成功或者失败回调
- (void)onResp:(BaseResp*)resp
{
    NSString *strMsg = [NSString stringWithFormat:@"errcode:%d", resp.errCode];
    NSString *strTitle;
    
    if([resp isKindOfClass:[SendMessageToWXResp class]])
    {
        strTitle = [NSString stringWithFormat:@"发送媒体消息结果"];
    }
    if([resp isKindOfClass:[PayResp class]]){
        //支付返回结果，实际支付结果需要去微信服务器端查询
        strTitle = [NSString stringWithFormat:@"支付结果"];
        
        switch (resp.errCode) {
            case WXSuccess:
                strMsg = @"支付结果：成功！";
                NSLog(@"支付成功－PaySuccess，retcode = %d", resp.errCode);
                break;
                
            default:
                strMsg = [NSString stringWithFormat:@"支付结果：失败！retcode = %d, retstr = %@", resp.errCode,resp.errStr];
                NSLog(@"错误，retcode = %d, retstr = %@", resp.errCode,resp.errStr);
                break;
        }
    }
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:strTitle message:strMsg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}
//-(void) onResp:(BaseResp*)resp
//{
//    NSString *strMsg = [NSString stringWithFormat:@"errcode:%d", resp.errCode];
//    NSString *strTitle;
//    
//    if([resp isKindOfClass:[SendMessageToWXResp class]])
//    {
//        strTitle = [NSString stringWithFormat:@"发送媒体消息结果"];
//    }
//    if([resp isKindOfClass:[PayResp class]]){
//        //支付返回结果，实际支付结果需要去微信服务器端查询
//        strTitle = [NSString stringWithFormat:@"支付结果"];
//        
//        switch (resp.errCode) {
//            case WXSuccess:{
//                strMsg = @"支付结果：成功！";
//                NSLog(@"支付成功－PaySuccess，retcode = %d", resp.errCode);
//                NSNotification *notification = [NSNotification notificationWithName:ORDER_PAY_NOTIFICATION object:@"success"];
//                [[NSNotificationCenter defaultCenter] postNotification:notification];
//                break;
//            }
//            default:{
//                strMsg = [NSString stringWithFormat:@"支付结果：失败！retcode = %d, retstr = %@", resp.errCode,resp.errStr];
//                NSLog(@"错误，retcode = %d, retstr = %@", resp.errCode,resp.errStr);
//                NSNotification *notification = [NSNotification notificationWithName:ORDER_PAY_NOTIFICATION object:@"fail"];
//                [[NSNotificationCenter defaultCenter] postNotification:notification];
//                break;
//            }
//        }
//    }
//    //    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:strTitle message:strMsg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//    //    [alert show];
//}

@end
