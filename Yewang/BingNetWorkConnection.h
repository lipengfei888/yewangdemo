//
//  BingNetWorkConnection.h
//  aaaaaaaaaaaaa
//
//  Created by Bing on 16/3/28.
//  Copyright © 2016年 Bing. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BingNetWorkRequest.h"

@interface BingNetWorkConnection : NSObject


/**
 *	@brief	封装系统请求
 *
 *	@param 	url 	地址
 *	@param 	type 	POST or GET
 *	@param 	dictionary 	参数
 *	@param 	finishBlock 	成功回调
 *	@param 	failedBlock 	失败回调
 */
+ (void)connectionWithUrl:(NSString *)url type:(BINGO_REQUEST_TYPE)type params:(NSDictionary *)dictionary FinishBlock:(BingFinishBlock)finishBlock FailedBlock:(BingFaileBlock)failedBlock;




//post请求封装
+ (BOOL)NetWorkIsOK;//检查网络是否可用

@end
