//
//  BingNetWorkConnection.m
//  aaaaaaaaaaaaa
//
//  Created by Bing on 16/3/28.
//  Copyright © 2016年 Bing. All rights reserved.
//

#import "BingNetWorkConnection.h"
#import "Reachability.h"

@implementation BingNetWorkConnection


+ (void)connectionWithUrl:(NSString *)url type:(BINGO_REQUEST_TYPE)type params:(NSMutableDictionary *)dictionary FinishBlock:(BingFinishBlock)finishBlock FailedBlock:(BingFaileBlock)failedBlock {
    BingNetWorkRequest * request = [[BingNetWorkRequest alloc]init];
    request.url         = url;
    request.dic         = dictionary.copy;
    request.type        = type;
    request.faileBlock  = failedBlock;
    request.finishBlock = finishBlock;
    
    [request startRequest];
}


//这个函数是判断网络是否可用的函数（wifi或者蜂窝数据可用，都返回YES）
+ (BOOL)NetWorkIsOK{
    if(
       ([[Reachability reachabilityForInternetConnection] currentReachabilityStatus]!= NotReachable)
       ||
       ([[Reachability reachabilityForLocalWiFi] currentReachabilityStatus]!= NotReachable)
       ){
        return YES;
    }else{
        return NO;
    }
}


@end
