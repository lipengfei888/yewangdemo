//
//  BingNetWorkRequest.h
//  aaaaaaaaaaaaa
//
//  Created by Bing on 16/3/28.
//  Copyright © 2016年 Bing. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

//请求类别
typedef enum {
    BINGO_REQUEST_POST              = 0x00,//POST
    BINGO_REQUEST_GET               = 0x01,//GET
}BINGO_REQUEST_TYPE;

typedef void (^BingFinishBlock)(id responseObject);
typedef void (^BingFaileBlock)(NSError *error);

@interface BingNetWorkRequest : NSObject<NSURLSessionDataDelegate>

@property (nonatomic, copy) NSMutableDictionary * dic;
@property (nonatomic, copy) NSString * url;

@property (nonatomic, copy) BingFinishBlock finishBlock;
@property (nonatomic, copy) BingFaileBlock faileBlock;

@property (nonatomic, assign) BINGO_REQUEST_TYPE type;

- (void)startRequest;

@end