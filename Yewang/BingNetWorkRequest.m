//
//  BingNetWorkRequest.m
//  aaaaaaaaaaaaa
//
//  Created by Bing on 16/3/28.
//  Copyright © 2016年 Bing. All rights reserved.
//

#import "BingNetWorkRequest.h"

@interface BingNetWorkRequest ()

@property (nonatomic, strong) NSMutableData *myData;
@end

@implementation BingNetWorkRequest

-(void)startRequest {
    
    //创建请求对象
    
    NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:self.url]];
    //设置超时时间
    request.timeoutInterval = 30;
    
    if (self.type == BINGO_REQUEST_GET) {
        NSLog(@"GET请求");
        // Get请求
        
    } else {
        NSLog(@"POST请求");
        //post
        NSString * string = [self parseParams:self.dic];
        
        NSData * data = [string dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
        
        request.HTTPMethod = @"POST";
        
        //根据后台接收和返回数据的类型对此处进行修改的
        [request addValue:@"text/html" forHTTPHeaderField:@"Content_Type"];
        
        //
        
        [request addValue:[NSString stringWithFormat:@"%ld",(unsigned long)data.length] forHTTPHeaderField:@"Content_Length"];
        
        request.HTTPBody = data;
        /*
         //block方法
         NSURLSession * session = [NSURLSession sharedSession];
         NSURLSessionDataTask * task = [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
         NSLog(@"error-->%@",error);
         NSLog(@"data-->%@",data);
         NSLog(@"response %@",response);
         NSDictionary * dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers | NSJSONReadingMutableLeaves error:nil];
         NSLog(@"返回结果-->%@",dict);
         }];
         */
    }
    
    //获得会话对象,并设置代理
    /*
     第一个参数：会话对象的配置信息defaultSessionConfiguration 表示默认配置
     第二个参数：谁成为代理，此处为控制器本身即self
     第三个参数：队列，该队列决定代理方法在哪个线程中调用，可以传主队列|非主队列
     [NSOperationQueue mainQueue]   主队列：   代理方法在主线程中调用
     [[NSOperationQueue alloc]init] 非主队列： 代理方法在子线程中调用
     */
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration] delegate:self delegateQueue:[NSOperationQueue mainQueue]];
    
    //根据会话对象创建一个Task(发送请求）
    // 创建任务(因为要使用代理方法,就不需要block方式的初始化了)
    
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request];
    
    //执行任务
    [task resume];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

//对应的代理方法如下:
// 1.接收到服务器的响应
- (void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask didReceiveResponse:(NSURLResponse *)response completionHandler:(void (^)(NSURLSessionResponseDisposition))completionHandler {
    NSLog(@"接收到响应");
    // 允许处理服务器的响应，才会继续接收服务器返回的数据
    completionHandler(NSURLSessionResponseAllow);
    if (_myData == nil)
    {
        _myData = [[NSMutableData alloc]init];
    }
    else
    {
        _myData.length = 0;
    }
    
}
// 2.接收到服务器的数据（可能调用多次）
- (void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask didReceiveData:(NSData *)data {
    NSLog(@"数据拼接");
    // 处理每次接收的数据
    [_myData appendData:data];
}
// 3.请求成功或者失败（如果失败，error有值）
- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(NSError *)error {
    NSLog(@"请求完成");
    // 请求完成,成功或者失败的处理
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    
    if (error) {//失败
        if (self.faileBlock) {
            self.faileBlock(error);
        }
    } else {//成功
        
        
        if (self.finishBlock) {
            self.finishBlock(self.myData);
        }
    }
    
}

//把NSDictionary解析成post格式的NSString字符串
- (NSString *)parseParams:(NSDictionary *)params{
    NSString *keyValueFormat;
    NSMutableString *result = [NSMutableString new];
    //实例化一个key枚举器用来存放dictionary的key
    NSEnumerator *keyEnum = [params keyEnumerator];
    id key;
    while (key = [keyEnum nextObject]) {
        keyValueFormat = [NSString stringWithFormat:@"%@=%@&",key,[params valueForKey:key]];
        [result appendString:keyValueFormat];
    }
    NSLog(@"post请求方法参数拼接之后的形式：%@",result);
    return result;
}


@end

