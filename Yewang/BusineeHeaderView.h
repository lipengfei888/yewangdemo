//
//  BusineeHeaderView.h
//  Yewang
//
//  Created by libin on 16/7/23.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import <UIKit/UIKit.h>
@class  ClubListInfo;

@protocol BusineeHeaderDelegate <NSObject>

- (void)addressAction;
- (void)clickSingleImgView:(NSString *)url;
- (void)clickPreferentialBtnAction;

@end

@interface BusineeHeaderView : UIView
///优惠买单
@property (nonatomic,strong) UIButton *preferentialBtn;

@property (nonatomic, strong) ClubListInfo *clubInfo;

@property (nonatomic,retain) id <BusineeHeaderDelegate> delegate;

@end
