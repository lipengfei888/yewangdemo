//
//  BusineeHeaderView.m
//  Yewang
//
//  Created by libin on 16/7/23.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import "BusineeHeaderView.h"
#import "JXBAdPageView.h"
#import "ClubListInfo.h"

@interface BusineeHeaderView ()<JXBAdPageViewDelegate>
///顶部图片
@property (nonatomic,strong) JXBAdPageView *bannerView;
///商家名称
@property (nonatomic,strong) UILabel *titleLabel;
///地址
@property (nonatomic,strong) UILabel *addressLabel;
///地址图标
@property (nonatomic,strong) UIImageView *addressImage;
///电话按钮
@property (nonatomic,strong) UIButton *phoneBtn;
///底部分割线
@property (nonatomic,strong) UIView *line1;
///具体电话号码
@property (nonatomic, strong) NSString *clubTel;

@end

@implementation BusineeHeaderView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        CGFloat height = SCREEN_HEIGHT/4.0;
        _bannerView = [[JXBAdPageView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, height)];
        _bannerView.clipsToBounds = YES;
        _bannerView.iDisplayTime = 2;
        _bannerView.bWebImage = YES;
        _bannerView.delegate = self;
        [self addSubview:_bannerView];
        _titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(15, height+5, SCREEN_WIDTH-130, 30)];
        _titleLabel.text = @"会所";
        _titleLabel.font = [UIFont systemFontOfSize:19];
        _titleLabel.textColor = [UIColor redColor];
        [self addSubview:_titleLabel];
        
        _preferentialBtn = [[UIButton alloc]initWithFrame:CGRectMake(SCREEN_WIDTH-100, height+5, 90, 30)];
        _preferentialBtn.layer.cornerRadius = 5;
        _preferentialBtn.titleLabel.font = [UIFont systemFontOfSize:15];
        _preferentialBtn.backgroundColor = [UIColor colorWithRed:0.99 green:0.75 blue:0.01 alpha:1];
        [_preferentialBtn setTitle:@"优惠买单" forState:UIControlStateNormal];
        _preferentialBtn.titleLabel.textColor = [UIColor whiteColor];
        _preferentialBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
        [_preferentialBtn addTarget:self action:@selector(clickPreferentialBtn) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_preferentialBtn];
    
        
        _addressImage = [UIImageView new];
        _addressImage.image = [UIImage imageNamed:@"addressImage.png"];
        _addressImage.contentMode = UIViewContentModeScaleAspectFit;
        [self addSubview:_addressImage];
        
        [_addressImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self).with.offset(15);
            make.top.equalTo(_titleLabel.mas_bottom).with.offset(5);
            make.size.mas_equalTo(CGSizeMake(25, 25));
        }];
        
        _addressLabel = [UILabel new];
        _addressLabel.font = [UIFont systemFontOfSize:15];
        _addressLabel.textColor = [UIColor colorWithWhite:0.4 alpha:1];
        [self addSubview:_addressLabel];
        _addressLabel.userInteractionEnabled = YES;
        UITapGestureRecognizer *addressTapGes = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(addressActionTapGes:)];
        [_addressLabel addGestureRecognizer:addressTapGes];
        
        _phoneBtn = [UIButton new];
        [_phoneBtn setImage:[UIImage imageNamed:@"businessPhone"] forState:UIControlStateNormal];
        [_phoneBtn addTarget:self action:@selector(clickPhoneBtn) forControlEvents:UIControlEventTouchUpInside];
        _phoneBtn.contentMode = UIViewContentModeScaleAspectFit;
        [self addSubview:_phoneBtn];
        
        [_addressLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_addressImage.mas_right).with.offset(5);
            make.centerY.equalTo(_addressImage.mas_centerY);
            make.height.mas_equalTo(@30);
            make.right.equalTo(_phoneBtn.mas_left).with.offset(-10);
        }];
        
        [_phoneBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self).with.offset(-20);
            make.top.equalTo(_preferentialBtn.mas_bottom).with.offset(5);
            make.size.mas_equalTo(CGSizeMake(50, 25));
        }];
        
        _line1 = [UIView new];
        _line1.backgroundColor = [UIColor colorWithWhite:0.8 alpha:1];
        [self addSubview:_line1];
        [_line1 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_phoneBtn.mas_bottom).with.offset(5);
            make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH, 1));
        }];
         
    }
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
}

- (void)setClubInfo:(ClubListInfo *)clubInfo{
    NSString *url = [NSString stringWithFormat:@"%@",[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",IMGURL,clubInfo.ClubPic]]];
    NSLog(@"////////////====%@",clubInfo.ClubPic);
    _titleLabel.text = clubInfo.ClubName;
    _addressLabel.text = clubInfo.Address;
    _clubTel = clubInfo.ContactNumber;
    [_bannerView startAdsWithBlock:@[url] block:^(NSInteger clickIndex){
        [self.delegate clickSingleImgView:url];
    }];
    _preferentialBtn.hidden = NO;
}
- (void)setWebImage:(UIImageView *)imgView imgUrl:(NSString *)imgUrl {
    [imgView sd_setImageWithURL:[NSURL URLWithString:imgUrl]];
}

// 优惠买单
- (void)clickPreferentialBtn
{
    [self.delegate clickPreferentialBtnAction];
}

//电话
- (void)clickPhoneBtn
{
    if ([UserHeader userIsLogin]) {
        NSMutableString *strPhone = [[NSMutableString alloc]initWithFormat:@"tel:%@",_clubTel];
        UIWebView *callWebView = [[UIWebView alloc]init];
        [callWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:strPhone]]];
        [self addSubview:callWebView];
    }
}

//地址
- (void)addressActionTapGes:(UITapGestureRecognizer *)tapGes
{
    [self.delegate addressAction];
}

- (void)dealloc{
    [_bannerView stopAds];
}

@end
