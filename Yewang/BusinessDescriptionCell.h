//
//  BusinessDescriptionCell.h
//  Yewang
//
//  Created by libin on 16/7/23.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface BusinessDescriptionCell : UITableViewCell

@property (nonatomic, strong) NSMutableDictionary *itemDic;
// 预定
@property (nonatomic,strong) UIButton *reeserveBtn;
@end
