//
//  BusinessDescriptionCell.m
//  Yewang
//
//  Created by libin on 16/7/23.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import "BusinessDescriptionCell.h"

@interface BusinessDescriptionCell ()

@property (nonatomic,strong) UIImageView *imgView;
@property (nonatomic,strong) UILabel *titleLabel;
// 优惠价
@property (nonatomic,strong) UILabel *discoutPrice;
@property (nonatomic,strong) UILabel *price;
@end

@implementation BusinessDescriptionCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        _imgView = [UIImageView new];
        [_imgView sd_setImageWithURL:nil placeholderImage:[UIImage imageNamed:@"yewang"]];
        _imgView.contentMode = UIViewContentModeScaleAspectFit;
        [self addSubview:_imgView];
        [_imgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self).with.offset(5);
            make.left.equalTo(self).with.offset(10);
            make.size.mas_equalTo(CGSizeMake(70, 60));
        }];
        
        _titleLabel = [UILabel new];
        _titleLabel.font = [UIFont systemFontOfSize:13];
        [self addSubview:_titleLabel];
        [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self).with.offset(8);
            make.left.equalTo(_imgView.mas_right).with.offset(10);
            make.size.mas_equalTo(CGSizeMake(200, 20));
        }];
        
        _discoutPrice = [UILabel new];
        _discoutPrice.textColor = [UIColor redColor];
        _discoutPrice.font = [UIFont systemFontOfSize:17];
        [self addSubview:_discoutPrice];
        [_discoutPrice mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_titleLabel.mas_bottom).with.offset(2);
            make.left.equalTo(_imgView.mas_right).with.offset(10);
            make.size.mas_equalTo(CGSizeMake(100, 40));
        }];
        
        _price = [UILabel new];
        _price.textAlignment = NSTextAlignmentCenter;
        _price.font = [UIFont systemFontOfSize:17];
        [self addSubview:_price];
        [_price mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_titleLabel.mas_bottom).with.offset(3);
            make.left.equalTo(_discoutPrice.mas_right).with.offset(10);
            make.size.mas_equalTo(CGSizeMake(100, 40));
        }];
        
        UILabel *line = [[UILabel alloc]initWithFrame:CGRectMake(0, 20, 100, 1)];
        line.backgroundColor = [UIColor colorWithWhite:0.8 alpha:1];
        [self.price addSubview:line];
        
        _reeserveBtn = [UIButton new];
        [_reeserveBtn setTitle:@"预定" forState:UIControlStateNormal];
        [_reeserveBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _reeserveBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        _reeserveBtn.backgroundColor = [UIColor colorWithRed:0.97 green:0.35 blue:0.32 alpha:1];
        _reeserveBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
        _reeserveBtn.layer.cornerRadius = 2.0;
        _reeserveBtn.layer.masksToBounds = YES;
        [self addSubview:_reeserveBtn];
        [_reeserveBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self).with.offset(10);
            make.right.equalTo(self).with.offset(-15);
            make.size.mas_equalTo(CGSizeMake(45, 23));
        }];
    }
    return self;
}

- (void)setItemDic:(NSMutableDictionary *)itemDic{
    [_imgView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",IMGURL,[itemDic objectForKey:@"ItemPic"]]] placeholderImage:[UIImage imageNamed:CACHEIMG] options:SDWebImageRefreshCached];
    [_titleLabel setText:[NSString stringWithFormat:@"%@    %@",[itemDic objectForKey:@"ItemName"],[itemDic objectForKey:@"Capacity"]]];
    [_discoutPrice setText:[NSString stringWithFormat:@"￥ %@",[[itemDic objectForKey:@"Price"]stringValue]]];
    [_price setText:[NSString stringWithFormat:@"￥ %@",[[itemDic objectForKey:@"MarketPrice"]stringValue]]];
    _reeserveBtn.userInteractionEnabled = NO;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
}

@end
