//
//  BusinessDescriptionVC.m
//  Yewang
//
//  Created by libin on 16/7/23.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import "BusinessDescriptionVC.h"
#import "BusineeHeaderView.h"
#import "BusinessDescriptionCell.h"
#import "ClubListInfo.h"
#import "LoginViewController.h"
#import "MyMapViewController.h"
#import "PreferentialPayViewController.h"
#import "ReserveTableViewController.h"
///第三版
#import "WineView.h"
#import "OrderView.h"
#import "UserCheckViewController.h"
#import "ThrowLineTool.h"
#import "WineModel.h"


#import <ShareSDK/ShareSDK.h>
#import <ShareSDKUI/ShareSDK+SSUI.h>

#define HEADERHEIGHT SCREEN_HEIGHT/4.0+70

@interface BusinessDescriptionVC ()<UITableViewDelegate,UITableViewDataSource,BusineeHeaderDelegate,LFLUISegmentedControlDelegate,UIScrollViewDelegate,WineViewDelegate,UIScrollViewDelegate,ThrowLineToolDelegate>
{
    UIButton *collectBtn;
    UIView *background;
    UIView *topView;
    NSString *itemID;
    
    
    ///包间/套餐数组
    NSMutableArray *_itemArray;
    
    
}
///底层全屏纵向滚动视图
@property (nonatomic, strong) UIScrollView *deepScrollView;
///顶部视图：商家信息
@property (nonatomic, strong) BusineeHeaderView *headerView;
///选择横向滚动视图
@property(nonatomic, strong) UIScrollView *mainScrollView;
///选择菜单
@property (nonatomic, strong) LFLUISegmentedControl * LFLuisement;
///视图一：套餐，包间选择视图
@property (nonatomic, strong) UITableView *myTableView;
///视图二：酒单选择视图
@property (nonatomic, strong) WineView *wineView;
///视图三：商家介绍
@property (nonatomic, strong) UIWebView *myWebView2;//,*myWebView;
///底部视图：购物车
@property (nonatomic, strong) OrderView *shoppcartView;
///购物车动画视图
@property (nonatomic, strong) UIImageView *redView;
///订单数组
@property (nonatomic, strong) NSMutableArray *ordersArray;


@end

@implementation BusinessDescriptionVC

- (void)viewWillAppear:(BOOL)animated{
    
    self.tabBarController.tabBar.hidden = YES;
    self.navigationController.navigationBar.hidden = YES;
    [super viewWillAppear:animated];
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [ThrowLineTool sharedTool].delegate = self;
    [self initWithData];
    [self initWithFrame];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateViewUI:) name:@"UpdateUI" object:nil];
    
}

- (void)initWithData{
    _itemArray = [NSMutableArray array];
    NSString *strUrl = [NSString stringWithFormat:@"%@Club/GetItemList?ClubID=%@",BASICURL,_clubInfo.ID];
    [BingNetWorkConnection connectionWithUrl:strUrl type:BINGO_REQUEST_GET params:nil FinishBlock:^(id responseObject) {
        
        NSArray *arr = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:nil];
        for (int i = 0; i < arr.count; i ++) {
            NSDictionary *dic = [arr objectAtIndex:i];
            if ([_clubInfo.ID isEqualToString:[dic objectForKey:@"ClubID"]]) {
                [_itemArray addObject:dic];
            }
        }
        [_myTableView reloadData];
    } FailedBlock:^(NSError *error) {
        NSLog(@"详情ERROR：%@",error);
    }];
    if ([UserHeader userIsLogin]) {
        /*
         Club/IsFavoriate 判断用户是否收藏了该会所
         */
        NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
        NSString *userID = [userDefault objectForKey:@"ID"];
        NSString *strUrl2 = [NSString stringWithFormat:@"%@Club/IsFavoriate",BASICURL];
        NSDictionary *dic = @{@"Key":userID,@"Value":_clubInfo.ID};
        [BingNetWorkConnection connectionWithUrl:strUrl2 type:BINGO_REQUEST_POST params:dic FinishBlock:^(id responseObject) {
            
            NSString *isCollectionStr = [[NSString alloc]initWithData:responseObject encoding:NSUTF8StringEncoding];
            NSLog(@"%@",isCollectionStr);
            if ([isCollectionStr isEqualToString:@"未收藏"]) {
                [collectBtn setImage:[UIImage imageNamed:@"collect.png"] forState:UIControlStateNormal];
            } else {
                [collectBtn setImage:[UIImage imageNamed:@"collected.png"] forState:UIControlStateNormal];
            }
            
        } FailedBlock:^(NSError *error) {
            NSLog(@"收藏ERROR：%@",error);
        }];
    } else {
        
    }
}

- (NSMutableArray *)ordersArray{
    if (!_ordersArray) {
        _ordersArray = [NSMutableArray new];
    }
    return _ordersArray;
}

- (void)initWithFrame{
    self.view.backgroundColor = [UIColor whiteColor];
    // 去掉状态栏空白
    self.automaticallyAdjustsScrollViewInsets = NO;
    //初始化底层滚动视图
    _deepScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    _deepScrollView.contentSize = CGSizeMake(SCREEN_WIDTH, SCREEN_HEIGHT+HEADERHEIGHT-64);
    _deepScrollView.delegate = self;
    [self.view addSubview:_deepScrollView];
    self.deepScrollView.backgroundColor = [UIColor whiteColor];
   //初始化segment
    self.LFLuisement=[LFLUISegmentedControl segmentWithFrame:CGRectMake(0, HEADERHEIGHT,SCREEN_WIDTH ,40) titleArray:@[@"优惠推荐",@"商品列表",@"商家简介"] defaultSelect:0];
    [self.LFLuisement lineColor:[UIColor redColor]];
    self.LFLuisement.delegate = self;
//    [self.view addSubview:self.LFLuisement];
//    [self.view addSubview:self.mainScrollView];
    [self.deepScrollView addSubview:self.LFLuisement];
    [self.deepScrollView addSubview:self.mainScrollView];
    
    [self initWithTopView];
    [self initWithShoppcartView];
    [self updateHeadView];
    
}

#pragma mark getter

- (UIScrollView *)mainScrollView {
    if(_mainScrollView == nil) {
        CGFloat begainScrollViewY = 40 + HEADERHEIGHT;
        _mainScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, begainScrollViewY, SCREEN_WIDTH,(SCREEN_HEIGHT -104))];
        _mainScrollView.bounces = NO;
        _mainScrollView.pagingEnabled = YES;
        _mainScrollView.contentSize = CGSizeMake(SCREEN_WIDTH * 4, (SCREEN_HEIGHT -104));
        //设置代理
        _mainScrollView.delegate = self;
        
        //套餐，包间列表
        _myTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-104) style:UITableViewStylePlain];
        _myTableView.delegate = self;
        _myTableView.dataSource = self;
        //去除下面多余的cell
        _myTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
        [_mainScrollView addSubview:_myTableView];
        
        //酒
        _wineView = [[WineView alloc] initWithFrame:CGRectMake(SCREEN_WIDTH, 0, SCREEN_WIDTH, SCREEN_HEIGHT-104)];
        _wineView.delegate = self;
        [_mainScrollView addSubview:_wineView];
        
        //商家介绍
        _myWebView2 = [[UIWebView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*2, 0, SCREEN_WIDTH, SCREEN_HEIGHT -104)];
        _myWebView2.scrollView.bounces = YES;
        [_mainScrollView addSubview:_myWebView2];
    }
    return _mainScrollView;
}

//初始化红色视图
- (UIView *)redView{
    if (!_redView) {
        _redView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
        _redView.image = [UIImage imageNamed:@"addDetail"];
        _redView.layer.cornerRadius = 10;
        _redView.layer.masksToBounds = YES;
    }
    return _redView;
}

- (void)initWithTopView{
    
    topView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 64)];
//    //将指定的视图推送到图层前面
    [self.view addSubview:topView];
    [self.view bringSubviewToFront:topView];
    ///返回按钮
    UIButton *leftBtn = [[UIButton alloc] init];
    [leftBtn addTarget:self action:@selector(SelectTheBack) forControlEvents:UIControlEventTouchUpInside];
    leftBtn.contentMode = UIViewContentModeScaleAspectFill;
    [leftBtn setImage:[UIImage imageNamed:@"white_back.png"] forState:UIControlStateNormal];
    [topView addSubview:leftBtn];
    [leftBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.sizeOffset(CGSizeMake(25, 25));
        make.left.equalTo(topView.mas_left).offset(15);
        make.centerY.equalTo(topView.mas_centerY);
    }];
    
     ///创建右边收藏按钮
    collectBtn = [[UIButton alloc]init];
    [collectBtn addTarget:self action:@selector(clickcollectionBtn) forControlEvents:UIControlEventTouchUpInside];
    collectBtn.contentMode = UIViewContentModeScaleAspectFill;
    [collectBtn setImage:[UIImage imageNamed:@"collect.png"] forState:UIControlStateNormal];
    [topView addSubview:collectBtn];
    [collectBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(topView.mas_right).offset(-15);
        make.size.mas_equalTo(CGSizeMake(25, 25));
        make.centerY.equalTo(topView.mas_centerY);
    }];
    ///创建右边分享按钮
    UIButton *shareBtn = [[UIButton alloc]init];
    [shareBtn addTarget:self action:@selector(clickShareBtn) forControlEvents:UIControlEventTouchUpInside];
    shareBtn.contentMode = UIViewContentModeScaleAspectFill;
    [shareBtn setImage:[UIImage imageNamed:@"businessshareBtn.png"] forState:UIControlStateNormal];
    [topView addSubview:shareBtn];
    [shareBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(collectBtn.mas_left).offset(-15);
        make.size.mas_equalTo(CGSizeMake(25, 25));
        make.centerY.equalTo(topView.mas_centerY);
    }];
    //是否显示“优惠买单”按钮
    NSString *showDiscount = [NSString stringWithFormat:@"%@",_clubInfo.Recommend];
    if ([showDiscount  isEqual: @"0"]) {
        _headerView.preferentialBtn.hidden = YES;
    }
   
}

- (void)initWithShoppcartView{
    if (!_shoppcartView) {
        _shoppcartView = [[OrderView alloc] initWithFrame:CGRectZero inView:self.view];
    }
    [self.view addSubview:_shoppcartView];
    [self.shoppcartView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left).offset(0);
        make.right.equalTo(self.view.mas_right).offset(0);
        make.height.mas_offset(@40);
        make.bottom.equalTo(self.view.mas_bottom).offset(0);
    }];
}

#pragma mark - HeaderView

- (void)updateHeadView
{
    if (!_headerView) {
        _headerView = [[BusineeHeaderView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, HEADERHEIGHT)];
        _headerView.delegate = self;
        [self.deepScrollView addSubview:_headerView];
        [_headerView setClubInfo:_clubInfo];
    }
}
/*****************************************/
#pragma mark --- UIScrollView代理方法

static NSInteger pageNumber = 0;
static NSInteger lastPosition = 0;
static bool isDown;
//一旦滚动就一直调用 直到停止
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (self.deepScrollView == scrollView) {
        int currentPosition = scrollView.contentOffset.y;
        if (currentPosition - lastPosition > 10) {
            lastPosition = currentPosition;
            NSLog(@"向上滑动");
            isDown = NO;
        } else if (lastPosition - currentPosition > 10) {
            lastPosition = currentPosition;
            NSLog(@"向下滑动");
            isDown = YES;
        }
    }
}

//减速到停止的时候（静止）的时候调用
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    if (self.mainScrollView == scrollView) {
        pageNumber = (int)(scrollView.contentOffset.x / SCREEN_WIDTH + 0.5);
        //    滑动SV里视图,切换标题
        if(pageNumber == 3){
            pageNumber = 0;
        }
        [self.LFLuisement selectTheSegument:pageNumber];
    } else if (self.deepScrollView == scrollView && isDown == YES){
        ///==============通过动画上下文使用UIKit动画
        //设置改变顶部视图颜色的动画
        [UIView beginAnimations:@"changeColor" context:nil];//开始动画
        [UIView setAnimationDuration:1.0];//动画时长
        //要进行动画的地方
        [topView setBackgroundColor:kRGBColor];
        [UIView commitAnimations];//动画结束
    } else if (self.deepScrollView == scrollView && isDown == NO) {
        ///===============通过代码块使用UIKit动画
        [UIView animateWithDuration:1.0 animations:^{
            [topView setBackgroundColor:[UIColor clearColor]];
        } completion:^(BOOL finished) {
            
        }];
        
    }
}

#pragma mark ---LFLUISegmentedControlDelegate
/**
 *  点击标题按钮
 *
 *  @param selection 对应下标 begain 0
 */
-(void)uisegumentSelectionChange:(NSInteger)selection{
    //    加入动画,显得不太过于生硬切换
    [UIView animateWithDuration:.2 animations:^{
        [self.mainScrollView setContentOffset:CGPointMake(SCREEN_WIDTH *selection, 0)];
    }];
    
    if (selection == 0) {
        NSLog(@"点击标题按钮：table");
        self.shoppcartView.hidden = NO;
    } else if (selection == 1) {
        NSLog(@"点击标题按钮：wine");
        self.shoppcartView.hidden = NO;
//        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",WEBURL,_clubInfo.WineUrl]];
//        [_myWebView loadRequest:[NSURLRequest requestWithURL:url]];
    } else if (selection == 2){
        NSLog(@"点击标题按钮：web");
        self.shoppcartView.hidden = YES;
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",WEBURL,_clubInfo.WebUrl]];
        [_myWebView2 loadRequest:[NSURLRequest requestWithURL:url]];
    }
}

#pragma mark - BusineeHeaderDelegate 
- (void)addressAction{
    UIStoryboard *mainStoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    MyMapViewController *myMap = [mainStoryBoard instantiateViewControllerWithIdentifier:@"mymap"];
    myMap.latitude = [_clubInfo.Latitude doubleValue];
    myMap.longitude = [_clubInfo.Longitude doubleValue];
    myMap.clubName = _clubInfo.ClubName;
    myMap.address = _clubInfo.Address;
    [self.navigationController pushViewController:myMap animated:YES];
}

- (void)clickPreferentialBtnAction{
    if ([UserHeader userIsLogin]) {
        PreferentialPayViewController *preferential = [[PreferentialPayViewController alloc]init];
        preferential.clubID = _clubInfo.ID;
        preferential.itemID = itemID;
        preferential.clubName = _clubInfo.ClubName;
        preferential.disCount = _clubInfo.Discount;
        [self.navigationController pushViewController:preferential animated:YES];
    } else {
        UIStoryboard *mainStoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        LoginViewController *login = [mainStoryBoard instantiateViewControllerWithIdentifier:@"login"];
        [self.navigationController pushViewController:login animated:YES];
    }
}

- (void)clickSingleImgView:(NSString *)url{
    
    //创建一个黑色视图做背景
    UIView *bgView = [[UIView alloc]initWithFrame:self.view.bounds];
    background = bgView;
    bgView.backgroundColor = [UIColor colorWithRed:41.0/255 green:36.0/255 blue:33.0/255 alpha:0.8];
    [self.view addSubview:bgView];
    
    //创建要显示图像的视图
    //初始化要显示的图片内容的imageView
    UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake(0.0, 200.0, self.view.bounds.size.width, self.view.bounds.size.height-400.0)];
    [imgView sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:[UIImage imageNamed:CACHEIMG] options:SDWebImageRefreshCached];
    [bgView addSubview:imgView];
    
    //添加手势，点击背景后退出全屏
    bgView.userInteractionEnabled = YES;
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(closeView)];
    [bgView addGestureRecognizer:tapGesture];
    //放大过程中的动画
    [self shakeToShow:bgView];
}

//放大过程中出现的缓慢动画
- (void)shakeToShow:(UIView *)aView{
    CAKeyframeAnimation *animation = [CAKeyframeAnimation animationWithKeyPath:@"transform"];
    animation.duration = 0.5;
    NSMutableArray *values = [NSMutableArray array];
    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(0.1, 0.1, 1.0)]];
    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(1.0, 1.0, 1.0)]];
    animation.values = values;
    [aView.layer addAnimation:animation forKey:nil];
}

//点击图片后退出大图模式
- (void)closeView{
    [background removeFromSuperview];
}

// 隐藏状态栏
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}
- (BOOL)prefersStatusBarHidden
{
    return NO;
}

- (void)SelectTheBack
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)clickShareBtn
{
    if ([UserHeader userIsLogin]) {
        NSMutableDictionary *shareParams = [NSMutableDictionary dictionary];
        [shareParams SSDKSetupShareParamsByText:@"满足你对夜生活的所有想象"
                                         images:[UIImage imageNamed:@"1.pic_hd.png"]
                                            url:[NSURL URLWithString:@"http://res.nightnet.cn/resource/Web/appshare/share_soft.html"]
                                          title:@"开启夜生活"
                                           type:SSDKContentTypeAuto];
        //2、分享（可以弹出我们的分享菜单和编辑界面）
        [ShareSDK showShareActionSheet:nil
                                 items:nil
                           shareParams:shareParams
                   onShareStateChanged:^(SSDKResponseState state, SSDKPlatformType platformType, NSDictionary *userData, SSDKContentEntity *contentEntity, NSError *error, BOOL end) {
                       
                       switch (state) {
                           case SSDKResponseStateSuccess:
                           {
                               UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"分享成功"
                                                                                                        message:nil
                                                                                                 preferredStyle:UIAlertControllerStyleAlert];
                               UIAlertAction *action = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleCancel handler:nil];
                               [alertController addAction:action];
                               [self presentViewController:alertController animated:YES completion:nil];
                               break;
                               
                           }
                           case SSDKResponseStateFail:
                           {
                               UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"分享失败"
                                                                                                        message:[NSString stringWithFormat:@"%@",error]
                                                                                                 preferredStyle:UIAlertControllerStyleAlert];
                               UIAlertAction *action = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleCancel handler:nil];
                               [alertController addAction:action];
                               [self presentViewController:alertController animated:YES completion:nil];
                               break;
                               
                               
                           }
                           default:
                               break;
                       }
                   }
         ];
    }else{
        LoginViewController *login = [self.storyboard instantiateViewControllerWithIdentifier:@"login"];
        [self.navigationController pushViewController:login animated:YES];
    }
}

- (void)clickcollectionBtn
{
    if ([UserHeader userIsLogin]) {
        NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
        NSString *userID = [userDefault objectForKey:@"ID"];
        NSString *strUrl = [NSString stringWithFormat:@"%@Club/ChangeFavoriate",BASICURL];
        NSDictionary *dic = @{@"Key":userID,@"Value":_clubInfo.ID};
        [SVProgressHUD showWithStatus:@"加载中，请稍候..."];
        [BingNetWorkConnection connectionWithUrl:strUrl type:BINGO_REQUEST_POST params:dic FinishBlock:^(id responseObject) {
            [SVProgressHUD dismiss];
            NSString *isCollectionStr = [[NSString alloc]initWithData:responseObject encoding:NSUTF8StringEncoding];
            NSLog(@"%@",isCollectionStr);
            if ([isCollectionStr isEqualToString:@"删除收藏"]) {
                [collectBtn setImage:[UIImage imageNamed:@"collect.png"] forState:UIControlStateNormal];
            } else {
                [collectBtn setImage:[UIImage imageNamed:@"collected.png"] forState:UIControlStateNormal];
            }
            
        } FailedBlock:^(NSError *error) {
            [SVProgressHUD dismiss];
            NSLog(@"收藏ERROR：%@",error);
        }];
    } else {
        LoginViewController *login = [self.storyboard instantiateViewControllerWithIdentifier:@"login"];
        [self.navigationController pushViewController:login animated:YES];
    }
}

#pragma mark - WineViewDelegate
- (void)clickRightBottomBtn:(NSString *)status{
    if ([status isEqualToString:@"确定"]) {
        //测试用户确认订单
        UserCheckViewController *userCheck = [[UserCheckViewController alloc] init];
        [self.navigationController pushViewController:userCheck animated:YES];
    }
}

- (void)wineAddOrSubOrder:(NSMutableDictionary *)dic OrderData:(NSMutableArray *)ordersArray isAdded:(BOOL)added andStartPoint:(CGPoint)start{
    
    CGRect parentRectB = [self.shoppcartView convertRect:self.shoppcartView.orderImgBtn.frame toView:self.view];
    
    //根据添加还是减少，决定是否执行添加的动画
    if (added) {
        [self.view addSubview:_redView];
        [[ThrowLineTool sharedTool] throwObject:self.redView from:start to:parentRectB.origin];
        self.ordersArray = [WineModel StoreOrder:dic OrderData:ordersArray isAdded:YES];
    } else {
        self.ordersArray = [WineModel StoreOrder:dic OrderData:ordersArray isAdded:NO];
    }
    
    self.shoppcartView.listView.objects = self.ordersArray;
    [self.shoppcartView updateFrame:self.shoppcartView.listView];
    [self.shoppcartView.listView.tableView reloadData];
    self.shoppcartView.badgeValue = [WineModel CountOthersWithOrderData:self.ordersArray];
    CGFloat price = [WineModel GetTotalPrice:self.ordersArray];
    [self.shoppcartView setTotalPrice:price];
    
}

#pragma mark - 设置购物车动画
- (void)animationDidFinish {
    
    [self.redView removeFromSuperview];
    [UIView animateWithDuration:0.1 animations:^{
        self.shoppcartView.orderImgBtn.transform = CGAffineTransformMakeScale(0.8, 0.8);
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.1 animations:^{
            self.shoppcartView.orderImgBtn.transform = CGAffineTransformMakeScale(1, 1);
        } completion:^(BOOL finished) {

        }];
    }];
}
#pragma mark - UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _itemArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"BusinessDescriptionCell";
    BusinessDescriptionCell *cell = (BusinessDescriptionCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[BusinessDescriptionCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    cell.backgroundColor = [UIColor whiteColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    NSMutableDictionary *dic = [_itemArray objectAtIndex:indexPath.row];
    [cell setItemDic:dic];
    if (indexPath.row == 0) {
        itemID = [dic objectForKey:@"ID"];
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([UserHeader userIsLogin]) {
        UIStoryboard *mainStoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        ReserveTableViewController *buy = [mainStoryBoard instantiateViewControllerWithIdentifier:@"reserve"];
        buy.dictionary = [_itemArray objectAtIndex:indexPath.row];
        buy.clubName = _clubInfo.ClubName;
        buy.clubMode = _clubInfo.Mode;
        [self.navigationController pushViewController:buy animated:YES];
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        
    } else {
        UIStoryboard *mainStoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        LoginViewController *login = [mainStoryBoard instantiateViewControllerWithIdentifier:@"login"];
        [self.navigationController pushViewController:login animated:YES];
    }
    
}

#pragma mark - 通知更新
- (void)updateViewUI:(NSNotification *)notification{
    
    NSMutableDictionary *dic =[NSMutableDictionary dictionaryWithDictionary: notification.userInfo];
    //重新计算订单数组。
    self.ordersArray = [WineModel StoreOrder:dic[@"update"] OrderData:self.ordersArray isAdded:[dic[@"isAdd"] boolValue]];
    self.shoppcartView.listView.objects = self.ordersArray;
    //设置高度。
    [self.shoppcartView updateFrame:self.shoppcartView.listView];
    [self.shoppcartView.listView.tableView reloadData];
    //设置数量、价格
    self.shoppcartView.badgeValue =  [WineModel CountOthersWithOrderData:self.ordersArray];
    double price = [WineModel GetTotalPrice:self.ordersArray];
    [self.shoppcartView setTotalPrice:price];
    //重新设置数据源
//    self.dataArray = [WineModel UpdateArray:self.dataArray atSelectDictionary:dic[@"update"]];
//    [self.maintable reloadData];
}

- (void)dealloc{
    //移除更新UI
    [[NSNotificationCenter defaultCenter] removeObserver:self forKeyPath:@"UpdateUI"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
