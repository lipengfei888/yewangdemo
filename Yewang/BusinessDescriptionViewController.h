//
//  BusinessDescriptionViewController.h
//  Yewang
//
//  Created by everhelp on 16/10/31.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ClubListInfo;

@interface BusinessDescriptionViewController : UIViewController

@property (nonatomic, strong) ClubListInfo *clubInfo;
@property (nonatomic, copy) NSString *pic;

/** 订单数据 */
@property (nonatomic, strong) NSMutableArray *orderArray;

/** 订单所选总数量 */
@property (nonatomic, assign) NSInteger totalOrderCount;

/** 订单总价 */
@property (assign, nonatomic) double totalPrice;

- (instancetype)initWithFrame:(CGRect)frame andClubID:(NSString *)ClubId;
@end
