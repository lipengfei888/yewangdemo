//
//  BusinessDescriptionViewController.m
//  Yewang
//
//  Created by everhelp on 16/10/31.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import "BusinessDescriptionViewController.h"
#import "BusineeHeaderView.h"
#import "ClubListInfo.h"
#import "LoginViewController.h"
#import "MyMapViewController.h"
#import "PreferentialPayViewController.h"
#import "AFNetworking.h"
//#import "ReserveTableViewController.h"
///第三版
#import "WineView.h"
#import "OrderView.h"
#import "UserCheckViewController.h"
#import "ThrowLineTool.h"
#import "WineModel.h"
#import "OrderDetailModel.h"
#import "GoodsCategory.h"
#import "GoodsModel.h"
#import "MenuItemCell.h"
#import "ShopCartView.h"
#import "ThrowLineTool.h"
#import "DetailListCell.h"
#import <ShareSDK/ShareSDK.h>
#import <ShareSDKUI/ShareSDK+SSUI.h>

#define HEADERHEIGHT SCREEN_HEIGHT/4.0+70
#define ZXColor(r,g,b) [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:1.0]

@interface BusinessDescriptionViewController ()<BusineeHeaderDelegate,LFLUISegmentedControlDelegate,UIScrollViewDelegate,WineViewDelegate,UIScrollViewDelegate,ThrowLineToolDelegate,NSXMLParserDelegate,WineViewDelegate,UITableViewDelegate, UITableViewDataSource, MenuItemCellDelegate, ThrowLineToolDelegate, DetailListCellDelegate>
{
    UIButton *collectBtn;
    UIView *background;
    UIView *topView;
    NSString *itemID;
    //    NSArray *titleArray;
    NSMutableArray *detailArray;
}
///底层全屏纵向滚动视图
@property (nonatomic, strong) UIScrollView *deepScrollView;
///顶部视图：商家信息
@property (nonatomic, strong) BusineeHeaderView *headerView;
///选择横向滚动视图
@property(nonatomic, strong) UIScrollView *mainScrollView;
///选择菜单
@property (nonatomic, strong) LFLUISegmentedControl * LFLuisement;
///视图一：酒单选择视图
@property (nonatomic, strong) WineView *wineView;
///视图二：商家介绍
@property (nonatomic, strong) UIWebView *myWebView2;//,*myWebView;
///底部视图：购物车
//@property (nonatomic, strong) OrderView *shoppcartView;
///购物车动画视图
//@property (nonatomic, strong) UIImageView *redView;
///订单数组
@property (nonatomic, strong) NSMutableArray *ordersArray;
//@property(strong, nonatomic)NSMutableArray *listArray;
@property(strong, nonatomic)NSMutableArray *titleArray;
@property (nonatomic, strong) NSMutableDictionary *comboDic;

@property (nonatomic, strong) UITableView   *leftTableView;
@property (nonatomic, strong) UITableView   *rightTableView;
@property (nonatomic, strong) NSArray       *dataArray;
@property (nonatomic, assign) BOOL          isRelate;
@property (nonatomic, strong) UIImageView   *redView;   //抛物线红点
@property (nonatomic, strong) ShopCartView  *shopCartView;
@property (nonatomic, assign) NSInteger     totalOrders;    //总数量
//@property (nonatomic, strong) NSMutableArray *titleArray;
//@property (nonatomic, strong) NSMutableArray *detailArray;
@property (nonatomic ,strong) NSMutableArray *detailArray;
@property (nonatomic ,strong) NSMutableArray *detailArray1;
@property (nonatomic ,strong) NSMutableArray *detailArray2;
@property (nonatomic ,strong) NSMutableArray *detailArray3;
@property (nonatomic ,strong) NSMutableArray *SequenceArray;
@property (nonatomic ,strong) NSMutableArray *groupArr;

@end

@implementation BusinessDescriptionViewController
static NSString * const cellID = @"MenuItemCell";
static NSString * const ListCellID = @"DetailListCell";

//顶部大图
//http://res.nightnet.cn/resource/images/84d912b3-fb81-43a8-b113-330146069746.jpg
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    self.tabBarController.tabBar.hidden = YES;
    self.navigationController.navigationBar.hidden = YES;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    //    [self getNetData];
    self.view.backgroundColor = [UIColor whiteColor];
//    self.edgesForExtendedLayout = UIRectEdgeNone;
//    CGRect viewBounds = self.view.bounds;
//    float navBarHeight = self.navigationController.navigationBar.frame.size.height + 20;
//    viewBounds.size.height = ([[UIScreen mainScreen] bounds].size.height) - navBarHeight;
//    self.view.bounds = viewBounds;
    [self initData];
    [ThrowLineTool sharedTool].delegate = self;
    _isRelate = YES;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateViewUI:) name:@"UpdateUI" object:nil];
}

-(void)initData {
    _SequenceArray = [NSMutableArray arrayWithCapacity:0];
    if (!_dataArray) {
        _dataArray = [NSArray new];
        
        _detailArray = [NSMutableArray arrayWithCapacity:0];
        _detailArray1 = [NSMutableArray arrayWithCapacity:0];
        _detailArray2 = [NSMutableArray arrayWithCapacity:0];
        _detailArray3 = [NSMutableArray arrayWithCapacity:0];
        self.titleArray = [NSMutableArray arrayWithCapacity:0];
//        NSString *urlString1 = @"http://api.nightnet.cn/api//Club/GetItemList?ClubID=3b212c80-6039-457f-bc09-14478606f83a";

        NSString *urlString1 = [NSString stringWithFormat:@"%@/Club/GetItemList?ClubID=%@",BASICURL,_clubInfo.ID];
        NSLog(@"urlString1=====%@",urlString1);
        AFHTTPSessionManager *manager1 = [AFHTTPSessionManager manager];
        manager1.requestSerializer = [AFHTTPRequestSerializer serializer];
        
        [manager1 GET:urlString1 parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject1) {
            
            GoodsCategory *category = [[GoodsCategory alloc] init];
            for (NSMutableDictionary *dic in responseObject1) {
                GoodsModel *model = [GoodsModel mj_objectWithKeyValues:dic];
                NSString *SequenceStr = [NSString stringWithFormat:@"%d",model.Sequence];
                [_SequenceArray addObject:SequenceStr];
                [_titleArray addObject:model.ItemType];
                [_detailArray addObject:dic];
                //                NSLog(@"_detailArray.count===%@",_detailArray);
                
            }
            _detailArray1 = [NSMutableArray new];
            [self groupSort];
            //去除多余元素
            NSMutableArray *listAry = [[NSMutableArray alloc]init];
            for (NSString *str in _SequenceArray) {
                if (![listAry containsObject:str]) {
                    [listAry addObject:str];
                }
            }
            NSMutableArray *listAry1 = [[NSMutableArray alloc]init];
            for (NSString *str in _titleArray) {
                if (![listAry1 containsObject:str]) {
                    [listAry1 addObject:str];
                }
            }
            _titleArray = listAry1;
            _SequenceArray = listAry;
            
            for (int i = 0; i<_titleArray.count; i++) {
                category.name = _titleArray[i];
//                NSLog(@"_titleArray===%@",_titleArray);
                category.desciption = _titleArray[i];
                category.goodsArray = _groupArr[i];
                NSDictionary *stuDict = category.mj_keyValues;
                [_detailArray2 addObject:stuDict];
            }
            NSLog(@"_titleArray===%@",_titleArray);

//            _dataArray = [GoodsCategory objectArrayWithKeyValuesArray:_detailArray2];
            _dataArray = [GoodsCategory mj_objectArrayWithKeyValuesArray:_detailArray2 ];
            NSLog(@"_detailArray2===%@",_detailArray2);
            [self initWithFrameLayout];
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            NSLog(@"error = %@", error);
        }];
        
    }
    
}

- (void)groupSort {
    // 1.定义一个测试的字典数组
    GoodsModel *model;
    NSMutableArray *dictArray = _detailArray;
    for (NSDictionary *dic in dictArray) {
        model = [GoodsModel mj_objectWithKeyValues:dic];
    };
    NSMutableArray *dictArr = [NSMutableArray array];
    for (int i = 0; i < dictArray.count; i++) {
        NSDictionary *dict = dictArray[i];
        [dictArr addObject:dict];
    }
    
    // 1、对数组按GroupTag排序
    
    NSArray *sortDesc = [NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"Sequence" ascending:YES]];
    NSArray *sortedArr = [dictArr sortedArrayUsingDescriptors:sortDesc];
    // NSLog(@"排序后的数组:%@",sortedArr);
    // 2、对数组进行分组，按GroupTag
    // 遍历,创建组数组,组数组中的每一个元素是一个模型数组
    _groupArr = [NSMutableArray array];
    NSMutableArray *currentArr = [NSMutableArray array];
    //    NSLog(@"class--%@",[currentArr class]);
    // 因为肯定有一个字典返回,先添加一个
    [currentArr addObject:sortedArr[0]];
    [_groupArr addObject:currentArr];
    // 如果不止一个,才要动画添加
    if(sortedArr.count > 1){
        for (int i = 1; i < sortedArr.count; i++) {
            // 先取出组数组中  上一个模型数组的第一个字典模型的groupID
            NSMutableArray *preModelArr = [_groupArr objectAtIndex:_groupArr.count-1];
            int preGroupID = [[[preModelArr objectAtIndex:0] objectForKey:@"Sequence"] intValue];
            //            NSLog(@"preGroupID===%d",preGroupID);
            // 取出当前字典,根据groupID比较,如果相同则添加到同一个模型数组;如果不相同,说明不是同一个组的
            NSDictionary *currentDict = sortedArr[i];
            int groupID = [[currentDict objectForKey:@"Sequence"] intValue];
            //            NSLog(@"groupID===%d",groupID);
            if (groupID == preGroupID) {
                [currentArr addObject:currentDict];
            }else{
                // 如果不相同,说明 有新的一组,那么创建一个模型数组,并添加到组数组_groupArr
                currentArr = [NSMutableArray array];
                [currentArr addObject:currentDict];
                [_groupArr addObject:currentArr];
            }
        }
    }
    
    // 3、遍历 对每一组 进行排序
    NSMutableArray *tempGroupArr = [NSMutableArray array];
    for (NSMutableArray *arr in _groupArr) {
        //        NSArray *sortDesc = [NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"Sequence" ascending:YES]];
        NSMutableArray *tempArr = [arr sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
            if([[obj1 objectForKey:@"Sequence"]intValue] < [[obj2 objectForKey:@"Sequence"]intValue]){
                return NSOrderedAscending;
            }
            if([[obj1 objectForKey:@"Sequence"]intValue] > [[obj2 objectForKey:@"Sequence"]intValue]){
                return NSOrderedDescending;
            }
            return NSOrderedSame;
        }];
        [tempGroupArr addObject:tempArr];
    }
    _groupArr = tempGroupArr;
    
}

- (UITableView *)leftTableView {
    if (nil == _leftTableView) {
        NSLog(@"CGRectGetMaxX(_LFLuisement.frame)===%.f",CGRectGetMaxX(_LFLuisement.frame));
        _leftTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width * 0.25, CGRectGetHeight(_mainScrollView.frame) - 50)];
        _leftTableView.backgroundColor = [UIColor whiteColor];
        _leftTableView.delegate = self;
        _leftTableView.dataSource = self;
        _leftTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [self.mainScrollView addSubview:_leftTableView];
    }
    return _leftTableView;
    
}

- (UITableView *)rightTableView {
    if (nil == _rightTableView) {
        NSLog(@"CGRectGetMin(_mainScrollView.frame)==%.f",CGRectGetMidY(_mainScrollView.frame));
        _rightTableView = [[UITableView alloc] initWithFrame:CGRectMake(self.view.frame.size.width * 0.25, 0, self.view.frame.size.width * 0.75, CGRectGetHeight(_mainScrollView.frame) - 50)];
        _rightTableView.backgroundColor = [UIColor whiteColor];
        _rightTableView.delegate = self;
        _rightTableView.dataSource = self;
        [self.mainScrollView addSubview:_rightTableView];
    }
    return _rightTableView;
}

- (ShopCartView *)shopCartView {
    if (!_shopCartView) {
        NSLog(@"%.f",_mainScrollView.frame.size.height);
        NSLog(@"self.mainScrollView===%.f",SCREEN_HEIGHT);
        _shopCartView = [[ShopCartView alloc] initWithFrame:CGRectMake(0, CGRectGetHeight(_mainScrollView.frame)-50, SCREEN_WIDTH, 50) inView:self.mainScrollView];
        _shopCartView.backgroundColor = [UIColor whiteColor];
        [self.mainScrollView addSubview:_shopCartView];
        //_shopCartView.orderArray = [NSMutableArray new];
        _shopCartView.detailListView.listTableView.delegate = self;
        _shopCartView.detailListView.listTableView.dataSource = self;
        [_shopCartView.detailListView.listTableView registerNib:[UINib nibWithNibName:NSStringFromClass([DetailListCell class]) bundle:nil] forCellReuseIdentifier:ListCellID];
    }
    return _shopCartView;
}

- (UIImageView *)redView {
    if (!_redView) {
        _redView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
        _redView.image = [UIImage imageNamed:@"adddetail"];
        _redView.layer.cornerRadius = 10;
    }
    return _redView;
}


- (NSMutableArray *)ordersArray{
    if (!_ordersArray) {
        _ordersArray = [NSMutableArray new];
    }
    return _ordersArray;
}

- (void)initWithFrameLayout{
    self.view.backgroundColor = [UIColor whiteColor];
    // 去掉状态栏空白
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    //初始化底层滚动视图
    _deepScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    _deepScrollView.contentSize = CGSizeMake(SCREEN_WIDTH, SCREEN_HEIGHT+HEADERHEIGHT-64);
    _deepScrollView.delegate = self;
    [self.view addSubview:_deepScrollView];
    
    [self initWithTopView];
    //初始化segment
    self.LFLuisement=[LFLUISegmentedControl segmentWithFrame:CGRectMake(0, HEADERHEIGHT,SCREEN_WIDTH ,40) titleArray:@[@"商品预定",@"商家简介>"] defaultSelect:0];
    [self.LFLuisement lineColor:[UIColor redColor]];
    self.LFLuisement.delegate = self;
    [self.deepScrollView addSubview:self.LFLuisement];
    [self.deepScrollView addSubview:self.mainScrollView];
    NSLog(@"clubInfo.ClubPic===%@",_clubInfo.ClubPic);
    //    [self initWithShoppcartView];
    [self updateHeadView];
    
}

#pragma mark getter

- (UIScrollView *)mainScrollView {
    if(_mainScrollView == nil) {
//        CGFloat begainScrollViewY = 40 + HEADERHEIGHT;
        _mainScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(_LFLuisement.frame), SCREEN_WIDTH,SCREEN_HEIGHT - CGRectGetMaxY(_LFLuisement.frame))];
//        _mainScrollView.backgroundColor = [UIColor blueColor];
        _mainScrollView.bounces = NO;
        _mainScrollView.pagingEnabled = YES;
        _mainScrollView.contentSize = CGSizeMake(SCREEN_WIDTH * 3, SCREEN_HEIGHT - CGRectGetMaxY(_LFLuisement.frame));
        //设置代理
        _mainScrollView.delegate = self;

        [self leftTableView];
        [self rightTableView];
        [self shopCartView];
#pragma mark---添加商家介绍
        _myWebView2 = [[UIWebView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH, 0, SCREEN_WIDTH, SCREEN_HEIGHT -CGRectGetMaxY(self.LFLuisement.frame) ) ];
        _myWebView2.scrollView.bounces = YES;
        [_mainScrollView addSubview:_myWebView2];
    }
    return _mainScrollView;
}

- (void)initWithTopView{
    
    topView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 64)];
    //将指定的视图推送到图层前面
    [self.view addSubview:topView];
    [self.view bringSubviewToFront:topView];
    ///返回按钮
    //    UIButton *leftBtn = [[UIButton alloc]initWithFrame:CGRectMake(15, 0, 25, 25)];
    UIButton *leftBtn = [[UIButton alloc] init];
    [leftBtn addTarget:self action:@selector(SelectTheBack) forControlEvents:UIControlEventTouchUpInside];
    leftBtn.contentMode = UIViewContentModeScaleAspectFill;
    [leftBtn setImage:[UIImage imageNamed:@"white_back.png"] forState:UIControlStateNormal];
    //    leftBtn.backgroundColor = [UIColor redColor];
    [topView addSubview:leftBtn];
    [leftBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.sizeOffset(CGSizeMake(25, 25));
        make.left.equalTo(topView.mas_left).offset(15);
        make.centerY.equalTo(topView.mas_centerY);
    }];
    
    ///创建右边收藏按钮
    collectBtn = [[UIButton alloc]init];
    [collectBtn addTarget:self action:@selector(clickcollectionBtn) forControlEvents:UIControlEventTouchUpInside];
    collectBtn.contentMode = UIViewContentModeScaleAspectFill;
    [collectBtn setImage:[UIImage imageNamed:@"collect.png"] forState:UIControlStateNormal];
    [topView addSubview:collectBtn];
    [collectBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(topView.mas_right).offset(-15);
        make.size.mas_equalTo(CGSizeMake(25, 25));
        make.centerY.equalTo(topView.mas_centerY);
    }];
    ///创建右边分享按钮
    UIButton *shareBtn = [[UIButton alloc]init];
    [shareBtn addTarget:self action:@selector(clickShareBtn) forControlEvents:UIControlEventTouchUpInside];
    shareBtn.contentMode = UIViewContentModeScaleAspectFill;
    [shareBtn setImage:[UIImage imageNamed:@"businessshareBtn.png"] forState:UIControlStateNormal];
    [topView addSubview:shareBtn];
    [shareBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(collectBtn.mas_left).offset(-15);
        make.size.mas_equalTo(CGSizeMake(25, 25));
        make.centerY.equalTo(topView.mas_centerY);
    }];
    //是否显示“优惠买单”按钮
    NSString *showDiscount = [NSString stringWithFormat:@"%@",_clubInfo.Recommend];
    if ([showDiscount  isEqual: @"0"]) {
        _headerView.preferentialBtn.hidden = YES;
    }
    
}

#pragma mark - HeaderView
- (void)updateHeadView
{
    if (!_headerView) {
        _headerView = [[BusineeHeaderView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, HEADERHEIGHT)];
        _headerView.delegate = self;
        [self.deepScrollView addSubview:_headerView];
        [_headerView setClubInfo:_clubInfo];
    }
}

/*****************************************/
#pragma mark --- UIScrollView代理方法

static NSInteger pageNumber = 0;
static NSInteger lastPosition = 0;
static bool isDown;
//一旦滚动就一直调用 直到停止
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (self.deepScrollView == scrollView) {
        int currentPosition = scrollView.contentOffset.y;
        if (currentPosition - lastPosition > 10) {
            lastPosition = currentPosition;
            NSLog(@"向上滑动");
            isDown = NO;
        } else if (lastPosition - currentPosition > 10) {
            lastPosition = currentPosition;
            NSLog(@"向下滑动");
            isDown = YES;
        }
    }
}

//减速到停止的时候（静止）的时候调用
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    if (self.mainScrollView == scrollView) {
        pageNumber = (int)(scrollView.contentOffset.x / SCREEN_WIDTH + 0.5);
        //滑动SV里视图,切换标题
        if(pageNumber == 2){
            pageNumber = 0;
        }
        [self.LFLuisement selectTheSegument:pageNumber];
    } else if (self.deepScrollView == scrollView && isDown == YES){
        ///==============通过动画上下文使用UIKit动画
        //设置改变顶部视图颜色的动画
        [UIView beginAnimations:@"changeColor" context:nil];//开始动画
        [UIView setAnimationDuration:1.0];//动画时长
        //要进行动画的地方
        [topView setBackgroundColor:kRGBColor];
        [UIView commitAnimations];//动画结束
    } else if (self.deepScrollView == scrollView && isDown == NO) {
        ///===============通过代码块使用UIKit动画
        [UIView animateWithDuration:1.0 animations:^{
            [topView setBackgroundColor:[UIColor clearColor]];
        } completion:^(BOOL finished) {
            
        }];
    }
}

#pragma mark ---LFLUISegmentedControlDelegate
/**
 *  点击标题按钮
 *
 *  @param selection 对应下标 begain 0
 */
-(void)uisegumentSelectionChange:(NSInteger)selection{
    //加入动画,显得不太过于生硬切换
    [UIView animateWithDuration:.2 animations:^{
        [self.mainScrollView setContentOffset:CGPointMake(SCREEN_WIDTH *selection, 0)];
    }];
    
    if (selection == 0) {
        NSLog(@"点击商品预定按钮");
        self.shopCartView.hidden = NO;
        //        _shopCartView.hidden = NO;
    } else if (selection == 1){
        NSLog(@"点击商家简介按钮");
        _shopCartView.hidden = YES;
        //        self.shoppcartView.hidden = YES;
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://res.nightnet.cn/resource/Web/introduce/lanqiangyu-jiangning.html"]];
        [_myWebView2 loadRequest:[NSURLRequest requestWithURL:url]];
    }
}

#pragma mark - BusineeHeaderDelegate
- (void)addressAction{
    UIStoryboard *mainStoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    MyMapViewController *myMap = [mainStoryBoard instantiateViewControllerWithIdentifier:@"mymap"];
    myMap.latitude = [_clubInfo.Latitude doubleValue];
    myMap.longitude = [_clubInfo.Longitude doubleValue];
    myMap.clubName = _clubInfo.ClubName;
    myMap.address = _clubInfo.Address;
    [self.navigationController pushViewController:myMap animated:YES];
}

- (void)clickPreferentialBtnAction{
    if ([UserHeader userIsLogin]) {
        PreferentialPayViewController *preferential = [[PreferentialPayViewController alloc]init];
        preferential.clubID = _clubInfo.ID;
        preferential.itemID = itemID;
        preferential.clubName = _clubInfo.ClubName;
        preferential.disCount = _clubInfo.Discount;
        [self.navigationController pushViewController:preferential animated:YES];
    } else {
        UIStoryboard *mainStoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        LoginViewController *login = [mainStoryBoard instantiateViewControllerWithIdentifier:@"login"];
        [self.navigationController pushViewController:login animated:YES];
    }
}

- (void)clickSingleImgView:(NSString *)url{
    
    //创建一个黑色视图做背景
    UIView *bgView = [[UIView alloc]initWithFrame:self.view.bounds];
    background = bgView;
    bgView.backgroundColor = [UIColor colorWithRed:41.0/255 green:36.0/255 blue:33.0/255 alpha:0.8];
    [self.view addSubview:bgView];
    
    //创建要显示图像的视图
    //初始化要显示的图片内容的imageView
    UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake(0.0, 200.0, self.view.bounds.size.width, self.view.bounds.size.height-400.0)];
    [imgView sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:[UIImage imageNamed:CACHEIMG] options:SDWebImageRefreshCached];
    [bgView addSubview:imgView];
    
    //添加手势，点击背景后退出全屏
    bgView.userInteractionEnabled = YES;
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(closeView)];
    [bgView addGestureRecognizer:tapGesture];
    //放大过程中的动画
    [self shakeToShow:bgView];
}

//放大过程中出现的缓慢动画
- (void)shakeToShow:(UIView *)aView{
    CAKeyframeAnimation *animation = [CAKeyframeAnimation animationWithKeyPath:@"transform"];
    animation.duration = 0.5;
    NSMutableArray *values = [NSMutableArray array];
    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(0.1, 0.1, 1.0)]];
    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(1.0, 1.0, 1.0)]];
    animation.values = values;
    [aView.layer addAnimation:animation forKey:nil];
}

//点击图片后退出大图模式
- (void)closeView{
    [background removeFromSuperview];
}

// 隐藏状态栏
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;//白色
}

- (BOOL)prefersStatusBarHidden
{
    return NO;
}

- (void)SelectTheBack
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)clickShareBtn
{
    if ([UserHeader userIsLogin]) {
        NSMutableDictionary *shareParams = [NSMutableDictionary dictionary];
        [shareParams SSDKSetupShareParamsByText:@"满足你对夜生活的所有想象"
                                         images:[UIImage imageNamed:@"1.pic_hd.png"]
                                            url:[NSURL URLWithString:@"http://res.nightnet.cn/resource/Web/appshare/share_soft.html"]
                                          title:@"开启夜生活"
                                           type:SSDKContentTypeAuto];
        //2、分享（可以弹出我们的分享菜单和编辑界面）
        [ShareSDK showShareActionSheet:nil
                                 items:nil
                           shareParams:shareParams
                   onShareStateChanged:^(SSDKResponseState state, SSDKPlatformType platformType, NSDictionary *userData, SSDKContentEntity *contentEntity, NSError *error, BOOL end) {
                       
                       switch (state) {
                           case SSDKResponseStateSuccess:
                           {
                               UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"分享成功"
                                                                                                        message:nil
                                                                                                 preferredStyle:UIAlertControllerStyleAlert];
                               UIAlertAction *action = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleCancel handler:nil];
                               [alertController addAction:action];
                               [self presentViewController:alertController animated:YES completion:nil];
                               break;
                               
                           }
                           case SSDKResponseStateFail:
                           {
                               UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"分享失败"
                                                                                                        message:[NSString stringWithFormat:@"%@",error]
                                                                                                 preferredStyle:UIAlertControllerStyleAlert];
                               UIAlertAction *action = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleCancel handler:nil];
                               [alertController addAction:action];
                               [self presentViewController:alertController animated:YES completion:nil];
                               break;
                               
                           }
                           default:
                               break;
                       }
                   }
         ];
    }else{
        LoginViewController *login = [self.storyboard instantiateViewControllerWithIdentifier:@"login"];
        [self.navigationController pushViewController:login animated:YES];
    }
}

- (void)clickcollectionBtn
{
    if ([UserHeader userIsLogin]) {
        NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
        NSString *userID = [userDefault objectForKey:@"ID"];
        NSString *strUrl = [NSString stringWithFormat:@"%@Club/ChangeFavoriate",BASICURL];
        NSDictionary *dic = @{@"Key":userID,@"Value":_clubInfo.ID};
        [SVProgressHUD showWithStatus:@"加载中，请稍候..."];
        [BingNetWorkConnection connectionWithUrl:strUrl type:BINGO_REQUEST_POST params:dic FinishBlock:^(id responseObject) {
            [SVProgressHUD dismiss];
            NSString *isCollectionStr = [[NSString alloc]initWithData:responseObject encoding:NSUTF8StringEncoding];
            NSLog(@"%@",isCollectionStr);
            if ([isCollectionStr isEqualToString:@"删除收藏"]) {
                [collectBtn setImage:[UIImage imageNamed:@"collect.png"] forState:UIControlStateNormal];
            } else {
                [collectBtn setImage:[UIImage imageNamed:@"collected.png"] forState:UIControlStateNormal];
            }
            
        } FailedBlock:^(NSError *error) {
            [SVProgressHUD dismiss];
            NSLog(@"收藏ERROR：%@",error);
        }];
    } else {
        LoginViewController *login = [self.storyboard instantiateViewControllerWithIdentifier:@"login"];
        [self.navigationController pushViewController:login animated:YES];
    }
}

#pragma mark - WineViewDelegate
- (void)clickRightBottomBtn:(NSString *)status{
    if ([status isEqualToString:@"确定"]) {
        //测试用户确认订单
        UserCheckViewController *userCheck = [[UserCheckViewController alloc] init];
        [self.navigationController pushViewController:userCheck animated:YES];
    }
}

/*
//- (void)wineAddOrSubOrder:(NSMutableDictionary *)dictionary OrderData:(NSMutableArray *)ordersArray isAdded:(BOOL)added andStartPoint:(CGPoint)start{
//
//    CGRect parentRectB = [self.shopCartView convertRect:self.shopCartView.orderImgBtn.frame toView:self.view];
//
//    //根据添加还是减少，决定是否执行添加的动画
//    if (added) {
//        [self.view addSubview:_redView];
//        [[ThrowLineTool sharedTool] throwObject:self.redView from:start to:parentRectB.origin];
//        self.ordersArray = [WineModel StoreOrder:dictionary OrderData:ordersArray isAdded:YES];
//    } else {
//        self.ordersArray = [WineModel StoreOrder:dictionary OrderData:ordersArray isAdded:NO];
//    }
//
//    self.shopCartView.listView.objects = self.ordersArray;
//    [self.shopCartView updateFrame:self.shopCartView.listView];
//    [self.shoppcartView.listView.tableView reloadData];
//    self.shoppcartView.badgeValue = [WineModel CountOthersWithOrderData:self.ordersArray];
//    CGFloat price = [WineModel GetTotalPrice:self.ordersArray];
//    [self.shoppcartView setTotalPrice:price];
//
//}

//#pragma mark - 通知更新
//- (void)updateViewUI:(NSNotification *)notification{
//
//    NSMutableDictionary *dic =[NSMutableDictionary dictionaryWithDictionary: notification.userInfo];
//    //重新计算订单数组。
//    self.ordersArray = [WineModel StoreOrder:dic[@"update"] OrderData:self.ordersArray isAdded:[dic[@"isAdd"] boolValue]];
//    self.shoppcartView.listView.objects = self.ordersArray;
//    //设置高度。
//    [self.shoppcartView updateFrame:self.shoppcartView.listView];
//    [self.shoppcartView.listView.tableView reloadData];
//    //设置数量、价格
//    self.shoppcartView.badgeValue =  [WineModel CountOthersWithOrderData:self.ordersArray];
//    double price = [WineModel GetTotalPrice:self.ordersArray];
//    [self.shoppcartView setTotalPrice:price];
//}
*/

- (void)dealloc{
    //移除更新UI
    [[NSNotificationCenter defaultCenter] removeObserver:self forKeyPath:@"UpdateUI"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark dataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (tableView == _leftTableView) {
        return 1;
    } else if (tableView == _rightTableView) {
        return _dataArray.count;
    } else {
        return 1;
    }
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    //该项目的demo就是数组里面装着很多字典，字典里面又有数组
    //NSDictionary *item = [_dataArray objectAtIndex:section];
    GoodsCategory *goodsCategory = _dataArray[section];
    if (tableView == _leftTableView) {
        return _dataArray.count;
    } else if (tableView == _rightTableView) {
        //return [[item objectForKey:@"goodsArray"] count];
        return goodsCategory.goodsArray.count;
    } else {
        return _orderArray.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (tableView == _leftTableView) {
        static NSString *CellIdentifier = @"Cell";
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            
            cell.backgroundColor = ZXColor(240, 240, 240);
            UIView *selectedBackgroundView = [[UIView alloc] initWithFrame:cell.frame];
            selectedBackgroundView.backgroundColor = [UIColor whiteColor];
            cell.selectedBackgroundView = selectedBackgroundView;
            
            // 左侧示意条
            UIView *liner = [[UIView alloc] initWithFrame: CGRectMake(0, 0, 6, 40)];
            liner.backgroundColor = [UIColor orangeColor];
            [selectedBackgroundView addSubview:liner];
        }
        
        GoodsCategory *goodsCategory = _dataArray[indexPath.row];
        cell.textLabel.text = goodsCategory.goodsCategoryName;
        
        return cell;
    } else if (tableView == _rightTableView) {
        
        MenuItemCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
        if (!cell) {
            cell = [[MenuItemCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
        }
        cell.delegate = self;
        
        GoodsCategory *goodsCategory = _dataArray[indexPath.section];
        GoodsModel *goods = goodsCategory.goodsArray[indexPath.row];
        cell.goods = goods;
        return cell;
    } else if (tableView == _shopCartView.detailListView.listTableView) {
        DetailListCell *cell = [tableView dequeueReusableCellWithIdentifier:ListCellID];
        cell.delegate = self;
        GoodsModel *goods = [_orderArray objectAtIndex:indexPath.row];
        cell.goods = goods;
        
        return cell;
    } else {
        return nil;
    }
}

#pragma mark delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView == _leftTableView) {
        return 40;
    } else if (tableView == _rightTableView) {
        return 50;
    } else {
        return 50;
    }
}

//返回每一个组头的高度，如果想达到有那种效果则一定要做这个操作
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (tableView == _rightTableView) {
        return 30;
    } else {
        return 0;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    if (tableView == _rightTableView) {
        return 0.01;
    } else {
        return 0;
    }
}

//返回组头部的那块View
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (tableView == _rightTableView) {
        UIView *view = [[UIView alloc]init];
        view.frame = CGRectMake(0, 0, tableView.frame.size.width, 30);
        view.backgroundColor = ZXColor(240, 240, 240);
        [view setAlpha:0.7];
        UILabel *label = [[UILabel alloc]initWithFrame:view.bounds];
        label.text = self.titleArray[section];
        [view addSubview:label];
        return view;
    } else {
        return nil;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    //左边tableView
    if (tableView == _leftTableView) {
        _isRelate = NO;
        [self.leftTableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionMiddle];
        
        //点击了左边的cell，让右边的tableView跟着滚动
        [self.rightTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:indexPath.row] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    } else if (tableView == _rightTableView) {
        [_rightTableView deselectRowAtIndexPath:indexPath animated:NO];
        
        NSLog(@"点击这里可以跳到详情页面");
    } else {
        [_shopCartView.detailListView.listTableView deselectRowAtIndexPath:indexPath animated:NO];
    }
}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section {
    if (_isRelate) {
        NSInteger topCellSection = [[[tableView indexPathsForVisibleRows] firstObject] section];
        if (tableView == _rightTableView) {
            [self.leftTableView selectRowAtIndexPath:[NSIndexPath indexPathForItem:topCellSection inSection:0] animated:YES scrollPosition:UITableViewScrollPositionMiddle];
        }
    }
}

- (void)tableView:(UITableView *)tableView didEndDisplayingFooterView:(UIView *)view forSection:(NSInteger)section {
    if (_isRelate) {
        NSInteger topCellSection = [[[tableView indexPathsForVisibleRows] firstObject] section];
        if (tableView == _rightTableView) {
            [self.leftTableView selectRowAtIndexPath:[NSIndexPath indexPathForItem:topCellSection inSection:0] animated:YES scrollPosition:UITableViewScrollPositionMiddle];
        }
    }
}

#pragma mark - UISCrollViewDelegate
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    _isRelate = YES;
}

#pragma mark - MenuItemCellDelegate
- (void)menuItemCellDidClickMinusButton:(MenuItemCell *)itemCell {
    // 计算总价
    _totalPrice = _totalPrice - itemCell.goods.Price;
    
    // 设置总价
    [_shopCartView setTotalPrice:_totalPrice];
    
    --self.totalOrders;
    _shopCartView.badgeValue = self.totalOrders;
    
    // 将商品从购物车中移除
    if (itemCell.goods.orderCount == 0) {
        [self.orderArray removeObject:itemCell.goods];
    }
    [_shopCartView.detailListView.listTableView reloadData];
    _shopCartView.detailListView.objects = _orderArray;
    _shopCartView.orderArray = _orderArray;
}

- (void)menuItemCellDidClickPlusButton:(MenuItemCell *)itemCell {
    // 计算总价
    _totalPrice = _totalPrice + itemCell.goods.Price;
    // 设置总价
    [_shopCartView setTotalPrice:_totalPrice];
    
    //通过坐标转换得到抛物线的起点和终点
    CGRect parentRectA = [itemCell convertRect:itemCell.plusButton.frame toView:self.view];
    CGRect parentRectB = [_shopCartView convertRect:_shopCartView.shopCartBtn.frame toView:self.view];
    [self.view addSubview:self.redView];
    [[ThrowLineTool sharedTool] throwObject:self.redView from:parentRectA.origin to:parentRectB.origin];
    ++self.totalOrders;
    _shopCartView.badgeValue = self.totalOrders;
    
    
    // 如果这个商品已经在购物车中，就不用再添加
    if ([self.orderArray containsObject:itemCell.goods]) {
        [_shopCartView.detailListView.listTableView reloadData];
    } else {
        // 添加需要购买的商品
        [self.orderArray addObject:itemCell.goods];
        [_shopCartView.detailListView.listTableView reloadData];
    }
    _shopCartView.detailListView.objects = _orderArray;
    _shopCartView.orderArray = _orderArray;
}

#pragma mark - ThrowLineToolDelegate
- (void)animationDidFinish {
    
    [self.redView removeFromSuperview];
    [UIView animateWithDuration:0.1 animations:^{
        _shopCartView.shopCartBtn.transform = CGAffineTransformMakeScale(0.8, 0.8);
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.1 animations:^{
            _shopCartView.shopCartBtn.transform = CGAffineTransformMakeScale(1, 1);
        } completion:^(BOOL finished) {
            
        }];
        
    }];
}

#pragma mark - DetailListCellDelegate
- (void)orderDetailCellPlusButtonClicked:(DetailListCell *)cell {
    NSLog(@"订单 + 按钮点击: %@ %@ %i", cell.goods.ID, cell.goods.ItemName, cell.goods.SingleNumber);
    // 计算总价
    _totalPrice = _totalPrice + cell.goods.Price;
    // 设置总价
    [_shopCartView setTotalPrice:_totalPrice];
    ++self.totalOrders;
    _shopCartView.badgeValue = self.totalOrders;
    
    _shopCartView.detailListView.objects = _orderArray;
    _shopCartView.orderArray = _orderArray;
    [_shopCartView updateFrame:_shopCartView.detailListView];
    [_shopCartView.detailListView.listTableView reloadData];
    [_rightTableView reloadData];
}

- (void)orderDetailCellMinusButtonClicked:(DetailListCell *)cell {
    NSLog(@"订单 - 按钮点击: %@ %@ %i", cell.goods.ID, cell.goods.ItemName, cell.goods.SingleNumber);
    // 计算总价
    _totalPrice = _totalPrice - cell.goods.Price;
    // 设置总价
    [_shopCartView setTotalPrice:_totalPrice];
    --self.totalOrders;
    _shopCartView.badgeValue = self.totalOrders;
    
    // 将商品从购物车中移除
    if (cell.goods.orderCount == 0) {
        [self.orderArray removeObject:cell.goods];
    }
    _shopCartView.detailListView.objects = _orderArray;
    _shopCartView.orderArray = _orderArray;
    [_shopCartView updateFrame:_shopCartView.detailListView];
    [_shopCartView.detailListView.listTableView reloadData];
    [_rightTableView reloadData];
}

@end
