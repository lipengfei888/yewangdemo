//
//  BusinessViewController.m
//  Yewang
//
//  Created by everhelp on 16/4/14.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import "BusinessViewController.h"
@interface BusinessViewController ()<UIScrollViewDelegate>{
    NSMutableArray *_imgArray;
    UIScrollView *_scrollV;
}

@end

@implementation BusinessViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initWithData];
    [self initWithFrame];
    self.navigationController.navigationBar.translucent = NO;
}
- (void)viewWillAppear:(BOOL)animated{
    self.navigationItem.title = @"我是商家";
    self.tabBarController.tabBar.hidden = YES;
    self.navigationController.navigationBar.hidden = NO;
    [super viewWillAppear:animated];
}
- (void)initWithData{
   _imgArray = [[NSMutableArray alloc]initWithObjects:@"slide1.jpg",@"slide2.jpg",@"slide3.jpg", nil];
}

- (void)initWithFrame{
    //滚动视图
    _scrollV = [[UIScrollView alloc]initWithFrame:CGRectMake(0.0, 0.0, SCREEN_WIDTH, SCREEN_HEIGHT-64)];
    _scrollV.delegate = self;
    _scrollV.contentSize = CGSizeMake(SCREEN_WIDTH, (SCREEN_HEIGHT-64)*_imgArray.count);
    //开启滚动分页功能，如果不需要这个功能关闭即可
    [_scrollV setPagingEnabled:YES];
    //隐藏横向与纵向的滚动条
    [_scrollV setShowsVerticalScrollIndicator:NO];
    [_scrollV setShowsHorizontalScrollIndicator:NO];
    for (int i=0; i<_imgArray.count; i++) {
        UIImage *img = [UIImage imageNamed:[_imgArray objectAtIndex:i]];
        UIImageView *imgView = [[UIImageView alloc] initWithImage:img];
        
        imgView.frame = CGRectMake(0.0, (SCREEN_HEIGHT-64)*i, SCREEN_WIDTH, SCREEN_HEIGHT-64);
        
        [_scrollV addSubview:imgView];
    }
    [self.view addSubview:_scrollV];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
