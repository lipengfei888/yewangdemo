//
//  BuyDetailViewController.h
//  Yewang
//
//  Created by everhelp on 16/5/13.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BuyDetailViewController : UIViewController
@property (nonatomic, copy) NSString *total;
@property (nonatomic, copy) NSString *arriveTime;
@property (nonatomic, copy) NSString *tel;
@property (nonatomic, copy) NSString *itemName;
@property (nonatomic, copy) NSString *clubName;
@property (nonatomic, copy) NSString *contactPerson;
@property (nonatomic, copy) NSString *price;
@property (nonatomic, copy) NSString *itemID;
@property (nonatomic, copy) NSString *clubID;
@property (nonatomic, copy) NSString *orderID;
@property (nonatomic, copy) NSString *itemNum;
@end
