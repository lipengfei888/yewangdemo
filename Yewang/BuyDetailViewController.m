//
//  BuyDetailViewController.m
//  Yewang
//
//  Created by everhelp on 16/5/13.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import "BuyDetailViewController.h"
#import "AlipayHeader.h"
#import "WXApiObject.h"
#import "WXApi.h"
#import "OrderTableViewController.h"
#import "ReserveSuccessViewController.h"

@interface BuyDetailViewController (){
    NSString *strID;
    NSString *_paymethd;
}

@property (weak, nonatomic) IBOutlet UILabel *totalLbl;
@property (weak, nonatomic) IBOutlet UIButton *balanceBtn;
@property (weak, nonatomic) IBOutlet UIButton *alipayBtn;
@property (weak, nonatomic) IBOutlet UIButton *wechatBtn;
@property (weak, nonatomic) IBOutlet UIButton *payBtn;

@end

@implementation BuyDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = @"支付订单";
    self.payBtn.layer.cornerRadius = 14;
    self.payBtn.layer.masksToBounds = YES;
    _totalLbl.text = _total;
    
    [self.alipayBtn setImage:[UIImage imageNamed:@"pay"] forState:UIControlStateNormal];
    if ([self.orderID isEqualToString:@"未添加"]) {
        //生成唯一标识符
        strID = [CheckString createCUID];//订单编号strID
    }else{
        strID = _orderID;
    }
    _paymethd = @"支付宝";
    
}

//账户余额
- (IBAction)clickBalancePay:(id)sender {
    _paymethd = @"余额";
//    [self.balanceBtn setImage:[UIImage imageNamed:@"支付_17"] forState:UIControlStateNormal];
//    [self.alipayBtn setImage:[UIImage imageNamed:@"支付_10"] forState:UIControlStateNormal];
//    [self.wechatBtn setImage:[UIImage imageNamed:@"支付_10"] forState:UIControlStateNormal];
    [self.balanceBtn setImage:[UIImage imageNamed:@"pay"] forState:UIControlStateNormal];
    [self.alipayBtn setImage:[UIImage imageNamed:@"notPay"] forState:UIControlStateNormal];
    [self.wechatBtn setImage:[UIImage imageNamed:@"notPay"] forState:UIControlStateNormal];
}
//支付宝
- (IBAction)clickAlipayPay:(id)sender {
    _paymethd = @"支付宝";
    [self.alipayBtn setImage:[UIImage imageNamed:@"pay"] forState:UIControlStateNormal];
    [self.balanceBtn setImage:[UIImage imageNamed:@"notPay"] forState:UIControlStateNormal];
    [self.wechatBtn setImage:[UIImage imageNamed:@"notPay"] forState:UIControlStateNormal];
}
//微信
- (IBAction)clickWechatPay:(id)sender {
    _paymethd = @"微信";
    [self.wechatBtn setImage:[UIImage imageNamed:@"pay"] forState:UIControlStateNormal];
    [self.alipayBtn setImage:[UIImage imageNamed:@"notPay"] forState:UIControlStateNormal];
    [self.balanceBtn setImage:[UIImage imageNamed:@"notPay"] forState:UIControlStateNormal];
}
- (IBAction)clickPay:(id)sender {
    if ([self.orderID isEqualToString:@"未添加"]) {
        //添加一个订单
        [self addOrder:NO andPayMethod:@"未付款"];
    }
    if ([_paymethd isEqualToString:@"支付宝"]) {
        [self ZFBPay];
    } else if ([_paymethd isEqualToString:@"微信"]) {
        [self payByWechat];
    } else if([_paymethd isEqualToString:@"余额"]) {
        [self payByBalance];
    }
}

- (void)addOrder:(BOOL)flag andPayMethod:(NSString *)payMethod{
    
    
    NSString *clubID = _clubID;
    NSUserDefaults *userdefault = [NSUserDefaults standardUserDefaults];
    NSString *userID = [userdefault objectForKey:@"ID"];
    NSString *itemID = _itemID;
    NSString *status ;
    if (flag) {
        status = @"已付款";
    } else {
        status = @"未付款";
    }
    NSDictionary *dic = @{@"ID":strID,@"ClubID":clubID,@"UserID":userID,@"ItemID":itemID,@"Price":_price,@"Mount":_itemNum,@"Total":_total,@"Status":status,@"PayMethod":payMethod,@"ContactPerson":_contactPerson,@"ContactNumber":_tel,@"ArriveTime":_arriveTime};
    
    if (flag) {
        NSString *payUrl = [NSString stringWithFormat:@"%@Order/PayOrder",BASICURL];
        [BingNetWorkConnection connectionWithUrl:payUrl type:BINGO_REQUEST_POST params:dic FinishBlock:^(id responseObject) {
            
            NSString *strResult = [[NSString alloc]initWithData:responseObject encoding:NSUTF8StringEncoding];
            NSLog(@"订单支付Result%@",strResult);
            
        } FailedBlock:^(NSError *error) {
            NSLog(@"订单支付===%@",error);
        } ];
    } else {
        NSString *strUrl = [NSString stringWithFormat:@"%@Order/AddOrder",BASICURL];
        [BingNetWorkConnection connectionWithUrl:strUrl type:BINGO_REQUEST_POST params:dic FinishBlock:^(id responseObject) {
            
            NSString *strResult = [[NSString alloc]initWithData:responseObject encoding:NSUTF8StringEncoding];
            NSLog(@"添加订单Result%@",strResult);
            
        } FailedBlock:^(NSError *error) {
            NSLog(@"添加订单===%@",error);
        } ];
    }
}

- (void)payByWechat
{
    NSString *res = [self jumpToPay];
    if( ![@"" isEqual:res] ){
        UIAlertView *alter = [[UIAlertView alloc] initWithTitle:@"支付失败" message:res delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [alter show];
    }
    
}

- (NSString *)jumpToPay {
    
    NSString *urlString   = @"http://wxpay.weixin.qq.com/pub_v2/app/app_pay.php?plat=ios";
    //解析服务端返回json数据
    NSError *error;
    //加载一个NSURL对象
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    //将请求的url数据放到NSData对象中
    NSData *response = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    if ( response != nil) {
        NSMutableDictionary *dict = NULL;
        //IOS5自带解析类NSJSONSerialization从response中解析出数据放到字典中
        dict = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableLeaves error:&error];
        
        if(dict != nil){
            NSMutableString *retcode = [dict objectForKey:@"retcode"];
            if (retcode.intValue == 0){
                NSMutableString *stamp  = [dict objectForKey:@"timestamp"];
                
                //调起微信支付
                PayReq* req             = [[PayReq alloc] init];
                req.partnerId           = [dict objectForKey:@"partnerid"];
                req.prepayId            = [dict objectForKey:@"prepayid"];
                req.nonceStr            = [dict objectForKey:@"noncestr"];
                req.timeStamp           = stamp.intValue;
                req.package             = [dict objectForKey:@"package"];
                req.sign                = [dict objectForKey:@"sign"];
                [WXApi sendReq:req];
                NSLog(@"%d",[WXApi sendReq:req]);
                //日志输出
                NSLog(@"appid=%@\npartid=%@\nprepayid=%@\nnoncestr=%@\ntimestamp=%ld\npackage=%@\nsign=%@",[dict objectForKey:@"appid"],req.partnerId,req.prepayId,req.nonceStr,(long)req.timeStamp,req.package,req.sign );
                return @"";
            }else{
                return [dict objectForKey:@"retmsg"];
            }
        }else{
            return @"服务器返回错误，未获取到json对象";
        }
    }else{
        return @"服务器返回错误";
    }
}

- (void)payByBalance{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"提示" message:@"您的余额为0.00元，推荐您使用支付宝付款。" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleCancel handler:nil];
    [alertController addAction:cancelAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

//客户端提示信息
- (void)alert:(NSString *)title msg:(NSString *)msg
{
    UIAlertView *alter = [[UIAlertView alloc] initWithTitle:title message:msg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    
    [alter show];
}

- (void)ZFBPay{
    /*
     *生成订单信息及签名
     */
    //将商品信息赋予AlixPayOrder的成员变量
    Order *order = [[Order alloc] init];
    order.partner = kPartnerID;
    order.seller = kSellerAccount;
    order.tradeNO = [AlipayToolKit genTradeNoWithTime]; //订单ID（由商家自行制定）
    order.productName = @"夜网订单"; //商品标题
    order.productDescription = [NSString stringWithFormat:@"%@间",_itemName]; //商品描述
    order.amount = [NSString stringWithFormat:@"%.2f",[_total doubleValue]]; //商品价格
    order.notifyURL =  kNotifyURL; //回调URL
    
    order.service = @"mobile.securitypay.pay";
    order.paymentType = @"1";
    order.inputCharset = @"utf-8";
    order.itBPay = @"30m";
    order.showUrl = @"m.alipay.com";
    //应用注册scheme,在AlixPayDemo-Info.plist定义URL types
    NSString *appScheme = kAppScheme;
    //将商品信息拼接成字符串
    NSString *orderSpec = [order description];
    //获取私钥并将商户信息签名,外部商户可以根据情况存放私钥和签名,只需要遵循RSA签名规范,并将签名字符串base64编码和UrlEncode
    id<DataSigner> signer = CreateRSADataSigner(kPrivateKey);
    NSString *signedString = [signer signString:orderSpec];
    //将签名成功字符串格式化为订单字符串,请严格按照该格式
    NSString *orderString = nil;
    if (signedString != nil) {
        orderString = [NSString stringWithFormat:@"%@&sign=\"%@\"&sign_type=\"%@\"",
                       orderSpec, signedString, @"RSA"];
        [[AlipaySDK defaultService] payOrder:orderString fromScheme:appScheme callback:^(NSDictionary *resultDic) {
            NSLog(@"2reslut = %@",resultDic);
            
            NSString * result = nil;
            
            if ([resultDic[@"resultStatus"] isEqualToString:@"9000"]) {
                result = @"支付成功";
                [self addOrder:YES andPayMethod:@"支付宝"];
                ReserveSuccessViewController *reserve = [[ReserveSuccessViewController alloc] init];
                reserve.orderID = strID;
                [self.navigationController pushViewController:reserve animated:YES];
            } else {
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:result message:@"支付未完成" preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                    
                    [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:1] animated:YES];
                    
                }];
                [alertController addAction:cancelAction];
                [self presentViewController:alertController animated:YES completion:nil];
            }
        }];
        //接收来自APPdelegate的通知(注册观察者)
        //获取通知中心单例对象
        NSNotificationCenter * center = [NSNotificationCenter defaultCenter];
        //添加当前类对象为一个观察者，name和object设置为nil，表示接收一切通知
        [center addObserver:self selector:@selector(notice:) name:@"payResult" object:nil];
    }
}

- (void)notice:(NSDictionary *)sender{
    NSString *str = [NSString stringWithFormat:@"%@",sender];
    NSString *resultStr = [[str substringFromIndex:185]substringToIndex:4];
    
    NSString * result = nil;
    if ([str rangeOfString:@"9000"].location !=NSNotFound) {
        result = @"支付成功";
        [self addOrder:YES andPayMethod:@"支付宝"];
        [self.navigationController popToRootViewControllerAnimated:YES];
    } else if ([resultStr isEqualToString:@"6001"]) {
        result = @"用户中途取消";
    } else if ([resultStr isEqualToString:@"6002"]) {
        result = @"网络连接出错";
    } else if ([resultStr isEqualToString:@"4000"]) {
        result = @"订单支付失败";
    } else if ([resultStr isEqualToString:@"8000"]) {
        result = @"正在处理中";
    } else {
        result = @"订单支付失败";
    }
    if ([str rangeOfString:@"9000"].location ==NSNotFound) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:result message:@"支付未完成" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            
            [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:1] animated:YES];
        }];
        [alertController addAction:cancelAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }
    
}
- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
