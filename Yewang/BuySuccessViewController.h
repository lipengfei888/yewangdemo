//
//  BuySuccessViewController.h
//  Yewang
//
//  Created by everhelp on 16/5/26.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BuySuccessViewController : UIViewController

@property (nonatomic, copy) NSString *clubID;

@end
