//
//  BuySuccessViewController.m
//  Yewang
//
//  Created by everhelp on 16/5/26.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import "BuySuccessViewController.h"

@interface BuySuccessViewController ()
@property (weak, nonatomic) IBOutlet UILabel *nameLbl;
@property (weak, nonatomic) IBOutlet UIButton *telBtn;
@property (weak, nonatomic) IBOutlet UIButton *downBtn;

@end

@implementation BuySuccessViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _downBtn.layer.cornerRadius = 12;
    _downBtn.layer.masksToBounds = YES;
    
    NSString *strUrl = [NSString stringWithFormat:@"%@Order/GetOrderViewSP?clubID=%@",BASICURL,_clubID];
    [BingNetWorkConnection connectionWithUrl:strUrl type:BINGO_REQUEST_GET params:nil FinishBlock:^(id responseObject) {
        
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:nil];
        [self.telBtn setTitle:[dic objectForKey:@"ContactNumber"] forState:UIControlStateNormal];
        self.nameLbl.text = [dic objectForKey:@"ContactPerson"];
    } FailedBlock:^(NSError *error) {
        NSLog(@"订单支付===%@",error);
    } ];
    
}
- (IBAction)clickTel:(id)sender {
    NSMutableString *strPhone = [[NSMutableString alloc]initWithFormat:@"tel:%@",self.telBtn.titleLabel.text];
    UIWebView *callWebView = [[UIWebView alloc]init];
    [callWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:strPhone]]];
    [self .view addSubview:callWebView];
}
- (IBAction)clickDown:(id)sender {
    //我的订单
    NSMutableString *strPhone = [[NSMutableString alloc]initWithFormat:@"tel:%@",self.telBtn.titleLabel.text];
    UIWebView *callWebView = [[UIWebView alloc]init];
    [callWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:strPhone]]];
    [self .view addSubview:callWebView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
