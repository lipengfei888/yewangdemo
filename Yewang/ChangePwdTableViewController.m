//
//  ChangePwdTableViewController.m
//  Yewang
//
//  Created by everhelp on 16/4/15.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import "ChangePwdTableViewController.h"

@interface ChangePwdTableViewController ()
@property (weak, nonatomic) IBOutlet UITextField *pwdOldTxt;
@property (weak, nonatomic) IBOutlet UITextField *pwdNewTxt;
@property (weak, nonatomic) IBOutlet UITextField *checkTxt;

@property (weak, nonatomic) IBOutlet UIButton *submitBtn;

@end

@implementation ChangePwdTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //去除下面多余的cell
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.navigationItem.title = @"修改密码";
    //修改按钮的圆角效果
    _submitBtn.layer.cornerRadius = 5;
    _submitBtn.layer.masksToBounds = YES;
    
}
- (void)viewWillAppear:(BOOL)animated{
    self.tabBarController.tabBar.hidden = YES;
    [super viewWillAppear:animated];
}

- (IBAction)clickSubmit:(id)sender {
    BOOL flag = [self checkPassword:self.pwdOldTxt.text andNewPwd:self.pwdNewTxt.text andCheckPwd:self.checkTxt.text];
    NSLog(@"密码格式判断=%d",flag);
    //密码框内容符合条件
    if (flag == 1){
        /*
         判断该UserID的密码是否正确(POST请求)
         */
        NSString *strUrl = [NSString stringWithFormat:@"%@User/CheckPassword",BASICURL];
        NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
        NSString *userID = [userDefault valueForKey:@"ID"];
        NSLog(@"supplier==id%@",userID);
        
        NSDictionary *dictionary = @{@"Key":userID,@"Value":self.pwdOldTxt.text};
        [BingNetWorkConnection connectionWithUrl:strUrl type:BINGO_REQUEST_POST params:dictionary FinishBlock:^(id responseObject) {
            
            NSString *string = [[NSString alloc]initWithData:responseObject encoding:NSUTF8StringEncoding];
            NSLog(@"str====%@",string);
            if ([string isEqualToString:@"密码错误"]) {
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"提示" message:@"原密码不正确，请重新输入" preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"好的" style:UIAlertActionStyleCancel handler:nil];
                [alertController addAction:alertAction];
                [self presentViewController:alertController animated:YES completion:nil];
            }else{
                /*
                 密码正确，修改密码（GET请求）
                 */
                NSString *strChangeUrl = [NSString stringWithFormat:@"%@User/GetChangePassword?ID=%@&Password=%@",BASICURL,userID,self.checkTxt.text];
                
                [BingNetWorkConnection connectionWithUrl:strChangeUrl type:BINGO_REQUEST_GET params:nil FinishBlock:^(id responseObject) {
                    
                    NSString *string = [[NSString alloc]initWithData:responseObject encoding:NSUTF8StringEncoding];
                    NSLog(@"str====%@",string);
                    if ([string isEqualToString:@"修改成功"]) {
                        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"提示" message:@"密码修改成功" preferredStyle:UIAlertControllerStyleAlert];
                        UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                            [self.navigationController popViewControllerAnimated:YES];
                        }];
                        [alertController addAction:alertAction];
                        [self presentViewController:alertController animated:YES completion:nil];
                        
                        
                    }else{
                        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"提示" message:@"密码修改失败" preferredStyle:UIAlertControllerStyleAlert];
                        UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"好的" style:UIAlertActionStyleCancel handler:nil];
                        [alertController addAction:alertAction];
                        [self presentViewController:alertController animated:YES completion:nil];
                    }
                    
                    
                } FailedBlock:^(NSError *error) {
                    NSLog(@"%@",error);
                }];
            }
            
            
        } FailedBlock:^(NSError *error) {
            NSLog(@"%@",error);
        }];
        
    }
    

}

/*
 对三个密码框进行条件判断
 */
- (BOOL)checkPassword:(NSString *)oldPwd andNewPwd:(NSString *)newPwd andCheckPwd:(NSString *)checkPwd{
    
    BOOL result = YES;
    
    //判断输入框是否为空字符串
    if ([[oldPwd stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]length] == 0) {
        
        result = NO;
        
        //初始化提示框
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:@"请输入密码" preferredStyle:UIAlertControllerStyleAlert];
        
        //创建cancel按钮
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"好的" style:UIAlertActionStyleCancel handler:nil];
        
        //将按钮添加到控制器
        [alert addAction:cancelAction];
        
        //弹出提示框
        [self presentViewController:alert animated:YES completion:nil];
        
    } else if ([[newPwd stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]length] == 0){
        
        result = NO;
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:@"密码不能为空，请输入密码" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"好的" style:UIAlertActionStyleCancel handler:nil];
        [alert addAction:cancelAction];
        [self presentViewController:alert animated:YES completion:nil];
    }
    if (![newPwd isEqualToString:checkPwd]){
        
        result = NO;
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:@"两次密码不一致，请重新输入" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"好的" style:UIAlertActionStyleCancel handler:nil];
        [alert addAction:cancelAction];
        [self presentViewController:alert animated:YES completion:nil];
    }
    
    return  result;
    
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.pwdOldTxt resignFirstResponder];
    [self.pwdNewTxt resignFirstResponder];
    [self.checkTxt resignFirstResponder];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
