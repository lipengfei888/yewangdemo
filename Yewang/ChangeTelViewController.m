//
//  ChangeTelViewController.m
//  Yewang
//
//  Created by everhelp on 16/4/15.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import "ChangeTelViewController.h"

@interface ChangeTelViewController (){
    NSTimer *counterDownTimer;
    int freezeCounter;
}
@property (weak, nonatomic) IBOutlet UITextField *pwdTxt;
@property (weak, nonatomic) IBOutlet UITextField *indentifyTxt;
@property (weak, nonatomic) IBOutlet UITextField *telTxt;
@property (weak, nonatomic) IBOutlet UIButton *getIndentifyBtn;
@property (weak, nonatomic) IBOutlet UIButton *submitBtn;

@end

@implementation ChangeTelViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"更改绑定手机";
    self.getIndentifyBtn.layer.cornerRadius = 8.0;
    self.submitBtn.layer.cornerRadius = 8.0;
    self.getIndentifyBtn.layer.masksToBounds = YES;
    self.submitBtn.layer.masksToBounds = YES;
}
- (void)viewWillAppear:(BOOL)animated{
    self.tabBarController.tabBar.hidden = YES;
    [super viewWillAppear:animated];
}
- (IBAction)getIdentifyingCode:(id)sender {
    if ([CheckString checkPhoneNumInput:_telTxt.text]) {
        [AVOSCloud requestSmsCodeWithPhoneNumber:_telTxt.text callback:^(BOOL succeeded, NSError *error) {
            // 发送失败可以查看 error 里面提供的信息
            [self freezeMoreRequest];
        }];
    } else {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:@"请输入正确的电话号码格式" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"好的" style:UIAlertActionStyleCancel handler:nil];
        [alert addAction:cancelAction];
        [self presentViewController:alert animated:YES completion:nil];
    }
}
- (IBAction)confirmModification:(id)sender {
    if (self.indentifyTxt.text.length < 6) {
        
        NSMutableDictionary* details = [NSMutableDictionary dictionary];
        [details setValue:@"验证码无效。" forKey:NSLocalizedDescriptionKey];
        NSError *error = [NSError errorWithDomain:@"LeanSMSDemo" code:1024 userInfo:details];
        [CommonUtils displayError:error];
        
    } else {
        [self changeTelNumber];
    }
         
}

- (void)changeTelNumber{
    NSString *strUrl = [NSString stringWithFormat:@"%@User/CheckPassword",BASICURL];
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *userID = [userDefault valueForKey:@"ID"];
    NSLog(@"supplier==id%@",userID);
    
    NSDictionary *dictionary = @{@"Key":userID,@"Value":self.pwdTxt.text};
    [BingNetWorkConnection connectionWithUrl:strUrl type:BINGO_REQUEST_POST params:dictionary FinishBlock:^(id responseObject) {
        
        NSString *string = [[NSString alloc]initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSLog(@"str====%@",string);
        if ([string isEqualToString:@"密码错误"]) {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"提示" message:@"密码不正确，请重新输入" preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"好的" style:UIAlertActionStyleCancel handler:nil];
            [alertController addAction:alertAction];
            [self presentViewController:alertController animated:YES completion:nil];
        }else{
            /*
             密码正确，修改密码（GET请求）
             */
            NSString *strChangeUrl = [NSString stringWithFormat:@"%@User/ChangeMobile",BASICURL];
            NSDictionary *dic = @{@"Key":userID,@"Value":_telTxt.text};
            
            [BingNetWorkConnection connectionWithUrl:strChangeUrl type:BINGO_REQUEST_POST params:dic FinishBlock:^(id responseObject) {
                
                NSString *string = [[NSString alloc]initWithData:responseObject encoding:NSUTF8StringEncoding];
                NSLog(@"str====%@",string);
                if ([string isEqualToString:@"修改成功"]) {
                    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"提示" message:@"手机号码修改成功" preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"好的" style:UIAlertActionStyleCancel handler:nil];
                    [alertController addAction:alertAction];
                    [self presentViewController:alertController animated:YES completion:nil];
                    
                    
                }else{
                    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"提示" message:@"手机号码修改失败" preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"好的" style:UIAlertActionStyleCancel handler:nil];
                    [alertController addAction:alertAction];
                    [self presentViewController:alertController animated:YES completion:nil];
                }
                
                
            } FailedBlock:^(NSError *error) {
                NSLog(@"%@",error);
            }];
        }
        
        
    } FailedBlock:^(NSError *error) {
        NSLog(@"%@",error);
    }];
}

- (void)freezeMoreRequest {
    // 一分钟内禁止再次发送
    [self.getIndentifyBtn setEnabled:NO];
    freezeCounter = 60;
    counterDownTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(countDownRequestTimer) userInfo:nil repeats:YES];
    [counterDownTimer fire];
    
    UIAlertController *alertControl = [UIAlertController alertControllerWithTitle:@"验证码已发送" message:@"验证码已发送到你请求的手机号码。如果没有收到，可以在一分钟后尝试重新发送。" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleCancel handler:nil];
    [alertControl addAction:action];
    [self presentViewController:alertControl animated:YES completion:nil];
}

- (void)countDownRequestTimer {
    static NSString *counterFormat = @"%d 秒后再获取";
    --freezeCounter;
    if (freezeCounter<= 0) {
        [counterDownTimer invalidate];
        counterDownTimer = nil;
        [self.getIndentifyBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
        [self.getIndentifyBtn setEnabled:YES];
    } else {
        [self.getIndentifyBtn setTitle:[NSString stringWithFormat:counterFormat, freezeCounter] forState:UIControlStateNormal];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
