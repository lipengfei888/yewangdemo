//
//  CheckString.h
//  Yewang
//
//  Created by everhelp on 16/4/19.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CheckString : NSObject

+ (BOOL)checkPhoneNumInput:(NSString *) telNumber;
+ (NSString *)createCUID;
@end
