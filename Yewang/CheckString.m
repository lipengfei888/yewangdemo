//
//  CheckString.m
//  Yewang
//
//  Created by everhelp on 16/4/19.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import "CheckString.h"

@implementation CheckString

#pragma 正则匹配手机号
+ (BOOL)checkPhoneNumInput:(NSString *) telNumber{
    
//    NSString * MOBILE = @"^1(3[0-9]|5[0-35-9]|8[025-9])\\d{8}$";
//    
//    NSString * CM = @"^1(34[0-8]|(3[5-9]|5[017-9]|8[278])\\d)\\d{7}$";
//    
//    NSString * CU = @"^1(3[0-2]|5[256]|8[56])\\d{8}$";
//    
//    NSString * CT = @"^1((33|53|8[09])[0-9]|349)\\d{7}$";
//    
//    // NSString * PHS = @"^0(10|2[0-5789]|\\d{3})\\d{7,8}$";
//    
//    NSPredicate *regextestmobile = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", MOBILE];
//    NSPredicate *regextestcm = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CM];
//    NSPredicate *regextestcu = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CU];
//    NSPredicate *regextestct = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CT];
//    BOOL res1 = [regextestmobile evaluateWithObject:telNumber];
//    BOOL res2 = [regextestcm evaluateWithObject:telNumber];
//    BOOL res3 = [regextestcu evaluateWithObject:telNumber];
//    BOOL res4 = [regextestct evaluateWithObject:telNumber];
//    
//    if (res1 || res2 || res3 || res4 )
//    {
//        return YES;
//    }
//    else
//    {
//        return NO;
//    }
    if (telNumber.length == 11) {
        return YES;
    }else{
        return NO;
    }
    
}

#pragma 生成唯一标识符
+ (NSString *)createCUID{
    NSString *  result;
    CFUUIDRef   uuid;
    CFStringRef uuidStr;
    uuid = CFUUIDCreate(NULL);
    uuidStr = CFUUIDCreateString(NULL, uuid);
    result =[NSString stringWithFormat:@"%@",uuidStr];
    CFRelease(uuidStr);
    CFRelease(uuid);
    return result;
}

@end
