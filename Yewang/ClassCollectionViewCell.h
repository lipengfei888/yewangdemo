//
//  ClassCollectionViewCell.h
//  Yewang
//
//  Created by everhelp on 16/9/22.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ClassCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIButton *btnName;

@end
