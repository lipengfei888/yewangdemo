//
//  ClassFilterTableViewCell.h
//  Yewang
//
//  Created by everhelp on 16/9/22.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^selectBlock)(NSInteger index);

@interface ClassFilterTableViewCell : UITableViewCell

- (void)showData:(id)data selectBlock:(selectBlock)block;///设置数据
+ (CGFloat)getHeight:(id)data;///设置高度

@end
