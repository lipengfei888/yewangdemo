//
//  ClassFilterTableViewCell.m
//  Yewang
//
//  Created by everhelp on 16/9/22.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import "ClassFilterTableViewCell.h"
#import "ClassCollectionViewCell.h"
#import <UIButton+WebCache.h>

#define KSubCellHeight  [UIScreen mainScreen].bounds.size.width/6

@interface ClassFilterTableViewCell ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
///分割线
@property (nonatomic, strong) UILabel *lineLbl;
///分类名称
@property (nonatomic, strong) UILabel *className;
///所有类型
@property (nonatomic, strong) UICollectionView *classcollectView;
@property (nonatomic, copy) selectBlock block;
@property (nonatomic, strong) NSArray *array;

@end


@implementation ClassFilterTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self == [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        ///分割线
        [self addSubview:self.lineLbl];
        [self.lineLbl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.mas_left).offset(0);
            make.top.equalTo(self.mas_top).offset(20);
            make.right.equalTo(self.mas_right).offset(0);
            make.height.mas_equalTo(1);
        }];
        ///分类名称
        [self addSubview:self.className];
        [self.className mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.mas_left).offset(30);
            make.centerY.equalTo(self.lineLbl).offset(0);
            make.width.mas_equalTo(50);
        }];
        ///显示所有分类
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
        self.classcollectView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
        self.classcollectView.backgroundColor = [UIColor whiteColor];
        self.classcollectView.delegate = self;
        self.classcollectView.dataSource = self;
        ///注册单元格
        UINib *nib = [UINib nibWithNibName:@"ClassCollectionViewCell" bundle:nil];
        [self.classcollectView registerNib:nib forCellWithReuseIdentifier:@"ClassCollectionViewCell"];
        [self addSubview:self.classcollectView];
        [self.classcollectView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.mas_left).offset(0);
            make.top.equalTo(self.className.mas_bottom).offset(10);
            make.right.equalTo(self.mas_right).offset(0);
            make.bottom.equalTo(self.mas_bottom).offset(0);
        }];
    }
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    return self;
}

+ (CGFloat)getHeight:(id)data{
    NSDictionary *dict = data;
    NSArray *array = [dict objectForKey:@"dataSource"];
    if (array.count%6 == 0 && array.count>=6) {
        return KSubCellHeight * (array.count/6) + 50;
    } else {
        return KSubCellHeight * ((array.count/6)+1)+50;
    }
}

- (void)showData:(id)data selectBlock:(selectBlock)block{
    NSDictionary *dict = data;
    self.block = block;
    self.className.text = [dict objectForKey:@"className"];
    self.array = [dict objectForKey:@"dataSource"];
    [self.classcollectView reloadData];
}

- (UILabel *)lineLbl{
    if (_lineLbl == nil) {
        _lineLbl = [[UILabel alloc] initWithFrame:CGRectZero];
        _lineLbl.backgroundColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.8];
        _lineLbl.font = [UIFont systemFontOfSize:14.0];
    }
    return _lineLbl;
}

- (UILabel *)className{
    if (_className == nil) {
        _className = [[UILabel alloc] initWithFrame:CGRectZero];
        _className.backgroundColor = [UIColor whiteColor];
        _className.textAlignment = NSTextAlignmentCenter;
        _className.font = [UIFont systemFontOfSize:14.0];
    }
    return _className;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

#pragma mark - UICollectionViewDelegate,UICollectionViewDataSource
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.array.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier = @"ClassCollectionViewCell";
    ClassCollectionViewCell *classCell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
//    [classCell.btnName setImage:[UIImage imageNamed:self.array[indexPath.item]] forState:UIControlStateNormal];
    NSString *imgUrl = [NSString stringWithFormat:@"http://res.nightnet.cn/Resource/Filter/%@",self.array[indexPath.item]];
    [classCell.btnName sd_setImageWithURL:[NSURL URLWithString:imgUrl] forState:UIControlStateNormal];
    [classCell.btnName setImageEdgeInsets:UIEdgeInsetsMake(0, 15, 0, 15)];
    [classCell.btnName setTintColor:[UIColor blackColor]];
    return classCell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"点击单元格了，mmmmmmmddddddddddd");
    if (self.block!=nil) {
        self.block(indexPath.item);
    }
}

//设置元素的大小框
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    UIEdgeInsets top = {0,0,0,0};
    return top;
}

//设置元素大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(SCREEN_WIDTH/6, SCREEN_WIDTH/6);
}

//cell的最小行间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 0;
}

//cell的最小列间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return 0;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
