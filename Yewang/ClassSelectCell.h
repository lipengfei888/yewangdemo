//
//  ClassSelectCell.h
//  Yewang
//
//  Created by everhelp on 16/10/18.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ClassSelectCell : UITableViewCell

///设置数据
- (void)showData:(id)data;
///设置高度
+ (CGFloat)getHeight:(id)data;

@end
