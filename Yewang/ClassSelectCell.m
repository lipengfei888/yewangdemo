//
//  ClassSelectCell.m
//  Yewang
//
//  Created by everhelp on 16/10/18.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import "ClassSelectCell.h"
#import "ClassCollectionViewCell.h"
#import <UIButton+WebCache.h>

#define KSubCellHeight [UIScreen mainScreen].bounds.size.width/6

@interface ClassSelectCell ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>

///所以类型
@property (nonatomic, strong) UICollectionView *classCollectionView;
///数据源
@property (nonatomic, strong) NSArray *array;

@end

@implementation ClassSelectCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self == [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        ///显示所以分类
        UICollectionViewLayout *layout = [[UICollectionViewLayout alloc] init];
        self.classCollectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
        self.classCollectionView.backgroundColor = [UIColor whiteColor];
        self.classCollectionView.delegate = self;
        self.classCollectionView.dataSource = self;
        ///注册单元格
        UINib *nib = [UINib nibWithNibName:@"ClassCollectionViewCell" bundle:nil];
        [self.classCollectionView registerNib:nib forCellWithReuseIdentifier:@"ClassCollectionViewCell"];
        [self addSubview:self.classCollectionView];
        [self.classCollectionView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.insets(UIEdgeInsetsMake(0, 0, 0, 0)).equalTo(self.contentView);
        }];
    }
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    return self;
}

+ (CGFloat)getHeight:(id)data{
    NSArray *arr = data;
    if (arr.count%6 == 0 && arr.count>=6) {
        return KSubCellHeight * (arr.count/6);
    } else {
        return KSubCellHeight * ((arr.count/6)+1);
    }
}

-(void)showData:(id)data{
    self.array = data;
    [self.classCollectionView reloadData];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

#pragma mark - UICollectionViewDelegate,UICollectionViewDataSource
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.array.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier = @"ClassCollectionViewCell";
    ClassCollectionViewCell *classCell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
//    [classCell.btnName setImage:[UIImage imageNamed:self.array[indexPath.item]] forState:UIControlStateNormal];
//    [classCell.btnName ]
    NSString *imgUrl = [NSString stringWithFormat:@"http://res.nightnet.cn/Resource/Filter/%@",self.array[indexPath.item]];
    [classCell.btnName sd_setImageWithURL:[NSURL URLWithString:imgUrl] forState:UIControlStateNormal];
    [classCell.btnName setImageEdgeInsets:UIEdgeInsetsMake(0, 15, 0, 15)];
    [classCell.btnName setTintColor:[UIColor blackColor]];
    return classCell;;
}
//设置元素的大小框
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    UIEdgeInsets top = {0,0,0,0};
    return top;
}
//设置元素大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(SCREEN_WIDTH/6, SCREEN_WIDTH/6);
}
//cell的最小行间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 0;
}
//cell的最小列间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return 0;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
