//
//  ClubListInfo.h
//  Yewang
//
//  Created by everhelp on 16/4/22.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ClubListInfo : NSObject<NSCoding>
///地址
@property (nonatomic, copy) NSString *Address;
///余额
@property (nonatomic, copy) NSString *Balance;
///分类
@property (nonatomic, copy) NSString *CategoryAs;
///所在城市
@property (nonatomic, copy) NSString *City;
///会所简介
@property (nonatomic, copy) NSString *ClubContent;
///商家名称
@property (nonatomic, copy) NSString *ClubName;
///商家图片
@property (nonatomic, copy) NSString *ClubPic;
///经理电话
@property (nonatomic, copy) NSString *ContactNumber;
///折扣
@property (nonatomic, copy) NSString *Discount;
///区域
@property (nonatomic, copy) NSString *District;
///ID
@property (nonatomic, copy) NSString *ID;
///是否营业
@property (nonatomic, copy) NSString *IsOpen;
///纬度
@property (nonatomic, copy) NSString *Latitude;
///经度
@property (nonatomic, copy) NSString *Longitude;
///
@property (nonatomic, copy) NSString *Mode;
///省份
@property (nonatomic, copy) NSString *Province;
///是否推荐
@property (nonatomic, copy) NSString *Recommend;
///
@property (nonatomic, copy) NSString *Region;
///分享
@property (nonatomic, copy) NSString *ShareUrl;
///起始价
@property (nonatomic, copy) NSString *StartWith;
///
@property (nonatomic, copy) NSString *WebUrl;
///
@property (nonatomic, copy) NSString *WineUrl;

#pragma -mark 字典转模型
-(instancetype)initWithDict:(NSDictionary *)dict;

@end
