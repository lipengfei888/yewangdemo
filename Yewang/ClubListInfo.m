//
//  ClubListInfo.m
//  Yewang
//
//  Created by everhelp on 16/4/22.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import "ClubListInfo.h"

#import "FMDBHepler.h"

@implementation ClubListInfo

#pragma -mark 字典转模型
-(instancetype)initWithDict:(NSDictionary *)dict{
    if (self == [super init]) {
        self.Address = dict[@"Address"];
        self.Balance = dict[@"Balance"];
        self.CategoryAs = dict[@"CategoryAs"];
        self.City = dict[@"City"];
        self.ClubContent = dict[@"ClubContent"];
        self.ClubName = dict[@"ClubName"];
        self.ClubPic = dict[@"ClubPic"];
        self.ContactNumber = dict[@"ContactNumber"];
        self.Discount = dict[@"Discount"];
        self.District = dict[@"District"];
        self.ID = dict[@"ID"];
        self.IsOpen = dict[@"IsOpen"];
        self.Latitude = dict[@"Latitude"];
        self.Longitude = dict[@"Longitude"];
        self.Mode = dict[@"Mode"];
        self.Province = dict[@"Province"];
        self.Recommend = dict[@"Recommend"];
        self.Region = dict[@"Region"];
        self.ShareUrl = dict[@"ShareUrl"];
        self.StartWith = dict[@"StartWith"];
        self.WebUrl = dict[@"WebUrl"];
        self.WineUrl = dict[@"WineUrl"];
        
        //缓存
        [[FMDBHepler sharedInstance] saveSteps:self];
    }
    return self;
}

///归档
-(void)encodeWithCoder:(NSCoder *)encoder{
    [encoder encodeObject:self.Address forKey:@"Address"];
    [encoder encodeObject:self.Balance forKey:@"Balance"];
    [encoder encodeObject:self.CategoryAs forKey:@"CategoryAs"];
    [encoder encodeObject:self.City forKey:@"City"];
    [encoder encodeObject:self.ClubContent forKey:@"ClubContent"];
    [encoder encodeObject:self.ClubName forKey:@"ClubName"];
    [encoder encodeObject:self.ClubPic forKey:@"ClubPic"];
    [encoder encodeObject:self.ContactNumber forKey:@"ContactNumber"];
    [encoder encodeObject:self.Discount forKey:@"Discount"];
    [encoder encodeObject:self.District forKey:@"District"];
    [encoder encodeObject:self.ID forKey:@"ID"];
    [encoder encodeObject:self.IsOpen forKey:@"IsOpen"];
    [encoder encodeObject:self.Latitude forKey:@"Latitude"];
    [encoder encodeObject:self.Longitude forKey:@"Longitude"];
    [encoder encodeObject:self.Mode forKey:@"Mode"];
    [encoder encodeObject:self.Province forKey:@"Province"];
    [encoder encodeObject:self.Recommend forKey:@"Recommend"];
    [encoder encodeObject:self.Region forKey:@"Region"];
    [encoder encodeObject:self.ShareUrl forKey:@"ShareUrl"];
    [encoder encodeObject:self.StartWith forKey:@"StartWith"];
    [encoder encodeObject:self.WebUrl forKey:@"WebUrl"];
    [encoder encodeObject:self.WineUrl forKey:@"WineUrl"];
}

///解档
-(instancetype)initWithCoder:(NSCoder *)aDecoder{
    if (self == [super init]) {
        self.Address = [aDecoder decodeObjectForKey:@"Address"];
        self.Balance = [aDecoder decodeObjectForKey:@"Balance"];
        self.CategoryAs = [aDecoder decodeObjectForKey:@"CategoryAs"];
        self.City = [aDecoder decodeObjectForKey:@"City"];
        self.ClubContent = [aDecoder decodeObjectForKey:@"ClubContent"];
        self.ClubName = [aDecoder decodeObjectForKey:@"ClubName"];
        self.ClubPic = [aDecoder decodeObjectForKey:@"ClubName"];
        self.ContactNumber = [aDecoder decodeObjectForKey:@"ContactNumber"];
        self.Discount = [aDecoder decodeObjectForKey:@"Discount"];
        self.District = [aDecoder decodeObjectForKey:@"District"];
        self.ID = [aDecoder decodeObjectForKey:@"ID"];
        self.IsOpen = [aDecoder decodeObjectForKey:@"IsOpen"];
        self.Latitude = [aDecoder decodeObjectForKey:@"Latitude"];
        self.Longitude = [aDecoder decodeObjectForKey:@"Longitude"];
        self.Mode = [aDecoder decodeObjectForKey:@"Mode"];
        self.Province = [aDecoder decodeObjectForKey:@"Province"];
        self.Recommend = [aDecoder decodeObjectForKey:@"Recommend"];
        self.Region = [aDecoder decodeObjectForKey:@"Region"];
        self.ShareUrl = [aDecoder decodeObjectForKey:@"ShareUrl"];
        self.StartWith = [aDecoder decodeObjectForKey:@"StartWith"];
        self.WebUrl = [aDecoder decodeObjectForKey:@"WebUrl"];
        self.WineUrl = [aDecoder decodeObjectForKey:@"WineUrl"];
    }
    return self;
}

@end
