//
//  ClubTableViewCell.h
//  Yewang
//
//  Created by everhelp on 16/4/11.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ClubTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *clubImgView;
@property (weak, nonatomic) IBOutlet UILabel *clubNameLbl;
@property (weak, nonatomic) IBOutlet UILabel *addressLbl;
@property (weak, nonatomic) IBOutlet UILabel *priceLbl;
@property (weak, nonatomic) IBOutlet UIButton *huiBtn;
@property (weak, nonatomic) IBOutlet UILabel *distanceLbl;

@end
