//
//  ClubTableViewCell.m
//  Yewang
//
//  Created by everhelp on 16/4/11.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import "ClubTableViewCell.h"

@implementation ClubTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.clubImgView.layer.cornerRadius = 5.0;
    self.clubImgView.layer.masksToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
