//
//  CollectionTableViewController.m
//  Yewang
//
//  Created by everhelp on 16/4/13.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import "CollectionTableViewController.h"
#import "CollectionTableViewCell.h"
#import "ClubListInfo.h"
#import "BusinessDescriptionVC.h"

@interface CollectionTableViewController (){
    NSMutableArray *_array;
    UIImageView *bgImgView;
}

@end

@implementation CollectionTableViewController

- (void)viewWillAppear:(BOOL)animated{
    self.tabBarController.tabBar.hidden = YES;
    self.navigationController.navigationBar.hidden = NO;
    [self initWithCollectionData];
    [super viewWillAppear:animated];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"收藏";
    self.navigationController.navigationBar.backgroundColor = [UIColor colorWithRed:255.0/255 green:73.0/255 blue:93.0/255 alpha:1.0];
    [self.tableView registerNib:[UINib nibWithNibName:@"CollectionTableViewCell" bundle:nil] forCellReuseIdentifier:@"collection"];
    self.tableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
    //去掉cell分割线
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self initWithCollectionData];
    [self initMyMJRefresh];
    
}
- (void)initMyMJRefresh{
    // 设置回调（一旦进入刷新状态，就调用target的action，也就是调用self的dataByMJRefreshHeader方法）
    MJRefreshGifHeader *header = [MJRefreshGifHeader headerWithRefreshingBlock:^{
        [self performSelector:@selector(initWithCollectionData) withObject:nil afterDelay:1.5];
    }];
    
    // 设置header
    self.tableView.mj_header = header;
}


//停止刷新
- (void)endRefresh{
    [self.tableView.mj_header endRefreshing];
}
- (void)initWithCollectionData{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *userID = [userDefault objectForKey:@"ID"];
    NSString *strUrl = [NSString stringWithFormat:@"%@Club/QueryFavoriateList",BASICURL];
    NSDictionary *dic = @{@"Key":userID,@"Value":@0};
    
    [SVProgressHUD showWithStatus:@"加载中，请稍候..."];
    
    [BingNetWorkConnection connectionWithUrl:strUrl type:BINGO_REQUEST_POST params:dic FinishBlock:^(id responseObject) {
        
        [SVProgressHUD dismiss];
        [self endRefresh];
        
        NSArray *arr = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:nil];
        if (arr.count==0) {
            if (bgImgView == nil) {
                bgImgView = [[UIImageView alloc]init];
                [bgImgView setImage:[UIImage imageNamed:@"notFound"]];
                [bgImgView setContentMode:UIViewContentModeScaleAspectFit];
                [self.view addSubview:bgImgView];
                [bgImgView mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.centerX.equalTo(self.view.mas_centerX);
                    make.top.equalTo(self.view.mas_top).offset(SCREEN_HEIGHT/5);
                    make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH/2, SCREEN_HEIGHT/3));
                }];
                [self.tableView setScrollEnabled:NO];
            }
        } else {
            if (bgImgView) {
                [bgImgView removeFromSuperview];
                [self.tableView setScrollEnabled:YES];
            }
        }
        _array = [[NSMutableArray alloc]initWithCapacity:0];
        
        for (int i = 0; i < arr.count; i ++) {
            if (![[arr objectAtIndex:0]isKindOfClass:[NSNull class]]) {
                NSDictionary *dictionary = [arr objectAtIndex:i];
                ClubListInfo *club = [ClubListInfo mj_objectWithKeyValues:dictionary];
                [_array addObject:club];
            }
           
        }
        //刷新表格
        [self.tableView reloadData];
        
    } FailedBlock:^(NSError *error) {
        NSLog(@"%@",error);
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (_array.count>0) {
        return [_array count];
    }else{
        return 0;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    if (_array.count>0) {
        CollectionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"collection" forIndexPath:indexPath];
        ClubListInfo *club = [_array objectAtIndex:indexPath.row];
        [cell.clubImgView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",IMGURL,club.ClubPic]]
                        placeholderImage:[UIImage imageNamed:CACHEIMG]
                                 options:SDWebImageRefreshCached];
        cell.clubNameLbl.text = club.ClubName;
        cell.addressLbl.text = club.Address;
        cell.priceLbl.text = [NSString stringWithFormat:@"%@元  起",club.StartWith];
        //是否显示“惠”
        NSNumber *hiddenHui = (NSNumber *)club.Recommend;
        if ([hiddenHui  isEqual: @0]) {
            cell.huiBtn.hidden = YES;
        }
        return cell;
    }else{
        return nil;
    }
    
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //选中行后去掉背景颜色
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    BusinessDescriptionVC *businessVC = [[BusinessDescriptionVC alloc]init];
    businessVC.clubInfo = [_array objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:businessVC animated:YES];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 87.0;
}

@end
