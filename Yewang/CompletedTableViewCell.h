//
//  CompletedTableViewCell.h
//  Yewang
//
//  Created by everhelp on 16/9/29.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^selectBlock)(NSInteger index);
///已完成单元格
@interface CompletedTableViewCell : UITableViewCell
///设置数据
- (void)showData:(id)data selectBlock:(selectBlock)block;

@end
