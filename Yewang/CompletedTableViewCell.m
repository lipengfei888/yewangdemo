//
//  CompletedTableViewCell.m
//  Yewang
//
//  Created by everhelp on 16/9/29.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import "CompletedTableViewCell.h"
#import "OrderListInfo.h"
@interface CompletedTableViewCell()
///商家名称
@property (nonatomic, strong) UILabel *clubNameLbl;
///订单状态
@property (nonatomic, strong) UILabel *statusLbl;
///商家图片
@property (nonatomic, strong) UIImageView *clubImgView;
///下单时间
@property (nonatomic, strong) UILabel *timeLbl;
///总价
@property (nonatomic, strong) UILabel *totalLbl;
///支付方式
@property (nonatomic, strong) UILabel *payMethodLbl;
///分割线
@property (nonatomic, strong) UILabel *line;
///分割线2
@property (nonatomic, strong) UILabel *line2;
///点击的回调
@property (nonatomic, assign) selectBlock block;

@end
@implementation CompletedTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self == [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        CGFloat basicWidth = SCREEN_WIDTH/4;
        //商家名称
        _clubNameLbl = [[UILabel alloc] init];
        _clubNameLbl.font = [UIFont systemFontOfSize:16];
        [self addSubview:_clubNameLbl];
        [self.clubNameLbl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.mas_left).offset(10);
            make.right.equalTo(self.mas_right).offset(-basicWidth);
            make.top.equalTo(self.mas_top).offset(5);
            make.height.mas_equalTo(@25);
        }];
        //订单状态
        _statusLbl = [[UILabel alloc] init];
        _statusLbl.textColor = kRGBColor;
        _statusLbl.font = [UIFont systemFontOfSize:15];
        [self addSubview:_statusLbl];
        [self.statusLbl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.mas_right).offset(-10);
            make.top.equalTo(self.mas_top).offset(5);
            make.width.mas_equalTo(basicWidth);
        }];
        //分割线
        _line = [[UILabel alloc] init];
        _line.backgroundColor = [UIColor groupTableViewBackgroundColor];
        [self addSubview:_line];
        [self.line mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.mas_left).offset(0);
            make.top.equalTo(self.clubNameLbl.mas_bottom).offset(2);
            make.right.equalTo(self.mas_right).offset(0);
            make.height.mas_equalTo(1);
        }];
        //商家图片
        _clubImgView = [[UIImageView alloc] init];
        [self addSubview:_clubImgView];
        [self.clubImgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.mas_left).offset(10);
            make.top.equalTo(self.line.mas_bottom).offset(2);
            make.right.equalTo(self.mas_right).offset(basicWidth-SCREEN_WIDTH);
            make.bottom.equalTo(self.mas_bottom).offset(-10);
        }];
        //下单时间
        _timeLbl = [[UILabel alloc] init];
        [self addSubview:_timeLbl];
        [self.timeLbl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.clubImgView.mas_right).offset(10);
            make.top.equalTo(self.clubImgView.mas_top).offset(0);
            make.right.equalTo(self.mas_right).offset(-30);
            make.height.mas_equalTo(@25);
        }];
        //总价
        _totalLbl = [[UILabel alloc]init];
        [self addSubview:_totalLbl];
        [self.totalLbl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.clubImgView.mas_right).offset(10);
            make.centerY.equalTo(self.clubImgView.mas_centerY).offset(0);
            make.right.equalTo(self.mas_right).offset(-30);
            make.height.mas_equalTo(@25);
        }];
        //支付方式
        _payMethodLbl = [[UILabel alloc] init];
        [self addSubview:_payMethodLbl];
        [self.payMethodLbl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.clubImgView.mas_right).offset(10);
            make.right.equalTo(self.mas_right).offset(-30);
            make.bottom.equalTo(self.clubImgView.mas_bottom).offset(0);
            make.height.mas_equalTo(@25);
        }];
        //分割线2
        _line2 = [[UILabel alloc] init];
        _line2.backgroundColor = [UIColor darkGrayColor];
        [self addSubview:_line2];
        [self.line2 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.mas_left).offset(0);
            make.bottom.equalTo(self.mas_bottom).offset(2);
            make.right.equalTo(self.mas_right).offset(0);
            make.height.mas_equalTo(1);
        }];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)showData:(id)data selectBlock:(selectBlock)block{
    self.block = block;
    OrderListInfo *orderInfo = data;
    self.clubNameLbl.text = orderInfo.ClubName;
    self.statusLbl.text = orderInfo.Status;
    [self.clubImgView setImage:[UIImage imageNamed:@"collected.png"]];
    NSString *dateStr = [[orderInfo.OrderTime substringFromIndex:6]substringToIndex:13];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:([dateStr longLongValue]/1000)];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSString *orderTime = [dateFormatter stringFromDate:date];
    self.timeLbl.text = [NSString stringWithFormat:@"下单时间：%@",orderTime];
    self.totalLbl.text = [NSString stringWithFormat:@"总价：￥ %.2f",[orderInfo.Total doubleValue]];
    self.payMethodLbl.text = [NSString stringWithFormat:@"方式：%@",orderInfo.PayMethod];
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
