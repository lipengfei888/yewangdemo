//
//  CouponTableViewCell.h
//  Yewang
//
//  Created by everhelp on 16/9/30.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^selectBlock)(NSInteger index);

@interface CouponTableViewCell : UITableViewCell

- (void)showData:(id)data selectBlock:(selectBlock)block;

@end
