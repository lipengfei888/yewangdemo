//
//  CouponTableViewCell.m
//  Yewang
//
//  Created by everhelp on 16/9/30.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import "CouponTableViewCell.h"
@interface CouponTableViewCell()
///背景图片
@property (nonatomic, strong) UIImageView *bgImgView;
///标题
@property (nonatomic, strong) UILabel *titleLbl;
///满减
@property (nonatomic, strong) UILabel *secondLbl;
///有效期
@property (nonatomic, strong) UILabel *usefulLbl;
///时间
@property (nonatomic, strong) UILabel *timeLbl;

@end
@implementation CouponTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self == [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        //字 宽
        CGFloat width = SCREEN_WIDTH/2;
        //背景图片
        _bgImgView = [[UIImageView alloc] init];
        [_bgImgView setImage:[UIImage imageNamed:@"coupon"]];
        [self addSubview:_bgImgView];
        [self.bgImgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self).with.insets(UIEdgeInsetsMake(5, 5, 5, 5));
        }];
        //标题
        _titleLbl = [[UILabel alloc] init];
        _titleLbl.textColor = [UIColor whiteColor];
        _titleLbl.font = [UIFont systemFontOfSize:18 weight:1];
        [_titleLbl setTextAlignment:NSTextAlignmentCenter];
        [self addSubview:_titleLbl];
        [self.titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.mas_centerX);
            make.top.equalTo(self.mas_top).offset(40);
            make.width.mas_equalTo(width);
        }];
        //满减
        _secondLbl = [[UILabel alloc] init];
        _secondLbl.textColor = [UIColor whiteColor];
        _secondLbl.font = [UIFont systemFontOfSize:18 weight:1];
        [_secondLbl setTextAlignment:NSTextAlignmentCenter];
        [self addSubview:_secondLbl];
        [self.secondLbl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.mas_centerX);
            make.top.equalTo(self.titleLbl.mas_bottom).offset(0);
            make.width.mas_equalTo(width);
        }];
        //有效期
        _usefulLbl = [[UILabel alloc] init];
        _usefulLbl.textColor = [UIColor whiteColor];
        _usefulLbl.font = [UIFont systemFontOfSize:12];
        [_usefulLbl setTextAlignment:NSTextAlignmentCenter];
        [self addSubview:_usefulLbl];
        [self.usefulLbl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.mas_centerX);
            make.top.equalTo(self.secondLbl.mas_bottom).offset(5);
            make.width.mas_equalTo(width);
        }];
        //时间
        _timeLbl = [[UILabel alloc] init];
        _timeLbl.textColor = [UIColor whiteColor];
        _timeLbl.font = [UIFont systemFontOfSize:12];
        [_timeLbl setTextAlignment:NSTextAlignmentCenter];
        [self addSubview:_timeLbl];
        [self.timeLbl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.mas_centerX);
            make.top.equalTo(self.usefulLbl.mas_bottom).offset(0);
            make.width.mas_equalTo(width);
        }];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)showData:(id)data selectBlock:(selectBlock)block{
    NSLog(@"卡券");
    _titleLbl.text = @"夜网用户专用";
    _secondLbl.text = @"满200减10";
    _usefulLbl.text = @"有效期7天";
    _timeLbl.text = @"2016年9月26日-2019年10月2日";
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
