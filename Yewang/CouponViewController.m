//
//  CouponViewController.m
//  Yewang
//
//  Created by everhelp on 16/9/30.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import "CouponViewController.h"
#import "CouponTableViewCell.h"

@interface CouponViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) UITableView *mainTable;
@end

@implementation CouponViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _mainTable = [[UITableView alloc] initWithFrame:CGRectMake(0.0, 0.0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    _mainTable.delegate = self;
    _mainTable.dataSource = self;
    [self.view addSubview:_mainTable];
    [self.mainTable registerClass:[CouponTableViewCell class] forCellReuseIdentifier:@"CouponTableViewCell"];
    _mainTable.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [_mainTable setSeparatorStyle:UITableViewCellSeparatorStyleNone];
}

#pragma mark - tableView 委托
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 3;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CouponTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CouponTableViewCell"];
    [cell showData:nil selectBlock:^(NSInteger index) {
        NSLog(@"选中卡券");
    }];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"这是第%ld张卡券",indexPath.row);
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 140;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
