//
//  DataBaseHandle.h
//  Yewang
//
//  Created by everhelp on 16/11/14.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import <Foundation/Foundation.h>
@class Combo;
@interface DataBaseHandle : NSObject
@property (nonatomic, strong) NSMutableDictionary *comboDic;

+(DataBaseHandle *)sharedDataBaseHandle;

-(void)seachAllCombo;
- (void)addComboDic:(Combo *)combo;

@end
