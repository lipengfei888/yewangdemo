//
//  DataBaseHandle.m
//  Yewang
//
//  Created by everhelp on 16/11/14.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import "DataBaseHandle.h"
static DataBaseHandle *handle = nil;
@implementation DataBaseHandle
+(DataBaseHandle *)sharedDataBaseHandle
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        handle = [DataBaseHandle new];
        handle.comboDic = [NSMutableDictionary dictionaryWithCapacity:0];
    });
    return handle;
}

@end
