//
//  DatePickerView.h
//  Yewang
//
//  Created by everhelp on 16/11/24.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DatePickerView : UIView
@property (nonatomic, strong) UIDatePicker *datePicker;

@end
