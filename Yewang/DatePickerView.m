//
//  DatePickerView.m
//  Yewang
//
//  Created by everhelp on 16/11/24.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import "DatePickerView.h"
#import "PayViewController.h"
@implementation DatePickerView
-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        _datePicker = [[UIDatePicker alloc]initWithFrame:CGRectMake(0, 0, 300, 200)];
        
        _datePicker.datePickerMode = UIDatePickerModeDateAndTime;
        NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"zh_CN"];//设置为中文
        _datePicker.locale = locale;
        //UIDatePicker最小时间    对应的是maximumDate
        
        //datePicker.minimumDate = [NSDate date];
        
        //秒  的跨越度   45  50  55  60
        
        _datePicker.minuteInterval = 5;
        
        [self addSubview:_datePicker];
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
        
        [formatter setDateFormat:@"YYYY-MM-dd,HH-mm"];
        
        NSDate *defaultDate = [NSDate dateWithTimeIntervalSince1970:[@"1460598080"doubleValue]];
        
        _datePicker.date = defaultDate;//设置UIDatePicker默认显示时间
        
        // 点击打印当前时间
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        
        button.frame = CGRectMake(20, 400, 80, 30);
        
        [button setTitle:@"sure"forState:UIControlStateNormal];
        
        [self addSubview:button];
        
        NSDateFormatter *forma = [[NSDateFormatter alloc]init];
        
        [forma setDateFormat:@"YYYY-MM-dd,HH-mm"];
        
        NSString *str = [forma stringFromDate:_datePicker.date]; //UIDatePicker显示的时间
        PayViewController *payVC = [PayViewController new];
        NSLog(@"str===%@",str);
        if (payVC.timeBlock) {
            payVC.timeBlock(str);
        }

    }
    return self;
}

@end
