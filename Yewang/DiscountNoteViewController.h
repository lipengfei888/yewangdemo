//
//  DiscountNoteViewController.h
//  Yewang
//
//  Created by everhelp on 16/8/9.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DiscountNoteViewController : UIViewController

@property (nonatomic, strong) NSString *discount;

@end
