//
//  DiscountNoteViewController.m
//  Yewang
//
//  Created by everhelp on 16/8/9.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import "DiscountNoteViewController.h"

@interface DiscountNoteViewController ()

@end

@implementation DiscountNoteViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithRed:244.0/255 green:244.0/255 blue:244.0/255 alpha:1.0];
    NSLog(@"%@",_discount);
    UILabel *topLbl = [[UILabel alloc]initWithFrame:CGRectMake(0.0, 74.0, SCREEN_WIDTH, 40)];
    topLbl.text = @"    规则说明";
    topLbl.font = [UIFont boldSystemFontOfSize:15];
    topLbl.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:topLbl];
    UILabel *firstLbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 115, SCREEN_WIDTH, 30)];
    firstLbl.text = @"  ·   优惠买单仅限到店支付，请勿提前购买";
    [self.view addSubview:firstLbl];
    UILabel *secondLbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 145, SCREEN_WIDTH, 30)];
    secondLbl.text = @"  ·   以下优惠是否能与店铺优惠同享，请您向商户咨询";
    [self.view addSubview:secondLbl];
    UILabel *thirdLbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 175, SCREEN_WIDTH, 30)];
    thirdLbl.text = @"  ·   如需发票，请您在消费时向商户咨询";
    [self.view addSubview:thirdLbl];
    
    NSArray *lblArray = @[firstLbl,secondLbl,thirdLbl];
    for (UILabel *lbl in lblArray) {
        lbl.font = [UIFont boldSystemFontOfSize:13];
        lbl.textColor = [UIColor grayColor];
        lbl.backgroundColor = [UIColor whiteColor];
    }
    UILabel *secondTopLbl = [[UILabel alloc]initWithFrame:CGRectMake(0.0, 215.0, SCREEN_WIDTH, 40)];
    secondTopLbl.text = @"    买单优惠";
    secondTopLbl.font = [UIFont boldSystemFontOfSize:15];
    secondTopLbl.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:secondTopLbl];
    UILabel *fourthLbl = [[UILabel alloc]initWithFrame:CGRectMake(0.0, 256.0, SCREEN_WIDTH, 50)];
    NSInteger discountLength = _discount.length;
    NSString *fourthStr = [NSString stringWithFormat:@"    %@折 可享",_discount];
    NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:fourthStr];
    [str addAttribute:NSForegroundColorAttributeName value:[UIColor orangeColor] range:NSMakeRange(4,discountLength)];
    [str addAttribute:NSForegroundColorAttributeName value:[UIColor orangeColor] range:NSMakeRange(4+discountLength,1)];
    [str addAttribute:NSForegroundColorAttributeName value:[UIColor grayColor] range:NSMakeRange(6+discountLength,2)];
    [str addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Arial-BoldItalicMT" size:26.0] range:NSMakeRange(4, discountLength)];
    [str addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeue-Bold" size:16.0] range:NSMakeRange(4+discountLength, 1)];
    [str addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Courier-oblique" size:13.0] range:NSMakeRange(6+discountLength, 2)];
    fourthLbl.attributedText = str;
    fourthLbl.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:fourthLbl];
     
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
