//
//  FMDBHepler.h
//  ArchiveDemo
//
//  Created by XienaShen on 16/9/3.
//  Copyright © 2016年 XienaShen. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "ClubListInfo.h"

@interface FMDBHepler : NSObject

///初始化帮助类
+(FMDBHepler *)sharedInstance;

///从数据库中获取数据
-(NSArray *)stepsFromSqlite;

///将数据写入数据库(进行缓存)
-(void)saveSteps:(ClubListInfo *)steps;

@end
