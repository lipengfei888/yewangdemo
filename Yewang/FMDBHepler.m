//
//  FMDBHepler.m
//  ArchiveDemo
//
//  Created by XienaShen on 16/9/3.
//  Copyright © 2016年 XienaShen. All rights reserved.
//

#import "FMDBHepler.h"
#import "FMDB.h"

#define DB @"dataSource.rdb"

@interface FMDBHepler()

@property (nonatomic,strong) FMDatabase *dataBase;

@end

static FMDBHepler *shareObj = nil;

@implementation FMDBHepler

///初始化帮助类
+(FMDBHepler *)sharedInstance{
    if (shareObj==nil) {
        shareObj = [[FMDBHepler alloc]init];
    }
    return shareObj;
}

#pragma mark 初始化数据库
-(instancetype)init {
    if (self = [super init]) {
        //得到数据库本地路径
        NSString *getBundlePath = [[NSBundle mainBundle] pathForResource:DB ofType:nil];
        //得到沙盒目录路径
        NSString *getDoucumentPath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)objectAtIndex:0] stringByAppendingPathComponent:DB];
        if (getDoucumentPath) {
            //创建文件流控制器
            NSFileManager *fm = [NSFileManager defaultManager];
            [fm copyItemAtPath:getBundlePath toPath:getDoucumentPath error:nil];
            NSLog(@"copy success");
        }
        self.dataBase = [FMDatabase databaseWithPath:getDoucumentPath];
    }
    return self;
}

///从数据库中获取数据
-(NSArray *)stepsFromSqlite{
    NSString *sql = @"SELECT * FROM t_step";
    NSMutableArray *steps = [NSMutableArray array];
   //打开数据库
    if ([self.dataBase open]) {
        //操作
        FMResultSet *set = [self.dataBase executeQuery:sql];
        while (set.next) {
            NSData *data = [set objectForColumnName:@"content"];
            ClubListInfo *step = [NSKeyedUnarchiver unarchiveObjectWithData:data];
            [steps addObject:step];
        }
        //关闭数据库
        [self.dataBase close];
    }
    
    return steps;
}

///将数据写入数据库(进行缓存)
-(void)saveSteps:(ClubListInfo *)steps{
    //打开数据库
    if ([self.dataBase open]) {
        //操作
        NSData *data = [NSKeyedArchiver archivedDataWithRootObject:steps];
        [self.dataBase executeUpdateWithFormat:@"INSERT INTO t_step(content) VALUES (%@);", data];
        //关闭数据库
        [self.dataBase close];
    }
    
    
}

@end
