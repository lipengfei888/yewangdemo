//
//  FeedBackViewController.m
//  Yewang
//
//  Created by everhelp on 16/4/15.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import "FeedBackViewController.h"

@interface FeedBackViewController ()
@property (weak, nonatomic) IBOutlet UITextView *contentTxt;

@end

@implementation FeedBackViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
- (void)viewWillAppear:(BOOL)animated{
    self.navigationItem.title = @"反馈信息";
    self.tabBarController.tabBar.hidden = YES;
    self.navigationController.navigationBar.hidden = NO;
    [super viewWillAppear:animated];
}
- (IBAction)clickSubmit:(id)sender {
    
    //判断输入框是否为空字符串
    if ([[self.contentTxt.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]length] == 0) {
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:@"内容不能为空，请重新输入" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"好的" style:UIAlertActionStyleCancel handler:nil];
        [alert addAction:cancelAction];
        [self presentViewController:alert animated:YES completion:nil];
        
    } else {
        
        if ([UserHeader userIsLogin]) {
            NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
            NSString *userID = [userDefault valueForKey:@"ID"];
            NSString *userName = [userDefault valueForKey:@"UserName"];
            NSString *Idstr = [CheckString createCUID];
            NSString *strUrl = [NSString stringWithFormat:@"%@Common/AddFeedback",BASICURL];
            NSDictionary *dic = @{@"ID":Idstr,@"UserID":userID,@"FeedbackName":userName,@"Content":self.contentTxt.text};
            
            [BingNetWorkConnection connectionWithUrl:strUrl type:BINGO_REQUEST_POST params:dic FinishBlock:^(id responseObject) {
                NSString *string = [[NSString alloc]initWithData:responseObject encoding:NSUTF8StringEncoding];
                NSLog(@"提交反馈信息：%@",string);
                
                if ([string isEqualToString:@"[订单成功]"]) {
                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:@"感谢您的反馈信息" preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"好的" style:UIAlertActionStyleCancel handler:nil];
                    [alert addAction:cancelAction];
                    [self presentViewController:alert animated:YES completion:nil];
                    self.contentTxt.text = @"";
                }else{
                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:@"提交失败，请稍后再试" preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"好的" style:UIAlertActionStyleCancel handler:nil];
                    [alert addAction:cancelAction];
                    [self presentViewController:alert animated:YES completion:nil];
                }
                
            } FailedBlock:^(NSError *error) {
                NSLog(@"提交反馈error:%@",error);
            }];
        }else{
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:@"请先登录" preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"好的" style:UIAlertActionStyleCancel handler:nil];
            [alert addAction:cancelAction];
            [self presentViewController:alert animated:YES completion:nil];
        }
        
    }
    
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [_contentTxt resignFirstResponder];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
