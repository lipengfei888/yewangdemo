//
//  FilterImgView.h
//  Yewang
//
//  Created by everhelp on 16/10/31.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FilterImgView : UIView

///图片滚动视图
@property (nonatomic, strong) UIScrollView *scrollView;

///初始化方法
- (instancetype)initWithFrame:(CGRect)frame andImgArray:(NSArray *)imgArray;

@end
