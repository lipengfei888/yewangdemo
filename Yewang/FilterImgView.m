//
//  FilterImgView.m
//  Yewang
//
//  Created by everhelp on 16/10/31.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#define IMGWidth self.bounds.size.width/3

#import "FilterImgView.h"

@implementation FilterImgView

- (instancetype)initWithFrame:(CGRect)frame andImgArray:(NSArray *)imgArray{
    if (self == [super initWithFrame:frame]) {
        
        [self initWithUIAndArray:imgArray];
    }
    return self;
}

- (void)initWithUIAndArray:(NSArray *)array{
    _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height)];
    _scrollView.contentSize = CGSizeMake(IMGWidth*3, self.bounds.size.height);
    [self addSubview:_scrollView];
    for (int i = 0; i < array.count; i ++) {
        NSString *imgName = array[i];
        UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(IMGWidth*i, 0, IMGWidth, self.bounds.size.height)];
        [imgView setImage:[UIImage imageNamed:imgName]];
        [_scrollView addSubview:imgView];
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
