//
//  FilterViewController.m
//  Yewang
//
//  Created by everhelp on 16/9/21.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import "FilterViewController.h"
#import "BusinessDescriptionVC.h"
#import "ClassFilterTableViewCell.h"
#import "ResultTableViewCell.h"
#import "ClubListInfo.h"
#import "ClassSelectCell.h"

@interface FilterViewController ()<UITableViewDelegate,UITableViewDataSource>{
    BOOL _update;
}
///主视图
@property (weak, nonatomic) IBOutlet UITableView *mainTableView;
///数据源
@property (nonatomic, strong) NSArray *array;
///筛选条件数组
@property (nonatomic, strong) NSMutableArray *selectArray;
///筛选结果数组
@property (nonatomic, strong) NSMutableArray *clubArray;
@property (nonatomic, strong) NSIndexPath *selectIndexPath;

@end

@implementation FilterViewController
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.tabBarController.tabBar.hidden = NO;
    self.navigationController.navigationBar.hidden = NO;
    self.navigationController.navigationBar.barTintColor = [UIColor whiteColor];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initWithFrame];
    [self initWithData];
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBar.barTintColor = kRGBColor;
}

#pragma mark - 初始化界面
- (void)initWithFrame{
    
    //设置navigationtitle
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 54)];
    titleLabel.font = [UIFont boldSystemFontOfSize:20];
    titleLabel.textColor = kRGBColor;
    titleLabel.backgroundColor = [UIColor whiteColor];
    titleLabel.textAlignment = NSTextAlignmentLeft;
    titleLabel.text = @"更懂你的筛选（多选）";
    self.navigationItem.titleView = titleLabel;
    
    //设置表格委托
    self.mainTableView.delegate = self;
    self.mainTableView.dataSource = self;
    self.mainTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    ///注册单元格
    [self.mainTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell"];
    [self.mainTableView registerClass:[ResultTableViewCell class] forCellReuseIdentifier:@"ResultTableViewCell"];
    
    UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 80, 40)];
    [btn setTitle:@"开始" forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(clickStart:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:btn];
}

#pragma mark - 初始化数据
- (void)initWithData{
    _update = NO;
    self.selectArray = [NSMutableArray new];
    self.clubArray = [NSMutableArray array];
    if (_update == NO) {
//        _array = @[@{@"className":@"分类",@"dataSource":@[@"清吧",@"嗨吧",@"商务KTV"]},@{@"className":@"心情",@"dataSource":@[@"开心",@"愤怒",@"郁闷",@"忧伤"]},@{@"className":@"标签",@"dataSource":@[@"足球",@"桌球",@"电影",@"萌宠",@"西餐",@"特色啤酒",@"民谣",@"乐队",@"热舞",@"女神",@"蒸汽朋克",@"小资"]}];
        [self dataOfFilterArray];
    }
    
    [self dataOfClubArray];
    
    [self.mainTableView reloadData];
}
- (void)dataOfFilterArray{
    NSString *strUrl = [NSString stringWithFormat:@"%@Club/GetFilterElement",BASICURL];
    [BingNetWorkConnection connectionWithUrl:strUrl type:BINGO_REQUEST_GET params:nil FinishBlock:^(id responseObject) {
        
        NSArray *array = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:nil];
        NSMutableArray *categroyArr = [NSMutableArray new];
        NSMutableArray *moodArr = [NSMutableArray new];
        NSMutableArray *labelArr = [NSMutableArray new];
        for (NSDictionary *dict in array) {
            NSString *className = dict[@"ElementType"];
            if ([className isEqualToString:@"Category"]) {
                [categroyArr addObject:dict[@"FilterElementIcon"]];
            } else if ([className isEqualToString:@"Mood"]) {
                [moodArr addObject:dict[@"FilterElementIcon"]];
            } else if ([className isEqualToString:@"Label"]) {
                [labelArr addObject:dict[@"FilterElementIcon"]];
            }
        }
        _array = @[@{@"className":@"分类",@"dataSource":categroyArr},@{@"className":@"心情",@"dataSource":moodArr},@{@"className":@"标签",@"dataSource":labelArr}];
        [self.mainTableView reloadData];
    } FailedBlock:^(NSError *error) {
        NSLog(@"filter error : %@",error.userInfo);
    }];
}
- (void)dataOfClubArray{
    NSString *strUrl = [NSString stringWithFormat:@"%@Club/GetRecommendClubList",BASICURL];
    [BingNetWorkConnection connectionWithUrl:strUrl type:BINGO_REQUEST_GET params:nil FinishBlock:^(id responseObject) {
        
        NSArray *arr = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:nil];
        for (int i = 0; i < arr.count; i ++) {
            NSDictionary *dic = [arr objectAtIndex:i];
            [_clubArray addObject:dic];
        }
        [self.mainTableView reloadData];
    } FailedBlock:^(NSError *error) {
        NSLog(@"searchpage error : %@",error.userInfo);
    }];
}

#pragma mark - 点击开始按钮
- (void)clickStart:(UIButton *)button{
    if (button.selected) {
        _update = YES;
        [self actionOfClickStart];
    } else {
        _update = NO;
    }
    button.selected = !button.selected;
    [self.mainTableView reloadData];
}

- (void)actionOfClickStart{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *userID = [userDefault objectForKey:@"ID"];
    NSString *strUrl = [NSString stringWithFormat:@"%@Club/AddFilterCollect",BASICURL];
    NSString *ID = [CheckString createCUID];
    NSDictionary *paramDict = @{@"ID":ID,@"UserID":userID,@"UserNumber":[userDefault objectForKey:@"UserName"],@"Category":@"主题吧",@"Mood":@"哀伤",@"Label":@"桌球",@"FilterTime":@"2016-10-27 14:30"};
    [BingNetWorkConnection connectionWithUrl:strUrl type:BINGO_REQUEST_POST params:paramDict FinishBlock:^(id responseObject) {
        NSLog(@"%@",[[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding]);
    } FailedBlock:^(NSError *error) {
        NSLog(@"filterCollect error :%@",error);
    }];
}

#pragma mark - UITableViewDelegate,UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if (_update) {
        return 2;
    } else {
        return 4;
    }
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (_update) {
        switch (section) {
            case 0:
                return self.selectArray.count>0?1:0;
                break;
            case 1:
                return _clubArray.count;
                break;
            default:
                break;
        }
    } else {
        switch (section) {
            case 0:
            case 1:
            case 2:
                return 1;
                break;
            case 3:
                return _clubArray.count;
                break;
            default:
                break;
        }
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (_update) {
        switch (indexPath.section) {
            case 0:
            {
                static NSString *identifier = @"ClassSelectCell";
                ClassSelectCell * classCell = (ClassSelectCell *)[tableView dequeueReusableCellWithIdentifier:identifier];
                if (classCell == nil) {
                    classCell = [[ClassSelectCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
                }
                [classCell showData:self.selectArray];
                return classCell;
            }
                break;
            case 1:
            {
                ResultTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ResultTableViewCell" forIndexPath:indexPath];
                [cell showData:_clubArray[indexPath.row] selectBlock:^(NSInteger index) {
                    NSLog(@"点击结果按钮： %li",index);
                }];
                return cell;
            }
                break;
            default:
                break;
        }
    } else {
        switch (indexPath.section) {
            case 0:
            {
                static NSString *identifier = @"ClassCollectionViewCell";
                ClassFilterTableViewCell *classCell = (ClassFilterTableViewCell *)[tableView dequeueReusableCellWithIdentifier:identifier];
                if (classCell == nil) {
                    classCell = [[ClassFilterTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
                }
                [classCell showData:_array[indexPath.section] selectBlock:^(NSInteger index) {
                    NSLog(@"点击分类：%li",index);
                    NSArray *array = _array[indexPath.section][@"dataSource"];
                    [self.selectArray addObject:array[index]];
                }];
                return classCell;
            }
                break;
            case 1:
            {
                static NSString *identifier = @"ClassCollectionViewCell";
                ClassFilterTableViewCell *classCell = (ClassFilterTableViewCell *)[tableView dequeueReusableCellWithIdentifier:identifier];
                if (classCell == nil) {
                    classCell = [[ClassFilterTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
                }
                [classCell showData:_array[indexPath.section] selectBlock:^(NSInteger index) {
                    NSLog(@"点击心情：%li",index);
                    NSArray *array = _array[indexPath.section][@"dataSource"];
                    [self.selectArray addObject:array[index]];
                }];
                return classCell;
            }
                break;
            case 2:
            {
                static NSString *identifier = @"ClassCollectionViewCell";
                ClassFilterTableViewCell *classCell = (ClassFilterTableViewCell *)[tableView dequeueReusableCellWithIdentifier:identifier];
                if (classCell == nil) {
                    classCell = [[ClassFilterTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
                }
                [classCell showData:_array[indexPath.section] selectBlock:^(NSInteger index) {
                    NSLog(@"点击标签：%li",index);
                    NSArray *array = _array[indexPath.section][@"dataSource"];
                    [self.selectArray addObject:array[index]];
                }];
                return classCell;
            }
                break;
            case 3:
            {
                ResultTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ResultTableViewCell" forIndexPath:indexPath];
                [cell showData:_clubArray[indexPath.row] selectBlock:^(NSInteger index) {
                    NSLog(@"点击结果按钮： %li",index);
                }];
                return cell;
            }
                break;
                
            default:
                break;
        }
    }
    
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 3) {
        //选中行后去掉背景颜色
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        NSLog(@"这个是点击结果行的效果 === %li",indexPath.row);
        BusinessDescriptionVC *vc = [[BusinessDescriptionVC alloc]init];
        vc.clubInfo = [ClubListInfo mj_objectWithKeyValues:[_clubArray objectAtIndex:indexPath.row]];
        [self.navigationController pushViewController:vc animated:YES];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.section) {
        case 0:
            return [ClassFilterTableViewCell getHeight:self.array[indexPath.section]];
            break;
        case 1:
            return [ClassFilterTableViewCell getHeight:self.array[indexPath.section]];
            break;
        case 2:
            return [ClassFilterTableViewCell getHeight:self.array[indexPath.section]];
            break;
        case 3:
            return 100;
            break;
        default:
            break;
    }
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    switch (section) {
        case 0:
        case 1:
        case 2:
            return 0;
            break;
        case 3:
            return 10;
        default:
            break;
    }
    return 0;
}

#pragma mark - 获取 NSIndexPath
- (NSIndexPath *)indexPathForView:(UIView *)view andTableView:(UITableView *)tableView{
    UIView *parentView = [view superview];
    while (![parentView isKindOfClass:[UITableViewCell class]] && parentView != nil) {
        parentView = parentView.superview;
    }
    return [tableView indexPathForCell:(UITableViewCell *)parentView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
