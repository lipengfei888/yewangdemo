//
//  ForgetPwdViewController.m
//  Yewang
//
//  Created by everhelp on 16/4/18.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import "ForgetPwdViewController.h"

@interface ForgetPwdViewController (){
    NSTimer *counterDownTimer;
    int freezeCounter;
}

@property (weak, nonatomic) IBOutlet UITextField *telTxt;
@property (weak, nonatomic) IBOutlet UITextField *identifyTxt;
@property (weak, nonatomic) IBOutlet UITextField *pwdTxt;
@property (weak, nonatomic) IBOutlet UITextField *checkTxt;
@property (weak, nonatomic) IBOutlet UIButton *identifyBtn;
@property (weak, nonatomic) IBOutlet UIButton *submitBtn;

@end

@implementation ForgetPwdViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
- (void)viewWillAppear:(BOOL)animated{
    self.navigationItem.title = @"忘记密码";
    self.tabBarController.tabBar.hidden = YES;
    self.identifyBtn.layer.cornerRadius = 8.0;
    self.identifyBtn.layer.masksToBounds = YES;
    self.submitBtn.layer.cornerRadius = 8.0;
    self.submitBtn.layer.masksToBounds = YES;
    [super viewWillAppear:animated];
}
- (IBAction)clickIdentifyBtn:(id)sender {
    if (![CheckString checkPhoneNumInput:_telTxt.text]){
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:@"电话号码不能为空" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"好的" style:UIAlertActionStyleCancel handler:nil];
        [alert addAction:cancelAction];
        [self presentViewController:alert animated:YES completion:nil];
    }else{
        
        [AVUser requestPasswordResetWithPhoneNumber:_telTxt.text block:^(BOOL succeeded, NSError *error) {
            if (succeeded) {
                [self freezeMoreRequest];
            } else {
                NSLog(@"忘记密码：%@",error);
            }
        }];
    }
}
- (IBAction)clickSubmitBtn:(id)sender {
    if ([self checkInputMsg]) {
        [AVUser resetPasswordWithSmsCode:_identifyTxt.text newPassword:_pwdTxt.text block:^(BOOL succeeded, NSError *error) {
            if (succeeded) {
                [self resetPassword];
            } else {
                
                [CommonUtils displayError:error];
            }
        }];
        
    }
}

- (void)resetPassword{
    
    NSString *strUrl = [NSString stringWithFormat:@"%@User/BackUserMobile",BASICURL];
    NSDictionary *dic = @{@"Key":@"Mobile",@"Value":_telTxt.text};
    [BingNetWorkConnection connectionWithUrl:strUrl type:BINGO_REQUEST_POST params:dic FinishBlock:^(id responseObject) {
        
        NSString *string = [[NSString alloc]initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSLog(@"str====%@",string);
        if ([string isEqualToString:@"无此手机号码"]) {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"提示" message:@"该手机号码未注册" preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"好的" style:UIAlertActionStyleCancel handler:nil];
            [alertController addAction:alertAction];
            [self presentViewController:alertController animated:YES completion:nil];
        } else {
            NSString *userID = [string stringByReplacingOccurrencesOfString:@"\"" withString:@""];
            NSString *strChangeUrl = [NSString stringWithFormat:@"%@User/GetChangePassword?ID=%@&Password=%@",BASICURL,userID,self.checkTxt.text];
            NSLog(@"%@",strChangeUrl);
            [BingNetWorkConnection connectionWithUrl:strChangeUrl type:BINGO_REQUEST_GET params:nil FinishBlock:^(id responseObject) {
                
                NSString *string2 = [[NSString alloc]initWithData:responseObject encoding:NSUTF8StringEncoding];
                NSLog(@"str====%@",string2);
                if ([string2 isEqualToString:@"修改成功"]) {
                    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"提示" message:@"密码修改成功" preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                        [self.navigationController popViewControllerAnimated:YES];
                    }];
                    [alertController addAction:alertAction];
                    [self presentViewController:alertController animated:YES completion:nil];
                    
                    
                }else{
                    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"提示" message:@"密码修改失败" preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"好的" style:UIAlertActionStyleCancel handler:nil];
                    [alertController addAction:alertAction];
                    [self presentViewController:alertController animated:YES completion:nil];
                }
                
                
            } FailedBlock:^(NSError *error) {
                NSLog(@"1%@",error);
            }];
        }
        
        
    } FailedBlock:^(NSError *error) {
        NSLog(@"2%@",error);
    }];
}

- (void)freezeMoreRequest {
    // 一分钟内禁止再次发送
    [self.identifyBtn setEnabled:NO];
    freezeCounter = 60;
    counterDownTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(countDownRequestTimer) userInfo:nil repeats:YES];
    [counterDownTimer fire];
    
    UIAlertController *alertControl = [UIAlertController alertControllerWithTitle:@"验证码已发送" message:@"验证码已发送到你请求的手机号码。如果没有收到，可以在一分钟后尝试重新发送。" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleCancel handler:nil];
    [alertControl addAction:action];
    [self presentViewController:alertControl animated:YES completion:nil];
}

- (void)countDownRequestTimer {
    static NSString *counterFormat = @"%d";
    --freezeCounter;
    if (freezeCounter<= 0) {
        [counterDownTimer invalidate];
        counterDownTimer = nil;
        [self.identifyBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
        [self.identifyBtn setEnabled:YES];
    } else {
        [self.identifyBtn setTitle:[NSString stringWithFormat:counterFormat, freezeCounter] forState:UIControlStateNormal];
    }
}

- (BOOL)checkInputMsg{
    BOOL flag = YES;
    
    NSString *newPassword = self.pwdTxt.text;
    NSString *checkPassword = self. checkTxt.text;
    NSString *smsCode = self.identifyTxt.text;
    if ([[newPassword stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]length] == 0){
        flag = NO;
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:@"密码不能为空，请输入密码" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleCancel handler:nil];
        [alert addAction:cancelAction];
        [self presentViewController:alert animated:YES completion:nil];
    }
    if (![newPassword isEqualToString:checkPassword]){
        
        flag = NO;
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:@"两次密码不一致，请重新输入" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleCancel handler:nil];
        [alert addAction:cancelAction];
        [self presentViewController:alert animated:YES completion:nil];
    }
    
    if (newPassword.length < 6) {
        
        flag = NO;
        
        NSMutableDictionary* details = [NSMutableDictionary dictionary];
        [details setValue:@"密码长度必须在 6 位（含）以上。" forKey:NSLocalizedDescriptionKey];
        NSError *error = [NSError errorWithDomain:@"LeanSMSDemo" code:1024 userInfo:details];
        [CommonUtils displayError:error];
        
    }
    if (smsCode.length < 6) {
        
        flag = NO;
        
        NSMutableDictionary* details = [NSMutableDictionary dictionary];
        [details setValue:@"验证码无效。" forKey:NSLocalizedDescriptionKey];
        NSError *error = [NSError errorWithDomain:@"LeanSMSDemo" code:1024 userInfo:details];
        [CommonUtils displayError:error];
        
    }
    return flag;
}
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [_pwdTxt resignFirstResponder];
    [_checkTxt resignFirstResponder];
    [_telTxt resignFirstResponder];
    [_identifyTxt resignFirstResponder];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
