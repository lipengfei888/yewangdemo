//
//  GroupWineTableViewCell.h
//  Yewang
//
//  Created by everhelp on 16/10/13.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GroupWineTableViewCell : UITableViewCell
@property (nonatomic, strong) UIButton *groupNameBtn;
@end
