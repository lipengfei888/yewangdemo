//
//  GroupWineTableViewCell.m
//  Yewang
//
//  Created by everhelp on 16/10/13.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import "GroupWineTableViewCell.h"

@implementation GroupWineTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self == [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        _groupNameBtn = [UIButton new];
        [_groupNameBtn setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
        [self addSubview:_groupNameBtn];
        [_groupNameBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self).insets(UIEdgeInsetsMake(0, 0, 0, 0));
        }];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
