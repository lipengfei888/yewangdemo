//
//  HomepageViewController.m
//  Yewang
//
//  Created by everhelp on 16/4/11.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import "HomepageViewController.h"
#import "SearchTableViewController.h"
#import "ClubTableViewCell.h"
#import "ClubListInfo.h"
#import "InformsViewController.h"
#import "JXBAdPageView.h"
#import "AddressPickerDemo.h"
#import "LoginViewController.h"
#import "BusinessDescriptionVC.h"
#import "IntroduceViewController.h"
#import "UIAlertController+WarnMessage.h"
#import "FMDBHepler.h"
#import <CoreLocation/CoreLocation.h>
#define BIGPICURL @"http://res.nightnet.cn/resource/Web/active/pic/"
#define PICWEBURL @"http://res.nightnet.cn/resource/Web/active/web/"
#define TOPHEIGHT SCREEN_HEIGHT/4.0+60

#import "BusinessDescriptionViewController.h"

NSInteger LMCOUNT = 0;///未开启权限+1，开启后归零

@interface HomepageViewController ()<UISearchBarDelegate,UITableViewDataSource,UITableViewDelegate,JXBAdPageViewDelegate,CLLocationManagerDelegate>{
    //Frame
    UISearchBar *customSearchBar;
    NSMutableArray *_imgArray;
    NSInteger _index;
    UIPageControl *_pageC;
    UIButton *leftBtn;
    UIView *topView;
    
    CLLocationManager *_locationManager;//定义Manager
    BOOL isLocationOpen;
    NSInteger tablePage;
}
@property (weak, nonatomic) IBOutlet UITableView *tableV;
@property (nonatomic, strong) NSString *locationNow;
@property (nonatomic,strong) JXBAdPageView *adView;

@property (nonatomic, strong) NSMutableArray *array;
@property (nonatomic, strong) FMDBHepler *helperManager;

@end

@implementation HomepageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    tablePage = 1;
    isLocationOpen = YES;
    if (![BingNetWorkConnection NetWorkIsOK]) {
        NSString *title = @"已为\"夜网\"关闭蜂窝移动数据";
        NSString *message =@"您可以在\"设置\"中为此应用程序打开蜂窝移动数据";
        [UIAlertController showUpdateSet:title andMessage:message withController:self];
    }
    [self initWithFrame];
    [self initWithHomeData];
    [self initMyMJRefresh];
    [self startLocating];
    
    
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.tabBarController.tabBar.hidden = NO;
    self.navigationController.navigationBar.hidden = YES;
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc addObserver:self selector:@selector(handleAddressChange:) name:@"addressPicker" object:nil];
    [nc addObserver:self selector:@selector(getLocationStatus:) name:@"getLocationStatus" object:nil];
}

-(void)handleAddressChange:(NSNotification *)sender{
    [leftBtn setTitle:[sender.userInfo objectForKey:@"City"] forState:UIControlStateNormal];
    _locationNow = [sender.userInfo objectForKey:@"City"];
    [self initWithHomeData];
}

- (void)getLocationStatus:(NSNotification *)sender{
//    NSString *status = [sender.userInfo objectForKey:@"status"];
//    NSLog(@"这个时候的定位权限状态:%@",status);
//    if ([status isEqualToString:@"open"]) {
//        [self startLocating];
//        isLocationOpen = YES;
//        LMCOUNT = 0;
//        [self.tableV reloadData];
//    } else {
//        isLocationOpen = NO;
//        LMCOUNT ++;
//        if (LMCOUNT == 1) {
//            NSString *title = @"已为\"夜网\"关闭定位服务";
//            NSString *message =@"您可以在\"设置\"中为此应用程序打开定位服务";
//            [UIAlertController showUpdateSet:title andMessage:message withController:self];
//            [self.tableV reloadData];
//            return;
//        }
//        
//    }
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
}

#pragma mark - 加载顶部图片
- (void)returnImgUrl{
    if ([BingNetWorkConnection NetWorkIsOK]) {
        //会所列表
        NSString *strUrl = [NSString stringWithFormat:@"%@Club/BigPicLink",BASICURL];
        _imgArray = [NSMutableArray new];
        [SVProgressHUD showWithStatus:@"加载中，请稍候..."];
        [BingNetWorkConnection connectionWithUrl:strUrl type:BINGO_REQUEST_POST params:nil FinishBlock:^(id responseObject) {
            
            [SVProgressHUD dismiss];
            if ([[NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:nil] isKindOfClass:[NSDictionary class]]) {
                
                [SVProgressHUD showErrorWithStatus:@"服务器维护中"];
                
            } else  {
                NSArray *arr = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:nil];
                NSMutableArray *webArr = [NSMutableArray arrayWithCapacity:0];
                for (NSDictionary *dic in arr) {
                    NSString *str = [NSString stringWithFormat:@"%@%@",BIGPICURL,[dic objectForKey:@"BigPic1"]];
                    NSString *str2 = [NSString stringWithFormat:@"%@%@",PICWEBURL,[dic objectForKey:@"HtmlLink"]];
                    [_imgArray addObject:str];
                    [webArr addObject:str2];
                }
                _adView.bWebImage = YES;
                [_adView startAdsWithBlock:_imgArray block:^(NSInteger clickIndex) {
                    NSLog(@"滚动图片下标：%ld",(long)clickIndex);
                    IntroduceViewController *introduce = [[IntroduceViewController alloc]init];
                    introduce.strUrl = [webArr objectAtIndex:clickIndex];
                    [self.navigationController pushViewController:introduce animated:YES];
                }];
            }
        } FailedBlock:^(NSError *error) {
            NSLog(@"主页面error：%@",error);
            [SVProgressHUD dismiss];
        }];
    } else {
        [_adView startAdsWithBlock:@[@"page1",@"page2"] block:^(NSInteger clickIndex) {
            NSLog(@"滚动图片下标======%ld",(long)clickIndex);
        }];
    }
}

#pragma mark - 加载数据
- (void)initWithHomeData{http://api.nightnet.cn/api/Club/GetRecommendClubList
    self.array = [NSMutableArray new];
    if ([BingNetWorkConnection NetWorkIsOK]) {
        //会所列表
        NSString *strUrl = [NSString stringWithFormat:@"%@Club/GetRecommendClubList",BASICURL];
        [SVProgressHUD showWithStatus:@"加载中，请稍候..."];
        [BingNetWorkConnection connectionWithUrl:strUrl type:BINGO_REQUEST_GET params:nil FinishBlock:^(id responseObject) {
            [SVProgressHUD dismiss];
            [self endRefresh];
            if ([[NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:nil] isKindOfClass:[NSDictionary class]]) {
                [SVProgressHUD showErrorWithStatus:@"服务器维护中"];
                self.helperManager = [[FMDBHepler sharedInstance] init];
                NSArray *arr = [_helperManager stepsFromSqlite];
                if (arr.count>0) {
                    [self loadSupplierIsRecommend:arr];
                }
            } else  {
                NSArray *arr = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:nil];
                if (_array.count > 0) {
                    [_array removeAllObjects];
                }
                if (tablePage*10>=arr.count) {
                    for (int j = 0; j<arr.count; j++) {
                        
                        NSDictionary *dic = [arr objectAtIndex:j];
                        ClubListInfo *clubInfo = [ClubListInfo mj_objectWithKeyValues:dic];
                        if ([clubInfo.City isEqualToString:leftBtn.titleLabel.text]) {
                            [_array addObject:clubInfo];
                        }
                    }
                    [self.tableV.mj_footer endRefreshingWithNoMoreData];
                }else{
                    for (int j = 0; j<tablePage*10; j++) {
                        
                        NSDictionary *dic = [arr objectAtIndex:j];
                        ClubListInfo *clubInfo = [ClubListInfo mj_objectWithKeyValues:dic];
                        if ([clubInfo.City isEqualToString:leftBtn.titleLabel.text]) {
                            [_array addObject:clubInfo];
                        }
                    }
                    [self endRefresh];
                }
                //刷新表格
                [self.tableV reloadData];
            }
            
        } FailedBlock:^(NSError *error) {
            NSLog(@"主页面error：%@",error);
            [SVProgressHUD dismiss];
        }];
    } else {
        self.helperManager = [[FMDBHepler sharedInstance] init];
        NSArray *arr = [_helperManager stepsFromSqlite];
        if (arr.count>0) {
            [self loadSupplierIsRecommend:arr];
        }
    }
}

#pragma mark - 从后台加载被推荐的商家
- (void)loadSupplierIsRecommend:(NSArray *)array{
    NSMutableArray *arr = [NSMutableArray new];
    for (NSDictionary *dict in array) {
        ClubListInfo *clubInfo = [ClubListInfo mj_objectWithKeyValues:dict];
        NSString *isRecommend = [NSString stringWithFormat:@"%@",clubInfo.Recommend];
        if ([isRecommend isEqualToString:@"1"]) {
            [arr addObject:clubInfo];
        }
    }
    if (_array.count > 0) {
        [_array removeAllObjects];
    }
    if (tablePage*10>=arr.count) {
        for (int j = 0; j<arr.count; j++) {
            
            NSDictionary *dic = [arr objectAtIndex:j];
            ClubListInfo *clubInfo = [ClubListInfo mj_objectWithKeyValues:dic];
            if ([clubInfo.City isEqualToString:leftBtn.titleLabel.text]) {
                [_array addObject:clubInfo];
            }
        }
        [self.tableV.mj_footer endRefreshingWithNoMoreData];
    }else{
        for (int j = 0; j<tablePage*10; j++) {
            
            NSDictionary *dic = [arr objectAtIndex:j];
            ClubListInfo *clubInfo = [ClubListInfo mj_objectWithKeyValues:dic];
            if ([clubInfo.City isEqualToString:leftBtn.titleLabel.text]) {
                [_array addObject:clubInfo];
            }
        }
    }
    [self.tableV reloadData];
}
#pragma mark - codeFrame
- (void)initWithFrame{
    //滚动视图图片
    UIView *topV = [[UIView alloc]initWithFrame:CGRectMake(0.0, 0.0, SCREEN_WIDTH, TOPHEIGHT+20)];
    
    _adView = [[JXBAdPageView alloc]initWithFrame:CGRectMake(0.0, 0.0, SCREEN_WIDTH, TOPHEIGHT)];
    _adView.iDisplayTime = 4;
    _adView.delegate = self;
    [self returnImgUrl];
    [topV addSubview:_adView];
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, TOPHEIGHT, SCREEN_WIDTH, 20)];
    label.text = @"  商家推荐";
    label.backgroundColor = [UIColor colorWithRed:241.0/255 green:241.0/255 blue:241.0/255 alpha:1.0];
    label.textColor = kRGBColor;
    [topV addSubview:label];
    UIImageView *recommendImgV = [[UIImageView alloc]initWithFrame:CGRectMake(80, TOPHEIGHT+2, 20, 16)];
//    recommendImgV.image = [UIImage imageNamed:@"recommend.jpg"];
    
    [topV addSubview:recommendImgV];
    _tableV.tableHeaderView = topV;
    
    topView = [[UIView alloc]initWithFrame:CGRectMake(0, 25, SCREEN_WIDTH, 30)];
    [self.view addSubview:topView];
#pragma mark---创建左边选择地址按钮
    leftBtn = [[UIButton alloc]init];
    [leftBtn setTitle:@"南京" forState:UIControlStateNormal];
    [leftBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [leftBtn addTarget:self action:@selector(SelectTheAddress) forControlEvents:UIControlEventTouchUpInside];
    [topView addSubview:leftBtn];
    [leftBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(topView).offset(20);
        make.centerY.equalTo(topView.mas_centerY);
        
    }];
#pragma mark---创建右边按钮
    UIButton *rightBtn = [[UIButton alloc]init];
    [rightBtn setImage:[UIImage imageNamed:@"systemInfomation.png"] forState:UIControlStateNormal];
    [rightBtn addTarget:self action:@selector(clickSystemMessage) forControlEvents:UIControlEventTouchUpInside];
    [topView addSubview:rightBtn];
    [rightBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(topView).offset(-20);
        make.centerY.equalTo(topView.mas_centerY);
    }];
#pragma mark---创建创建searchbar
    UISearchBar *searchBar = [[UISearchBar alloc] init];
    searchBar.delegate = self;
    searchBar.layer.cornerRadius = 10;
    searchBar.layer.masksToBounds = YES;
    [searchBar.layer setBorderWidth:8];
    [searchBar.layer setBorderColor:[UIColor whiteColor].CGColor];  //设置边框为白色
    searchBar.placeholder = @"请输入搜索的关键词";
    [topView addSubview:searchBar];
    [searchBar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(leftBtn.mas_right).offset(15);
        make.right.equalTo(rightBtn.mas_left).offset(-15);
        make.centerY.equalTo(topView.mas_centerY);
        make.height.mas_equalTo(@30);
    }];
    /*
     实现表格委托
     */
    _tableV.dataSource = self;
    _tableV.delegate = self;
    //加载单元格
    [_tableV registerNib:[UINib nibWithNibName:@"ClubTableViewCell" bundle:nil] forCellReuseIdentifier:@"clubcell"];
    _tableV.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
    //取消选中行阴影效果
    [_tableV setSeparatorStyle:UITableViewCellSeparatorStyleNone];
}

#pragma mark - 定位
/**开始定位的方法*/
- (void)startLocating
{
    if ([CLLocationManager locationServicesEnabled] &&([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse || [CLLocationManager authorizationStatus] == kCLAuthorizationStatusNotDetermined)) {
        //定位功能可用，开始定位
        isLocationOpen = YES;
        //请求定位服务
        _locationManager=[[CLLocationManager alloc]init];
        _locationManager.delegate = self;
        //设置定位精确到米
        _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        //设置过滤器为无
        _locationManager.distanceFilter = kCLDistanceFilterNone;
        // 开始定位
        [_locationManager startUpdatingLocation];
        
    } else if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied){
        isLocationOpen = NO;
        NSLog(@"定位功能不可用，提示用户或忽略23333");
    }
}

-(void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    [_locationManager stopUpdatingLocation];
    CLGeocoder * geoCoder = [[CLGeocoder alloc] init];
    [geoCoder reverseGeocodeLocation:manager.location completionHandler:^(NSArray *placemarks, NSError *error) {
        for (CLPlacemark * placemark in placemarks) {
            NSDictionary *test = [placemark addressDictionary];
            //  Country(国家)  State(省份)  City(城市)  SubLocality(区)
            _locationNow = [[test objectForKey:@"City"] stringByReplacingOccurrencesOfString:@"市" withString:@""];
            //=======================================================
            if (![_locationNow isEqualToString:leftBtn.titleLabel.text]) {
                
                [self initWithHomeData];
            }
        }
    }];
    double delayInSeconds = 2.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        // 执行事件
        [self changeCity];
    });
}
- (void)changeCity{
    
    NSString *message = [NSString stringWithFormat:@"检测到您当前所在的位置是 %@，是否切换到当前所在位置",_locationNow];
    if (_locationNow != nil) {
        if (![_locationNow isEqualToString:leftBtn.titleLabel.text]) {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"提示" message:message preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *change = [UIAlertAction actionWithTitle:@"切换" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [leftBtn setTitle:_locationNow forState:UIControlStateNormal];
                [self initWithHomeData];
            }];
            UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                _locationNow = leftBtn.titleLabel.text;
            }];
            [alertController addAction:change];
            [alertController addAction:cancel];
            [self presentViewController:alertController animated:YES completion:nil];
        }
    }
}

#pragma mark - MJRefresh
- (void)initMyMJRefresh{
    // 设置回调（一旦进入刷新状态，就调用target的action，也就是调用self的dataByMJRefreshHeader方法）
    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        tablePage = 1;
        [self performSelector:@selector(initWithHomeData) withObject:nil afterDelay:1.5];
    }];
    // 设置header
    _tableV.mj_header = header;
    MJRefreshAutoNormalFooter *footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        tablePage++;
        [self performSelector:@selector(initWithHomeData) withObject:nil afterDelay:1.5];
    }];
    _tableV.mj_footer = footer;
    
}
//停止刷新
- (void)endRefresh{
    [self.tableV.mj_header endRefreshing];
    [self.tableV.mj_footer endRefreshing];
}
#pragma mark - LeftItem 选择地区
- (void)SelectTheAddress{
    
    AddressPickerDemo *addressPickerDemo = [[AddressPickerDemo alloc] init];
    UINavigationController *navigation = [[UINavigationController alloc] initWithRootViewController:addressPickerDemo];
    [self presentViewController:navigation animated:YES completion:nil];
}
#pragma mark - RightItem 系统消息
//系统消息
- (void)clickSystemMessage{
    self.tabBarController.tabBar.hidden = YES;
    UIBarButtonItem *bar = [[UIBarButtonItem alloc] initWithTitle:@"首页" style:0 target:nil action:nil];
    [self.navigationItem setBackBarButtonItem:bar];
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *userID = [userDefault valueForKey:@"ID"];
    if (userID == nil) {
        LoginViewController *login = [self.storyboard instantiateViewControllerWithIdentifier:@"login"];
        [self.navigationController pushViewController:login animated:YES];
    } else {
    
        InformsViewController *sysInfo = [self.storyboard instantiateViewControllerWithIdentifier:@"informsView"];
        [self.navigationController pushViewController:sysInfo animated:YES];
    }
    
}

#pragma mark - JXBAdPageViewDelegate
//首页轮播图
- (void)setWebImage:(UIImageView *)imgView imgUrl:(NSString *)imgUrl{
        [imgView sd_setImageWithURL:[NSURL URLWithString:imgUrl]];
}

- (void)dealloc{
    [_adView stopAds];
    
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}

#pragma mark - UISearchBarDelegate
- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar{
    searchBar.showsCancelButton = YES;
    
    return YES;
}
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    [searchBar resignFirstResponder];
    searchBar.showsCancelButton = NO;
    [self initWithHomeData];
}
//点击搜索按钮后
-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    
    if (![searchBar.text isEqual:@""]) {
        NSString *strUrl = [NSString stringWithFormat:@"%@Club/QueryClubList",BASICURL];
        [SVProgressHUD showWithStatus:@"加载中，请稍候..."];
        NSDictionary *dic = @{@"Value":searchBar.text};
        [BingNetWorkConnection connectionWithUrl:strUrl type:BINGO_REQUEST_POST params:dic FinishBlock:^(id responseObject) {
            
            [SVProgressHUD dismiss];
            
            NSArray *arr = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:nil];
            
            [_array removeAllObjects];
            for (int i = 0; i < arr.count; i ++) {
                NSDictionary *dic = [arr objectAtIndex:i];
                //                ClubListInfo *clubInfo = [[ClubListInfo alloc]init];
                //                [clubInfo setValuesForKeysWithDictionary:dic];
                ClubListInfo *clubInfo = [ClubListInfo mj_objectWithKeyValues:dic];
                if ([clubInfo.City isEqualToString:leftBtn.titleLabel.text]) {
                    [_array addObject:clubInfo];
                }
            }
            
            //刷新表格
            [self.tableV reloadData];
            [searchBar resignFirstResponder];
            searchBar.showsCancelButton = NO;
        } FailedBlock:^(NSError *error) {
            
        }];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (_array.count>0) {
        return _array.count;
    }else{
        return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ClubTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"clubcell" forIndexPath:indexPath];
    
    if (_array.count>0) {
        ClubListInfo *clubInfo = _array[indexPath.row];
        NSLog(@"_array====%@",clubInfo.ClubPic);
        cell.clubNameLbl.text = clubInfo.ClubName;
        [cell.clubImgView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",IMGURL,clubInfo.ClubPic]] placeholderImage:[UIImage imageNamed:CACHEIMG] options:SDWebImageRefreshCached];
        cell.addressLbl.text  =clubInfo.Address;
        cell.priceLbl.text = [NSString stringWithFormat:@"人均 ￥%@",clubInfo.StartWith];
        //显示距离
        if (isLocationOpen){
            cell.distanceLbl.hidden = NO;
            //计算距离
            CLLocation *current = [[CLLocation alloc] initWithLatitude:[clubInfo.Latitude doubleValue] longitude:[clubInfo.Longitude doubleValue]];
            //第二个坐标
            CLLocation *before = _locationManager.location;
            // 计算距离
            CLLocationDistance meters=[current distanceFromLocation:before];
            int m = meters/1;
            cell.distanceLbl.text = [NSString stringWithFormat:@" %.0f.%ikm",meters/1000,m%1000/100];
        } else {
            cell.distanceLbl.hidden = YES;
        }
        //是否显示“惠”
//        NSString *hiddenHui = [NSString stringWithFormat:@"%@",clubInfo.Recommend];
//        if ([hiddenHui  isEqual: @"0"]) {
//            cell.huiBtn.hidden = YES;
//        }else{
//            cell.huiBtn.hidden = NO;
//        }
        if ([clubInfo.Mode isEqualToString:@"A"]) {
            cell.huiBtn.hidden = NO;
        } else {
            cell.huiBtn.hidden = YES;
        }
            
    }
    return cell;
}
//设置行高
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 89.0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //选中行后去掉背景颜色
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
//    BusinessDescriptionVC *vc = [[BusinessDescriptionVC alloc]init];
    BusinessDescriptionViewController *vc = [[BusinessDescriptionViewController alloc] init];
    vc.clubInfo = [_array objectAtIndex:indexPath.row];
    NSLog(@"vc.clubInfo.ID====%@",vc.clubInfo.ID);
    vc.pic = vc.clubInfo.ClubPic;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset{
    
    
    if(targetContentOffset->y>150)
    {
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:2.0];
        topView.hidden = YES;
        [UIView commitAnimations];
    }
    else
    {
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:2.0];
        topView.hidden = NO;
        
        [UIView commitAnimations];
    }
    [UIView commitAnimations];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
