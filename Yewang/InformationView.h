//
//  InformationView.h
//  Yewang
//
//  Created by everhelp on 16/11/24.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InformationView : UIView<UITextFieldDelegate>
{
    BOOL isClick;
}
@property (nonatomic,strong) UIView *timeView, *nameView, *phoneNumberView, *OnlinePaymentView;
@property(nonatomic, strong) UILabel *timeLabel, *nameLabel, *phoneNumberLabel, *OnlinePaymentLabel;
@property(nonatomic, strong) UITextField *nameTF, *phoneNumberTF;
@property (nonatomic, strong) UIButton *timeButton, *dotButton;

@end
