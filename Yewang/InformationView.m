//
//  InformationView.m
//  Yewang
//
//  Created by everhelp on 16/11/24.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import "InformationView.h"
#define kMarginX 15
#define kMarginY 10
#define kLabelH 45
#define kLabelW 120
#define kFont 14
#define kTFH 25
#define kTFW 150
@implementation InformationView

-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        _timeView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, kLabelH)];
        _timeView.backgroundColor = [UIColor whiteColor];
        [self addSubview:_timeView];
        
        _timeLabel = [[UILabel alloc] initWithFrame: CGRectMake(kMarginX, kMarginY, kLabelW, kTFH)];
        _timeLabel.text = @"送达时间";
        [_timeView addSubview:_timeLabel];
        
        _timeButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _timeButton.frame = CGRectMake(CGRectGetMaxX(_timeLabel.frame), kMarginY, kTFW, kTFH);
        [_timeButton setTitle:@"请选择时间" forState:UIControlStateNormal];
        [_timeButton addTarget:self action:@selector(ToChooseTime) forControlEvents:UIControlEventTouchUpInside];
        [_timeButton setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
        [_timeView addSubview:_timeButton];

        
        _nameView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(_timeView.frame) + 1, SCREEN_WIDTH, kLabelH)];
        _nameView.backgroundColor = [UIColor whiteColor];
        [self addSubview:_nameView];
        
        _nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(kMarginX, kMarginY,  kTFW, kTFH)];
        _nameLabel.text = @"联系人";
        [_nameView addSubview:_nameLabel];
        
        _nameTF = [[UITextField alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_nameLabel.frame), kMarginY, kTFW, kTFH)];
        _nameTF.borderStyle = UITextBorderStyleRoundedRect;
//        _nameTF.placeholder = @"先生";
        _nameTF.delegate = self;
        _nameTF.tag = 100;
        _nameTF.layer.borderColor = [UIColor colorWithRed:14/255.0 green:174/255.0 blue:131/255.0 alpha:1].CGColor;
        _nameTF.layer.borderWidth = 1.0;
        _nameTF.layer.cornerRadius = 5.0;
        [_nameView addSubview:_nameTF];

        _phoneNumberView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(_nameView.frame) + 1, SCREEN_WIDTH, kLabelH)];
        _phoneNumberView.backgroundColor = [UIColor whiteColor];
        [self addSubview:_phoneNumberView];
        
        _phoneNumberLabel = [[UILabel alloc] initWithFrame:CGRectMake(kMarginX, kMarginY,  kTFW, kTFH)];
        _phoneNumberLabel.text = @"联系电话";
        [_phoneNumberView addSubview:_phoneNumberLabel];
        
        _phoneNumberTF = [[UITextField alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_nameLabel.frame), kMarginY, kTFW, kTFH)];
        _phoneNumberTF.borderStyle = UITextBorderStyleRoundedRect;
//        _phoneNumberTF.placeholder = @"15258849399";
        _phoneNumberTF.layer.borderColor = [UIColor colorWithRed:14/255.0 green:174/255.0 blue:131/255.0 alpha:1].CGColor;
        _phoneNumberTF.layer.borderWidth = 1.0;
        _phoneNumberTF.layer.cornerRadius = 5.0;
        _phoneNumberTF.delegate = self;
        _phoneNumberTF.tag = 200;
        [_phoneNumberView addSubview:_phoneNumberTF];
        
        _OnlinePaymentView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(_phoneNumberView.frame) + 20, SCREEN_WIDTH, kLabelH)];
        _OnlinePaymentView.backgroundColor = [UIColor whiteColor];
        [self addSubview:_OnlinePaymentView];
        
        _OnlinePaymentLabel = [[UILabel alloc] initWithFrame:CGRectMake(kMarginX, kMarginY, kTFW, kTFH)];
        _OnlinePaymentLabel.text = @"在线支付";
        [_OnlinePaymentView addSubview:_OnlinePaymentLabel];
        
        _dotButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _dotButton.frame = CGRectMake(SCREEN_WIDTH - 50, kMarginY, 20, 20);
        [_dotButton setBackgroundImage:[UIImage imageNamed:@"pay"] forState:UIControlStateNormal];
        [_dotButton addTarget:self action:@selector(ClickDotButton) forControlEvents:UIControlEventTouchUpInside];
        [_dotButton setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
        [_OnlinePaymentView addSubview:_dotButton];

        isClick = YES;
    }
    return self;

}

- (void)ClickDotButton
{
    if (isClick) {
        [_dotButton setBackgroundImage:[UIImage imageNamed:@"notPay"] forState:UIControlStateNormal];
        
    } else {
        [_dotButton setBackgroundImage:[UIImage imageNamed:@"pay"] forState:UIControlStateNormal];
    }
    isClick = !isClick;
    NSLog(@"isClick===%d",isClick);
}

@end
