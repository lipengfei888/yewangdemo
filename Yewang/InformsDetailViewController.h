//
//  InformsDetailViewController.h
//  Yewang_Business
//
//  Created by everhelp on 16/4/6.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InformsDetailViewController : UIViewController

@property (copy, nonatomic)NSString *titleI;
@property (copy, nonatomic)NSString *content;

@end
