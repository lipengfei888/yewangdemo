//
//  InformsDetailViewController.m
//  Yewang_Business
//
//  Created by everhelp on 16/4/6.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import "InformsDetailViewController.h"

@interface InformsDetailViewController ()
@property (weak, nonatomic) IBOutlet UILabel *titleLbl;
@property (weak, nonatomic) IBOutlet UITextView *contentTxt;

@end

@implementation InformsDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"消息详情";
    self.titleLbl.text = _titleI;
    self.contentTxt.text = [NSString stringWithFormat:@"    %@",_content];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
