//
//  InformsTableViewCell.m
//  Yewang_Business
//
//  Created by everhelp on 16/4/6.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import "InformsTableViewCell.h"

@implementation InformsTableViewCell

- (void)awakeFromNib {
    _checkBtn.layer.cornerRadius = 4;
    _checkBtn.layer.masksToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
