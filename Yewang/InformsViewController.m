//
//  InformsViewController.m
//  Yewang_Business
//
//  Created by everhelp on 16/4/6.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import "InformsViewController.h"
#import "InformsTableViewCell.h"
#import "BingNetWorkConnection.h"
#import "InformsDetailViewController.h"
#import <MJRefresh.h>

#define INFORMS @"informs"

@interface InformsViewController ()<UITableViewDataSource,UITableViewDelegate>{
    NSMutableArray *_array;
}
@property (weak, nonatomic) IBOutlet UILabel *messageLbl;
@property (weak, nonatomic) IBOutlet UITableView *table;

@end

@implementation InformsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBar.hidden = NO;
    self.navigationItem.leftBarButtonItem.title = @"返回";
    _array = [[NSMutableArray alloc]initWithCapacity:0];
    
    //注册自定义单元格
    [_table registerNib:[UINib nibWithNibName:@"InformsTableViewCell" bundle:nil] forCellReuseIdentifier:INFORMS];
    //设置表格委托
    _table.delegate = self;
    _table.dataSource = self;
    //取消选中行阴影效果
    [_table setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    NSString *strUrl = [NSString stringWithFormat:@"%@Common/QueryMessageCount",BASICURL];
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *userID = [userDefault valueForKey:@"ID"];
    NSDictionary *dic = @{@"Key":userID,@"Value":@"User"};
    [BingNetWorkConnection connectionWithUrl:strUrl type:BINGO_REQUEST_POST params:dic FinishBlock:^(id responseObject) {
        
        int num = [[NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:nil]intValue];
        self.messageLbl.text = [NSString stringWithFormat:@"您有%i条未读信息",num];
        
    } FailedBlock:^(NSError *error) {
        NSLog(@"系统消息error===%@",error);
    }];
    
    [self initWithData];
    [self initMyMJRefresh];
    
}
- (void)initMyMJRefresh{
    // 设置回调（一旦进入刷新状态，就调用target的action，也就是调用self的dataByMJRefreshHeader方法）
    MJRefreshGifHeader *header = [MJRefreshGifHeader headerWithRefreshingBlock:^{
        [self performSelector:@selector(initWithData) withObject:nil afterDelay:1.5];
    }];
    
    // 设置header
    _table.mj_header = header;
}


//停止刷新
- (void)endRefresh{
    [self.table.mj_header endRefreshing];
}

- (void)initWithData{
    NSString *strUrl = [NSString stringWithFormat:@"%@Common/FetchMessage",BASICURL];
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *userID = [userDefault valueForKey:@"ID"];
    NSDictionary *dic = @{@"Key":userID,@"Value":@"User"};
    [BingNetWorkConnection connectionWithUrl:strUrl type:BINGO_REQUEST_POST params:dic FinishBlock:^(id responseObject) {
        [self endRefresh];
        _array = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:nil];
        [self.table reloadData];
    } FailedBlock:^(NSError *error) {
        NSLog(@"系统消息error===%@",error);
    }];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSLog(@"array.count:%li",(unsigned long)_array.count);
    if (_array.count>0) {
        return [_array count];
    }else{
        return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    InformsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:INFORMS forIndexPath:indexPath];
    if (_array.count>0) {
        NSDictionary *dic = [_array objectAtIndex:indexPath.row];
        cell.titleLbl.text = [dic objectForKey:@"Title"];
        cell.contactLbl.text = [dic objectForKey:@"Content"];
        NSString *dateStr = [[[dic objectForKey:@"SendDate"] substringFromIndex:6]substringToIndex:13];
        NSDate *date = [NSDate dateWithTimeIntervalSince1970:([dateStr longLongValue]/1000)];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSString *sendTime = [dateFormatter stringFromDate:date];
        cell.timeLbl.text = sendTime;
        
        cell.checkBtn.tag = indexPath.row;
        [cell.checkBtn addTarget:self action:@selector(clickCheck:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //选中行后去掉背景颜色
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    InformsDetailViewController *detail = [self.storyboard instantiateViewControllerWithIdentifier:@"informsDetail"];
    NSDictionary *dic = [_array objectAtIndex:indexPath.row];
    detail.titleI = [dic objectForKey:@"Title"];
    detail.content = [dic objectForKey:@"Content"];
    
    [self.navigationController pushViewController:detail animated:YES];
}

- (void)clickCheck:(UIButton *)button{
    InformsDetailViewController *detail = [self.storyboard instantiateViewControllerWithIdentifier:@"informsDetail"];
    NSDictionary *dic = [_array objectAtIndex:button.tag];
    detail.titleI = [dic objectForKey:@"Title"];
    detail.content = [dic objectForKey:@"Content"];
    
    [self.navigationController pushViewController:detail animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 72;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
