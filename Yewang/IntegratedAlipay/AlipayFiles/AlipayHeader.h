//
//  AlipayHeader.h
//  IntegratedAlipay
//
//  Created by Winann on 15/1/9.
//  Copyright (c) 2015年 Winann. All rights reserved.
//

/**
 *  1. 将本工程中的IntegratedAlipay文件夹导入工程中，记得选copy；
 *  2. 点击项目名称,点击“Build Settings”选项卡,在搜索框中,以关键字“search” 搜索,对“Header Search Paths”增加头文件路径:“$(SRCROOT)/项目名称/IntegratedAlipay/AlipayFiles”（注意：不包括引号，如果不是放到项目根目录下，请在项目名称后面加上相应的目录名）；
 *  3. 点击项目名称,点击“Build Phases”选项卡,在“Link Binary with Librarles” 选项中,新增“AlipaySDK.framework”和“SystemConfiguration.framework” 两个系统库文件。如果项目中已有这两个库文件,可不必再增加；
 *  4. 在本头文件中设置kPartnerID、kSellerAccount、kNotifyURL、kAppScheme和kPrivateKey的值（所有的值在支付宝回复的邮件里面：注意，建议除appScheme以外的字段都从服务器请求）；
 *  5. 点击项目名称,点击“Info”选项卡，在URL types里面添加一项，Identifier可以不填，URL schemes必须和appScheme的值相同，用于支付宝处理回到应用的事件；
 *  6. 在需要用的地方导入“AlipayHeader.h”，并使用“[AlipayRequestConfig alipayWithPartner:...”方法进行支付；
 *  7. 在AppDelegate中处理事件回调（可直接复制下面内容）：
 - (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
 //如果极简 SDK 不可用,会跳转支付宝钱包进行支付,需要将支付宝钱包的支付结果回传给 SDK if ([url.host isEqualToString:@"safepay"]) {
 [[AlipaySDK defaultService] processOrderWithPaymentResult:url standbyCallback:^(NSDictionary *resultDic) {
 NSLog(@"result = %@",resultDic);
 }];
 if ([url.host isEqualToString:@"platformapi"]){//支付宝钱包快登授权返回 authCode
 [[AlipaySDK defaultService] processAuthResult:url standbyCallback:^(NSDictionary *resultDic) {
 NSLog(@"result = %@",resultDic);
 }];
 }
 return YES;
 }
 */

#ifndef IntegratedAlipay_AlipayHeader_h
#define IntegratedAlipay_AlipayHeader_h


#import <AlipaySDK/AlipaySDK.h>     // 导入AlipaySDK
#import "AlipayRequestConfig.h"     // 导入支付类
#import "Order.h"                   // 导入订单类
#import "DataSigner.h"              // 生成signer的类：获取私钥并将商户信息签名,外部商户可以根据情况存放私钥和签名,只需要遵循 RSA 签名规范, 并将签名字符串 base64 编码和 UrlEncode

#import <Foundation/Foundation.h>   // 导入Foundation，防止某些类出现类似：“Cannot find interface declaration for 'NSObject', superclass of 'Base64'”的错误提示


/**
 *  partner:合作身份者ID,以 2088 开头由 16 位纯数字组成的字符串。
 *
 */
#define kPartnerID @"2088221607152510"
//#define kPartnerID @"2088121058066926"


/**
 *  seller:支付宝收款账号,手机号码或邮箱格式。
 */
#define kSellerAccount @"geyuan@anshare.cc"
//#define kSellerAccount @"zhhz88@sina.com"

/**
 *  支付宝服务器主动通知商户 网站里指定的页面 http 路径。
 */
#define kNotifyURL @"http://api.nightnet.cn/api/Club/GetClubList"


/**
 *  appSckeme:应用注册scheme,在Info.plist定义URLtypes，处理支付宝回调
 */
#define kAppScheme @"nightnet"
//#define kAppScheme @"nightclub"



/**
 *  private_key:商户方的私钥,pkcs8 格式。
 *///
///此字段有效
#define kPrivateKey @"MIICWwIBAAKBgQD7kh1MI2WvD8Nj4ocL/1wBjJiWjDfsNhZFqnFraFG2zOFewhjUSoRavP8La+uZ3lxUgFSo5AYM3bu1ft5N5OsXJwHbu0vthzySdteKIDO+n+aAO21o3ljEnXsJ5G7hV+CmST+slrU4rIy0LzW5RSGtoSqMXNoFP/uBHwqqFHD/aQIDAQABAoGATnj+kJB+GISHQ3IXBCQMtCMCOfvVOeaJPgPzyzhIPQhdyKwtCJOSA6fuusZYQK//BxDc164BQSQteGjxuUSOHBVn/gyJAR3ZH9kRA1Q1RiQfECNYgqZMp7X2rDolpr7Caizajj+FE4GVCaWaqqmADtiGN2cePrTj2m1dUv1pZC0CQQD/qQcEqkPmQKDLiG2faxPJS/nwOSlcWICsSG0Fz6JomMAj5oq2B0YL6eVti3ttnGqIJRUNn3Of6Vc1mVAOl04DAkEA++eyIb/Vf1hqNaXwMfvknNv27f12LxjyhlzGKLHktDwUFomLJFKYSyZUc9hyXBN29YtCOM4gB+bLPPWS11/HIwJAJ89NLXfJ3OfBYhAbadCFozm2cKal/yA04R7iAx6i3oCyoqR6gv+884v3Ush+bsIFgK+tiJKEsajnEIyP/8WluwJAAq7TDNz4LyUH7I5Z4o5mPu1w0dQTQCkp3Bh8284ZO3owVJUecEKY8KP13k392dEzQSY8jFJVsP/aRknvSG/ytQJAN7aAV1uF6JrMkYGmMkFPYomzwAVF4FfdauqAEQeQ5gw0rV84wZxjX7Qvol3VN8Qqkyorh164+ZZ4fRmBm2/4lg=="
//此字段尝试
//#define kPrivateKey @"MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBALvqm8HvQ1mcMJd9A7Ulwyk2tY+J/BtUIJLuXPTJ+GRXldtrWL7esNYrr7qfEN+HNvP62Pp5kJwTB9IVEbb8U//piRcFF7GkPn3aXquq9iYzQ7wMvZNitaB/MwcjTJkS7bnNvDaZJCEmM2g9pvvEEllkGab7btp7BfybDSVVcyEPAgMBAAECgYEAmTFqZxCZ9EybZHQa+KtfTZL3MpxHtTPBFmdRypF4eLzaQzVg9MFwojidGuOgCsodZ4sBJjjCRipRfteESIuuIZU/SFHI5gCIuN9lZqgtjzGVLL9kD15swnSw83AtCG5Q1KrsbA8u7IJJNtpXukSsVhaZM1u8loVCW0dVD5Q4lDECQQDpcEKfbxkYGWxuVJ36bv2K2kVijWUJT2bjj4qCldF0AyDJHpnoLENEfduI69es+0ipBuRTzfp+n/9s6/zMMYdZAkEAzhQKSG8ENR3XJDPYbaaZc3pBpxzpEzAHKi+yk1zEVjyDxfhWBaefn6Tl8RjwKDZbBRF+35NBixemFv+5HH3GpwJAcvNk2z3KDaP3UR057ehZ0dZJ9I3MAxmmgMheXI+J5KblriPzhfDF7yRKxtHcEsJqtVE/fkUf5NrE3M4YXjTFgQJAfXrEXCCE/lbCJkAPW4ziyOa7TScUYGORKUXLxWDqB9xnHEc/EchkMI/2lxxpNwMJ3wctKrN+aFQpBL4HF+sZ5wJAfcw033kb+/LyFzQqAO92soU2ddhC1ryvcqSmJck8FLV8GecvSXZxAQICeFBQyto0R2E5XGX4zW8mPd+rNyy9ig=="
#endif
