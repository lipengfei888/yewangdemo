//
//  IntroduceViewController.h
//  Yewang
//
//  Created by everhelp on 16/8/22.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IntroduceViewController : UIViewController

@property (nonatomic, strong) NSString *strUrl;

@end
