//
//  LoginViewController.m
//  Yewang
//
//  Created by everhelp on 16/4/13.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import "LoginViewController.h"
#import "RegisterViewController.h"
#import "ForgetPwdViewController.h"

@interface LoginViewController ()
@property (weak, nonatomic) IBOutlet UITextField *nameTxt;
@property (weak, nonatomic) IBOutlet UITextField *pwdTxt;
@property (weak, nonatomic) IBOutlet UIButton *loginBtn;

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
}
- (void)viewWillAppear:(BOOL)animated{
    self.navigationItem.title = @"登录";
    self.navigationController.navigationBar.hidden = NO;
    self.tabBarController.tabBar.hidden = YES;
    self.loginBtn.layer.cornerRadius = 8.0;
    self.loginBtn.layer.masksToBounds = YES;
    [super viewWillAppear:animated];
}

- (IBAction)clickLogin:(id)sender {
    
    if ([CheckString checkPhoneNumInput:_nameTxt.text]) {
        NSString *strUrl = [NSString stringWithFormat:@"%@Reg/Login",BASICURL];
        NSDictionary *dic = @{@"UserName":_nameTxt.text,@"Password":_pwdTxt.text};
        [BingNetWorkConnection connectionWithUrl:strUrl type:BINGO_REQUEST_POST params:dic FinishBlock:^(id responseObject) {
            
            [self checkLogin:responseObject];
            
        } FailedBlock:^(NSError *error) {
            NSLog(@"登录：%@",error);
        }];
    } else {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"提示" message:@"请输入格式正确的电话号码" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleCancel handler:nil];
        [alertController addAction:action];
        [self presentViewController:alertController animated:YES completion:nil];
    }
    
    
}

- (void)checkLogin:(id)responseObject{
    
    if ([[NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers | NSJSONReadingMutableLeaves error:nil] isKindOfClass:[NSArray class]]) {
        
        NSArray *array = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers | NSJSONReadingMutableLeaves error:nil];
        NSDictionary *dictionary = [array objectAtIndex:0];
        NSLog(@"登录信息：%@",dictionary);
        
        /*
         用户登录成功
         */
        //获取NSUserDefault单例
        NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
        //登录成功后把用户的详细信息存储到userDefault
        [userDefault setObject:[dictionary objectForKey:@"HeadIcon"] forKey:@"HeadIcon"];
        [userDefault setObject:[dictionary objectForKey:@"ID"] forKey:@"ID"];
        [userDefault setObject:[dictionary objectForKey:@"Money"] forKey:@"Money"];
        [userDefault setObject:[dictionary objectForKey:@"Mobile"] forKey:@"Mobile"];
        [userDefault setObject:[dictionary objectForKey:@"NickName"] forKey:@"NickName"];
        [userDefault setObject:[dictionary objectForKey:@"Password"] forKey:@"Password"];
        [userDefault setObject:[dictionary objectForKey:@"Region"] forKey:@"Region"];
        [userDefault setObject:[dictionary objectForKey:@"Sex"] forKey:@"Sex"];
        [userDefault setObject:[dictionary objectForKey:@"UserName"] forKey:@"UserName"];
        [userDefault synchronize];
        
        NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",HEADICONURL,[dictionary objectForKey:@"HeadIcon"]]]];
        NSString * DocumentsPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
        NSFileManager *fileManager = [NSFileManager defaultManager];
        [fileManager createDirectoryAtPath:DocumentsPath withIntermediateDirectories:YES attributes:nil error:nil];
        [fileManager createFileAtPath:[DocumentsPath stringByAppendingString:@"/headImage.jpg"] contents:data attributes:nil];
//        NSString *documentPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        NSLog(@"%@",DOCUMENTPATH);
        //返回我的页面
        [self.navigationController popViewControllerAnimated:YES];
    } else if ([[NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers | NSJSONReadingMutableLeaves error:nil] isKindOfClass:[NSDictionary class]]){
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"登录失败" message:@"账号或密码错误" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleCancel handler:nil];
        [alertController addAction:action];
        [self presentViewController:alertController animated:YES completion:nil];
    }
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [_pwdTxt resignFirstResponder];
    [_nameTxt resignFirstResponder];
}

- (IBAction)clickForgetPwd:(id)sender {
    ForgetPwdViewController *forgetPwd = [self.storyboard instantiateViewControllerWithIdentifier:@"forgetpwd"];
    [self.navigationController pushViewController:forgetPwd animated:YES];
}

- (IBAction)clickRegister:(id)sender {
    RegisterViewController *registerV = [self.storyboard instantiateViewControllerWithIdentifier:@"register"];
    [self.navigationController pushViewController:registerV animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
