//
//  MoreTableViewCell.h
//  Yewang
//
//  Created by everhelp on 16/4/14.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MoreTableViewCell : UITableViewCell{
    UIView *bView;
}


@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UILabel *titleLbl;

@end
