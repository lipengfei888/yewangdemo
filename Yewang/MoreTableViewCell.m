//
//  MoreTableViewCell.m
//  Yewang
//
//  Created by everhelp on 16/4/14.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import "MoreTableViewCell.h"

@implementation MoreTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    bView = [[UIView alloc]initWithFrame:CGRectMake(0, self.bounds.origin.y, SCREEN_WIDTH, 1)];
    [bView setBackgroundColor:[UIColor lightGrayColor]];
    [self addSubview:bView];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
