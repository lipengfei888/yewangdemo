//
//  MoreTableViewController.m
//  Yewang
//
//  Created by everhelp on 16/4/14.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import "MoreTableViewController.h"
#import "MoreTableViewCell.h"
#import "AboutUsViewController.h"
#import "BusinessViewController.h"
#import "FeedBackViewController.h"
#import "UIAlertController+WarnMessage.h"
#import "FMDBHepler.h"

#import <ShareSDK/ShareSDK.h>
#import <ShareSDKUI/ShareSDK+SSUI.h>
@interface MoreTableViewController ()

@end

@implementation MoreTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //去除下面多余的cell
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.edgesForExtendedLayout = UIRectEdgeNone;//解决tableView被导航栏盖住的问题
   
    UIImageView *topImgView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 170)];
    topImgView.image = [UIImage imageNamed:@"图片.png"];
    self.tableView.tableHeaderView = topImgView;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
}
- (void)viewWillAppear:(BOOL)animated{
    self.navigationController.navigationBar.hidden = NO;
    [super viewWillAppear:animated];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 5;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MoreTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"more" forIndexPath:indexPath];
//    cell.backgroundColor = [UIColor yellowColor];
    if (indexPath.row == 0) {
        cell.imgView.image = [UIImage imageNamed:@"关于我们.png"];
        cell.titleLbl.text = @"关于我们";
    } else if (indexPath.row == 1) {
        cell.imgView.image = [UIImage imageNamed:@"意见反馈.png"];
        cell.titleLbl.text = @"意见反馈";
    } else if (indexPath.row == 2) {
        cell.imgView.image = [UIImage imageNamed:@"分享推荐.png"];
        cell.titleLbl.text = @"分享推荐";
    } else if (indexPath.row == 3) {
        cell.imgView.image = [UIImage imageNamed:@"我是商家.png"];
        cell.titleLbl.text = @"我是商家";
    } else {
        cell.imgView.image = [UIImage imageNamed:@"清空缓存.png"];
        cell.titleLbl.text = @"清空缓存";
    }
    [tableView setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //选中行后去掉背景颜色
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (indexPath.row == 0) {//关于我们
        AboutUsViewController *aboutus = [self.storyboard instantiateViewControllerWithIdentifier:@"aboutUs"];
        [self.navigationController pushViewController:aboutus animated:YES];
    } else if (indexPath.row == 1) {//意见反馈
        FeedBackViewController *feedback = [self.storyboard instantiateViewControllerWithIdentifier:@"feedback"];
        [self.navigationController pushViewController:feedback animated:YES];
    } else if (indexPath.row == 2) {//分享推荐
        NSLog(@"fenxiang");//http://www.star0.com/resource/Web/appshare/share_soft.html
        NSMutableDictionary *shareParams = [NSMutableDictionary dictionary];
        [shareParams SSDKSetupShareParamsByText:@"满足你对夜生活的所有想象"
                                         images:[UIImage imageNamed:@"1.pic_hd.png"]
                                            url:[NSURL URLWithString:@"http://res.nightnet.cn/resource/Web/appshare/share_soft.html"]
                                          title:@"开启夜生活"
                                           type:SSDKContentTypeAuto];
        //2、分享（可以弹出我们的分享菜单和编辑界面）http://res.nightnet.cn/resource/Web/appshare/share_soft.html
        [ShareSDK showShareActionSheet:nil //要显示菜单的视图, iPad版中此参数作为弹出菜单的参照视图，只有传这个才可以弹出我们的分享菜单，可以传分享的按钮对象或者自己创建小的view 对象，iPhone可以传nil不会影响
                                 items:nil
                           shareParams:shareParams
                   onShareStateChanged:^(SSDKResponseState state, SSDKPlatformType platformType, NSDictionary *userData, SSDKContentEntity *contentEntity, NSError *error, BOOL end) {
                       
                       switch (state) {
                           case SSDKResponseStateSuccess:
                           {
                               UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"分享成功" message:nil preferredStyle:UIAlertControllerStyleAlert];
                               UIAlertAction *action = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleCancel handler:nil];
                               [alertController addAction:action];
                               [self presentViewController:alertController animated:YES completion:nil];
                               break;
                               
                           }
                           case SSDKResponseStateFail:
                           {
                               UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"分享失败" message:[NSString stringWithFormat:@"%@",error] preferredStyle:UIAlertControllerStyleAlert];
                               UIAlertAction *action = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleCancel handler:nil];
                               [alertController addAction:action];
                               [self presentViewController:alertController animated:YES completion:nil];
                               break;
                               
                               
                           }
                           default:
                               break;
                       }
                   }
         ];
    } else if (indexPath.row == 3) {//我是商家
        BusinessViewController *business = [self.storyboard instantiateViewControllerWithIdentifier:@"business"];
        [self.navigationController pushViewController:business animated:YES];
    }
    else if (indexPath.row == 4) {//清空缓存
        
        [self delTheCache];
    }
}

- (void)delTheCache{
    //图片字节大小
    int imgByteSize = (int)[SDImageCache sharedImageCache].getSize;
    //图片M大小
    CGFloat imgCacheSize = imgByteSize / 1024.0 / 1024.0;
    //数据字节大小
    int dataByteSize = (int)[self folderSizeAtPath:[DOCUMENTPATH stringByAppendingPathComponent:@"dataSource.rdb"]];
    //数据M大小
    CGFloat dataCacheSize = dataByteSize / 1024.0 / 1024.0;
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"清除缓存" message:@"缓存被清理后将重新加载" preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *delImgCacheAction = [UIAlertAction actionWithTitle:[NSString stringWithFormat:@"图片缓存：%.2fM",imgCacheSize] style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [[SDImageCache sharedImageCache] clearDisk];
    }];
    UIAlertAction *delDataCacheAction = [UIAlertAction actionWithTitle:[NSString stringWithFormat:@"数据缓存：%.2fM",dataCacheSize] style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        ///删除本地rdb文件
        NSString *paths = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)objectAtIndex:0]stringByAppendingPathComponent:@"dataSource.rdb"];
        
        NSFileManager *fileMgr = [NSFileManager defaultManager];
        BOOL bRet = [fileMgr fileExistsAtPath:paths];
        if (bRet) {
            NSError *err;
            [fileMgr removeItemAtPath:paths error:&err];
        }
        
    }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
    [alertController addAction:delImgCacheAction];
    [alertController addAction:delDataCacheAction];
    [alertController addAction:cancelAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

#pragma -mark 获取本地rdb文件的大小
-(long long)folderSizeAtPath:(NSString *)folderPath{
    NSFileManager *manager = [NSFileManager defaultManager];
    ///获取单个文件大小
    if ([manager fileExistsAtPath:folderPath]) {
        return [[manager attributesOfItemAtPath:folderPath error:nil]fileSize];
    }
    return 0;
}

@end
