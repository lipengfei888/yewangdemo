//
//  MyMapViewController.h
//  Yewang
//
//  Created by everhelp on 16/5/4.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyMapViewController : UIViewController

@property (nonatomic,assign) double latitude;
@property (nonatomic,assign) double longitude;
@property (nonatomic, copy) NSString *clubName;
@property (nonatomic, copy) NSString *address;

@end
