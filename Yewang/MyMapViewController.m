//
//  MyMapViewController.m
//  Yewang
//
//  Created by everhelp on 16/5/4.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import "MyMapViewController.h"
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>
#import <BaiduMapAPI_Map/BMKMapView.h>
#import <BaiduMapAPI_Map/BMKPointAnnotation.h>
#import <BaiduMapAPI_Map/BMKPinAnnotationView.h>

@interface MyMapViewController ()<BMKMapViewDelegate>{
    
}

@property (nonatomic, strong) BMKMapView *mapView;

@end

@implementation MyMapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBar.hidden = NO;
    
    //创建MapView
    _mapView = [[BMKMapView alloc]initWithFrame:CGRectMake(0.0, 0.0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    self.view = _mapView;
//    [self.view addSubview:_mapView];
    [_mapView setMapType:BMKMapTypeStandard];
    
//    [self initGUI];
    
}
- (void)viewDidAppear:(BOOL)animated{
    [self initGUI];
}

- (void)viewWillAppear:(BOOL)animated{
    [_mapView viewWillAppear];
    _mapView.delegate = self;//此处不使用时置nil，否则影响内存的释放
}

- (void)viewWillDisappear:(BOOL)animated{
    [_mapView viewWillDisappear];
    _mapView.delegate = nil;//不用时，置空nil
}

#pragma mark 添加地图控件
-(void)initGUI{
    
    if (_latitude == 0&&_longitude == 0) {
        CLLocationCoordinate2D coordinate=CLLocationCoordinate2DMake(32.022397205899999,118.86421240759999);
        BMKPointAnnotation* annotation = [[BMKPointAnnotation alloc]init];
        annotation.title=@"南京夜网网络科技有限公司";
        annotation.subtitle=@"南理工科技园";
        annotation.coordinate=coordinate;
        BMKCoordinateRegion region ;//表示范围的结构体
        region.center = coordinate;//中心点
        region.span.latitudeDelta = 0.05;//经度范围（设置为0.1表示显示范围为0.2的纬度范围）
        region.span.longitudeDelta = 0.05;//纬度范围
        [_mapView setRegion:region animated:YES];
        [self.mapView addAnnotation:annotation];
    }else{
        CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(_latitude, _longitude);
        BMKPointAnnotation *annotation = [[BMKPointAnnotation alloc]init];
        annotation.coordinate = coordinate;
        annotation.title = _clubName;
        annotation.subtitle = _address;
        BMKCoordinateRegion region ;//表示范围的结构体
        region.center = coordinate;//中心点
        region.span.latitudeDelta = 0.05;//经度范围（设置为0.1表示显示范围为0.2的纬度范围）
        region.span.longitudeDelta = 0.05;//纬度范围
        [_mapView setRegion:region animated:YES];
        [self.mapView addAnnotation:annotation];
    }

}
- (BMKAnnotationView *)mapView:(BMKMapView *)mapView viewForAnnotation:(id <BMKAnnotation>)annotation
{
    if ([annotation isKindOfClass:[BMKPointAnnotation class]]) {
        BMKPinAnnotationView *newAnnotationView = [[BMKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"myAnnotation"];
        newAnnotationView.pinColor = BMKPinAnnotationColorPurple;
        newAnnotationView.animatesDrop = YES;// 设置该标注点动画显示
        return newAnnotationView;
    }
    return nil;
}
- (void)mapView:(MKMapView *)mapView didAddAnnotationViews:(NSArray<MKAnnotationView *> *)views{
    BMKAnnotationView *piview = (BMKAnnotationView *)[views objectAtIndex:0];
    //标题和子标题自动显示
    [_mapView selectAnnotation:piview.annotation animated:YES];
}

- (IBAction)clickBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
