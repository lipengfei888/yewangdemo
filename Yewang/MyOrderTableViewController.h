//
//  MyOrderTableViewController.h
//  Yewang
//
//  Created by everhelp on 16/4/29.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import <UIKit/UIKit.h>

@class OrderListInfo;

@interface MyOrderTableViewController : UITableViewController

@property (nonatomic, strong) OrderListInfo *order;

@end
