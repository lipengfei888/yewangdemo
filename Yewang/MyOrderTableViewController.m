//
//  MyOrderTableViewController.m
//  Yewang
//
//  Created by everhelp on 16/4/29.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import "MyOrderTableViewController.h"
#import "OrderListInfo.h"
#import "AlipayHeader.h"
#import "BuyDetailViewController.h"
#import "BuySuccessViewController.h"

//宏定义（截图大小）
#define WIDTH self.view.frame.size.width
#define HEIGHT self.view.frame.size.height-64

@interface MyOrderTableViewController ()

@property (weak, nonatomic) IBOutlet UILabel *statusLbl;
@property (weak, nonatomic) IBOutlet UILabel *priceLbl;
@property (weak, nonatomic) IBOutlet UILabel *orderIDLbl;
@property (weak, nonatomic) IBOutlet UILabel *itemInfoLbl;
@property (weak, nonatomic) IBOutlet UILabel *arriveTimeLbl;
@property (weak, nonatomic) IBOutlet UILabel *payMethodLbl;
@property (weak, nonatomic) IBOutlet UILabel *contactPersonLbl;
@property (weak, nonatomic) IBOutlet UILabel *contactNumberLbl;
@property (weak, nonatomic) IBOutlet UILabel *payMentCodeLbl;
@property (weak, nonatomic) IBOutlet UIButton *firstBtn;
@property (weak, nonatomic) IBOutlet UIButton *secondBtn;

@end

@implementation MyOrderTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"订单详情";
    _firstBtn.layer.cornerRadius = 10;
    _firstBtn.layer.masksToBounds = YES;
    _secondBtn.layer.cornerRadius = 10;
    _secondBtn.layer.masksToBounds = YES;
    
    _statusLbl.text = _order.Status;
    _priceLbl.text = [NSString stringWithFormat:@"￥ %.2f",[_order.Total doubleValue]];
    _orderIDLbl.text = _order.OrderNumber;
    _itemInfoLbl.text = [NSString stringWithFormat:@"%@ %@",_order.ClubName,_order.ItemName];
    _arriveTimeLbl.text = _order.ArriveTime;
    _payMethodLbl.text = _order.PayMethod;
    _contactPersonLbl.text = _order.ContactPerson;
    _contactNumberLbl.text = _order.ContactNumber;
    _payMentCodeLbl.text = _order.PaymentCode;
    
    if ([_order.Status isEqualToString:@"未付款"]) {
        [_firstBtn setTitle:@"支付" forState:UIControlStateNormal];
        [_firstBtn addTarget:self action:@selector(clickPayItem) forControlEvents:UIControlEventTouchUpInside];
        [_secondBtn setHidden:YES];
    } else if ([_order.Status isEqualToString:@"已付款"]) {
        [_firstBtn setTitle:@"联系经理" forState:UIControlStateNormal];
        [_firstBtn addTarget:self action:@selector(clickCallManager) forControlEvents:UIControlEventTouchUpInside];
        [_secondBtn setHidden:NO];
        [_secondBtn setTitle:@"申请退款" forState:UIControlStateNormal];
        [_secondBtn addTarget:self action:@selector(clickBackMoney) forControlEvents:UIControlEventTouchUpInside];
    } else if ([_order.Status isEqualToString:@"已消费"]) {
        [_firstBtn setTitle:@"上传账单" forState:UIControlStateNormal];
        [_firstBtn addTarget:self action:@selector(clickUpItemImg) forControlEvents:UIControlEventTouchUpInside];
        [_secondBtn setHidden:YES];
    } else {
        [_firstBtn setHidden:YES];
        [_secondBtn setHidden:YES];
    }
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
}
#pragma mark - 付款
- (void)clickPayItem{
    
    BuyDetailViewController *buyDetail = [self.storyboard instantiateViewControllerWithIdentifier:@"buydetail"];
    buyDetail.itemName = _order.ItemName;
    buyDetail.price = _order.Price ;
    buyDetail.itemID = _order.ItemID;
    buyDetail.clubID = _order.ClubID;
    buyDetail.total = _order.Total;
    buyDetail.arriveTime = _order.ArriveTime;
    buyDetail.tel = _order.ContactNumber;
    buyDetail.itemNum = _order.Mount;
    buyDetail.contactPerson = _order.ContactPerson;
    buyDetail.orderID = _order.ID;
    [self.navigationController pushViewController:buyDetail animated:YES];
    
}
#pragma mark - 联系客户经理
- (void)clickCallManager{
    
    NSString *strUrl = [NSString stringWithFormat:@"%@Club/GetClub?clubID=%@",BASICURL,_order.ClubID];
    [BingNetWorkConnection connectionWithUrl:strUrl type:BINGO_REQUEST_GET params:nil FinishBlock:^(id responseObject) {
        NSLog(@"%@",[NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves error:nil]);
        NSDictionary *dictionary = [[NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves error:nil] objectAtIndex:0];
        NSString *cAS = [dictionary objectForKey:@"CategoryAs"];
        if ([cAS isEqualToString:@"清吧"] || [cAS isEqualToString:@"餐吧"] || [cAS isEqualToString:@"主题吧"]) {
            //直接拨打电话
            NSMutableString *strPhone = [[NSMutableString alloc]initWithFormat:@"tel:%@",[dictionary objectForKey:@"ContactNumber"]];
            UIWebView *callWebView = [[UIWebView alloc]init];
            [callWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:strPhone]]];
            [self .view addSubview:callWebView];
        } else if ([cAS isEqualToString:@"嗨吧"] ||[cAS isEqualToString:@"夜店"]){
            //跳转显示经理
            BuySuccessViewController *buySuccess = [self.storyboard instantiateViewControllerWithIdentifier:@"buysuccess"];
            buySuccess.clubID = _order.ClubID;
            [self.navigationController pushViewController:buySuccess animated:YES];
        }
    } FailedBlock:^(NSError *error) {
        
    }];
}

#pragma mark - 上传账单
- (void)clickUpItemImg{
    
    UIImage *img = [self snapshot:self.view];
    NSString *strUrl = [NSString stringWithFormat:@"%@Order/UploadUserImg" ,BASICURL];
    //图片转成base64字符串
    NSData *data;
    if (UIImagePNGRepresentation(img) == nil) {
        data = UIImageJPEGRepresentation(img, 1);
    } else {
        data = UIImagePNGRepresentation(img);
    }
    NSString *strImg = [data base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    NSString *encodeImgStr = [strImg stringByReplacingOccurrencesOfString:@"+" withString:@"%2B"];
    NSDictionary *dic = @{@"Key":_order.ID,@"Value":encodeImgStr};
    
    [BingNetWorkConnection connectionWithUrl:strUrl type:BINGO_REQUEST_POST params:dic FinishBlock:^(id responseObject) {
        
        NSString *string = [[NSString alloc]initWithData:responseObject encoding:NSUTF8StringEncoding];
        UIAlertController *alertControl = [UIAlertController alertControllerWithTitle:string message:nil preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleCancel handler:nil];
        [alertControl addAction:action];
        [self presentViewController:alertControl animated:YES completion:nil];
        
    } FailedBlock:^(NSError *error) {
        NSLog(@"提交订单error===%@",error);
    }];
    
//    UIImageWriteToSavedPhotosAlbum(img, self, @selector(image:didFinishSavingWithError:contextInfo:), NULL);
    
}
//保存图片到相册
-(void)image:(UIImage*)image didFinishSavingWithError:(NSError*)error contextInfo:(void*)contextInfo
{
    NSString *msg = nil;
    if (error != NULL) {
        msg = @"图片保存到相册失败";
    }else{
        msg = @"图片保存到相册成功";
    }
    NSLog(@"%@",msg);
}
/*截图*/
- (UIImage *)snapshot:(UIView *)view{
    
    CGSize size = CGSizeMake(WIDTH, HEIGHT);
    UIGraphicsBeginImageContextWithOptions(size,YES,0);
    
    [view drawViewHierarchyInRect:view.bounds afterScreenUpdates:YES];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return image;
    
}
#pragma mark - 申请退款
- (void)clickBackMoney{
    
    UIAlertController *alertcontroller = [UIAlertController alertControllerWithTitle:@"确认退款吗 ？" message:@"提示：根据您的支付方式的不同，退款会产生0.6%-2%不等的手续费" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        NSString *strUrl = [NSString stringWithFormat:@"%@Order/ReturnMoney",BASICURL];
        NSDictionary *dic = @{@"Key":_order.ID,@"Value":@0};
        [BingNetWorkConnection connectionWithUrl:strUrl type:BINGO_REQUEST_POST params:dic FinishBlock:^(id responseObject) {
            
            NSString *result = [[NSString alloc]initWithData:responseObject encoding:NSUTF8StringEncoding];
            if ([result isEqualToString:@"[退款成功]"]) {
                UIAlertController *alertcontroller = [UIAlertController alertControllerWithTitle:@"退款处理中" message:@"退款将在1-2个工作日内退还至您的账户，请注意查收" preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *action = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:nil];
                [alertcontroller addAction:action];
                [self presentViewController:alertcontroller animated:YES completion:nil];
                [_secondBtn setTitle:@"退款中" forState:UIControlStateNormal];
                _secondBtn.userInteractionEnabled = NO;
                [self.tableView reloadData];
                [self.navigationController popViewControllerAnimated:YES];
            }
            
            
        } FailedBlock:^(NSError *error) {
            NSLog(@"提交订单error===%@",error);
        }];
    }];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:nil];
    [alertcontroller addAction:action];
    [alertcontroller addAction:cancel];
    [self presentViewController:alertcontroller animated:YES completion:nil];
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //选中行后去掉背景颜色
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
