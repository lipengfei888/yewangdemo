//
//  MyViewController.m
//  Yewang
//
//  Created by everhelp on 16/7/18.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import "MyViewController.h"
#import "LoginViewController.h"
#import "RegisterViewController.h"
#import "PersonalTableViewController.h"
#import "CollectionTableViewController.h"
#import "WalletViewController.h"
#import "OrderTableViewController.h"
#import "MoreTableViewController.h"
#define kWalletCellH 50

@interface MyViewController ()<UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIActionSheetDelegate>{
    //创建一个全局的UIImagePickerController对象
    UIImagePickerController *_imagePickerController;
    //修改昵称的全局变量
    NSUserDefaults *userDefault;
    NSString *userID;
}
///背景图片
@property (nonatomic, strong) UIImageView *bgImgView;
///头像按钮
@property (nonatomic, strong) UIButton *headImgBtn;
///用户昵称
@property (nonatomic, strong) UILabel *nameLbl;
///注册按钮
@property (nonatomic, strong) UIButton *registerBtn;
///登录按钮
@property (nonatomic, strong) UIButton *loginBtn;
///右边三角按钮
@property (nonatomic, strong) UIButton *nextBtn;
///我的订单视图
@property (nonatomic, strong) UIView *myOrderView;
///分类订单视图
@property (nonatomic, strong) UIView *orderView;
@property (nonatomic, strong) UITableViewCell *myOrderCell;
///已预订按钮
@property (nonatomic, strong) UIButton *accountPaidBtn;
///未付款按钮
@property (nonatomic, strong) UIButton *nonpaymentBtn;
///退款按钮
@property (nonatomic, strong) UIButton *refundBtn;
///已消费按钮
@property (nonatomic, strong) UIButton *usedBtn;
///收藏
@property (nonatomic, strong) UITableViewCell *collectCell;
@property (nonatomic, strong) UIView *collectView;
///钱包
@property (nonatomic, strong) UIView *walletView;
@property (nonatomic, strong) UITableViewCell *walletCell;
//更多
@property (nonatomic, strong) UIView *moreView;
@property (nonatomic, strong) UITableViewCell *moreCell;

@end

@implementation MyViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.tabBarController.tabBar.hidden = NO;
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    userDefault = [NSUserDefaults standardUserDefaults];
    userID = [userDefault objectForKey:@"ID"];
    [self initWithFrame];
}

- (void)viewWillDisappear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    [super viewWillDisappear:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initWithFrame];
}


- (void)initWithFrame{
    
    self.view.backgroundColor = [UIColor colorWithRed:224.0/255 green:225.0/255 blue:226.0/255 alpha:1.0];
    
    //顶部背景图片
    if (!_bgImgView) {
        _bgImgView = [[UIImageView alloc]init];
    }
    _bgImgView.userInteractionEnabled = YES;
    [self.view addSubview:_bgImgView];
    [_bgImgView setImage:[UIImage imageNamed:@"Personal_backgroundPic.jpg"]];
    [_bgImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view).with.insets(UIEdgeInsetsMake(0.0, 0.0, self.view.frame.size.height/7*5, 0.0));
    }];
    
    //添加手势
    UITapGestureRecognizer *tapGesTop = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(personDetailAction:)];
    [_bgImgView addGestureRecognizer:tapGesTop];
    
    //左边注册按钮
    if (!_registerBtn) {
        _registerBtn = [[UIButton alloc]init];
    }
    [_registerBtn setTitle:@"注册" forState:UIControlStateNormal];
    _registerBtn.tintColor = [UIColor whiteColor];
    [_bgImgView addSubview:_registerBtn];
    [_registerBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_bgImgView).offset(20.0);
        make.left.equalTo(_bgImgView).offset(0.0);
//        make.width.mas_equalTo(@70);
        make.width.mas_equalTo(@100);
        make.height.mas_equalTo(@40);
    }];
    [_registerBtn addTarget:self action:@selector(registerClickAction) forControlEvents:UIControlEventTouchUpInside];
    
    //右边 登录/退出 按钮
    if (!_loginBtn) {
        _loginBtn = [[UIButton alloc]init];
    }
    _loginBtn.tintColor = [UIColor whiteColor];
    [_bgImgView addSubview:_loginBtn];
    [_loginBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_bgImgView).offset(100.0);
        make.right.equalTo(_bgImgView).offset(-10.0);
        make.width.mas_equalTo(_registerBtn.mas_width);
        make.height.mas_equalTo(_registerBtn.mas_height);
    }];
    [_loginBtn addTarget:self action:@selector(loginClickAction:) forControlEvents:UIControlEventTouchUpInside];
    _loginBtn.backgroundColor = [UIColor redColor];
    if (userID != nil) {
        _registerBtn.hidden = YES;
        [_loginBtn setTitle:@"退出登录" forState:UIControlStateNormal];
        _loginBtn.tag = 1001;
    }else{
        [_loginBtn setTitle:@"立即登录" forState:UIControlStateNormal];
        _loginBtn.tag = 1000;
    }
    
    //用户头像按钮
    if (!_headImgBtn) {
        _headImgBtn = [[UIButton alloc]init];
    }
    _headImgBtn.layer.cornerRadius = SCREEN_WIDTH/5/2;
    _headImgBtn.layer.masksToBounds = YES;
    _headImgBtn.userInteractionEnabled = YES;
    if (userID == nil) {
        [_headImgBtn setBackgroundImage:[UIImage imageNamed:@"head.jpg"] forState:UIControlStateNormal];
    } else {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
        NSString *filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"headImage.jpg"];
        UIImage *img = [UIImage imageWithContentsOfFile:filePath];
        if (img == nil || img == NULL) {
            [_headImgBtn setBackgroundImage:[UIImage imageNamed:@"head.jpg"] forState:UIControlStateNormal];
        } else {
            [_headImgBtn setBackgroundImage:img forState:UIControlStateNormal];

        }
    }
    
    [_headImgBtn addTarget:self action:@selector(changeHeadImgAction) forControlEvents:UIControlEventTouchUpInside];
    [_bgImgView addSubview:_headImgBtn];
    [_headImgBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(_bgImgView).offset(-30.0);
        make.left.equalTo(_bgImgView).offset(20.0);
        make.height.mas_equalTo(SCREEN_WIDTH/5);
        make.width.mas_equalTo(SCREEN_WIDTH/5);
    }];
    //用户名
    if (!_nameLbl) {
        _nameLbl = [[UILabel alloc]init];
    }
    
    _nameLbl.textColor = [UIColor whiteColor];
    _nameLbl.font = [UIFont fontWithName:@"Helvetica-Bold" size:20];
    if (userID == nil) {
//        _nameLbl.text = @"用户名";
        _nameLbl.text = @"未登录";
    } else {
        _nameLbl.text = [userDefault objectForKey:@"NickName"];
    }
    [_bgImgView addSubview:_nameLbl];
    [_nameLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(_headImgBtn);
        make.left.equalTo(_headImgBtn.mas_right).offset(20);
        make.right.equalTo(_bgImgView.mas_right).offset(-40);
        make.height.mas_equalTo(@40);
    }];
    /*
    //右边箭头
    if (!_nextBtn) {
        _nextBtn = [[UIButton alloc]init];
        [_nameLbl addSubview:_nextBtn];
    }
    [_nextBtn setImage:[UIImage imageNamed:@"右箭头.png"] forState:UIControlStateNormal];
    [_nextBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(_nameLbl);
        make.right.equalTo(_nameLbl.mas_right).offset(-15);
        make.width.mas_equalTo(@10);
        make.height.mas_equalTo(@20);
    }];
     */
    //我的订单视图
    if (!_myOrderView) {
        _myOrderView = [[UIView alloc]init];
        [self.view addSubview:_myOrderView];
    }
    _myOrderView.backgroundColor = [UIColor whiteColor];
    _myOrderView.userInteractionEnabled = YES;
    [_myOrderView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_bgImgView.mas_bottom).offset(15.0);
        make.width.equalTo(self.view.mas_width);
        make.left.equalTo(self.view.mas_left);
        make.height.mas_equalTo(@44);
    }];
    
    if (!_myOrderCell) {
        _myOrderCell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:nil];
        [_myOrderView addSubview: _myOrderCell];
    }
    _myOrderCell.imageView.image = [UIImage imageNamed:@"order.png"];
    _myOrderCell.textLabel.text = @"我的订单";
    _myOrderCell.textLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:18];
    _myOrderCell.detailTextLabel.text = @"查看所有订单";
    _myOrderCell.detailTextLabel.font = [UIFont fontWithName:@"Helvetica" size:14];
    _myOrderCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    [_myOrderCell mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(_myOrderView).with.insets(UIEdgeInsetsMake(5.0, 15.0, 5.0, 0.0));
    }];
    
    //订单分类视图
    
    if (!_orderView) {
        _orderView = [[UIView alloc]init];
        [self.view addSubview:_orderView];
        [self createMyOrderView];
    }
    _orderView.backgroundColor = [UIColor whiteColor];
    [_orderView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_myOrderView.mas_bottom).offset(2.0);
        make.width.equalTo(self.view.mas_width);
        make.left.equalTo(self.view.mas_left);
        make.height.mas_equalTo(@80);
    }];
    //跳转到订单(查看所有)
    UITapGestureRecognizer *orderTapges = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(allOrderAction:)];
    [_myOrderView addGestureRecognizer:orderTapges];
    //已预订
    _accountPaidBtn.tag = 2001;
    [_accountPaidBtn addTarget:self action:@selector(orderClickAction:) forControlEvents:UIControlEventTouchUpInside];
    //未付款
    _nonpaymentBtn.tag  =2002;
    [_nonpaymentBtn addTarget:self action:@selector(orderClickAction:) forControlEvents:UIControlEventTouchUpInside];
    //退款
    _refundBtn.tag = 2003;
    [_refundBtn addTarget:self action:@selector(orderClickAction:) forControlEvents:UIControlEventTouchUpInside];
    //已消费
    _usedBtn.tag = 2004;
    [_usedBtn addTarget:self action:@selector(orderClickAction:) forControlEvents:UIControlEventTouchUpInside];
    //我的钱包
    
    if (!_walletView) {
        _walletView = [[UIView alloc]init];
        [self.view addSubview:_walletView];
    }
    _walletView.backgroundColor = [UIColor whiteColor];
    _walletView.userInteractionEnabled = YES;
    [_walletView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_orderView.mas_bottom).offset(10.0);
        make.width.equalTo(self.view.mas_width);
        make.left.equalTo(self.view.mas_left);
        make.height.mas_equalTo(@kWalletCellH);
    }];
    
    if (!_walletCell) {
        _walletCell = [[UITableViewCell alloc]init];
        [_walletView addSubview: _walletCell];
    }
    _walletCell.imageView.image = [UIImage imageNamed:@"money"];
    _walletCell.textLabel.text = @"钱包";
    [_walletCell mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(_walletView).with.insets(UIEdgeInsetsMake(15.0, 15.0, 15.0, 0.0));
    }];
    //添加手势
//    UITapGestureRecognizer *tapGesWallet = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(walletAction:)];
//    [_walletView addGestureRecognizer:tapGesWallet];
    //我的收藏
    
    if (!_collectView) {
        _collectView = [[UIView alloc]init];
        [self.view addSubview:_collectView];
    }
    _collectView.backgroundColor = [UIColor whiteColor];
    _collectView.userInteractionEnabled = YES;
    [_collectView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_walletView.mas_bottom).offset(10.0);
        make.width.equalTo(self.view.mas_width);
        make.left.equalTo(self.view.mas_left);
        make.height.mas_equalTo(@kWalletCellH);
    }];
    if (!_collectCell) {
        _collectCell = [[UITableViewCell alloc]init];
        [_collectView addSubview: _collectCell];
    }
    _collectCell.imageView.image = [UIImage imageNamed:@"collected.png"];
    _collectCell.textLabel.text = @"收藏";
    [_collectCell mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(_collectView).with.insets(UIEdgeInsetsMake(15.0, 15.0, 15.0, 0.0));
    }];
    
    //更多
    if (!_moreView) {
        _moreView = [[UIView alloc]init];
        [self.view addSubview:_moreView];
    }
    _moreView.backgroundColor = [UIColor whiteColor];
    _moreView.userInteractionEnabled = YES;
    [_moreView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_collectView.mas_bottom).offset(10.0);
        make.width.equalTo(self.view.mas_width);
        make.left.equalTo(self.view.mas_left);
        make.height.mas_equalTo(@kWalletCellH);
    }];
    if (!_moreCell) {
        _moreCell = [[UITableViewCell alloc]init];
        [_moreView addSubview: _moreCell];
    }
    _moreCell.imageView.image = [UIImage imageNamed:@"collection.png"];
    _moreCell.textLabel.text = @"更多";
    [_moreCell mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(_moreView).with.insets(UIEdgeInsetsMake(15.0, 15.0, 15.0, 0.0));
    }];
    
    //添加手势
    UITapGestureRecognizer *tapGesCollect = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(collectAction:)];
    [_collectView addGestureRecognizer:tapGesCollect];
    
    UITapGestureRecognizer *tapGesMore = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(moreAction:)];
    [_moreView addGestureRecognizer:tapGesMore];
}

- (void)createMyOrderView{
    
    CGFloat btnWidth = self.view.frame.size.width/4;
    CGSize btnSize = CGSizeMake(btnWidth, 60);
    //已付款
    if (!_accountPaidBtn) {
        _accountPaidBtn = [[UIButton alloc]init];
        [_orderView addSubview:_accountPaidBtn];
    }
    [_accountPaidBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(btnSize);
        make.leading.equalTo(_orderView).offset(0);
        make.top.equalTo(_orderView.mas_top).offset(0);
    }];
    //未付款
    if (!_nonpaymentBtn) {
        _nonpaymentBtn = [[UIButton alloc]init];
        [_orderView addSubview:_nonpaymentBtn];
    }
    [_nonpaymentBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(btnSize);
//        make.centerY.equalTo(_orderView.mas_centerY);
        make.top.equalTo(_orderView.mas_top).offset(0);
        make.left.equalTo(_accountPaidBtn.mas_right).offset(0);
    }];
    //退款
    if (!_refundBtn) {
        _refundBtn = [[UIButton alloc]init];
        [_orderView addSubview:_refundBtn];
    }
    [_refundBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(btnSize);
        make.top.equalTo(_orderView.mas_top).offset(0);
        make.left.equalTo(_nonpaymentBtn.mas_right).offset(0);
    }];
    //已消费
    if (!_usedBtn) {
        _usedBtn = [[UIButton alloc] init];
        [_orderView addSubview:_usedBtn];
    }
    [_usedBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(btnSize);
        make.top.equalTo(_orderView.mas_top).offset(0);
        make.left.equalTo(_refundBtn.mas_right).offset(0);
    }];
    
    NSArray *btnArray = @[_accountPaidBtn,_nonpaymentBtn,_refundBtn,_usedBtn];
    NSArray *imgArray = @[@"paid.png",@"payment.png",@"refund.png",@"paid.png"];
    NSArray *strArray = @[@"已预定",@"未付款",@"退款",@"已消费"];
    for (int i = 0;i < 4;i++) {
        UIButton *btn = [btnArray objectAtIndex:i];
        [btn setImage:[UIImage imageNamed:[imgArray objectAtIndex:i]] forState:UIControlStateNormal];
        [btn setImageEdgeInsets:UIEdgeInsetsMake(10, (btnSize.width-40)/2, 10, (btnSize.width-40)/2)];
        
        UILabel *rightLbl = [[UILabel alloc]init];
        rightLbl.text = [strArray objectAtIndex:i];
        rightLbl.font = [UIFont systemFontOfSize:14];
        [_orderView addSubview:rightLbl];
        [rightLbl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(btn.mas_bottom).offset(0);
            make.centerX.equalTo(btn.mas_centerX);
        }];
    }
}

#pragma mark - Action
- (void)registerClickAction{
    RegisterViewController *registerV = [self.storyboard instantiateViewControllerWithIdentifier:@"register"];
    [self.navigationController pushViewController:registerV animated:YES];
}
#pragma mark - 退出
- (void)loginClickAction:(UIButton *)btn {
    
    if (btn.tag == 1000) {
        LoginViewController *login = [self.storyboard instantiateViewControllerWithIdentifier:@"login"];
        [self.navigationController pushViewController:login animated:YES];
    } else {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"提示" message:@"您真的要退出吗？" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
        UIAlertAction *logoutAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
            NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
            [user removeObjectForKey:@"HeadIcon"];
            [user removeObjectForKey:@"ID"];
            [user removeObjectForKey:@"Money"];
            [user removeObjectForKey:@"Mobile"];
            [user removeObjectForKey:@"NickName"];
            [user removeObjectForKey:@"Password"];
            [user removeObjectForKey:@"Region"];
            [user removeObjectForKey:@"Sex"];
            [user removeObjectForKey:@"UserName"];
            [user synchronize];
            self.registerBtn.hidden = NO;
            [_loginBtn setTitle:@"立即登录" forState:UIControlStateNormal];
            [_loginBtn setTag:1000];
            self.nameLbl.text = @"用户名";
            [_headImgBtn setBackgroundImage:[UIImage imageNamed:@"head.jpg"] forState:UIControlStateNormal];
            
        }];
        [alertController addAction:logoutAction];
        [alertController addAction:cancelAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }
    
}
#pragma mark - 更换头像
- (void)changeHeadImgAction{
    if (userID == nil || _loginBtn.tag == 1000) {
        LoginViewController *login = [self.storyboard instantiateViewControllerWithIdentifier:@"login"];
        [self.navigationController pushViewController:login animated:YES];
    } else {
        _imagePickerController = [[UIImagePickerController alloc] init];
        _imagePickerController.delegate = self;
        _imagePickerController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        _imagePickerController.allowsEditing = YES;
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"设置头像" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
        //拍照
        UIAlertAction *cameraAction = [UIAlertAction actionWithTitle:@"拍照" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            _imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
        //设置摄像头模式（拍照）
            _imagePickerController.cameraCaptureMode = UIImagePickerControllerCameraCaptureModePhoto;
            [self presentViewController:_imagePickerController animated:YES completion:nil];
        }];
        //从相册获取
        UIAlertAction *albumAction = [UIAlertAction actionWithTitle:@"从相册获取" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            _imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        //设置选择后的图片可被编辑
            _imagePickerController.allowsEditing = YES;
            [self presentViewController:_imagePickerController animated:YES completion:nil];
        }];
        [alertController addAction:cancelAction];
        [alertController addAction:cameraAction];
        [alertController addAction:albumAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }
}
- (void)personDetailAction:(UITapGestureRecognizer *)param{
    if (userID == nil || _loginBtn.tag == 1000) {
        LoginViewController *login = [self.storyboard instantiateViewControllerWithIdentifier:@"login"];
        [self.navigationController pushViewController:login animated:YES];
    } else{
        PersonalTableViewController *personal = [self.storyboard instantiateViewControllerWithIdentifier:@"personal"];
        UIBarButtonItem *bar = [[UIBarButtonItem alloc] initWithTitle:@"我的" style:0 target:nil action:nil];
        [self.navigationItem setBackBarButtonItem:bar];
        [self.navigationController pushViewController:personal animated:YES];
    }
    
}

#pragma mark---收藏Action
//点击收藏进入下个页面
- (void)collectAction:(UITapGestureRecognizer *)param{
    if (userID == nil || _loginBtn.tag == 1000) {
        LoginViewController *login = [self.storyboard instantiateViewControllerWithIdentifier:@"login"];
        [self.navigationController pushViewController:login animated:YES];
    } else {
        //我的收藏
        CollectionTableViewController *collection = [self.storyboard instantiateViewControllerWithIdentifier:@"collectionView"];
        UIBarButtonItem *bar = [[UIBarButtonItem alloc] initWithTitle:@"我的" style:0 target:nil action:nil];
        [self.navigationItem setBackBarButtonItem:bar];
        [self.navigationController pushViewController:collection animated:YES];
    }
    
}
#pragma mark - 更多Action
-(void)moreAction:(UIGestureRecognizer *)param
{
    MoreTableViewController *more = [self.storyboard instantiateViewControllerWithIdentifier:@"moreView"];
    UIBarButtonItem *bar = [[UIBarButtonItem alloc] initWithTitle:@"我的" style:0 target:nil action:nil];
    [self.navigationItem setBackBarButtonItem:bar];
    [self.navigationController pushViewController:more animated:YES];
    
}
- (void)walletAction:(UITapGestureRecognizer *)param{
    if (userID == nil || _loginBtn.tag == 1000) {
        LoginViewController *login = [self.storyboard instantiateViewControllerWithIdentifier:@"login"];
        [self.navigationController pushViewController:login animated:YES];
    } else {
        //我的钱包
//        WalletViewController *order = [self.storyboard instantiateViewControllerWithIdentifier:@"walletView"];
        WalletViewController *wallet = [[WalletViewController alloc] init];
        [self.navigationController pushViewController:wallet animated:YES];
    }
    
}
#pragma mark - 全部订单Action
- (void)allOrderAction:(UITapGestureRecognizer *)param{
    if (userID == nil || _loginBtn.tag == 1000) {
        LoginViewController *login = [self.storyboard instantiateViewControllerWithIdentifier:@"login"];
        [self.navigationController pushViewController:login animated:YES];
    } else {
        OrderTableViewController *order = [[OrderTableViewController alloc]init];
        order.orderClassify = @"全部订单";
        [self.navigationController pushViewController:order animated:YES];
    }
    
}
#pragma mark - 已付款。未付款。退款。已消费
- (void)orderClickAction:(UIButton *)btn{
    if (userID == nil || _loginBtn.tag == 1000) {
        LoginViewController *login = [self.storyboard instantiateViewControllerWithIdentifier:@"login"];
        [self.navigationController pushViewController:login animated:YES];
    } else {
        OrderTableViewController *order = [[OrderTableViewController alloc]init];
        if (btn.tag == 2001) {
            order.orderClassify = @"已付款";
        } else if (btn.tag == 2002) {
            order.orderClassify = @"未付款";
        } else if (btn.tag == 2003) {
            order.orderClassify = @"退款中";
        } else if (btn.tag == 2004){
            order.orderClassify = @"已消费";
        }
        [self.navigationController pushViewController:order animated:YES];
    }
}
#pragma mark UIImagePickerControllerDelegate
//选取原始图片
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    //从字典key获取image的地址
    UIImage *oriaineImage = info[UIImagePickerControllerEditedImage];
    //压缩图片
    UIImage *scaleImage = [self scaleImage:oriaineImage toScale:0.3];
    
    [self changeImg:scaleImage];
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (UIImage *)scaleImage:(UIImage *)image toScale:(float)scaleSize{
    UIGraphicsBeginImageContext(CGSizeMake(image.size.width*scaleSize, image.size.height*scaleSize));
    [image drawInRect:CGRectMake(0, 0, image.size.width*scaleSize, image.size.height*scaleSize)];
    UIImage *scaleImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return scaleImage;
}
- (void)changeImg:(UIImage *)image2{
    [SVProgressHUD show];
    NSData *data;
    if (UIImagePNGRepresentation(image2) == nil) {
        data = UIImageJPEGRepresentation(image2, 1.0);
    } else {
        data = UIImagePNGRepresentation(image2);
    }
    NSString * DocumentsPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    [fileManager createDirectoryAtPath:DocumentsPath withIntermediateDirectories:YES attributes:nil error:nil];
    [fileManager createFileAtPath:[DocumentsPath stringByAppendingString:@"/headImage.jpg"] contents:data attributes:nil];
    NSString *strImg = [data base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    NSString *baseString = [strImg stringByReplacingOccurrencesOfString:@"+" withString:@"%2B"];
    NSString *strUrl = [NSString stringWithFormat:@"%@Reg/UploadImg",BASICURL];
    NSDictionary *dic = @{@"Key":userID,@"Value":baseString};
    [BingNetWorkConnection connectionWithUrl:strUrl type:BINGO_REQUEST_POST params:dic FinishBlock:^(id responseObject) {
        [SVProgressHUD dismiss];
        NSString *string  = [[NSString alloc]initWithData:responseObject encoding:NSUTF8StringEncoding];
        if ([string isEqualToString:@"[上传成功]"]) {
            NSString *strUrl = [NSString stringWithFormat:@"%@/User/GetUserInfo?userID=%@",BASICURL,userID];
            [BingNetWorkConnection connectionWithUrl:strUrl type:BINGO_REQUEST_GET params:nil FinishBlock:^(id responseObject) {
                NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers | NSJSONReadingMutableLeaves error:nil];
                [userDefault removeObjectForKey:@"HeadICon"];
                [userDefault setObject:[dictionary objectForKey:@"HeadIcon"] forKey:@"HeadIcon"];
                [userDefault synchronize];
                NSLog(@"%@",dictionary);
            } FailedBlock:^(NSError *error) {
                NSLog(@"查询信息：%@",error);
            }];
        }
        
        
    } FailedBlock:^(NSError *error) {
        NSLog(@"上传头像：%@",error);
    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
