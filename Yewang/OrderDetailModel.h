//
//  OrderDetailModel.h
//  Yewang
//
//  Created by everhelp on 16/10/8.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OrderDetailModel : NSObject
///商户名称
@property (nonatomic, copy) NSString *tradeName;
///套餐/包间名称
@property (nonatomic, copy) NSString *productName;
///酒（数组）
@property (nonatomic, copy) NSArray *wineArray;
///到达时间
@property (nonatomic, copy) NSString *arrivalTime;
///联系人
@property (nonatomic, copy) NSString *contactPerson;
///联系电话
@property (nonatomic, copy) NSString *contactNumber;
///卡券
@property (nonatomic, copy) NSString *coupon;
///是否是在线支付
@property (nonatomic, copy) NSString *isPayOnline;
///备注
@property (nonatomic, copy) NSString *remark;
@end
