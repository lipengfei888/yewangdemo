//
//  OrderListInfo.h
//  Yewang_Business
//
//  Created by everhelp on 16/3/31.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OrderListInfo : NSObject
///到达时间
@property (copy, nonatomic) NSString *ArriveTime;
///商家ID
@property (copy, nonatomic) NSString *ClubID;
///商家名称
@property (copy, nonatomic) NSString *ClubName;
///联系人（经理）电话
@property (copy, nonatomic) NSString *ContactNumber;
///联系人（经理）
@property (copy, nonatomic) NSString *ContactPerson;
///订单ID
@property (copy, nonatomic) NSString *ID;
///包间（套餐）ID
@property (copy, nonatomic) NSString *ItemID;
///包间（套餐）名称
@property (copy, nonatomic) NSString *ItemName;
///单价
@property (copy, nonatomic) NSString *Money;
///数量
@property (copy, nonatomic) NSString *Mount;
///订单编号
@property (copy, nonatomic) NSString *OrderNumber;
///下单时间
@property (copy, nonatomic)NSString *OrderTime;
///付款方式
@property (copy, nonatomic)NSString *PayMethod;
///验证码
@property (copy, nonatomic)NSString *PaymentCode;
///应付价格（单价*数量）
@property (copy, nonatomic) NSString *Price;
///订单状态
@property (copy, nonatomic)NSString *Status;
///商家上传的账单
@property (copy, nonatomic)NSString *SupplierBill;
///实际支付价格（单价*数量+优惠条件）
@property (copy, nonatomic) NSString *Total;
///用户上传账单
@property (copy, nonatomic)NSString *UserBill;
///用户ID
@property (copy, nonatomic)NSString *UserID;
///用户名称
@property (copy, nonatomic)NSString *UserName;
///用户是否删除此订单
@property (copy, nonatomic)NSString *IsDeleted;

@end
