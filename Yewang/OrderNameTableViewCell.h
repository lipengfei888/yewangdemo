//
//  OrderNameTableViewCell.h
//  Yewang
//
//  Created by everhelp on 16/10/9.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderNameTableViewCell : UITableViewCell
///商户名称(UILabel)
@property (nonatomic, weak) IBOutlet UILabel *tradeNameLbl;
///套餐、包间名称
@property (nonatomic, weak) IBOutlet UILabel *packageNameLbl;
///套餐、包间数量
@property (nonatomic, weak) IBOutlet UILabel *packageNumberLbl;
///套餐、包间价格
@property (nonatomic, weak) IBOutlet UILabel *packagePriceLbl;
@end
