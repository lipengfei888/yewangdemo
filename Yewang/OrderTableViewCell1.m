//
//  OrderTableViewCell1.m
//  Yewang
//
//  Created by everhelp on 16/11/23.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import "OrderTableViewCell1.h"
#define kMargin 15
#define kLabelH 20
#define kLabelW 50
#define kFont 14
@implementation OrderTableViewCell1

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        _label1 = [[UILabel alloc] initWithFrame:CGRectMake(kMargin, kMargin, 100, kLabelH)];
        _label1.text = @"套餐A";
        [self.contentView addSubview:_label1];
        
        _label2 = [[UILabel alloc] initWithFrame:CGRectMake(kMargin, kMargin+CGRectGetMaxY(_label1.frame), SCREEN_WIDTH - kMargin, kLabelH)];
        _label2.text = @"66666666666666";
        _label2.textColor = [UIColor grayColor];
        _label2.font = [UIFont systemFontOfSize:kFont];
        [self.contentView addSubview:_label2];
        
        _label4 = [[UILabel alloc] initWithFrame:CGRectMake(SCREEN_WIDTH - kLabelW, kMargin, kLabelW, kLabelH)];
        _label4.text = @"222";
        _label4.textColor = [UIColor redColor];
        _label4.font = [UIFont systemFontOfSize:kFont];
        [self.contentView addSubview:_label4];

        _label3 = [[UILabel alloc] initWithFrame:CGRectMake(SCREEN_WIDTH - 2*kLabelW, kMargin, kLabelW, kLabelH)];
        _label3.text = @"x1";
        _label3.textColor = [UIColor redColor];
        _label3.font = [UIFont systemFontOfSize:kFont];
        [self.contentView addSubview:_label3];

    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
