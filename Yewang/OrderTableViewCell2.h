//
//  OrderTableViewCell2.h
//  Yewang
//
//  Created by everhelp on 16/11/23.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderTableViewCell2 : UITableViewCell
@property(nonatomic, strong) UILabel *nameLabel, *countLabel, *priceLabel;
@end
