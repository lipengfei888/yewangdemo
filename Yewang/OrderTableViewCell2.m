//
//  OrderTableViewCell2.m
//  Yewang
//
//  Created by everhelp on 16/11/23.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import "OrderTableViewCell2.h"
#define kMargin 15
#define kLabelH 20
#define kLabelW 50
#define kFont 14
@implementation OrderTableViewCell2

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        _nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(kMargin, kMargin, kLabelW, kLabelH)];
        _nameLabel.text = @"666";
        [self.contentView addSubview:_nameLabel];
        
        _countLabel = [[UILabel alloc] initWithFrame:CGRectMake(SCREEN_WIDTH-2*kLabelW, kMargin, kLabelW, kLabelH)];
        _countLabel.text = @"x1";
        _countLabel.textColor = [UIColor redColor];
        [self.contentView addSubview:_countLabel];
        
        _priceLabel = [[UILabel alloc] initWithFrame:CGRectMake(SCREEN_WIDTH-kLabelW, kMargin, kLabelW, kLabelH)];
        _priceLabel.text = @"100";
        _priceLabel.textColor = [UIColor redColor];
        [self.contentView addSubview:_priceLabel];
    }
    return self;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
