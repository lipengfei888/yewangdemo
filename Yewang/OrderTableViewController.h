//
//  OrderTableViewController.h
//  Yewang
//
//  Created by everhelp on 16/7/19.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderTableViewController : UITableViewController

@property (nonatomic, strong) NSString *orderClassify;

@end
