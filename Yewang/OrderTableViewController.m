//
//  OrderTableViewController.m
//  Yewang
//
//  Created by everhelp on 16/7/19.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import "OrderTableViewController.h"
#import "OrderListInfo.h"
#import "OrderTableViewCell.h"
#import "MyOrderTableViewController.h"
#import "CompletedTableViewCell.h"

@interface OrderTableViewController (){
    NSMutableArray *_array;
    NSString *userID;
    UIImageView *bgImgView;
}

@end

@implementation OrderTableViewController
- (void)viewWillAppear:(BOOL)animated{
    self.tabBarController.tabBar.hidden = YES;
    [self initWithData:_orderClassify];
    [super viewWillAppear:animated];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.rightBarButtonItem = self.editButtonItem;
    if ([_orderClassify isEqualToString:@"已付款"]) {
        self.navigationItem.title = @"已预订";
    } else {
        self.navigationItem.title = self.orderClassify;
    }
    //设置统一的返回按钮
    self.navigationItem.backBarButtonItem= [[UIBarButtonItem alloc]initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleDefault;
    //注册单元格
    [self.tableView registerNib:[UINib nibWithNibName:@"OrderTableViewCell" bundle:nil] forCellReuseIdentifier:@"order"];
    [self.tableView registerClass:[CompletedTableViewCell class] forCellReuseIdentifier:@"CompletedTableViewCell"];
    //去除下面多余的cell
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    //去掉cell分割线
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    userID = [userDefault objectForKey:@"ID"];
    
    [self initWithData:_orderClassify];
    [self initMyMJRefresh];
    
}
- (void)initMyMJRefresh{
    // 设置回调（一旦进入刷新状态，就调用target的action，也就是调用self的dataByMJRefreshHeader方法）
    MJRefreshGifHeader *header = [MJRefreshGifHeader headerWithRefreshingBlock:^{
        
        [self performSelector:@selector(initWithData:) withObject:_orderClassify afterDelay:1.5];
        
    }];
    // 设置header
    self.tableView.mj_header = header;
}
//停止刷新
- (void)endRefresh{
    [self.tableView.mj_header endRefreshing];
}
- (void)initWithData:(NSString *)strStatus{
    
    if ([strStatus isEqualToString:@"全部订单"]) {
        
        NSString *strUrl = [NSString stringWithFormat:@"%@Order/GetOrderViewList?UserID=%@",BASICURL,userID];
        [SVProgressHUD showWithStatus:@"加载中，请稍候..."];
        [BingNetWorkConnection connectionWithUrl:strUrl type:BINGO_REQUEST_GET params:nil FinishBlock:^(id responseObject) {
            
            [SVProgressHUD dismiss];
            [self endRefresh];
            
            NSArray *arr = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:nil];
            if (arr.count==0) {
                if (bgImgView == nil) {
                    bgImgView = [[UIImageView alloc]init];
                    [bgImgView setImage:[UIImage imageNamed:@"notFound"]];
                    [bgImgView setContentMode:UIViewContentModeScaleAspectFit];
                    [self.view addSubview:bgImgView];
                    [bgImgView mas_makeConstraints:^(MASConstraintMaker *make) {
                        make.centerX.equalTo(self.view.mas_centerX);
                        make.top.equalTo(self.view.mas_top).offset(SCREEN_HEIGHT/5);
                        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH/2, SCREEN_HEIGHT/3));
                    }];
                    [self.tableView setScrollEnabled:NO];
                }
            } else {
                if (bgImgView) {
                    [bgImgView removeFromSuperview];
                    [self.tableView setScrollEnabled:YES];
                }
            }
            _array = [[NSMutableArray alloc]initWithCapacity:0];
            if (_array.count>0) {
                [_array removeAllObjects];
            }
            for (int i = 0; i < arr.count; i ++) {
          
                
                NSDictionary *dic = [arr objectAtIndex:i];
                OrderListInfo *order = [OrderListInfo mj_objectWithKeyValues:dic];
                [_array addObject:order];
            }
            
            //刷新表格
            [self.tableView reloadData];
            
        } FailedBlock:^(NSError *error) {
            NSLog(@"%@",error);
        }];
    } else {
        NSString *strUrl = [NSString stringWithFormat:@"%@Order/QueryOrderViewList",BASICURL];
        NSDictionary *dic = @{@"Key":userID,@"Value":strStatus};
        
        [SVProgressHUD showWithStatus:@"加载中，请稍候..."];
        
        [BingNetWorkConnection connectionWithUrl:strUrl type:BINGO_REQUEST_POST params:dic FinishBlock:^(id responseObject) {
            
            [SVProgressHUD dismiss];
            [self endRefresh];
            
            NSArray *arr = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:nil];
            if (arr.count==0) {
                if (bgImgView == nil) {
                    bgImgView = [[UIImageView alloc]init];
                    [bgImgView setImage:[UIImage imageNamed:@"notFound"]];
                    [bgImgView setContentMode:UIViewContentModeScaleAspectFit];
                    [self.view addSubview:bgImgView];
                    [bgImgView mas_makeConstraints:^(MASConstraintMaker *make) {
                        make.centerX.equalTo(self.view.mas_centerX);
                        make.top.equalTo(self.view.mas_top).offset(SCREEN_HEIGHT/5);
                        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH/2, SCREEN_HEIGHT/3));
                    }];
                    [self.tableView setScrollEnabled:NO];
                }
            } else {
                if (bgImgView) {
                    [bgImgView removeFromSuperview];
                    [self.tableView setScrollEnabled:YES];
                }
            }
            _array = [[NSMutableArray alloc]initWithCapacity:0];
            if (_array.count>0) {
                [_array removeAllObjects];
            }
            for (int i = 0; i < arr.count; i ++) {
                NSDictionary *dic = [arr objectAtIndex:i];
                OrderListInfo *order = [OrderListInfo mj_objectWithKeyValues:dic];
                [_array addObject:order];
            }
            //刷新表格
            [self.tableView reloadData];
            
        } FailedBlock:^(NSError *error) {
            NSLog(@"%@",error);
        }];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (_array.count>0) {
        return [_array count];
    } else {
        return 0;
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
//    if ([_orderClassify isEqualToString:@"已消费"]) {
//        CompletedTableViewCell *cell;
//        if (cell == nil) {
//            cell = (CompletedTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"CompletedTableViewCell" forIndexPath:indexPath];
//        }
//        if (_array.count>0) {
//            OrderListInfo *order = [_array objectAtIndex:indexPath.row];
//            [cell showData:order selectBlock:^(NSInteger index) {
//                NSLog(@"lsdhgioghoeigrsioorgorgorighighlkdgndflguero");
//            }];
//        }
//        return cell;
//    } else {
        OrderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"order" forIndexPath:indexPath];
        cell.clickBtn.layer.cornerRadius = 5.0;
        cell.clickBtn.layer.masksToBounds = YES;
        [cell.clickBtn addTarget:self action:@selector(clickCheckBtn:) forControlEvents:UIControlEventTouchUpInside];
        cell.clickBtn.tag = indexPath.row;
        
        if (_array.count>0) {
            OrderListInfo *order = [_array objectAtIndex:indexPath.row];
            cell.clubNmaeLbl.text = order.ClubName;
            cell.itemNameLbl.text = order.ItemName;
            cell.paymentLbl.text = order.PayMethod;
            cell.statusLbl.text = order.Status;
            NSString *dateStr = [[order.OrderTime substringFromIndex:6]substringToIndex:13];
            NSDate *date = [NSDate dateWithTimeIntervalSince1970:([dateStr longLongValue]/1000)];
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
            [dateFormatter setDateFormat:@"yyyy年MM月dd日 HH:mm:ss"];
            NSString *orderTime = [dateFormatter stringFromDate:date];
            cell.timeLbl.text = orderTime;
            cell.clickBtn.tag = indexPath.row;
        }
        return cell;
//    }
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //选中行后去掉背景颜色
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    UIStoryboard *mainStoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    MyOrderTableViewController *myOrder = [mainStoryBoard instantiateViewControllerWithIdentifier:@"myorder"];
    OrderListInfo *order = [_array objectAtIndex:indexPath.row];
    myOrder.order = order;
    [self.navigationController pushViewController:myOrder animated:YES];
}
- (void)clickCheckBtn:(UIButton *)button{
    UIStoryboard *mainStoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    MyOrderTableViewController *myOrder = [mainStoryBoard instantiateViewControllerWithIdentifier:@"myorder"];
    OrderListInfo *order = [_array objectAtIndex:button.tag];
    myOrder.order = order;
    [self.navigationController pushViewController:myOrder animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 100.0;
}

// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [self deleteOrder:indexPath.row];
        [_array removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }    
}

- (void)deleteOrder:(NSInteger)row{
    NSString *strUrl = [NSString stringWithFormat:@"%@Order/DelOrder",BASICURL];
    OrderListInfo *order = [_array objectAtIndex:row];
    NSDictionary *paramDic = @{@"Key":order.ID,@"Value":@0};
    [BingNetWorkConnection connectionWithUrl:strUrl type:BINGO_REQUEST_POST params:paramDic FinishBlock:^(id responseObject) {
        NSLog(@"%@",[[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding]);
    } FailedBlock:^(NSError *error) {
        NSLog(@"删除订单：%@",error);
    }];

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
