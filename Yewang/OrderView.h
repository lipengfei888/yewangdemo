//
//  OrderView.h
//  Yewang
//
//  Created by everhelp on 16/10/10.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BadgeView.h"
#import "SelectedListView.h"
#import "OverlayView.h"

typedef void(^SelectedBtnBlock)(UIButton *btn);

@interface OrderView : UIView
///按钮--订单图片
@property (nonatomic, strong) UIButton *orderImgBtn;
///价格标签
@property (nonatomic, strong) UILabel *totalPriceLbl;
///价格-double
@property (nonatomic, assign) CGFloat totalPrice;
///去支付按钮
@property (nonatomic, strong) UIButton *payBtn;
///订单按钮上的数字badge
@property (nonatomic, strong) BadgeView *badge;
///badge-number
@property (nonatomic, assign) NSInteger badgeValue;
///选择的订单列表
@property (nonatomic, strong) SelectedListView *listView;
///背景图层
@property (nonatomic, strong) UIView *parentView;
///遮挡视图
@property (nonatomic, strong) OverlayView *overlayView;
///是否打开订单列表
@property (nonatomic, assign) BOOL open;

///初始化block
@property (nonatomic, copy) SelectedBtnBlock btnBlock;

- (instancetype)initWithFrame:(CGRect)frame inView:(UIView *)parentView;

- (void)setBadgeValue:(NSInteger)badgeValue;

- (void)setTotalPrice:(CGFloat)totalPrice;

- (void)dismissAnimated:(BOOL)animated;

- (void)updateFrame:(SelectedListView *)listView;

- (void)selectBlock:(SelectedBtnBlock)block;

@end
