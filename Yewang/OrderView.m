//
//  OrderView.m
//  Yewang
//
//  Created by everhelp on 16/10/10.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import "OrderView.h"
@interface OrderView ()

@end
@implementation OrderView
- (instancetype)initWithFrame:(CGRect)frame inView:(UIView *)parentView{
    if (self = [super initWithFrame:frame]) {
        
        self.parentView = parentView;
        
        self.backgroundColor = [UIColor darkGrayColor];
        //初始化订单图片按钮
        _orderImgBtn = [UIButton new];
        [_orderImgBtn setImage:[UIImage imageNamed:@"orderImg"] forState:UIControlStateNormal];
        _orderImgBtn.tag = 200;
        [self addSubview:_orderImgBtn];
        [self.orderImgBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.mas_left).offset(30);
            make.centerY.equalTo(self.mas_centerY);
            make.width.mas_equalTo(@25);
            make.height.mas_equalTo(@30);
        }];
        [self.orderImgBtn addTarget:self action:@selector(shoppingCartClick:) forControlEvents:UIControlEventTouchUpInside];
        //初始化价格标签
        _totalPriceLbl = [[UILabel alloc] init];
        _totalPriceLbl.textColor = [UIColor whiteColor];
        _totalPriceLbl.font = [UIFont systemFontOfSize:15.0];
        [self addSubview:_totalPriceLbl];
        [self.totalPriceLbl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.orderImgBtn.mas_right).offset(10);
            make.centerY.equalTo(self.mas_centerY);
            make.width.mas_equalTo(@100);
        }];
        //初始化去支付按钮
        _payBtn = [UIButton new];
        _payBtn.layer.borderColor = [[UIColor whiteColor] CGColor];
        _payBtn.layer.borderWidth = 1.0f;
        _payBtn.layer.cornerRadius = 6.0;
        _payBtn.layer.masksToBounds = YES;
        _payBtn.tag = 201;
        _payBtn.enabled = NO;
        [self addSubview:_payBtn];
        [self.payBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.mas_centerY);
            make.right.equalTo(self.mas_right).offset(-20);
            make.width.mas_equalTo(@60);
            make.height.mas_equalTo(@34);
        }];
        [self.payBtn addTarget:self action:@selector(clickBtn:) forControlEvents:UIControlEventTouchUpInside];
        [_payBtn setTitle:@"title" forState:UIControlStateNormal];
        [_totalPriceLbl setText:@"￥ 0"];
        
        //小圆点
        self.badge = [[BadgeView alloc] initWithFrame:CGRectMake(20, -3, 16, 16) withString:nil withTextColor:[UIColor whiteColor]];
        [self.orderImgBtn addSubview:_badge];
        self.badge.hidden = YES;
        
        //初始化背景视图和已添加商品列表
        int maxHeight = self.parentView.frame.size.height - 250;
        self.listView = [[SelectedListView alloc] initWithFrame:CGRectMake(0,self.parentView.bounds.size.height - maxHeight, SCREEN_WIDTH, maxHeight) withObjects:nil  canReorder:YES];
        
        self.overlayView = [[OverlayView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-30)];
        self.overlayView.orderView = self;
        [self.overlayView addSubview:self];
        [self.parentView addSubview:self.overlayView];
        self.overlayView.alpha = 0.0;
        self.open = NO;
        
    }
    return self;
}

- (void)setBadgeValue:(NSInteger)badgeValue{
    _badgeValue = badgeValue;
    self.badge.textLabel.text = [NSString stringWithFormat:@"%lu",badgeValue];
    if (badgeValue > 0) {
        self.badge.hidden = NO;
    } else {
        self.badge.hidden = YES;
    }
}

- (void)clickBtn:(UIButton *)btn{
    if (self.btnBlock != nil) {
        self.btnBlock(btn);
    }
}

- (void)selectBlock:(SelectedBtnBlock)block{
    self.btnBlock = block;
}

#pragma mark - 点击购物车
- (void)shoppingCartClick:(id)sender{
    if (self.badgeValue <= 0) {
        [self.orderImgBtn setUserInteractionEnabled:NO];
        return;
    }
    [self updateFrame:self.listView];
    [self.overlayView addSubview:self.listView];
    
    [UIView animateWithDuration:0.5 animations:^{
        
        self.overlayView.alpha = 1.0;
        
        
    } completion:^(BOOL finished) {
        self.open = YES;
    }];
    
}

#pragma mark - 计算价格
- (void)setTotalPrice:(CGFloat)totalPrice{
    _totalPrice = totalPrice;
    if (self.totalPrice > 0) {
        self.totalPriceLbl.font = [UIFont systemFontOfSize:18.0f weight:1];
        self.totalPriceLbl.text = [NSString stringWithFormat:@"￥ %.2f",totalPrice];
        self.payBtn.enabled = YES;
        self.payBtn.backgroundColor = kRGBColor;
    } else {
        self.totalPriceLbl.font = [UIFont systemFontOfSize:15.0];
        self.totalPriceLbl.text = @"￥ 0";
        self.totalPriceLbl.textColor = [UIColor whiteColor];
        self.payBtn.enabled = NO;
        self.payBtn.backgroundColor = [UIColor clearColor];
    }
}

#pragma mark - 更新ListView的高度
- (void)updateFrame:(SelectedListView *)listView{
    if (listView.objects.count == 0) {
        [self dismissAnimated:YES];
        return;
    }
    
    float height = 0;
    height = [listView.objects count]* 44;
    int  maxHeight = self.parentView.frame.size.height-250;
    if (height >= maxHeight) {
        height = maxHeight;
    }
    self.listView.frame = CGRectMake(self.listView.frame.origin.x, self.parentView.bounds.size.height - height - 44, SCREEN_WIDTH, height);
    
}

#pragma mark - dismiss
- (void)dismissAnimated:(BOOL)animated{
    [self.orderImgBtn bringSubviewToFront:self.overlayView];
    [UIView animateWithDuration:0.5 animations:^{
        _overlayView.alpha = 0.0;
    } completion:^(BOOL finished) {
        self.open = NO;
    }];
}

@end
