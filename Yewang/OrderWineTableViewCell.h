//
//  OrderWineTableViewCell.h
//  Yewang
//
//  Created by everhelp on 16/10/8.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderWineTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *wineNameLbl;
@property (weak, nonatomic) IBOutlet UILabel *wineNumberLbl;
@property (weak, nonatomic) IBOutlet UILabel *winePriceLbl;

@end
