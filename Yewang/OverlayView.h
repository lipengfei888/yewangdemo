//
//  OverlayView.h
//  MeiTuanWaiMai
//
//  Created by maxin on 16/1/5.
//  Copyright © 2016年 maxin. All rights reserved.
//

#import <UIKit/UIKit.h>
@class OrderView;

@interface OverlayView : UIView

@property (nonatomic,strong) OrderView *orderView;
@end
