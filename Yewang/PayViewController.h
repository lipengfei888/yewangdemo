//
//  PayViewController.h
//  Yewang
//
//  Created by everhelp on 16/11/24.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import <UIKit/UIKit.h>
@class OrderListInfo;
@class ShopCartView, DatePickerView, InformationView;
typedef void (^TimeBlock) (NSString *);
@interface PayViewController : UIViewController
//@property (nonatomic, strong) OrderListInfo *order1;
@property (nonatomic, strong) OrderListInfo *order;
@property (nonatomic, strong) UITableView *tableVC;
@property (nonatomic, strong) UIButton *timeButton;
@property (nonatomic,strong) ShopCartView *shopCartView;
@property (nonatomic, strong) NSString *totlePrice;
@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) UIButton *goPayButton;
@property (nonatomic, strong) DatePickerView *datePickerView;
@property (nonatomic, strong) InformationView *informationView;
@property (nonatomic, copy) TimeBlock timeBlock;
@end
