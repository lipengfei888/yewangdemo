//
//  PayViewController.m
//  Yewang
//
//  Created by everhelp on 16/11/24.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import "PayViewController.h"
#import "OrderTableViewCell1.h"
#import "OrderTableViewCell2.h"
#import "ShopCartView.h"
#import "DatePickerView.h"
#import "InformationView.h"
#import "ZYKeyboardUtil.h"
#import "DetailPayViewController.h"
#import "BuyDetailViewController.h"
@interface PayViewController ()<UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate, UITextFieldDelegate>
@property (strong, nonatomic) ZYKeyboardUtil *keyboardUtil;
@end

@implementation PayViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor colorWithWhite:0.9 alpha:1.0];
    self.navigationController.navigationBar.hidden = NO;
    self.title = @"确认订单";
    [self creatAllViews];
    [self configKeyBoardRespond];
}

- (void)creatAllViews
{
    _tableVC = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 350) style:UITableViewStylePlain];
    _tableVC.delegate = self;
    _tableVC.dataSource = self;
    [self.view addSubview:_tableVC];
    
    _informationView = [[InformationView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(_tableVC.frame)+20, SCREEN_WIDTH, 200)];
    _informationView.nameTF.delegate = self;
    [self.view addSubview:_informationView];
    
    UIView *goPayView = [[UIView alloc] initWithFrame:CGRectMake(0, SCREEN_HEIGHT-50, SCREEN_WIDTH, 50)];
    [self.view addSubview:goPayView];
    goPayView.backgroundColor = [UIColor colorWithWhite:0.3 alpha:1.0];
    
    UILabel *priceLabel = [[UILabel alloc] initWithFrame:CGRectMake(70, 10, SCREEN_WIDTH, 30)];
    priceLabel.text = self.totlePrice;
    priceLabel.textColor = [UIColor redColor];
    [goPayView addSubview:priceLabel];
    
    self.imageView = [[UIImageView alloc] initWithFrame:CGRectMake(20, 10, 26, 30)];
    self.imageView.image = [UIImage imageNamed:@"orderImg"];
    [goPayView addSubview:self.imageView];
    self.goPayButton = [UIButton buttonWithType:UIButtonTypeCustom];
    //    [self.goPayButton setBackgroundImage:[UIImage imageNamed:@"gouwuche"] forState:UIControlStateNormal];
    self.goPayButton.frame = CGRectMake(SCREEN_WIDTH - 100, 10, 80, 30);
    self.goPayButton.backgroundColor = [UIColor redColor];
    [self.goPayButton setTitle:@"去支付" forState:UIControlStateNormal];
    [self.goPayButton addTarget:self action:@selector(payAction) forControlEvents:UIControlEventTouchUpInside];
    self.goPayButton.layer.cornerRadius = 5;
    [goPayView addSubview:self.goPayButton];
    
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(keyboardHide:)];
    //设置成NO表示当前控件响应后会传播到其他控件上，默认为YES。
    tapGestureRecognizer.cancelsTouchesInView = YES;
    //将触摸事件添加到当前view
    [self.view addGestureRecognizer:tapGestureRecognizer];
    
}

-(void)ACtion
{
    _datePickerView.hidden = YES;
}

//去支付按钮
-(void)payAction
{
    //将我们的storyBoard实例化，“Main”为StoryBoard的名称
    UIStoryboard *mainStoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    BuyDetailViewController *buyDetail = [mainStoryBoard instantiateViewControllerWithIdentifier:@"buydetail"];
//    buyDetail.itemName = _order.ItemName;
//    buyDetail.price = _order.Price ;
    buyDetail.price = @"20" ;
//    buyDetail.itemID = _order.ItemID;
//    buyDetail.clubID = _order.ClubID;
//    buyDetail.total = _order.Total;
//    buyDetail.arriveTime = _order.ArriveTime;
//    buyDetail.tel = _order.ContactNumber;
//    buyDetail.itemNum = _order.Mount;
//    buyDetail.contactPerson = _order.ContactPerson;
//    buyDetail.orderID = _order.ID;
    [self.navigationController pushViewController:buyDetail animated:YES];

    
}
//回收键盘
-(void)keyboardHide:(UITapGestureRecognizer*)tap{
    [_informationView.nameTF resignFirstResponder];
    [_informationView.phoneNumberTF resignFirstResponder];
}

 - (void)shopCartClicked
{
    
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 1;
    }
    return 3;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    tableView.scrollEnabled = NO;
    static NSString *cellIdentifier1 = @"cell1";
    static NSString *cellIdentifier2 = @"cell2";
    static NSString *cellIdentifier3 = @"cell3";
    static NSString *cellIdentifier4 = @"cell4";
    if (indexPath.section == 0) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier1];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier1];
            cell.textLabel.text = @"商品名称";
        }
        return cell;
    } else if (indexPath.section == 1) {
            if (indexPath.row == 0) {
                OrderTableViewCell1 *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier2];
                if (cell == nil) {
                    cell = [[OrderTableViewCell1 alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier2];
                }
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                return cell;
                
            } else {
                OrderTableViewCell2 *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier3];
                if (cell == nil) {
                    cell = [[OrderTableViewCell2 alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier3];
                }
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                return cell;
            }
        }
            UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier4];
            cell.textLabel.text = @"66666666";
            //去除多余cell
            tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;

}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 1) {
        if (indexPath.row == 0) {
            return 100;
        }else {
            return 40;
        }
    }
    return 40;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 20;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //点击cell不变色
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

- (void)clickButton {
    //    NSLog(@"6666");
    self.datePickerView = [[DatePickerView alloc] initWithFrame:CGRectMake(0, SCREEN_HEIGHT - 200, SCREEN_WIDTH, 200)];
    self.datePickerView.backgroundColor = [UIColor colorWithWhite:0.9 alpha:1.0];
    [self.view addSubview:self.datePickerView];
    __weak typeof(self) weakSelf = self;
    self.timeBlock = ^(NSString *timeStr){
        
    weakSelf.timeButton.titleLabel.text = timeStr;
        
    };
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)configKeyBoardRespond {
    self.keyboardUtil = [[ZYKeyboardUtil alloc] init];
    __weak PayViewController *weakSelf = self;
    [_keyboardUtil setAnimateWhenKeyboardAppearAutomaticAnimBlock:^(ZYKeyboardUtil *keyboardUtil) {
        [keyboardUtil adaptiveViewHandleWithController:weakSelf adaptiveView:weakSelf.informationView.nameTF,weakSelf.informationView.phoneNumberTF, nil];
    }];
#pragma explain - 获取键盘信息
    [_keyboardUtil setPrintKeyboardInfoBlock:^(ZYKeyboardUtil *keyboardUtil, KeyboardInfo *keyboardInfo) {
        NSLog(@"\n\n拿到键盘信息 和 ZYKeyboardUtil对象");
    }];
}

#pragma mark delegate
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    return YES;
}

@end
