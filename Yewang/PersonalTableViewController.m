//
//  PersonalTableViewController.m
//  Yewang
//
//  Created by everhelp on 16/4/14.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import "PersonalTableViewController.h"
#import "ChangeTelViewController.h"
#import "AddressPickerDemo.h"
#import "ChangePwdTableViewController.h"

@interface PersonalTableViewController ()<UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIActionSheetDelegate>{
    //创建一个全局的UIImagePickerController对象
    UIImagePickerController *_imagePickerController;
    //修改昵称的全局变量
    NSUserDefaults *userDefault;
    NSString *userID;
}
@property (weak, nonatomic) IBOutlet UIImageView *headImgView;
@property (weak, nonatomic) IBOutlet UILabel *nickNameLbl;
@property (weak, nonatomic) IBOutlet UILabel *sexLbl;
@property (weak, nonatomic) IBOutlet UILabel *areaLbl;
@property (weak, nonatomic) IBOutlet UILabel *phoneNumLbl;

@end

@implementation PersonalTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    userDefault = [NSUserDefaults standardUserDefaults];
    userID = [userDefault objectForKey:@"ID"];
}

- (void)viewWillAppear:(BOOL)animated{
    [self initWithFrame];
    [self initWithData];
    
    [super viewWillAppear:animated];
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc addObserver:self
           selector:@selector(handleAddressChange:)
               name:@"addressPicker"
             object:nil];
    
}

-(void)handleAddressChange:(NSNotification *)sender{
    
    _areaLbl.text = [sender.userInfo objectForKey:@"City"];

}

- (void)initWithFrame{
    
    self.tabBarController.tabBar.hidden = YES;
    self.navigationItem.title = @"个人信息";
    self.tableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    _headImgView.layer.cornerRadius = 13;
    _headImgView.layer.masksToBounds = YES;
    
}

- (void)initWithData{
    NSString *strUrl = [NSString stringWithFormat:@"%@User/GetUserInfo?userID=%@",BASICURL,userID];
    [BingNetWorkConnection connectionWithUrl:strUrl type:BINGO_REQUEST_GET params:nil FinishBlock:^(id responseObject) {
        NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers | NSJSONReadingMutableLeaves error:nil];
       [_headImgView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",HEADICONURL,[dictionary objectForKey:@"HeadIcon"]]]placeholderImage:[UIImage imageNamed:@"head.jpg"] options:SDWebImageRefreshCached];
        _phoneNumLbl.text = [dictionary objectForKey:@"Mobile"];
        _nickNameLbl.text = [dictionary objectForKey:@"NickName"];
        _sexLbl.text = [dictionary objectForKey:@"Sex"];
        _areaLbl.text = [dictionary objectForKey:@"Region"];
        
    } FailedBlock:^(NSError *error) {
        NSLog(@"查询信息：%@",error);
    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    //选中行后去掉背景颜色
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if (indexPath.row == 0) {
        /*
         //修改头像
         */
        //创建UIImagePickerController对象
        _imagePickerController = [[UIImagePickerController alloc] init];
        _imagePickerController.delegate = self;
        _imagePickerController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        _imagePickerController.allowsEditing = YES;
        //选择拍照或是从相册获取
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"设置头像" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
        //拍照
        UIAlertAction *cameraAction = [UIAlertAction actionWithTitle:@"拍照" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            _imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
            //设置摄像头模式（拍照）
            _imagePickerController.cameraCaptureMode = UIImagePickerControllerCameraCaptureModePhoto;
            [self presentViewController:_imagePickerController animated:YES completion:nil];
            
        }];
        //从相册获取
        UIAlertAction *albumAction = [UIAlertAction actionWithTitle:@"从相册获取" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            _imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            [self presentViewController:_imagePickerController animated:YES completion:nil];
            
        }];
        
        [alertController addAction:cancelAction];
        [alertController addAction:cameraAction];
        [alertController addAction:albumAction];
        
        [self presentViewController:alertController animated:YES completion:nil];
    } else if (indexPath.row == 1) {
        /*
         //修改昵称
         */
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"更改昵称" message:nil preferredStyle:UIAlertControllerStyleAlert];
        //添加一个输入框
        [alertController addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
            
           textField.text = [userDefault objectForKey:@"NickName"];
        }];
        
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
        UIAlertAction *otherAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            NSString *strUrl = [NSString stringWithFormat:@"%@User/ModifyNickName",BASICURL];
            UITextField *nickName = alertController.textFields.firstObject;
            if ([[nickName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]length] == 0){
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:@"昵称不能为空，请输入昵称" preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleCancel handler:nil];
                [alert addAction:cancelAction];
                [self presentViewController:alert animated:YES completion:nil];
            } else {
                NSDictionary *dic = @{@"Key":userID,@"Value":nickName.text};
                [BingNetWorkConnection connectionWithUrl:strUrl type:BINGO_REQUEST_POST params:dic FinishBlock:^(id responseObject) {
                    NSString *string = [[NSString alloc]initWithData:responseObject encoding:NSUTF8StringEncoding];
                    
                    NSLog(@"%@",string);
                    [self showAlertController:string];
                    _nickNameLbl.text = nickName.text;
                    [userDefault removeObjectForKey:@"NickName"];
                    [userDefault setObject:nickName.text forKey:@"NickName"];
                    [userDefault synchronize];
                    
                } FailedBlock:^(NSError *error) {
                    NSLog(@"修改昵称：%@",error);
                }];
            }
            
        }];
        [alertController addAction:cancelAction];
        [alertController addAction:otherAction];
        [self presentViewController:alertController animated:YES completion:nil];
    } else if (indexPath.row == 2) {
        /*
         //更改性别
         */
        NSString *strUrl = [NSString stringWithFormat:@"%@User/ModifySex",BASICURL];
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"性别" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
        UIAlertAction *manAction = [UIAlertAction actionWithTitle:@"男" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            NSDictionary *dic = @{@"Key":userID,@"Value":@"男"};
            [BingNetWorkConnection connectionWithUrl:strUrl type:BINGO_REQUEST_POST params:dic FinishBlock:^(id responseObject) {
                NSString *string = [[NSString alloc]initWithData:responseObject encoding:NSUTF8StringEncoding];
                
                NSLog(@"%@",string);
                [self showAlertController:string];
                _sexLbl.text = @"男";
                [userDefault removeObjectForKey:@"Sex"];
                [userDefault setObject:@"男" forKey:@"Sex"];
                [userDefault synchronize];
                
            } FailedBlock:^(NSError *error) {
                NSLog(@"修改性别：%@",error);
            }];
            
        }];
        UIAlertAction *womanAction = [UIAlertAction actionWithTitle:@"女" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            NSDictionary *dic = @{@"Key":userID,@"Value":@"女"};
            [BingNetWorkConnection connectionWithUrl:strUrl type:BINGO_REQUEST_POST params:dic FinishBlock:^(id responseObject) {
                NSString *string = [[NSString alloc]initWithData:responseObject encoding:NSUTF8StringEncoding];
                
                NSLog(@"%@",string);
                [self showAlertController:string];
                _sexLbl.text = @"女";
                [userDefault removeObjectForKey:@"Sex"];
                [userDefault setObject:@"女" forKey:@"Sex"];
                [userDefault synchronize];
            } FailedBlock:^(NSError *error) {
                NSLog(@"修改性别：%@",error);
            }];
            
        }];
        
        [alertController addAction:cancelAction];
        [alertController addAction:manAction];
        [alertController addAction:womanAction];
        [self presentViewController:alertController animated:YES completion:nil];
    } else if (indexPath.row == 3) {
        /*
            修改地址
         */
        AddressPickerDemo *addressPickerDemo = [[AddressPickerDemo alloc] init];
        UINavigationController *navigation = [[UINavigationController alloc] initWithRootViewController:addressPickerDemo];
        [self presentViewController:navigation animated:YES completion:nil];
    } else if (indexPath.row == 4) {
        /*
            修改电话号码
         */
//        ChangeTelViewController *changeTel = [self.storyboard instantiateViewControllerWithIdentifier:@"changetel"];
//        [self.navigationController pushViewController:changeTel animated:YES];
    } else if (indexPath.row == 5) {
        /*
            修改密码
         */
        ChangePwdTableViewController *changePwd = [self.storyboard instantiateViewControllerWithIdentifier:@"changepwd"];
        [self.navigationController pushViewController:changePwd animated:YES];
    }
}

- (void)showAlertController:(NSString *)string{
    
    if ([string isEqualToString:@"修改成功"]) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"修改成功" message:@"" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleCancel handler:nil];
        [alertController addAction:action];
        [self presentViewController:alertController animated:YES completion:nil];
    } else {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"修改失败" message:@"" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleCancel handler:nil];
        [alertController addAction:action];
        [self presentViewController:alertController animated:YES completion:nil];
    }
    
}

#pragma mark UIImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    //从字典key获取image的地址
    UIImage *oriaineImage = info[UIImagePickerControllerEditedImage];
    //压缩图片
    UIImage *scaleImage = [self scaleImage:oriaineImage toScale:0.3];

    [self changeImg:scaleImage];
    [picker dismissViewControllerAnimated:YES completion:nil];
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (UIImage *)scaleImage:(UIImage *)image toScale:(float)scaleSize{
    UIGraphicsBeginImageContext(CGSizeMake(image.size.width*scaleSize, image.size.height*scaleSize));
    [image drawInRect:CGRectMake(0, 0, image.size.width*scaleSize, image.size.height*scaleSize)];
    UIImage *scaleImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return scaleImage;
}

- (void)changeImg:(UIImage *)image2{
    [SVProgressHUD show];
    NSData *data;
    if (UIImagePNGRepresentation(image2) == nil) {
        data = UIImageJPEGRepresentation(image2, 1.0);
    } else {
        data = UIImagePNGRepresentation(image2);
    }
    NSString * DocumentsPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    [fileManager createDirectoryAtPath:DocumentsPath withIntermediateDirectories:YES attributes:nil error:nil];
    [fileManager createFileAtPath:[DocumentsPath stringByAppendingString:@"/headImage.jpg"] contents:data attributes:nil];
    NSLog(@"%@",[[NSString alloc]initWithFormat:@"%@%@",DocumentsPath,  @"/headImage.jpg"]);
    NSString *strImg = [data base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];    NSString *baseString = [strImg stringByReplacingOccurrencesOfString:@"+" withString:@"%2B"];
    NSString *strUrl = [NSString stringWithFormat:@"%@Reg/UploadImg",BASICURL];
    NSDictionary *dic = @{@"Key":userID,@"Value":baseString};
    [BingNetWorkConnection connectionWithUrl:strUrl type:BINGO_REQUEST_POST params:dic FinishBlock:^(id responseObject) {
        [SVProgressHUD dismiss];
        NSString *string  = [[NSString alloc]initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSLog(@"上传头像===%@",string);
        
        NSString *strUrl = [NSString stringWithFormat:@"%@/User/GetUserInfo?userID=%@",BASICURL,userID];
        [BingNetWorkConnection connectionWithUrl:strUrl type:BINGO_REQUEST_GET params:nil FinishBlock:^(id responseObject) {
            NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers | NSJSONReadingMutableLeaves error:nil];
            [userDefault removeObjectForKey:@"HeadICon"];
            [userDefault setObject:[dictionary objectForKey:@"HeadIcon"] forKey:@"HeadIcon"];
            [userDefault synchronize];
            
            [_headImgView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",HEADICONURL,[dictionary objectForKey:@"HeadIcon"]]]placeholderImage:[UIImage imageNamed:@"head.jpg"] options:SDWebImageRefreshCached];
            
        } FailedBlock:^(NSError *error) {
            NSLog(@"查询信息：%@",error);
        }];
        
    } FailedBlock:^(NSError *error) {
        NSLog(@"上传头像：%@",error);
    }];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
