//
//  PreferentialPayViewController.h
//  Yewang
//
//  Created by everhelp on 16/8/2.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PreferentialPayViewController : UIViewController

@property (nonatomic, copy) NSString *clubID;
@property (nonatomic, copy) NSString *itemID;
@property (nonatomic, copy) NSString *clubName;
@property (nonatomic, copy) NSString *disCount;

@end
