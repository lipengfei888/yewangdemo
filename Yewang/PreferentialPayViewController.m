//
//  PreferentialPayViewController.m
//  Yewang
//
//  Created by everhelp on 16/8/2.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import "PreferentialPayViewController.h"
#import "AlipayHeader.h"
#import "DiscountNoteViewController.h"

@interface PreferentialPayViewController ()<UITextFieldDelegate>{
    ///消费总额
    UITextField *totalTxt;
    ///不享受折扣金额
    UIButton *noDiscountBtn;
    ///确认买单
    UIButton *submitBtn;
    ///折扣
    UILabel *discountMoneyLbl;
    ///实付金额
    UILabel *payAmountLbl;
    ///实付金额(double)
    double payAmountD;
    
}

@end

@implementation PreferentialPayViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationController.navigationBar.hidden = NO;
    self.navigationItem.title = _clubName;
    
    [self initWithFrame];
}

- (void)initWithFrame{
    [self.view setBackgroundColor:[UIColor colorWithRed:234.0/255 green:234.0/255 blue:234.0/255 alpha:1.0]];
    
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc]initWithTitle:@"优惠说明" style:UIBarButtonItemStylePlain target:self action:@selector(clickRightItem)];
    self.navigationItem.rightBarButtonItem = rightItem;
    
    UIView *topView = [[UIView alloc]initWithFrame:CGRectMake(10, 84, SCREEN_WIDTH-20,50)];
    [topView setBackgroundColor:[UIColor whiteColor]];
    topView.layer.cornerRadius = 3;
    topView.layer.masksToBounds = YES;
    [self.view addSubview:topView];
    UILabel *label1 = [[UILabel alloc]init];
    label1.text = @"消费总额：";
    label1.font = [UIFont fontWithName:@"Arial" size:15];
    [topView addSubview:label1];
    [label1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(topView.mas_centerY);
        make.left.equalTo(topView).offset(10);
        make.width.mas_equalTo(@80);
    }];
    //消费总额
    totalTxt = [[UITextField alloc] init];
    totalTxt.delegate = self;
    totalTxt.placeholder = @"输入到店消费总额";
    totalTxt.font = [UIFont fontWithName:@"Arial" size:15];
    totalTxt.textColor = [UIColor orangeColor];
    totalTxt.textAlignment = UIControlContentHorizontalAlignmentRight;
    totalTxt.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    [totalTxt addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [topView addSubview:totalTxt];
    [totalTxt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(topView.mas_centerY);
        make.right.equalTo(topView).offset(-5);
        make.left.equalTo(label1.mas_right).offset(30);
    }];
    //不优惠金额
    noDiscountBtn = [[UIButton alloc]init];
    noDiscountBtn.titleLabel.font = [UIFont fontWithName:@"Arial" size:13];
    [noDiscountBtn setTitle:@"输入不享优惠金额" forState:UIControlStateNormal];
    [noDiscountBtn setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];
    [self.view addSubview:noDiscountBtn];
    [noDiscountBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(topView.mas_left);
        make.top.equalTo(topView.mas_bottom).offset(5);
    }];
//    noDiscountBtn.hidden = YES;
    [noDiscountBtn addTarget:self action:@selector(clickNoDiscountBtn) forControlEvents:UIControlEventTouchUpInside];
    
    UIView *centerView = [[UIView alloc]init];
    centerView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:centerView];
    [centerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(self.view.mas_width);
        make.left.equalTo(self.view).offset(0);
        make.top.equalTo(noDiscountBtn.mas_bottom).offset(5);
        make.height.mas_equalTo(@44);
    }];
    //显示折扣是多少
    UILabel *discountLbl = [[UILabel alloc]init];
    discountLbl.text = [NSString stringWithFormat:@"%@折",_disCount];
    discountLbl.font = [UIFont fontWithName:@"Arial" size:15];
    discountLbl.backgroundColor = [UIColor whiteColor];
    [centerView addSubview:discountLbl];
    [discountLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(centerView.mas_centerY);
        make.left.equalTo(centerView).offset(10);
        make.width.mas_equalTo(@80);
    }];
    //折扣的金额
    discountMoneyLbl = [[UILabel alloc]init];
    discountMoneyLbl.text = @"￥ 0";
    discountMoneyLbl.font = [UIFont fontWithName:@"Arial" size:16];
    discountMoneyLbl.textColor = [UIColor orangeColor];
    discountMoneyLbl.textAlignment = UIControlContentHorizontalAlignmentRight;
    [centerView addSubview:discountMoneyLbl];
    [discountMoneyLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(centerView.mas_centerY);
        make.right.equalTo(centerView).offset(-5);
        make.left.equalTo(discountLbl.mas_right).offset(30);
    }];
    UIView *bottomView = [[UIView alloc]init];
    bottomView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:bottomView];
    [bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(self.view.mas_width);
        make.left.equalTo(self.view).offset(0);
        make.top.equalTo(discountLbl.mas_bottom).offset(15);
        make.height.mas_equalTo(@44);
    }];
    
    UILabel *label2 = [[UILabel alloc]init];
    label2.text = @"实付金额";
    label2.tintColor = [UIColor grayColor];
    label2.font = [UIFont fontWithName:@"Arial" size:15];
    [bottomView addSubview:label2];
    [label2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(bottomView.mas_centerY);
        make.left.equalTo(bottomView).offset(10);
        make.width.mas_equalTo(@80);
    }];
    //实际支付金额
    payAmountLbl = [[UILabel alloc]init];
    payAmountLbl.text = @"￥ 0";
    payAmountLbl.font = [UIFont fontWithName:@"Arial" size:16];
    payAmountLbl.textColor = [UIColor orangeColor];
    payAmountLbl.textAlignment = UIControlContentHorizontalAlignmentRight;
    [bottomView addSubview:payAmountLbl];
    [payAmountLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(bottomView.mas_centerY);
        make.right.equalTo(bottomView).offset(-5);
        make.left.equalTo(label2.mas_right).offset(30);
    }];
    
    //确认买单
    submitBtn = [[UIButton alloc]init];
    [submitBtn setTitle:@"确认买单" forState:UIControlStateNormal];
    [submitBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [submitBtn setBackgroundColor:[UIColor orangeColor]];
    submitBtn.layer.cornerRadius = 3;
    submitBtn.layer.masksToBounds = YES;
    [self.view addSubview:submitBtn];
    [submitBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(bottomView.mas_bottom).offset(15);
        make.height.mas_equalTo(@44);
        make.left.equalTo(self.view).offset(20);
        make.right.equalTo(self.view).offset(-20);
    }];
    
    [submitBtn addTarget:self action:@selector(clickSubmitBtn) forControlEvents:UIControlEventTouchUpInside];
    
}

- (void)clickRightItem{
    DiscountNoteViewController *discountNote = [[DiscountNoteViewController alloc]init];
    discountNote.discount = _disCount;
    [self.navigationController pushViewController:discountNote animated:YES];
}

- (void) textFieldDidChange:(id) sender {
    UITextField *_field = (UITextField *)sender;
    if ([_field.text doubleValue]>0) {

        payAmountD = [_field.text doubleValue]*[_disCount doubleValue]*0.1;
        payAmountLbl.text = [NSString stringWithFormat:@"￥%.2f",payAmountD];
        discountMoneyLbl.text = [NSString stringWithFormat:@"￥%.2f",[totalTxt.text doubleValue]-payAmountD];
        [submitBtn setTitle:[NSString stringWithFormat:@"确认买单 %@",payAmountLbl.text] forState:UIControlStateNormal];
    } else {
        payAmountD = 0;
        payAmountLbl.text = @"￥ 0";
        discountMoneyLbl.text = @"￥ 0";
        [noDiscountBtn setTitle:@"输入不享受优惠金额" forState:UIControlStateNormal];
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    [totalTxt resignFirstResponder];
}
#pragma mark - 
- (void)clickSubmitBtn{
    if ([totalTxt.text doubleValue]>0) {
        [self ZFBPay];
    } else {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"请先输入到店消费总额" message:nil preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *checkAction = [UIAlertAction actionWithTitle:@"我知道了" style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:checkAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }
}
#pragma mark - 输入不享受折扣金额
- (void)clickNoDiscountBtn{
    
    if ([totalTxt.text isEqualToString:@""]) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"请先输入到店消费总额" message:nil preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *checkAction = [UIAlertAction actionWithTitle:@"我知道了" style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:checkAction];
        [self presentViewController:alertController animated:YES completion:nil];
    } else {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"输入不享优惠金额" message:@"详情请咨询服务员" preferredStyle:UIAlertControllerStyleAlert];
        [alertController addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
            [textField addTarget:self action:@selector(noDiscountTextFieldDidChange:andAlertC:) forControlEvents:UIControlEventEditingChanged];
        }];
        
        UIAlertAction *checkAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            UITextField *firstTF = alertController.textFields.firstObject;
            double noDiscount = [firstTF.text doubleValue];
            [noDiscountBtn setTitle:[NSString stringWithFormat:@"不享受折扣金额：%.2f",noDiscount] forState:UIControlStateNormal];
            if (noDiscount >= [totalTxt.text doubleValue]) {
                [submitBtn setTitle:@"输入金额有误" forState:UIControlStateNormal];
                [submitBtn setBackgroundColor:[UIColor lightGrayColor]];
                [submitBtn setUserInteractionEnabled:NO];
            } else {
                payAmountD = [_disCount doubleValue]*0.1*([totalTxt.text doubleValue] - noDiscount)+noDiscount;
                payAmountLbl.text = [NSString stringWithFormat:@"￥%.2f",payAmountD];
                discountMoneyLbl.text = [NSString stringWithFormat:@"-￥%.2f",[totalTxt.text doubleValue]-payAmountD-noDiscount];
                [submitBtn setTitle:[NSString stringWithFormat:@"确认买单 %@",payAmountLbl.text] forState:UIControlStateNormal];
                [submitBtn setBackgroundColor:[UIColor orangeColor]];
                [submitBtn setUserInteractionEnabled:YES];
            }
            
        }];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
        
        [alertController addAction:checkAction];
        [alertController addAction:cancelAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }
}

- (void)noDiscountTextFieldDidChange:(id)sender andAlertC:(UIAlertController *)alertController{
    UITextField *_field = (UITextField *)sender;
    NSLog(@"%@",_field.text);
    double noDiscount = [_field.text doubleValue];
    if (noDiscount > [totalTxt.text doubleValue]) {
        alertController.title = @"输入金额已超出消费总额";
        alertController.message = @"请重新输入";
        [alertController.actions objectAtIndex:1].enabled = NO;
    }
}

- (void)ZFBPay{
    /*
     *生成订单信息及签名
     */
    //将商品信息赋予AlixPayOrder的成员变量
    Order *order = [[Order alloc] init];
    order.partner = kPartnerID;
    order.seller = kSellerAccount;
    order.tradeNO = [AlipayToolKit genTradeNoWithTime]; //订单ID（由商家自行制定）
    order.productName = @"夜网订单"; //商品标题
    order.productDescription = [NSString stringWithFormat:@"夜网-优惠买单"]; //商品描述
    order.amount = [NSString stringWithFormat:@"%.2f",payAmountD]; //商品价格
    order.notifyURL =  kNotifyURL; //回调URL
    
    order.service = @"mobile.securitypay.pay";
    order.paymentType = @"1";
    order.inputCharset = @"utf-8";
    order.itBPay = @"30m";
    order.showUrl = @"m.alipay.com";
    
    //应用注册scheme,在AlixPayDemo-Info.plist定义URL types
    NSString *appScheme = kAppScheme;
    
    //将商品信息拼接成字符串
    NSString *orderSpec = [order description];
    
    //获取私钥并将商户信息签名,外部商户可以根据情况存放私钥和签名,只需要遵循RSA签名规范,并将签名字符串base64编码和UrlEncode
    id<DataSigner> signer = CreateRSADataSigner(kPrivateKey);
    //    NSLog(@"==%@",kPrivateKey);
    NSString *signedString = [signer signString:orderSpec];
    
    
    //将签名成功字符串格式化为订单字符串,请严格按照该格式
    NSString *orderString = nil;
    if (signedString != nil) {
        orderString = [NSString stringWithFormat:@"%@&sign=\"%@\"&sign_type=\"%@\"",
                       orderSpec, signedString, @"RSA"];
        
        [[AlipaySDK defaultService] payOrder:orderString fromScheme:appScheme callback:^(NSDictionary *resultDic) {
            NSLog(@"3reslut = %@",resultDic);
            
            NSString * result = nil;
            
            if ([resultDic[@"resultStatus"] isEqualToString:@"9000"]) {
                result = @"支付成功";
                [self clickConfirmationCheck];
            } else {
               
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:result message:@"支付未完成" preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleCancel handler:nil];
                [alertController addAction:cancelAction];
                [self presentViewController:alertController animated:YES completion:nil];
            }
            
            
        }];
        //接收来自APPdelegate的通知(注册观察者)
        //获取通知中心单例对象
        NSNotificationCenter * center = [NSNotificationCenter defaultCenter];
        //添加当前类对象为一个观察者，name和object设置为nil，表示接收一切通知
        [center addObserver:self selector:@selector(notice:) name:@"payAmountResult" object:nil];
        
    }
    
}

- (void)notice:(NSDictionary *)sender{
    NSString *str = [NSString stringWithFormat:@"%@",sender];
    NSString *resultStr = [[str substringFromIndex:185]substringToIndex:4];
    
    NSString * result = nil;
    if ([str rangeOfString:@"9000"].location !=NSNotFound) {
        result = @"支付成功";
        [self clickConfirmationCheck];
    } else if ([resultStr isEqualToString:@"6001"]) {
        result = @"用户中途取消";
    } else if ([resultStr isEqualToString:@"6002"]) {
        result = @"网络连接出错";
    } else if ([resultStr isEqualToString:@"4000"]) {
        result = @"订单支付失败";
    } else if ([resultStr isEqualToString:@"8000"]) {
        result = @"正在处理中";
    } else {
        result = @"订单支付失败";
    }
    if ([str rangeOfString:@"9000"].location ==NSNotFound) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:result message:@"支付未完成" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        [alertController addAction:cancelAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }
    
}

///调用优惠买单接口添加信息
- (void)clickConfirmationCheck1{
    NSString *strID = [CheckString createCUID];
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *userID = [userDefault objectForKey:@"ID"];
    //total ：消费金额 ；price ：实际支付金额
    NSDictionary *paramDic = @{@"ID":strID,@"ClubID":_clubID,@"UserID":userID,@"Price":totalTxt.text,@"Total":@(payAmountD),@"ContactPerson":[userDefault objectForKey:@"NickName"],@"ContactNumber":[userDefault objectForKey:@"Mobile"],@"PayMethod":@"支付宝",@"NoDiscount":@([totalTxt.text doubleValue]-payAmountD),@"Discount":_disCount};
    NSString *strUrl = [NSString stringWithFormat:@"%@Discount/AddDiscountPayment",BASICURL];
    [BingNetWorkConnection connectionWithUrl:strUrl type:BINGO_REQUEST_POST params:paramDic FinishBlock:^(id responseObject) {
        NSString *result = [[NSString alloc]initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSLog(@"result==%@",result);
        if ([result isEqualToString:@"[优惠付款成功]"]) {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:result message:@"支付完成" preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleCancel handler:nil];
            [alertController addAction:cancelAction];
            [self presentViewController:alertController animated:YES completion:nil];
        } else {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:result message:@"支付未完成" preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleCancel handler:nil];
            [alertController addAction:cancelAction];
            [self presentViewController:alertController animated:YES completion:nil];
        }
    } FailedBlock:^(NSError *error) {
    } ];
}
///调用原有的订单接口添加信息
- (void)clickConfirmationCheck{
    NSString *strID = [CheckString createCUID];//订单编号strID
    NSUserDefaults *userdefault = [NSUserDefaults standardUserDefaults];
    NSString *userID = [userdefault objectForKey:@"ID"];
    NSString *userName = [userdefault objectForKey:@"NickName"];
    NSString *mobile = [userdefault objectForKey:@"Mobile"];
    NSDateFormatter *dateformatter = [[NSDateFormatter alloc]init];
    [dateformatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    NSString *dateStr = [dateformatter stringFromDate:[NSDate date]];
    NSString *total = [NSString stringWithFormat:@"%.2f",payAmountD];
    ///添加订单
    NSDictionary *dic = @{@"ID":strID,@"ClubID":_clubID,@"UserID":userID,@"ItemID":_itemID,@"Price":totalTxt.text,@"Mount":@"1",@"Total":total,@"Status":@"已消费",@"PayMethod":@"优惠买单",@"ContactPerson":userName,@"ContactNumber":mobile,@"ArriveTime":dateStr};
    //调用添加订单接口（后台处理）
    NSString *strUrl = [NSString stringWithFormat:@"%@Order/AddOrder",BASICURL];
    [BingNetWorkConnection connectionWithUrl:strUrl type:BINGO_REQUEST_POST params:dic FinishBlock:^(id responseObject) {
        
        NSString *strResult = [[NSString alloc]initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSLog(@"Result：%@",strResult);
        if ([strResult isEqualToString:@"[订单成功]"]) {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"支付完成" message:@"优惠买单已完成，可在我的订单（已消费）中查看" preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                [self.navigationController popViewControllerAnimated:YES];
            }];
            [alertController addAction:cancelAction];
            [self presentViewController:alertController animated:YES completion:nil];
        }
    } FailedBlock:^(NSError *error) {
        NSLog(@"添加订单===%@",error);
    } ];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [totalTxt resignFirstResponder];
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
