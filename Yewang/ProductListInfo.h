//
//  ProductListInfo.h
//  Yewang
//
//  Created by everhelp on 16/11/1.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ProductListInfo : NSObject

///商品ID
@property (nonatomic, copy) NSString *ID;
///商家ID
@property (nonatomic, copy) NSString *ClubID;
///商品名称
@property (nonatomic, copy) NSString *ItemName;
///商品图片名称
@property (nonatomic, copy) NSString *ItemPic;
///推荐使用人数
@property (nonatomic, copy) NSString *Capacity;
///线上价格
@property (nonatomic, copy) NSString *Price;
///市场价格
@property (nonatomic, copy) NSString *MarketPrice;
///商品详情
@property (nonatomic, copy) NSString *ItemContent;
///商品是否有效
@property (nonatomic, copy) NSString *Status;
///商品类型
@property (nonatomic, copy) NSString *ItemType;
///是否删除
@property (nonatomic, copy) NSString *IsDeleted;
///数量
@property (nonatomic, copy) NSString *Count;

@end
