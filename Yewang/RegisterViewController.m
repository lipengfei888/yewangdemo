//
//  RegisterViewController.m
//  Yewang
//
//  Created by everhelp on 16/4/13.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import "RegisterViewController.h"

@interface RegisterViewController (){
    NSTimer *counterDownTimer;
    int freezeCounter;
}
@property (weak, nonatomic) IBOutlet UITextField *telTxt;
@property (weak, nonatomic) IBOutlet UITextField *identifyTxt;
@property (weak, nonatomic) IBOutlet UITextField *pwdTxt;
@property (weak, nonatomic) IBOutlet UITextField *checkTxt;
@property (weak, nonatomic) IBOutlet UIButton *identifyBtn;
@property (weak, nonatomic) IBOutlet UIButton *submitBtn;

@end

@implementation RegisterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
- (void)viewWillAppear:(BOOL)animated{
    self.navigationItem.title = @"注册";
    self.tabBarController.tabBar.hidden = YES;
    self.identifyBtn.layer.cornerRadius = 8.0;
    self.identifyBtn.layer.masksToBounds = YES;
    self.submitBtn.layer.cornerRadius = 8.0;
    self.submitBtn.layer.masksToBounds = YES;
    [super viewWillAppear:animated];
}
/*
    获取注册的验证码
 */

- (IBAction)clickIdentifyBtn:(id)sender {
    
    if ([CheckString checkPhoneNumInput:_telTxt.text]) {
        //判断是否是已经注册过的电话号码
        NSString *strUrl = [NSString stringWithFormat:@"%@Reg/GetCheckUserName?UserName=%@",BASICURL,_telTxt.text];
        [BingNetWorkConnection connectionWithUrl:strUrl type:BINGO_REQUEST_GET params:nil FinishBlock:^(id responseObject) {
            
            NSString *str = [[NSString alloc]initWithData:responseObject encoding:NSUTF8StringEncoding];
            if ([str isEqualToString:@"用户名被占用"]) {
                UIAlertController *alertControl = [UIAlertController alertControllerWithTitle:@"该手机号码已经被使用" message:nil preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *action = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleCancel handler:nil];
                [alertControl addAction:action];
                [self presentViewController:alertControl animated:YES completion:nil];
            } else {
                
                [AVOSCloud requestSmsCodeWithPhoneNumber:_telTxt.text callback:^(BOOL succeeded, NSError *error) {
                    // 发送失败可以查看 error 里面提供的信息
                    
                    if (succeeded) {
                        [self freezeMoreRequest];
                    }else{
                        NSLog(@"短信发送失败：%@",error);
                    }
                }];
            }
            
        } FailedBlock:^(NSError *error) {
            NSLog(@"测试：%@",error);
        }];
        
    } else {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:@"请输入正确的电话号码格式" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"好的" style:UIAlertActionStyleCancel handler:nil];
        [alert addAction:cancelAction];
        [self presentViewController:alert animated:YES completion:nil];
    }
    
}
/*
    提交
 */
- (IBAction)clickSubmitBtn:(id)sender {
    
    if ([self checkInputMsg]) {
        [AVUser signUpOrLoginWithMobilePhoneNumberInBackground:_telTxt.text smsCode:_identifyTxt.text block:^(AVUser *user, NSError *error) {
            // 如果 error 为空就可以表示登录成功了，并且 user 是一个全新的用户
            if (error == nil) {
                user.username = _telTxt.text;
                user.password = _pwdTxt.text;
                [user signUpInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                    if (succeeded) {
                        // 注册成功
                        //向后台数据库添加用户
                        [self registerUser];
                         
                    }
                }];
            }else {
                // 失败的原因可能有多种，常见的是用户名已经存在。
                UIAlertController *alertControl = [UIAlertController alertControllerWithTitle:@"该手机号码已经被使用" message:nil preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *action = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleCancel handler:nil];
                [alertControl addAction:action];
                [self presentViewController:alertControl animated:YES completion:nil];
            }
        }];
    }
}

//当注册成功时，向服务器添加该用户
- (void)registerUser{
    NSString *strUrl = [NSString stringWithFormat:@"%@Reg/Register",BASICURL];
    NSString *IDstr = [CheckString createCUID];
    NSDictionary *dic = @{@"ID":IDstr,@"UserName":_telTxt.text,@"Password":_pwdTxt.text,@"Mobile":_telTxt.text,@"RealName":@" ",@"Money":@0,@"HeadIcon":@"头像.png",@"NickName":@"先生",@"Sex":@"男",@"Region":@"南京"};
    [BingNetWorkConnection connectionWithUrl:strUrl type:BINGO_REQUEST_POST params:dic FinishBlock:^(id responseObject) {
        NSString *result ;
        if ([[[NSString alloc]initWithData:responseObject encoding:NSUTF8StringEncoding] isEqualToString:@"已被注册"]) {
            result = @"该手机号码已经被使用";
        } else {
            NSArray *array = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers | NSJSONReadingMutableLeaves error:nil];
            NSDictionary *dictionary = [array objectAtIndex:0];
            NSLog(@"注册信息：%@",dictionary);
            
            /*
             用户登录成功
             */
            //获取NSUserDefault单例
            NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
            //登录成功后把用户的详细信息存储到userDefault
            [userDefault setObject:[dictionary objectForKey:@"HeadIcon"] forKey:@"HeadIcon"];
            [userDefault setObject:[dictionary objectForKey:@"ID"] forKey:@"ID"];
            [userDefault setObject:[dictionary objectForKey:@"Mobile"] forKey:@"Mobile"];
            [userDefault setObject:[dictionary objectForKey:@"NickName"] forKey:@"NickName"];
            [userDefault setObject:[dictionary objectForKey:@"Password"] forKey:@"Password"];
            [userDefault setObject:[dictionary objectForKey:@"Region"] forKey:@"Region"];
            [userDefault setObject:[dictionary objectForKey:@"Sex"] forKey:@"Sex"];
            [userDefault setObject:[dictionary objectForKey:@"UserName"] forKey:@"UserName"];
            [userDefault synchronize];
            
            result = @"注册成功";
            
        }
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:result message:@"" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            //返回上一个页面
            [self.navigationController popViewControllerAnimated:YES];
        }];
        [alertController addAction:action];
        [self presentViewController:alertController animated:YES completion:nil];
        
    } FailedBlock:^(NSError *error) {
        NSLog(@"注册：%@",error);
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)freezeMoreRequest {
    // 一分钟内禁止再次发送
    [self.identifyBtn setEnabled:NO];
    freezeCounter = 60;
    counterDownTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(countDownRequestTimer) userInfo:nil repeats:YES];
    [counterDownTimer fire];
    
    UIAlertController *alertControl = [UIAlertController alertControllerWithTitle:@"验证码已发送" message:@"验证码已发送到你请求的手机号码。如果没有收到，可以在一分钟后尝试重新发送。" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleCancel handler:nil];
    [alertControl addAction:action];
    [self presentViewController:alertControl animated:YES completion:nil];
}

- (void)countDownRequestTimer {
    static NSString *counterFormat = @"%d";
    --freezeCounter;
    if (freezeCounter<= 0) {
        [counterDownTimer invalidate];
        counterDownTimer = nil;
        [self.identifyBtn setTitle:@"验证码" forState:UIControlStateNormal];
        [self.identifyBtn setEnabled:YES];
    } else {
        [self.identifyBtn setTitle:[NSString stringWithFormat:counterFormat, freezeCounter] forState:UIControlStateNormal];
    }
}

- (BOOL)checkInputMsg{
    BOOL flag = YES;
    
    NSString *newPassword = self.pwdTxt.text;
    NSString *checkPassword = self. checkTxt.text;
    NSString *smsCode = self.identifyTxt.text;
    if ([[newPassword stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]length] == 0){
        flag = NO;
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:@"密码不能为空，请输入密码" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleCancel handler:nil];
        [alert addAction:cancelAction];
        [self presentViewController:alert animated:YES completion:nil];
    } else if (![newPassword isEqualToString:checkPassword]){
        
        flag = NO;
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:@"两次密码不一致，请重新输入" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleCancel handler:nil];
        [alert addAction:cancelAction];
        [self presentViewController:alert animated:YES completion:nil];
    } else if (newPassword.length < 6) {
        
        flag = NO;
        
        NSMutableDictionary* details = [NSMutableDictionary dictionary];
        [details setValue:@"密码长度必须在 6 位（含）以上。" forKey:NSLocalizedDescriptionKey];
        NSError *error = [NSError errorWithDomain:@"LeanSMSDemo" code:1024 userInfo:details];
        [CommonUtils displayError:error];
        
    } else if (smsCode.length < 6) {
        
        flag = NO;
        
        NSMutableDictionary* details = [NSMutableDictionary dictionary];
        [details setValue:@"验证码无效。" forKey:NSLocalizedDescriptionKey];
        NSError *error = [NSError errorWithDomain:@"LeanSMSDemo" code:1024 userInfo:details];
        [CommonUtils displayError:error];
        
    }
    return flag;
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [_pwdTxt resignFirstResponder];
    [_checkTxt resignFirstResponder];
    [_telTxt resignFirstResponder];
    [_identifyTxt resignFirstResponder];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
