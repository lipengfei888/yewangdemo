//
//  RemarkTableViewCell.h
//  Yewang
//
//  Created by everhelp on 16/10/9.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RemarkTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *couponLbl;
@property (weak, nonatomic) IBOutlet UIButton *payOnLineBtn;
@property (weak, nonatomic) IBOutlet UITextView *remarkTxtView;

@end
