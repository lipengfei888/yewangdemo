//
//  RemarkTableViewCell.m
//  Yewang
//
//  Created by everhelp on 16/10/9.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import "RemarkTableViewCell.h"

@implementation RemarkTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.remarkTxtView.layer.borderColor = [[UIColor groupTableViewBackgroundColor] CGColor];
    self.remarkTxtView.layer.borderWidth = 1.0f;
    self.remarkTxtView.layer.cornerRadius = 5.0f;
    self.remarkTxtView.layer.masksToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
