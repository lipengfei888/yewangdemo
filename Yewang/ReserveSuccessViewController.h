//
//  ReserveSuccessViewController.h
//  Yewang
//
//  Created by everhelp on 16/9/12.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReserveSuccessViewController : UIViewController

@property (nonatomic, copy) NSString *orderID;


@end
