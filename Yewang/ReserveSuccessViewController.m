//
//  ReserveSuccessViewController.m
//  Yewang
//
//  Created by everhelp on 16/9/12.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import "ReserveSuccessViewController.h"

@interface ReserveSuccessViewController ()

@property (nonatomic, strong) UIImageView *imgView;///图片
@property (nonatomic, strong) UILabel *promptLbl;///提示语
@property (nonatomic, strong) UILabel *promptCodeLbl;///提示语
@property (nonatomic, strong) UILabel *payCodeLbl;///付款码
@property (nonatomic, strong) UIButton *doneBtn;///完成

@end

@implementation ReserveSuccessViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initWithFrame];
    [self initWithData];
    
}
///加载界面
- (void)initWithFrame{
    
    [self.view setBackgroundColor:[UIColor whiteColor]];
    CGFloat distance = SCREEN_WIDTH/5;
    _imgView = [[UIImageView alloc] initWithFrame:CGRectMake(distance, 90, distance*3, distance*3)];
    [_imgView setImage:[UIImage imageNamed:@"勾.png"]];
    [self.view addSubview:_imgView];
    _promptLbl = [UILabel new];
    _promptLbl.text = @"您可以在我的订单中查看详细信息";
    _promptLbl.textColor = kRGBColor;
    [self.view addSubview:_promptLbl];
    [_promptLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_imgView.mas_bottom).offset(15);
        make.centerX.equalTo(self.view.mas_centerX);
        make.height.mas_equalTo(@25);
    }];
    _promptCodeLbl = [UILabel new];
    _promptCodeLbl.text = @"请到店后向商家出示您的验证码";
    _promptCodeLbl.textColor = kRGBColor;
    [self.view addSubview:_promptCodeLbl];
    [_promptCodeLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_promptLbl.mas_bottom).offset(30);
        make.centerX.equalTo(self.view.mas_centerX);
        make.height.mas_equalTo(@25);
    }];
    _payCodeLbl = [UILabel new];
    _payCodeLbl.textColor = kRGBColor;
    [self.view addSubview:_payCodeLbl];
    [_payCodeLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_promptCodeLbl.mas_bottom).offset(30);
        make.centerX.equalTo(self.view.mas_centerX);
        make.height.mas_equalTo(@25);
    }];
    
    _doneBtn = [[UIButton alloc] initWithFrame:CGRectMake(20, SCREEN_HEIGHT-50, SCREEN_WIDTH-40, 40)];
    [_doneBtn setTitle:@"完成" forState:UIControlStateNormal];
    [_doneBtn setTintColor:[UIColor whiteColor]];
    [_doneBtn setBackgroundColor:kRGBColor];
    _doneBtn.layer.cornerRadius = 13;
    _doneBtn.layer.masksToBounds = YES;
    [self.view addSubview:_doneBtn];
    [_doneBtn addTarget:self action:@selector(clickDone) forControlEvents:UIControlEventTouchUpInside];
}

- (void)initWithData{
    NSString *strUrl = [NSString stringWithFormat:@"%@Order/GetOrderView?orderID=%@",BASICURL,_orderID];
    [BingNetWorkConnection connectionWithUrl:strUrl type:BINGO_REQUEST_GET params:nil FinishBlock:^(id responseObject) {
        
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:nil];
        NSLog(@"%@",dict);
        _payCodeLbl.text = dict[@"PaymentCode"];
        
    } FailedBlock:^(NSError *error) {
        NSLog(@"%@",error);
    }];
}
///返回商家二级界面
- (void)clickDone{
    
    [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:1] animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
