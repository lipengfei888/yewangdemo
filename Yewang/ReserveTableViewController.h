//
//  ReserveTableViewController.h
//  Yewang
//
//  Created by everhelp on 16/9/16.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReserveTableViewController : UITableViewController

@property (nonatomic, strong) NSDictionary *dictionary;
@property (nonatomic, strong) NSString *clubName;
@property (nonatomic, strong) NSString *clubMode;

@end
