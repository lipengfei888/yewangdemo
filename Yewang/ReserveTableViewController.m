//
//  ReserveTableViewController.m
//  Yewang
//
//  Created by everhelp on 16/9/16.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import "ReserveTableViewController.h"
#import "BuyDetailViewController.h"
#import "ReserveSuccessViewController.h"

@interface ReserveTableViewController (){
    NSInteger _itemNum;
    CGFloat num;
    UIDatePicker *_datePicker;
}

@property (weak, nonatomic) IBOutlet UILabel *titleLbl;///预定/套餐title
@property (weak, nonatomic) IBOutlet UILabel *detailLbl;///预定/套餐detail
@property (weak, nonatomic) IBOutlet UITextField *itemNumTxt;///预定数
@property (weak, nonatomic) IBOutlet UITextField *contactTxt;///联系人
@property (weak, nonatomic) IBOutlet UITextField *contactNumberTxt;///联系电话
@property (weak, nonatomic) IBOutlet UITextField *timeTxt;///到达时间
@property (weak, nonatomic) IBOutlet UILabel *totalTxt;///总计
@property (weak, nonatomic) IBOutlet UILabel *firstLbl;///标题一
@property (weak, nonatomic) IBOutlet UILabel *secondLbl;///标题二
@property (weak, nonatomic) IBOutlet UIButton *subBtn;///预定按钮
@property (weak, nonatomic) IBOutlet UIButton *addBtn;///增加预定数按钮
@property (weak, nonatomic) IBOutlet UIButton *submitBtn;///减少预定数按钮
@property (nonatomic, assign) CGFloat detailLblHeight;///详情Lbl高度

@end

@implementation ReserveTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initWithFrame];
}

- (void)initWithFrame{
    self.navigationController.navigationBar.hidden = NO;
    self.navigationItem.title = @"预定";
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    self.submitBtn.layer.cornerRadius = 8.0;
    self.submitBtn.layer.masksToBounds = YES;
    _itemNum = 1.0;
    num = [[_dictionary objectForKey:@"Price"]floatValue];
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *nickName = [userDefault objectForKey:@"NickName"];
    NSString *mobile = [userDefault objectForKey:@"Mobile"];
    
    self.titleLbl.text = [NSString stringWithFormat:@"%@  %@",[_dictionary objectForKey:@"ItemName"],[_dictionary objectForKey:@"Capacity"]];
    if ([_dictionary[@"ItemContent"] isKindOfClass:[NSNull class]]) {
        _detailLbl.text = @"最低消费额度";
    } else {
        _detailLbl.text = [_dictionary objectForKey:@"ItemContent"];
    }
    _detailLbl.numberOfLines = 0;
    _detailLbl.lineBreakMode = NSLineBreakByTruncatingTail;
    _detailLbl.backgroundColor = [UIColor whiteColor];
    CGSize maximumLabelSize = CGSizeMake(SCREEN_WIDTH-140, 9999);
    //关键语句
    CGSize expectSize = [_detailLbl sizeThatFits:maximumLabelSize];
    //把frame给回label
    [_detailLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(expectSize.height);
    }];
    _detailLblHeight = expectSize.height+40;
    self.contactNumberTxt.text = mobile;
    self.contactTxt.text = nickName;
    self.itemNumTxt.text = [NSString stringWithFormat:@"%li",(long)_itemNum];
    self.totalTxt.text = [NSString stringWithFormat:@"%.2f",num];
    _datePicker = [[UIDatePicker alloc]initWithFrame:CGRectMake(0.0 ,self.view.frame.size.height-216, 0.0,0.0)];
    _datePicker.datePickerMode = UIDatePickerModeDateAndTime;//英文月份 日期年
    NSLocale *locale = [[NSLocale alloc]initWithLocaleIdentifier:@"zh_CN"];//设置为中文显示
    _datePicker.minimumDate = [NSDate date];
    _datePicker.locale = locale;
    _datePicker.tag=0;
    [ _datePicker addTarget:self action:@selector(dateChanged:)forControlEvents:UIControlEventValueChanged ];
    _timeTxt.inputView = _datePicker;//add是一个UIView视图
    
    if ([[_dictionary objectForKey:@"ItemName"] rangeOfString:@"套餐"].location != NSNotFound ) {
        _firstLbl.text = @"套餐信息";
        _secondLbl.text = @"套餐详情";
    }

}

- (void)dateChanged:(id)sender{
    UIDatePicker *picker = (UIDatePicker *)sender;
    NSDateFormatter *dateformatter = [[NSDateFormatter alloc]init];
    [dateformatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    NSString *dateStr = [dateformatter stringFromDate:picker.date];
    self.timeTxt.text = [NSString stringWithFormat:@"%@", dateStr];
}
//添加包间
- (IBAction)clickAdd:(id)sender {
    _itemNum++;
    self.totalTxt.text = [NSString stringWithFormat:@"%.2f",(num*_itemNum)];
    self.itemNumTxt.text = [NSString stringWithFormat:@"%li",(long)_itemNum];
}
//减包间
- (IBAction)clickSub:(id)sender {
    _itemNum--;
    self.totalTxt.text = [NSString stringWithFormat:@"%.2f",(num*_itemNum)];
    self.itemNumTxt.text = [NSString stringWithFormat:@"%li",(long)_itemNum];
    if (_itemNum<=1) {
        _itemNum = 1;
        self.totalTxt.text = [NSString stringWithFormat:@"%.2f",(num*_itemNum)];
        self.itemNumTxt.text = [NSString stringWithFormat:@"%li",(long)_itemNum];
    }
}
- (IBAction)clickSubmit:(id)sender {
    
    if ([[_timeTxt.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]length] == 0) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"请选择到达时间" message:@"" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action = [UIAlertAction actionWithTitle:@"马上填写" style:UIAlertActionStyleCancel handler:nil];
        [alertController addAction:action];
        [self presentViewController:alertController animated:YES completion:nil];
    } else {
        //A类商家，可线上支付
        if ([_clubMode isEqualToString:@"A"]) {
            BuyDetailViewController *buyDetail = [self.storyboard instantiateViewControllerWithIdentifier:@"buydetail"];
            buyDetail.itemName = [_dictionary objectForKey:@"ItemName"];
            buyDetail.total = _totalTxt.text;
            buyDetail.arriveTime = _timeTxt.text;
            buyDetail.tel = _contactNumberTxt.text;
            buyDetail.clubName = _clubName;
            buyDetail.itemNum = _itemNumTxt.text;
            buyDetail.contactPerson = _contactTxt.text;
            buyDetail.price = [_dictionary objectForKey:@"Price"];
            buyDetail.itemID = [_dictionary objectForKey:@"ID"];
            buyDetail.clubID = [_dictionary objectForKey:@"ClubID"];
            buyDetail.orderID = @"未添加";
            [self.navigationController pushViewController:buyDetail animated:YES];
        } else if ([_clubMode isEqualToString:@"B"]){
            //B类商家，可线下支付
            [self addOrder];
        }
//        else  {////记得删除
//            [self addOrder];
//        }
    }
}

///线下支付，添加订单(状态为：已支付)
- (void)addOrder{
    NSString *strID = [CheckString createCUID];//订单编号strID
    NSString *clubID = [_dictionary objectForKey:@"ClubID"];
    NSUserDefaults *userdefault = [NSUserDefaults standardUserDefaults];
    NSString *userID = [userdefault objectForKey:@"ID"];
    NSString *itemID = [_dictionary objectForKey:@"ID"];
    ///添加订单
    NSDictionary *dic = @{@"ID":strID,@"ClubID":clubID,@"UserID":userID,@"ItemID":itemID,@"Price":[_dictionary objectForKey:@"Price"],@"Mount":_itemNumTxt.text,@"Total":@"0.0",@"Status":@"未付款",@"PayMethod":@"未付款",@"ContactPerson":_contactTxt.text,@"ContactNumber":_contactNumberTxt.text,@"ArriveTime":_timeTxt.text};
    
    NSString *strUrl = [NSString stringWithFormat:@"%@Order/AddOrder",BASICURL];
    [BingNetWorkConnection connectionWithUrl:strUrl type:BINGO_REQUEST_POST params:dic FinishBlock:^(id responseObject) {
        
        NSString *strResult = [[NSString alloc]initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSLog(@"添加订单Result%@",strResult);
        if ([strResult isEqualToString:@"[订单成功]"]) {
            ///添加订单成功，修改订单状态
            NSDictionary *dic2 = @{@"ID":strID,@"ClubID":clubID,@"UserID":userID,@"ItemID":itemID,@"Price":[_dictionary objectForKey:@"Price"],@"Mount":_itemNumTxt.text,@"Total":@"0.0",@"Status":@"已付款",@"PayMethod":@"线下支付",@"ContactPerson":_contactTxt.text,@"ContactNumber":_contactNumberTxt.text,@"ArriveTime":_timeTxt.text};
            NSString *payUrl = [NSString stringWithFormat:@"%@Order/PayOrder",BASICURL];
            [BingNetWorkConnection connectionWithUrl:payUrl type:BINGO_REQUEST_POST params:dic2 FinishBlock:^(id responseObject) {
                
                NSString *strResult2 = [[NSString alloc]initWithData:responseObject encoding:NSUTF8StringEncoding];
                NSLog(@"订单支付Result%@",strResult);
                if ([strResult2 isEqualToString:@"[支付成功]"]) {
                    ReserveSuccessViewController *reserve = [[ReserveSuccessViewController alloc] init];
                    reserve.orderID = strID;
                    [self.navigationController pushViewController:reserve animated:YES];
                }
            } FailedBlock:^(NSError *error) {
                NSLog(@"订单支付===%@",error);
            } ];
        }
    } FailedBlock:^(NSError *error) {
        NSLog(@"添加订单===%@",error);
    } ];
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //选中行后去掉背景颜色
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [_timeTxt resignFirstResponder];
    [_contactTxt resignFirstResponder];
    [_contactNumberTxt resignFirstResponder];
    NSLog(@"%ld",(long)indexPath.row);
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 1) {
        return _detailLblHeight;
    } else {
        return 44;
    }
}

#pragma mark - 添加表格头视图
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake(0.0, 0.0, SCREEN_WIDTH, SCREEN_HEIGHT/4)];
    
    [imgView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",IMGURL,[_dictionary objectForKey:@"ItemPic"]]] placeholderImage:[UIImage imageNamed:CACHEIMG] options:SDWebImageRefreshCached];
    
    return imgView;
}
#pragma mark - 头视图高度
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return SCREEN_HEIGHT/4;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
