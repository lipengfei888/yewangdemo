//
//  ResultTableViewCell.h
//  Yewang
//
//  Created by everhelp on 16/9/23.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^selectBlock)(NSInteger index);

@interface ResultTableViewCell : UITableViewCell

///设置数据
- (void)showData:(id)data selectBlock:(selectBlock)block;
///设置高度
+ (CGFloat)getHeight;

@end
