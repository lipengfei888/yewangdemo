//
//  ResultTableViewCell.m
//  Yewang
//
//  Created by everhelp on 16/9/23.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import "ResultTableViewCell.h"
#import "FilterImgView.h"

#define kFontColor [[UIColor lightGrayColor]colorWithAlphaComponent:0.8]

@interface ResultTableViewCell()
///商户图片
@property (nonatomic, strong) UIImageView *clubImgView;
///商户名称
@property (nonatomic, strong) UILabel *clubNameLbl;
///商户地址
@property (nonatomic, strong) UILabel *addressLbl;
///距离
@property (nonatomic, strong) UILabel *distanceLbl;
///消费价格
@property (nonatomic, strong) UILabel *priceLbl;
///折扣
@property (nonatomic, strong) UILabel *discountLbl;
///折扣图片
@property (nonatomic, strong) UIImageView *discountImgView;
///视图内分割线
@property (nonatomic, strong) UILabel *lineLbl;
///表格分割线
@property (nonatomic, strong) UILabel *tableLineLbl;
///商家类别图片(滚动视图)
@property (nonatomic, strong) FilterImgView *scrollView;
///点击的回调
@property (nonatomic, assign) selectBlock block;
@end

@implementation ResultTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self == [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        ///基础宽度
        CGFloat basicWidth = SCREEN_WIDTH/4.5;
        ///表格分割线
        _tableLineLbl = [UILabel new];
        _tableLineLbl.backgroundColor = [UIColor lightGrayColor];
        [self addSubview:_tableLineLbl];
        [self.tableLineLbl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.mas_left).offset(1);
            make.top.equalTo(self.mas_top).offset(1);
            make.right.equalTo(self.mas_right).offset(1);
            make.height.mas_equalTo(1);
        }];
        ///商家图片
        _clubImgView = [[UIImageView alloc] initWithFrame:CGRectZero];
        _clubImgView.layer.cornerRadius = 5;
        _clubImgView.layer.masksToBounds = YES;
        [self addSubview:_clubImgView];
        [self.clubImgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.mas_left).offset(5);
            make.top.equalTo(self.mas_top).offset(10);
            make.bottom.equalTo(self.mas_bottom).offset(-30);
            make.width.mas_equalTo(basicWidth);
        }];
        ///商家名称
        _clubNameLbl = [[UILabel alloc] init];
        [_clubNameLbl setFont:[UIFont systemFontOfSize:16]];
        [self addSubview:_clubNameLbl];
        [self.clubNameLbl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.clubImgView.mas_right).offset(5);
            make.top.equalTo(self.mas_top).offset(5);
            make.right.equalTo(self.mas_right).offset(-130);
            make.height.mas_equalTo(@25);
        }];
        ///商家地址
        _addressLbl = [[UILabel alloc] init];
        [_addressLbl setFont:[UIFont systemFontOfSize:12]];
        [_addressLbl setTextColor:[UIColor lightGrayColor]];
        [self addSubview:_addressLbl];
        [self.addressLbl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.clubImgView.mas_right).offset(5);
            make.centerY.equalTo(self.clubImgView.mas_centerY);
            make.right.equalTo(self.mas_right).offset(-130);
            make.height.mas_equalTo(@20);
        }];
        ///距离
        _distanceLbl = [[UILabel alloc] init];
        [_distanceLbl setFont:[UIFont systemFontOfSize:12]];
        [_distanceLbl setTextColor:[UIColor lightGrayColor]];
        [self addSubview:_distanceLbl];
        [self.distanceLbl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.addressLbl.mas_centerY);
            make.right.equalTo(self.mas_right).offset(-70);
            make.left.equalTo(self.addressLbl.mas_right).offset(10);
        }];
        ///价格
        _priceLbl = [[UILabel alloc] init];
        [_priceLbl setFont:[UIFont systemFontOfSize:16]];
        [_priceLbl setTextColor:kRGBColor];
        [self addSubview:_priceLbl];
        [self.priceLbl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.clubImgView.mas_right).offset(5);
            make.top.equalTo(self.addressLbl.mas_bottom).offset(0);
            make.width.mas_equalTo(@150);
            make.height.mas_equalTo(@25);
        }];
        ///折扣图片
        _discountImgView = [[UIImageView alloc] init];
        [self addSubview:_discountImgView];
        [self.discountImgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.priceLbl.mas_left).offset(0);
            make.top.equalTo(self.priceLbl.mas_bottom).offset(2);
            make.height.mas_equalTo(@18);
            make.width.mas_equalTo(@18);
        }];
        ///折扣
        _discountLbl = [[UILabel alloc] init];
        [_discountLbl setFont:[UIFont systemFontOfSize:12]];
        [_discountLbl setTextColor:kFontColor];
        [self addSubview:_discountLbl];
        [self.discountLbl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.priceLbl.mas_bottom).offset(2);
            make.left.equalTo(self.discountImgView.mas_right).offset(3);
        }];
        ///商家类别图片（滚动视图）
        _scrollView = [[FilterImgView alloc] initWithFrame:CGRectZero andImgArray:@[@"足球2",@"桌球2",@"小资2"]];
        [self addSubview:_scrollView];
        [self.scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.distanceLbl.mas_right).offset(-5);
            make.top.equalTo(self.mas_top).offset(0);
            make.right.equalTo(self.mas_right).offset(-5);
            make.height.mas_equalTo(@60);
        }];
        
        ///视图内分割线
        _lineLbl = [[UILabel alloc] init];
        _lineLbl.backgroundColor = kFontColor;
        [self addSubview:_lineLbl];
        [self.lineLbl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.clubImgView.mas_right).offset(5);
            make.top.equalTo(self.clubImgView.mas_bottom).offset(0);
            make.right.equalTo(self.mas_right).offset(-20);
            make.height.mas_equalTo(@1);
        }];
        
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)showData:(id)data selectBlock:(selectBlock)block{
    NSDictionary *dict = data;
    self.block = block;
    [self.clubImgView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",IMGURL,dict[@"ClubPic"]]] placeholderImage:[UIImage imageNamed:CACHEIMG]
                                 options:SDWebImageRefreshCached];
    self.clubNameLbl.text = dict[@"ClubName"];
    self.addressLbl.text = [NSString stringWithFormat:@"地址:%@",dict[@"Address"]];
    self.priceLbl.text = [NSString stringWithFormat:@"￥ %@",dict[@"StartWith"]];
    self.discountLbl.text = [NSString stringWithFormat:@"%@ 折",dict[@"Discount"]];
    self.distanceLbl.text = @"1.5km";
    [self.discountImgView setImage:[UIImage imageNamed:@"cheap.png"]];
}

+ (CGFloat)getHeight{
    return 115;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
