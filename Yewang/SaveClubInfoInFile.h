//
//  SaveClubInfoInFile.h
//  Yewang
//
//  Created by everhelp on 16/9/1.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SaveClubInfoInFile : NSObject

+ (void)saveAllClubInfo;
+ (NSMutableArray *)readAllClubInfo;

+ (void)saveCache:(int)type andID:(int)_id andString:(NSString *)str;//保存到沙盒
+ (void)getCache:(int)type andID:(int)_id;//读取本地沙盒


@end
