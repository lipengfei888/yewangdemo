//
//  SaveClubInfoInFile.m
//  Yewang
//
//  Created by everhelp on 16/9/1.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import "SaveClubInfoInFile.h"
#import "ClubListInfo.h"

@implementation SaveClubInfoInFile

+ (void)saveAllClubInfo{
    
    
    
    NSString *strUrl = [NSString stringWithFormat:@"%@Club/GetClubList?Id=6",BASICURL];
    [BingNetWorkConnection connectionWithUrl:strUrl type:BINGO_REQUEST_GET params:nil FinishBlock:^(id responseObject) {
        if ([[NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:nil] isKindOfClass:[NSDictionary class]]) {
            
        } else  {
            NSArray *arr = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:nil];
//            for (int i = 0; i < arr.count; i ++) {
//                NSDictionary *dic = [arr objectAtIndex:i];
//                ClubListInfo *clubInfo = [ClubListInfo mj_objectWithKeyValues:dic];
//                [array addObject:clubInfo];
//              
//            }
            
            NSFileManager *fileManager = [NSFileManager defaultManager];
            NSString *fileName = [DOCUMENTPATH stringByAppendingPathComponent:@"ClubInfoFile.plist"];
            [fileManager createFileAtPath:fileName contents:nil attributes:nil];
            
            [arr writeToFile:fileName atomically:YES];
            NSLog(@"%@",fileName);
        }
    } FailedBlock:^(NSError *error) {
        
    }];
}

+ (NSMutableArray *)readAllClubInfo{
    
//    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *fileName = [DOCUMENTPATH stringByAppendingPathComponent:@"ClubInfoFile.plist"];
//    [fileManager createFileAtPath:fileName contents:nil attributes:nil];
    
    NSMutableArray *array = [NSMutableArray arrayWithContentsOfFile:fileName];
    return array;
}

+ (void)saveCache:(int)type andID:(int)_id andString:(NSString *)str{
    
}

+ (void)getCache:(int)type andID:(int)_id{
    
}

@end
