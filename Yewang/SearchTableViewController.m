//
//  SearchTableViewController.m
//  Yewang
//
//  Created by everhelp on 16/4/11.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import "SearchTableViewController.h"
#import "ClubTableViewCell.h"
#import "DOPDropDownMenu.h"
#import "ClubListInfo.h"
#import <CoreLocation/CoreLocation.h>
#import "AddressPickerDemo.h"
#import "BusinessDescriptionVC.h"
#import "FMDBHepler.h"

@interface SearchTableViewController ()<UISearchBarDelegate,DOPDropDownMenuDataSource, DOPDropDownMenuDelegate,CLLocationManagerDelegate>{
    NSMutableArray *_array;///数据源
    CLLocationManager *_locationManager;///定义Manager
    NSMutableArray *_counties;///城市列表
    UIBarButtonItem *leftItem;///城市按钮
    NSString *nowLocationStr;///当前位置
    NSMutableDictionary *filterDic;///筛选字典
    UIImageView *bgImgView;///无结果显示图片
    BOOL isLocationOpen;///是否能获取定位信息
}

@property (nonatomic, strong) FMDBHepler *helperManager;

@end

@implementation SearchTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _counties = [NSMutableArray arrayWithObjects:@"浦口区",@"栖霞区",@"雨花台区",@"江宁区",@"秦淮区",@"建邺区",@"鼓楼区",@"下关区",@"六合区",@"高淳县",@"白下区",@"玄武区",@"浦溧水县", nil];
   
    [self initWithFrame];
    if ([BingNetWorkConnection NetWorkIsOK]) {
        [self getDataSource];
        [self getLocation];
        [self initMyMJRefresh];
    } else {
        [self getDataCache];
    }
    
    filterDic = [NSMutableDictionary dictionary];
    [filterDic setValue:@(_locationManager.location.coordinate.longitude) forKey:@"longitude"];
    [filterDic setValue:@(_locationManager.location.coordinate.latitude) forKey:@"latitude"];
    [filterDic setValue:@"全部区域" forKey:@"area"];
    [filterDic setValue:@0 forKey:@"distance"];
    [filterDic setValue:@"筛选" forKey:@"classify"];
    
}
- (void)viewWillAppear:(BOOL)animated{
    self.tabBarController.tabBar.hidden = NO;
    self.navigationController.navigationBar.hidden = NO;
    NSNotificationCenter * center = [NSNotificationCenter defaultCenter];
    //添加当前类对象为一个观察者，name和object设置为nil，表示接收一切通知
    [center addObserver:self selector:@selector(getArea:) name:@"area" object:nil];
    [super viewWillAppear:animated];
}

//移除通知
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
}

- (void)getArea:(NSNotification *)notification{
    //加载数据
    leftItem.title = [notification.userInfo objectForKey:@"City"];
    NSArray * provinces = [[NSArray alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"area.plist" ofType:nil]];
    NSString *state;//省名
    
    NSString *city;//市名
    if (_counties.count>0) {
        [_counties removeAllObjects];
    }
    
    for (NSDictionary *proDic in provinces) {
        
        state = [proDic objectForKey:@"state"];
        if ([state isEqualToString:[notification.userInfo objectForKey:@"City"]]) {
            for (NSDictionary *cityDic in [proDic objectForKey:@"cities"]) {
                [_counties addObject:[cityDic objectForKey:@"city"]];
            }
            break;
        } else {
            for (NSDictionary *cityDic in [proDic objectForKey:@"cities"]) {
                city = [cityDic objectForKey:@"city"];
                if ([city isEqualToString:[notification.userInfo objectForKey:@"City"]]) {
                    [_counties addObjectsFromArray:[cityDic objectForKey:@"areas"]];
                    break;
                }
            }
        }
        if (_counties.count>0) {
            break;
        }
    }
    [self getDataSource];
}

- (void)initWithFrame{
    leftItem = [[UIBarButtonItem alloc]initWithTitle:@"南京" style:UIBarButtonItemStyleDone target:self action:@selector(SelectTheAddress)];
    self.navigationItem.leftBarButtonItem = leftItem;
    /*
     创建表格头视图
     */
    DOPDropDownMenu *menu = [[DOPDropDownMenu alloc] initWithOrigin:CGPointMake(0, 0) andHeight:40];
    menu.dataSource = self;
    menu.delegate = self;
    self.tableView.tableHeaderView = menu;
    /*
     创建searchbar
     */
    UIView *titleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH-100, 30)];//allocate titleView
    UIColor *color =  self.navigationController.navigationBar.backgroundColor;
    
    [titleView setBackgroundColor:color];
    
    UISearchBar *searchBar = [[UISearchBar alloc] init];
    
    searchBar.delegate = self;
    searchBar.frame = CGRectMake(0, 0, SCREEN_WIDTH-100, 30);
    searchBar.backgroundColor = color;
    searchBar.layer.cornerRadius = 10;
    searchBar.layer.masksToBounds = YES;
    [searchBar.layer setBorderWidth:8];
    searchBar.translucent = YES;//设置控件是否具有透视效果
    [searchBar.layer setBorderColor:[UIColor whiteColor].CGColor];  //设置边框为白色
    
    searchBar.placeholder = @"请输入您要搜索的关键词";
    [titleView addSubview:searchBar];
    
    //Set to titleView
    [self.navigationItem.titleView sizeToFit];
    self.navigationItem.titleView = titleView;
#pragma mark---去掉cell分割线
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    //注册单元格
    [self.tableView registerNib:[UINib nibWithNibName:@"ClubTableViewCell" bundle:nil] forCellReuseIdentifier:@"clubcell"];
    
}

#pragma mark---选择地区
- (void)SelectTheAddress{
    
    AddressPickerDemo *addressPickerDemo = [[AddressPickerDemo alloc] init];
    UINavigationController *navigation = [[UINavigationController alloc] initWithRootViewController:addressPickerDemo];
    [self presentViewController:navigation animated:YES completion:nil];
}

- (void)initMyMJRefresh{
    // 设置回调（一旦进入刷新状态，就调用target的action，也就是调用self的dataByMJRefreshHeader方法）
    MJRefreshGifHeader *header = [MJRefreshGifHeader headerWithRefreshingBlock:^{
        [self performSelector:@selector(backFilterClubList) withObject:nil afterDelay:1.5];
    }];
    // 设置header
    self.tableView.mj_header = header;
}
//停止刷新
- (void)endRefresh{
    [self.tableView.mj_header endRefreshing];
}
- (void)getDataCache{
    //初始化数组
    _array = [NSMutableArray new];
    self.helperManager = [[FMDBHepler sharedInstance]init];
    NSArray *arr = [self.helperManager stepsFromSqlite];
    if (arr.count) {
        _array = [NSMutableArray arrayWithArray:arr];
    }
}

- (void)getDataSource{
    _array = [NSMutableArray new];
    NSString *strUrl = [NSString stringWithFormat:@"%@Club/GetClubList?Id=6",BASICURL];
    [SVProgressHUD showWithStatus:@"加载中，请稍候..."];
    
    [BingNetWorkConnection connectionWithUrl:strUrl type:BINGO_REQUEST_GET params:nil FinishBlock:^(id responseObject) {
        
        [SVProgressHUD dismiss];
        if ([[NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:nil] isKindOfClass:[NSDictionary class]]) {
            
            [SVProgressHUD showErrorWithStatus:@"服务器维护中"];
            
        } else  {
            NSArray *arr = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:nil];
            if (_array.count>0) {
                [_array removeAllObjects];
            }
            for (int i = 0; i < arr.count; i ++) {
                NSDictionary *dic = [arr objectAtIndex:i];
                ClubListInfo *clubInfo = [ClubListInfo mj_objectWithKeyValues:dic];
                
                if ([clubInfo.City isEqualToString:leftItem.title]) {
                    [_array addObject:clubInfo];
                }
            }
        }
        //刷新表格
        [self.tableView reloadData];
    } FailedBlock:^(NSError *error) {
        NSLog(@"searchpage error : %@",error.userInfo);
    }];
}

#pragma mark---获取当前位置的经纬度
- (void)getLocation{
    
    if ([CLLocationManager locationServicesEnabled] &&([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse || [CLLocationManager authorizationStatus] == kCLAuthorizationStatusNotDetermined)) {
            //定位功能可用，开始定位
        NSLog(@"定位功能可用，开始定位");
        isLocationOpen = YES;
        nowLocationStr = @"  正在检测当前位置...";
        //请求定位服务
        
        _locationManager=[[CLLocationManager alloc]init];
        _locationManager.delegate = self;
        //设置定位精确到米
        _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        //设置过滤器为无
        _locationManager.distanceFilter = kCLDistanceFilterNone;
        //这句话iOS8以上版本使用
        [_locationManager requestWhenInUseAuthorization];
        // 开始定位
        [_locationManager startUpdatingLocation];
    } else if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied){
        NSLog(@"定位功能不可用，提示用户或忽略");
        isLocationOpen = NO;
        return;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}

#pragma mark - CLLocationManagerDelegate
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations{
    CLLocation *newLocation = _locationManager.location;//[locations objectAtIndex:0];
    // 获取当前所在的城市名
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    //根据经纬度反向地理编译出地址信息
    [geocoder reverseGeocodeLocation:newLocation completionHandler:^(NSArray *array, NSError *error){
        if (array.count > 0){
            CLPlacemark *placemark = [array objectAtIndex:0];
            //将获得的所有信息显示到label上
            nowLocationStr = [NSString stringWithFormat:@"    当前：%@",placemark.name];
            [self.tableView reloadData];
            //获取城市
            NSString *city = placemark.locality;
            if (!city) {
                //四大直辖市的城市信息无法通过locality获得，只能通过获取省份的方法来获得（如果city为空，则可知为直辖市）
                city = placemark.administrativeArea;
            }
        }
        else if (error == nil && [array count] == 0)
        {
            NSLog(@"No results were returned.");
        }
        else if (error != nil)
        {
            NSLog(@"An error occurred = %@", error);
        }
    }];
    //系统会一直更新数据，直到选择停止更新，因为我们只需要获得一次经纬度即可，所以获取之后就停止更新
    [manager stopUpdatingLocation];
}


#pragma mark - SearchBar data source
- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar{
    
    return YES;
}
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    [searchBar resignFirstResponder];
    searchBar.showsCancelButton = NO;
    [self getDataSource];
}
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    searchBar.showsCancelButton = YES;
}
#pragma mark---点击搜索按钮事件
-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{

    if (![searchBar.text isEqual:@""]) {
        NSString *strUrl = [NSString stringWithFormat:@"%@Club/QueryClubList",BASICURL];
        [SVProgressHUD showWithStatus:@"加载中，请稍候..."];
        NSDictionary *dic = @{@"Value":searchBar.text};
        [BingNetWorkConnection connectionWithUrl:strUrl type:BINGO_REQUEST_POST params:dic FinishBlock:^(id responseObject) {
            
            [SVProgressHUD dismiss];
            
            NSArray *arr = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:nil];
            [_array removeAllObjects];
            for (int i = 0; i < arr.count; i ++) {
                NSDictionary *dic = [arr objectAtIndex:i];
                ClubListInfo *clubInfo = [ClubListInfo mj_objectWithKeyValues:dic];
                [_array addObject:clubInfo];
            }
            
            [searchBar resignFirstResponder];
            searchBar.showsCancelButton = NO;
            //刷新表格
            [self.tableView reloadData];
        } FailedBlock:^(NSError *error) {
            
        }];
    }
}

#pragma mark - Menu view data source
- (NSInteger)numberOfColumnsInMenu:(DOPDropDownMenu *)menu {
    return 4;
}

- (NSInteger)menu:(DOPDropDownMenu *)menu numberOfRowsInColumn:(NSInteger)column {
    if (column == 0) {
        return _counties.count+1;
    } else if (column == 1) {
        return 4;
    } else if (column == 2) {
        return 3;
    } else  {
        return 6;
    }
}

- (NSString *)menu:(DOPDropDownMenu *)menu titleForRowAtIndexPath:(DOPIndexPath *)indexPath {
    
    switch (indexPath.column) {
        case 0:
            if (indexPath.row == 0) {
                return @"全部区域";
            }else{
                return [_counties objectAtIndex:indexPath.row-1];
            }
            
            break;
        case 1:
            switch (indexPath.row) {
            case 0:
                return @"全部距离";
                break;
            case 1:
                return @"5公里";
                break;
            case 2:
                return @"7公里";
                break;
            case 3:
                return @"10公里";
                break;
                
            default:
                return @"全部距离";
                break;
        }
            return @"全部距离";
            break;
        case 2:
            switch (indexPath.row) {
                case 0:
                    return @"排序";
                    break;
                case 1:
                    return @"价格";
                    break;
                case 2:
                    return @"距离";
                    break;
                    
                default:
                    return @"排序";
                    break;
            
        }
            return @"排序";
            break;
        case 3:
            switch (indexPath.row) {
            case 0:
                    return @"筛选";
                    break;
            case 1:
                return @"清吧";
                break;
            case 2:
                return @"嗨吧";
                break;
            case 3:
                return @"餐吧";
                break;
            case 4:
                return @"主题吧";
                break;
            case 5:
                return @"会所";
                break;
            
            default:
                return @"筛选";
                break;
        }
            
            break;
        default:
            return nil;
            break;
    }
}

- (void)menu:(DOPDropDownMenu *)menu didSelectRowAtIndexPath:(DOPIndexPath *)indexPath {

    NSString *title = [menu titleForRowAtIndexPath:indexPath];
    
    //区域
    if (indexPath.column == 0) {
        //不是全部
        if (indexPath.row !=0) {
            [filterDic setValue:title forKey:@"area"];
        } else {
            [filterDic setValue:@"全部区域" forKey:@"area"];
        }
    }
    //距离
    if (indexPath.column == 1) {
        
        if (indexPath.row <1) {
            [filterDic setValue:@0 forKey:@"distance"];
        } else {
            if (indexPath.row == 1) {
                [filterDic setValue:@5000 forKey:@"distance"];
            }else if (indexPath.row == 2) {
                [filterDic setValue:@7000 forKey:@"distance"];
            }else if (indexPath.row == 3) {
                [filterDic setValue:@10000 forKey:@"distance"];
            }
        }
    }
    //排序
    if (indexPath.column == 2) {
        if (indexPath.row == 0) {
        } else {
            if (indexPath.row == 1) {
                [filterDic setValue:@"price" forKey:@"orderBy"];
            }
            if (indexPath.row == 2) {
                [filterDic setValue:@"distance" forKey:@"orderBy"];
            }
        }
    }
    //筛选
    if (indexPath.column == 3) {
        if (indexPath.row >0) {
            [filterDic setValue:title forKey:@"classify"];
        } else {
            [filterDic setValue:@"筛选" forKey:@"classify"];
        }
    }
    [self backFilterClubList];
}

- (void)backFilterClubList{
    NSString *strUrl = [NSString stringWithFormat:@"%@Club/BackFilterClubList",BASICURL];
    NSLog(@"%@",strUrl);
    NSDictionary *dic = @{@"CategoryAs":[filterDic objectForKey:@"classify"],@"District":[filterDic objectForKey:@"area"]};
    [BingNetWorkConnection connectionWithUrl:strUrl type:BINGO_REQUEST_POST params:dic FinishBlock:^(id responseObject) {
        [self endRefresh];
        if ([[NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:nil] isKindOfClass:[NSDictionary class]]) {
            
            [SVProgressHUD showErrorWithStatus:@"服务器维护中"];
            
        } else  {
            NSString *str  =[[NSString alloc]initWithData:responseObject encoding:NSUTF8StringEncoding];
            if ([str isEqualToString:@"[]"]) {
                if (_array.count>0) {
                    [_array removeAllObjects];
                }
                if (bgImgView == nil) {
                    bgImgView = [[UIImageView alloc]init];
                    [bgImgView setImage:[UIImage imageNamed:@"notFound"]];
                    [bgImgView setContentMode:UIViewContentModeScaleAspectFit];
                    [self.view addSubview:bgImgView];
                    [bgImgView mas_makeConstraints:^(MASConstraintMaker *make) {
                        make.centerX.equalTo(self.view.mas_centerX);
                        make.top.equalTo(self.view.mas_top).offset(SCREEN_HEIGHT/5);
                        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH/2, SCREEN_HEIGHT/3));
                    }];
                    [self.tableView setScrollEnabled:NO];
                }
                
            }else if([str isEqualToString:@"[无商户]"]){
                if (_array.count>0) {
                    [_array removeAllObjects];
                }
            }else {
                if (bgImgView) {
                    [bgImgView removeFromSuperview];
                    [self.tableView setScrollEnabled:YES];
                }
                NSArray *arr = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:nil];
                NSLog(@"%@",[arr objectAtIndex:0]);
                if (_array.count>0) {
                    [_array removeAllObjects];
                }
                for (int i = 0; i < arr.count; i ++) {
                    NSDictionary *dic = [arr objectAtIndex:i];
                    ClubListInfo *clubInfo = [ClubListInfo mj_objectWithKeyValues:dic];
                    if ([clubInfo.City isEqualToString:leftItem.title]) {
                        [_array addObject:clubInfo];
                    }
                }
                
                [self arrayReorder];
                [self getDistance:_array andDistance:[[filterDic objectForKey:@"distance"] doubleValue]];
            }
            
            [self.tableView reloadData];
        }
        
    } FailedBlock:^(NSError *error) {
        
    }];
}

- (void)arrayReorder{
    //排序
    if ([filterDic objectForKey:@"orderBy"]){
        if ([[filterDic objectForKey:@"orderBy"] isEqualToString:@"price"]){
            NSArray *arr = [_array sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
                ClubListInfo *club1 = obj2;
                ClubListInfo *club2 = obj1;
                if ([club1.StartWith doubleValue] > [club2.StartWith doubleValue]) {
                    return (NSComparisonResult)NSOrderedAscending;
                }
                if ([club1.StartWith doubleValue] < [club2.StartWith doubleValue]) {
                    return (NSComparisonResult)NSOrderedDescending;
                }
                return (NSComparisonResult)NSOrderedSame;
            }];
            [_array removeAllObjects];
            [_array addObjectsFromArray:arr];
            
        } else if ([[filterDic objectForKey:@"orderBy"] isEqualToString:@"distance"]) {
            NSArray *arr = (NSMutableArray *)[_array sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
                ClubListInfo *club1 = obj2;
                ClubListInfo *club2 = obj1;
                
                CLLocation *current1 = [[CLLocation alloc] initWithLatitude:[club1.Latitude doubleValue] longitude:[club1.Longitude doubleValue]];
                CLLocation *current2 = [[CLLocation alloc] initWithLatitude:[club2.Latitude doubleValue] longitude:[club2.Longitude doubleValue]];
                CLLocation *before = _locationManager.location;
                CLLocationDistance meters1=[current1 distanceFromLocation:before];
                CLLocationDistance meters2=[current2 distanceFromLocation:before];
                if (meters1 > meters2) {
                    return (NSComparisonResult)NSOrderedAscending;
                }
                if (meters1 < meters2) {
                    return (NSComparisonResult)NSOrderedDescending;
                }
                return (NSComparisonResult)NSOrderedSame;
            }];
            [_array removeAllObjects];
            [_array addObjectsFromArray:arr];
            
        }
    }
}

- (void)getDistance:(NSMutableArray *)array andDistance:(double)distance{
    if (distance>100) {
        for (int i = 0; i < array.count; i ++) {
            ClubListInfo *clubInfo = [array objectAtIndex:i];
            CLLocation *current = [[CLLocation alloc] initWithLatitude:[clubInfo.Latitude doubleValue] longitude:[clubInfo.Longitude doubleValue]];
            CLLocation *before = _locationManager.location;
            CLLocationDistance meters=[current distanceFromLocation:before];
            if (meters >=distance) {
                [array removeObjectAtIndex:i];
                i--;
            }
        }
    }
    
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
        if (_array.count>0) {
            return _array.count;
        }else{
            return 0;
        }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ClubTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"clubcell" forIndexPath:indexPath];
    
    if (_array.count>0) {
        ClubListInfo *clubInfo = [_array objectAtIndex:indexPath.row];
        
        cell.clubNameLbl.text = clubInfo.ClubName;
        [cell.clubImgView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",IMGURL,clubInfo.ClubPic]] placeholderImage:[UIImage imageNamed:CACHEIMG]
                                     options:SDWebImageRefreshCached];
        cell.addressLbl.text  =clubInfo.Address;
        cell.priceLbl.text = [NSString stringWithFormat:@"人均 ￥%@",clubInfo.StartWith];
        //显示距离
        if (isLocationOpen){
            cell.distanceLbl.hidden = NO;
            //计算距离
            CLLocation *current = [[CLLocation alloc] initWithLatitude:[clubInfo.Latitude doubleValue] longitude:[clubInfo.Longitude doubleValue]];
            //第二个坐标
            CLLocation *before = _locationManager.location;
            // 计算距离
            CLLocationDistance meters=[current distanceFromLocation:before];
            cell.distanceLbl.text = [NSString stringWithFormat:@"< %.0fkm",meters/1000+1];
            
        } else {
            cell.distanceLbl.hidden = YES;
        }
        //是否显示“惠”
//        NSString *hiddenHui = [NSString stringWithFormat:@"%@",clubInfo.Recommend];
//        if ([hiddenHui  isEqual: @"0"]) {
//            cell.huiBtn.hidden = YES;
//        }else{
//            cell.huiBtn.hidden = NO;
//        }
        if ([clubInfo.Mode isEqualToString:@"A"]) {
            cell.huiBtn.hidden = NO;
        } else {
            cell.huiBtn.hidden = YES;
        }
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //选中行后去掉背景颜色
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    BusinessDescriptionVC *vc = [[BusinessDescriptionVC alloc]init];
    vc.clubInfo = [_array objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:vc animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 89.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    /*
     创建表格头视图
     */
    UIView *headView = [[UIView alloc]initWithFrame:CGRectMake(0.0, 0.0, self.view.frame.size.width, 30.0)];
    
    //创建定位label
    UILabel *locationLbl = [[UILabel alloc]initWithFrame:CGRectMake(0.0, 0.0, self.view.frame.size.width, 30.0)];
    locationLbl.text = nowLocationStr;
    locationLbl.font = [UIFont fontWithName:@"Helvetica" size:14.0];
    locationLbl.backgroundColor = [UIColor colorWithRed:224.0/255 green:225.0/255 blue:226.0/255 alpha:1.0];
    [headView addSubview:locationLbl];
    
    return headView;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 30.0;
}


/*
#pragma mark - Navigation

 In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
     Get the new view controller using [segue destinationViewController].
     Pass the selected object to the new view controller.
}
*/

@end
