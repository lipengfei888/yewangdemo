//
//  SelectedListView.h
//  Yewang
//
//  Created by everhelp on 16/10/26.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelectedListView : UIView <UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) NSMutableArray *objects;

@property (nonatomic, strong) UITableView *tableView;

- (instancetype)initWithFrame:(CGRect)frame withObjects:(NSMutableArray *)objects;

- (instancetype)initWithFrame:(CGRect)frame withObjects:(NSMutableArray *)objects canReorder:(BOOL)reOrder;

@end
