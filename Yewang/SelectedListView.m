//
//  SelectedListView.m
//  Yewang
//
//  Created by everhelp on 16/10/26.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import "SelectedListView.h"
#import "SelectedTableViewCell.h"

@implementation SelectedListView

- (instancetype)initWithFrame:(CGRect)frame withObjects:(NSMutableArray *)objects{
    return [self initWithFrame:frame withObjects:objects canReorder:NO];
}

- (instancetype)initWithFrame:(CGRect)frame withObjects:(NSMutableArray *)objects canReorder:(BOOL)reOrder{
    
    if (self == [super initWithFrame:frame]) {
        
        self.objects = [NSMutableArray arrayWithArray:objects];
        
        [self layoutUI];
        [self.tableView reloadData];
    }
    
    return self;
    
}

- (void)layoutUI{
    self.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.7];
    self.tableView = [[UITableView alloc] initWithFrame:self.bounds];
    self.tableView.bounces = NO;
    self.tableView.showsHorizontalScrollIndicator = NO;
    self.tableView.showsVerticalScrollIndicator = NO;
//    self.tableView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.tableView];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.tableView registerNib:[UINib nibWithNibName:@"SelectedTableViewCell" bundle:nil] forCellReuseIdentifier:@"SelectedTableViewCell"];
    
}

#pragma mark - UITableViewDelegate,UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _objects.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    SelectedTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SelectedTableViewCell" forIndexPath:indexPath];
    NSMutableDictionary *dic = _objects[indexPath.row];
    [cell dictionaryModel:dic];
    
    cell.operationBlock = ^(NSInteger number, BOOL plus){
        NSMutableDictionary *dict = self.objects[indexPath.row];
        //更新选中订单列表中的数量
        [dict setValue:[NSNumber numberWithInteger:number] forKey:@"Count"];
        
        NSMutableDictionary *notification = [[NSMutableDictionary alloc] init];
        [notification setValue:[NSNumber numberWithBool:plus] forKey:@"isAdd"];
        [notification setValue:dict forKey:@"update"];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"UpdateUI" object:self userInfo:notification];
    };
    
    return cell;
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
