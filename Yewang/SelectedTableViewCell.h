//
//  SelectedTableViewCell.h
//  Yewang
//
//  Created by everhelp on 16/10/10.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelectedTableViewCell : UITableViewCell
///名字
@property (weak, nonatomic) IBOutlet UILabel *wineNameLbl;
///价格
@property (weak, nonatomic) IBOutlet UILabel *priceLbl;
///减少
@property (weak, nonatomic) IBOutlet UIButton *subBtn;
///数量
@property (weak, nonatomic) IBOutlet UILabel *countLbl;
///增加
@property (weak, nonatomic) IBOutlet UIButton *addBtn;

@property (nonatomic, copy) void (^operationBlock)(NSInteger number, BOOL plus);

@property (nonatomic, assign) NSInteger number;

- (void)dictionaryModel:(NSMutableDictionary *)dicModel;

@end
