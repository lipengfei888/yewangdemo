//
//  SelectedTableViewCell.m
//  Yewang
//
//  Created by everhelp on 16/10/10.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import "SelectedTableViewCell.h"

@implementation SelectedTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (void)dictionaryModel:(NSMutableDictionary *)dicModel{
    self.wineNameLbl.text = dicModel[@"ItemName"];
    self.priceLbl.text = [NSString stringWithFormat:@"￥ %@",dicModel[@"Price"]];
    self.countLbl.text = [NSString stringWithFormat:@"%@",dicModel[@"Count"]];
}

- (IBAction)minusClick:(id)sender {
    self.number = [self.countLbl.text integerValue];
    self.number -= 1;
    [self showNumber:self.number];
    self.operationBlock(self.number,NO);
}

- (IBAction)plusClick:(id)sender {
    self.number = [self.countLbl.text integerValue];
    self.number += 1;
    [self showNumber:self.number];
    self.operationBlock(self.number,YES);
}

- (void)showNumber:(NSUInteger)count{
    self.countLbl.text = [NSString stringWithFormat:@"%lu",(unsigned long)self.number];
}

@end
