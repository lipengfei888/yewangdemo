//
//  UIAlertController+WarnMessage.h
//  Yewang
//
//  Created by everhelp on 16/9/2.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIAlertController (WarnMessage)
+ (void)showWarnTitle:(NSString *)title andMessage:(NSString *)message withController:(UIViewController *)viewcontroller ;
+ (void)showUpdateSet:(NSString *)title andMessage:(NSString *)message withController:(UIViewController *)viewcontroller;
@end
