//
//  UIAlertController+WarnMessage.m
//  Yewang
//
//  Created by everhelp on 16/9/2.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import "UIAlertController+WarnMessage.h"

@implementation UIAlertController (WarnMessage)

+ (void)showWarnTitle:(NSString *)title andMessage:(NSString *)message withController:(UIViewController *)viewcontroller{
    
}

+ (void)showUpdateSet:(NSString *)title andMessage:(NSString *)message withController:(UIViewController *)viewcontroller{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *goSetAction = [UIAlertAction actionWithTitle:@"去设置" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
        if ([[UIApplication sharedApplication] canOpenURL:url]) {
            NSURL *appUrl = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
            [[UIApplication sharedApplication] openURL:appUrl];
        }
    }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"好的" style:UIAlertActionStyleCancel handler:nil];
    [alertController addAction:goSetAction];
    [alertController addAction:cancelAction];
    [viewcontroller presentViewController:alertController animated:YES completion:nil];
}

@end
