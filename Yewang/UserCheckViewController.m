//
//  UserCheckViewController.m
//  Yewang
//
//  Created by everhelp on 16/10/8.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import "UserCheckViewController.h"
///商品名称和套餐名称显示
#import "OrderNameTableViewCell.h"
///订酒列表
#import "OrderWineTableViewCell.h"
///联系用户列表
#import "ContactPersonTableViewCell.h"
///卡券、备注视图
#import "RemarkTableViewCell.h"
///订单页的model
#import "OrderDetailModel.h"
///底部预定视图
#import "OrderView.h"
///支付页
#import "BuyDetailViewController.h"

@interface UserCheckViewController ()<UITableViewDelegate,UITableViewDataSource>
///主表
@property (nonatomic, strong) UITableView *mainTableView;
///底部购买视图
@property (nonatomic, strong) OrderView *bottomView;

@end

@implementation UserCheckViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tabBarController.tabBar.hidden = YES;
    self.navigationController.navigationBar.hidden = NO;
    self.navigationItem.title = @"确认订单";
    [self initFrameByCode];
}

///加载控件
- (void)initFrameByCode{
    //初始化主表
    _mainTableView = [UITableView new];
    _mainTableView.delegate = self;
    _mainTableView.dataSource = self;
    [self.view addSubview:_mainTableView];
    [self.mainTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view).insets(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    //注册表格单元格
    [self.mainTableView registerNib:[UINib nibWithNibName:@"OrderNameTableViewCell" bundle:nil] forCellReuseIdentifier:@"OrderNameTableViewCell"];
    [self.mainTableView registerNib:[UINib nibWithNibName:@"OrderWineTableViewCell" bundle:nil] forCellReuseIdentifier:@"OrderWineTableViewCell"];
    [self.mainTableView registerNib:[UINib nibWithNibName:@"ContactPersonTableViewCell" bundle:nil] forCellReuseIdentifier:@"ContactPersonTableViewCell"];
    [self.mainTableView registerNib:[UINib nibWithNibName:@"RemarkTableViewCell" bundle:nil] forCellReuseIdentifier:@"RemarkTableViewCell"];
    //初始化底部视图
    _bottomView = [[OrderView alloc] initWithFrame:CGRectZero inView:self.view];
    [self.view addSubview:_bottomView];
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view).insets(UIEdgeInsetsMake(SCREEN_HEIGHT-40, 0, 0, 0));
    }];
}

#pragma mark - UITableViewDelegate,UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 4;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    switch (section) {
        case 0:
            return 1;
            break;
        case 1:
            return 2;
            break;
        case 2:
            return 3;
            break;
        case 3:
            return 1;
            break;
        default:
            break;
    }
    return 0;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.section) {
        case 0:
        {
            static NSString *identifier = @"OrderNameTableViewCell";
            OrderNameTableViewCell *classCell = (OrderNameTableViewCell *)[tableView dequeueReusableCellWithIdentifier:identifier];
            if (classCell == nil) {
                classCell = [[OrderNameTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
            }
            classCell.tradeNameLbl.text = @"     这家店叫从前";
            classCell.packageNameLbl.text = @"套餐名字";
            classCell.packageNumberLbl.text = @"x 1";
            classCell.packagePriceLbl.text = @"￥ 2333";
            return classCell;
        }
            break;
        case 1:
        {
            static NSString *identifier = @"OrderWineTableViewCell";
            OrderWineTableViewCell *classCell = (OrderWineTableViewCell *)[tableView dequeueReusableCellWithIdentifier:identifier];
            if (classCell == nil) {
                classCell = [[OrderWineTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
            }
            classCell.wineNameLbl.text = @"杰克丹尼";
            classCell.wineNumberLbl.text = @"x 1";
            classCell.winePriceLbl.text = @"￥ 488";
            return classCell;
        }
            break;
        case 2:
        {
            static NSString *identifier = @"ContactPersonTableViewCell";
            ContactPersonTableViewCell *classCell = (ContactPersonTableViewCell *)[tableView dequeueReusableCellWithIdentifier:identifier];
            if (classCell == nil) {
                classCell = [[ContactPersonTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
            }
            classCell.titleLbl.text = @"联系人：";
            classCell.detailLbl.text = @"谁谁谁";
            return classCell;
        }
            break;
        case 3:
        {
            static NSString *identifier = @"RemarkTableViewCell";
            RemarkTableViewCell *classCell = (RemarkTableViewCell *)[tableView dequeueReusableCellWithIdentifier:identifier];
            if (classCell == nil) {
                classCell = [[RemarkTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
            }
            return classCell;
        }
            break;
        default:
            break;
    }
    return 0;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    switch (section) {
        case 0:
            return 10;
            break;
        case 1:
            return 1;
            break;
        case 2:
        case 3:
            return 10;
            break;
        default:
            break;
    }
    return 0;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.section) {
        case 0:
            return 130;
            break;
        case 1:
            return 40;
            break;
        case 2:
            return 40;
            break;
        case 3:
            return 180;
            break;
        default:
            break;
    }
    return 0;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
