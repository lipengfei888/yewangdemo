//
//  UserHeader.h
//  Yewang
//
//  Created by everhelp on 16/7/25.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserHeader : NSObject

+ (BOOL)userIsLogin;

@end
