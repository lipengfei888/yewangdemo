//
//  UserHeader.m
//  Yewang
//
//  Created by everhelp on 16/7/25.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import "UserHeader.h"

@implementation UserHeader

+ (BOOL)userIsLogin{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *userID = [userDefault objectForKey:@"ID"];
    if (userID == nil) {
        return NO;
    }else{
        return YES;
    }
}

@end
