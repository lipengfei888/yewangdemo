//
//  WalletViewController.m
//  Yewang
//
//  Created by everhelp on 16/4/13.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import "WalletViewController.h"
#import "CouponViewController.h"

@interface WalletViewController ()
///账户余额
@property (nonatomic, strong) UILabel *balanceLbl;
///充值
@property (nonatomic, strong) UIButton *rechargeBtn;
///提现
@property (nonatomic, strong) UIButton *takeoutBtn;
///卡券视图
@property (nonatomic, strong) UIView *couponView;

@end

@implementation WalletViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initWithFrame];
    [self initWithTouchUp];
}
- (void)viewWillAppear:(BOOL)animated{
    self.tabBarController.tabBar.hidden = YES;
    [super viewWillAppear:animated];
}

- (void)initWithFrame{
    //设置导航栏标题
    self.navigationItem.title = @"用户名";
    //设置视图背景为灰色
    self.view.backgroundColor = [UIColor colorWithRed:224.0/255 green:225.0/255 blue:226.0/255 alpha:1.0];
    //账户余额
    _balanceLbl = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 74, SCREEN_WIDTH, 40.0)];
    _balanceLbl.backgroundColor = [UIColor whiteColor];
    _balanceLbl.font = [UIFont systemFontOfSize:18.0];
    _balanceLbl.text = [NSString stringWithFormat:@"    账户余额        ￥ %.2f",1000000.0];
    [self.view addSubview:_balanceLbl];
    //充值
    _rechargeBtn = [[UIButton alloc] init];
    [_rechargeBtn setBackgroundColor:[UIColor whiteColor]];
    [_rechargeBtn setImage:[UIImage imageNamed:@"recharge"] forState:UIControlStateNormal];
    [_rechargeBtn setImageEdgeInsets:UIEdgeInsetsMake(15, 55, 55, 55)];
    [_rechargeBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.view addSubview:_rechargeBtn];
    [self.rechargeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left).offset(0);
        make.top.equalTo(self.balanceLbl.mas_bottom).offset(5);
        make.width.mas_equalTo(@(SCREEN_WIDTH/2-1));
        make.height.mas_equalTo(@(SCREEN_WIDTH/2-40));
    }];
    UILabel *rechargeLbl = [[UILabel alloc] init];
    rechargeLbl.text = @"充值";
    rechargeLbl.font = [UIFont italicSystemFontOfSize:18];
    [self.rechargeBtn addSubview:rechargeLbl];
    [rechargeLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.rechargeBtn.mas_centerX);
        make.bottom.equalTo(self.rechargeBtn.mas_bottom).offset(-20);
        make.height.mas_equalTo(@30);
    }];
    //提现
    _takeoutBtn = [[UIButton alloc] init];
    [_takeoutBtn setBackgroundColor:[UIColor whiteColor]];
    [_takeoutBtn setImage:[UIImage imageNamed:@"takeout"] forState:UIControlStateNormal];
    [_takeoutBtn setImageEdgeInsets:UIEdgeInsetsMake(15, 55, 55, 55)];
    [_takeoutBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.view addSubview:_takeoutBtn];
    [self.takeoutBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.view.mas_right).offset(0);
        make.top.equalTo(self.balanceLbl.mas_bottom).offset(5);
        make.width.mas_equalTo(@(SCREEN_WIDTH/2));
        make.height.mas_equalTo(@(SCREEN_WIDTH/2-40));
    }];
    UILabel *takeoutLbl = [[UILabel alloc] init];
    takeoutLbl.text = @"提现";
    takeoutLbl.font = [UIFont italicSystemFontOfSize:18];
    [self.takeoutBtn addSubview:takeoutLbl];
    [takeoutLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.takeoutBtn.mas_centerX);
        make.bottom.equalTo(self.takeoutBtn.mas_bottom).offset(-20);
        make.height.mas_equalTo(@30);
    }];
    //卡券
    _couponView = [[UIView alloc] init];
    [_couponView setBackgroundColor:[UIColor whiteColor]];
    [self.view addSubview:_couponView];
    [self.couponView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left).offset(0);
        make.top.equalTo(self.rechargeBtn.mas_bottom).offset(2);
        make.width.mas_equalTo(SCREEN_WIDTH);
        make.height.mas_equalTo(@70);
    }];
    //卡券图片
    UIImageView *couponImgView = [[UIImageView alloc] init];
    [couponImgView setImage:[UIImage imageNamed:@"coupon1"]];
    [couponImgView setContentMode:UIViewContentModeScaleAspectFit];
    [self.couponView addSubview:couponImgView];
    [couponImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.couponView.mas_centerY);
        make.left.equalTo(self.couponView.mas_left).offset(20);
        make.width.mas_equalTo(@30);
        
    }];
    //卡券 字
    UILabel *couponLbl = [UILabel new];
    couponLbl.text = @"卡券";
    couponLbl.font = [UIFont systemFontOfSize:24];
    [self.couponView addSubview:couponLbl];
    [couponLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.couponView.mas_centerY);
        make.left.equalTo(couponImgView.mas_right).offset(20);
        make.width.mas_equalTo(@60);
    }];
    //卡券 数量
    UILabel *numLbl = [[UILabel alloc] init];
    numLbl.text = @"2张";
    numLbl.font = [UIFont systemFontOfSize:15];
    numLbl.textColor = [UIColor grayColor];
    [self.couponView addSubview:numLbl];
    [numLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.couponView.mas_centerY);
        make.left.equalTo(couponLbl.mas_right).offset(20);
//        make.width.mas_equalTo(@60);
    }];
    
}

- (void)initWithTouchUp{
    [self.rechargeBtn addTarget:self action:@selector(clickRechargeBtn) forControlEvents:UIControlEventTouchUpInside];
    [self.takeoutBtn addTarget:self action:@selector(clickTakeoutBtn) forControlEvents:UIControlEventTouchUpInside];
    UITapGestureRecognizer *tapges = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(clickCoupon)];
    [self.couponView addGestureRecognizer:tapges];
}

///点击充值
- (void)clickRechargeBtn{
    NSLog(@"recharge");
}
///点击提现
- (void)clickTakeoutBtn{
    NSLog(@"takeout");
}
///点击卡券
- (void)clickCoupon{
    NSLog(@"coupon");
    CouponViewController *coupon = [[CouponViewController alloc] init];
    [self.navigationController pushViewController:coupon animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
