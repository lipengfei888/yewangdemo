//
//  WineModel.h
//  Yewang
//
//  Created by everhelp on 16/10/14.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface WineModel : NSObject

#pragma mark - 计算价格
+ (double)GetTotalPrice:(NSMutableArray *)dataArray;

#pragma mark - 计算订单数据
+ (NSMutableArray *)StoreOrder:(NSMutableDictionary *)dictionary OrderData:(NSMutableArray *)orderArray isAdded:(BOOL)added;

#pragma mark - 计算数量
+ (NSInteger) CountOthersWithOrderData:(NSMutableArray *)orderArray;

#pragma mark - 更新显示数据
+ (NSMutableArray *)UpdateArray:(NSMutableArray *)dataArray atSelectDictionary:(NSMutableDictionary *)dictionary;

@end
