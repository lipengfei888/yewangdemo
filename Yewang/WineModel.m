//
//  WineModel.m
//  Yewang
//
//  Created by everhelp on 16/10/14.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import "WineModel.h"

@implementation WineModel

#pragma mark - 计算价格
+ (double)GetTotalPrice:(NSMutableArray *)dataArray{
    
    double nTotal = 0.0;
    for (NSDictionary *dic in dataArray) {
        if ([dic objectForKey:@"Count"] != nil) {
            nTotal += [[dic objectForKey:@"Count"] integerValue] * [[dic objectForKey:@"Price"] doubleValue];
        }
    }
    return nTotal;
}

#pragma mark - 计算订单数据
+ (NSMutableArray *)StoreOrder:(NSMutableDictionary *)dictionary OrderData:(NSMutableArray *)orderArray isAdded:(BOOL)added{
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:dictionary];
    //增加订单和减少订单
    if (added) {
        
        if (orderArray.count != 0) {
            BOOL flage = YES;
            for (NSMutableDictionary *dic in orderArray) {
                if (dic[@"ID"] == dict[@"ID"]) {
                    NSInteger count = [dict[@"Count"] integerValue];
                    [dic setValue:[NSString stringWithFormat:@"%ld",(long)count] forKey:@"Count"];
                    flage = NO;
                    break;
                }
            }
            if (flage) {
                [orderArray addObject:dict];
            }
        } else {
            [orderArray addObject:dict];
        }
    } else {
        
        for (int i=0; i<orderArray.count; i++) {
            
            NSMutableDictionary *dic = orderArray[i];
            
            if ([dict objectForKey:@"ID"] == [dic objectForKey:@"ID"]) {
                
                if ([dict[@"Count"] integerValue] == 0) {
                    [orderArray removeObjectAtIndex:i];
                    break;
                }
                if ([dic[@"Count"] integerValue] == 0) {
                    [orderArray removeObjectAtIndex:i];
                } else {
                    [dic setValue:[NSString stringWithFormat:@"%@",dict[@"Count"]] forKey:@"Count"];
                }
                break;
            }
        }
    }
    
    return orderArray;
}

#pragma mark - 计算数量
+ (NSInteger) CountOthersWithOrderData:(NSMutableArray *)orderArray{
    
    NSInteger count = 0;
    for (int i = 0; i < orderArray.count; i++) {
        NSMutableDictionary *dic = orderArray[i];
        count += [dic[@"Count"] integerValue];
    }
    
    return count;
}

#pragma mark - 更新显示数据
+ (NSMutableArray *)UpdateArray:(NSMutableArray *)dataArray atSelectDictionary:(NSMutableDictionary *)dictionary{
    
    for (NSMutableDictionary *dic in dataArray) {
        if (dic[@"ID"] == dictionary[@"ID"]) {
            
            NSInteger count = [dictionary[@"Count"] integerValue];
            [dic setValue:[NSString stringWithFormat:@"%ld",(long)count] forKey:@"Count"];
            break;
            
        }
    }
    
    return dataArray;
}

@end
