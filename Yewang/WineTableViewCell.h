//
//  WineTableViewCell.h
//  Yewang
//
//  Created by everhelp on 16/10/10.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface WineTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *wineNameLbl;
@property (weak, nonatomic) IBOutlet UILabel *winePriceLbl;
@property (weak, nonatomic) IBOutlet UILabel *countLbl;
@property (weak, nonatomic) IBOutlet UIButton *wineSubBtn;
@property (weak, nonatomic) IBOutlet UIButton *wineAddBtn;
///数量
@property (nonatomic, assign) NSInteger wineCount;
//增加/减少订单数量，是否需要动画效果
@property (nonatomic, copy) void (^plusBlock)(NSInteger count,BOOL animated);

- (void)setTableViewVell:(NSMutableDictionary *)model;

@end
