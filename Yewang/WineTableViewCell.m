//
//  WineTableViewCell.m
//  Yewang
//
//  Created by everhelp on 16/10/10.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import "WineTableViewCell.h"

@interface WineTableViewCell ()
@end
@implementation WineTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.wineSubBtn.tag = 200;
    self.wineAddBtn.tag = 202;
    self.countLbl.hidden = YES;
    self.wineSubBtn.hidden = YES;
    UILabel *bgLbl = [UILabel new];
    bgLbl.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [self addSubview:bgLbl];
    [bgLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(@1);
        make.left.equalTo(self.mas_left).offset(0);
        make.right.equalTo(self.mas_right).offset(0);
        make.bottom.equalTo(self.mas_bottom).offset(0);
    }];
}
- (IBAction)clickSubBtn:(UIButton *)sender {
    
    self.wineCount = [self.countLbl.text intValue];
    self.wineCount -= 1;
    self.plusBlock(self.wineCount,NO);
    [self showOrderNumbers:self.wineCount];
}

- (IBAction)clickAddBtn:(UIButton *)sender {
    
    self.wineCount = [self.countLbl.text intValue];
    self.wineCount += 1;
    self.plusBlock(self.wineCount,YES);
    [self showOrderNumbers:self.wineCount];
    
}

- (void)setTableViewVell:(NSMutableDictionary *)model{
    self.wineNameLbl.text = model[@"ItemName"];
    self.detailTextLabel.text = model[@"ItemContent"];
    self.winePriceLbl.text = [model[@"Price"] stringValue];
    self.countLbl.text = model[@"Count"];
    NSLog(@"modelCount=====%@",model[@"Count"]);
    if ([model[@"Count"] intValue] > 0) {
        [self.wineSubBtn setHidden:NO];
        [self.countLbl setHidden:NO];
    } else {
        [self.wineSubBtn setHidden:YES];
        [self.countLbl setHidden:YES];
    }
}

//显示加减的图片
- (void)showOrderNumbers:(NSUInteger)count{
    self.countLbl.text = [NSString stringWithFormat:@"%lu",(unsigned long)self.wineCount];
    if (self.wineCount>0) {
        [self.wineSubBtn setHidden:NO];
        [self.countLbl setHidden:NO];
    } else {
        [self.wineSubBtn setHidden:YES];
        [self.countLbl setHidden:YES];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
