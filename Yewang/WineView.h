//
//  WineView.h
//  Yewang
//
//  Created by everhelp on 16/10/12.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import <UIKit/UIKit.h>
@class WineView;

@protocol WineViewDelegate <NSObject>

- (void)clickRightBottomBtn:(NSString *)status;

- (void)wineAddOrSubOrder:(NSMutableDictionary *)dictionary OrderData:(NSMutableArray *)ordersArray isAdded:(BOOL)added andStartPoint:(CGPoint)start;

@optional
- (void)view:(WineView *)lgView didSelectRowAtIndexPath:(NSIndexPath *)indexPath Title:(NSString *)title;
@end

@interface WineView : UIView<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, assign)id <WineViewDelegate> delegate;

- (instancetype)initWithFrame:(CGRect)frame andClubID:(NSString *)ClubId;

@end
