//
//  WineView.m
//  Yewang
//
//  Created by everhelp on 16/10/12.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import "WineView.h"
#import "OrderView.h"
#import "WineTableViewCell.h"
#import "SelectedTableViewCell.h"
#import "GroupWineTableViewCell.h"
#import "WineModel.h"

@interface WineView ()
{
    BOOL isRight;
    BOOL isClick;
}
///左侧酒列表
@property (nonatomic, strong) UITableView *groupWineTableView;
///右侧酒的分组表格
@property (nonatomic, strong) UITableView *wineTableView;

///左侧酒列表数组
@property (nonatomic, strong) NSMutableArray *groupArray;
///酒分类数组
@property (nonatomic, strong) NSMutableArray *wineArray;
///订单数组
@property (nonatomic, strong) NSMutableArray *ordersArray;
@end

@implementation WineView
#pragma mark - 加载视图

- (instancetype)initWithFrame:(CGRect)frame andClubID:(NSString *)ClubId{
    if (self == [super initWithFrame:frame]) {
        [self initWithData:ClubId];
        [self initWithFrameByCode];
        
        ///注册刷新当前的通知
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(UpdateViewUI:) name:@"UpdateUI" object:nil];
        
    }
    return self;
}


- (void)initWithFrameByCode{
    //初始化左侧酒的分类
    _groupWineTableView = [[UITableView alloc] init];
    _groupWineTableView.delegate = self;
    _groupWineTableView.dataSource = self;
    [self addSubview:_groupWineTableView];
    _groupWineTableView.tag = 100;
    [self.groupWineTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top).offset(0);
        make.left.equalTo(self.mas_left).offset(0);
        make.width.mas_offset(@(SCREEN_WIDTH/4));
        make.bottom.equalTo(self).offset(-40);
    }];
    //去除下面多余的cell
    self.groupWineTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    //初始化右侧酒的分组表格
    _wineTableView = [[UITableView alloc] init];
    _wineTableView.delegate = self;
    _wineTableView.dataSource = self;
    [self addSubview:_wineTableView];
    _wineTableView.tag = 200;
    [self.wineTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top).offset(0);
        make.right.equalTo(self.mas_right).offset(0);
        make.width.mas_offset(@(SCREEN_WIDTH/4*3));
        make.bottom.equalTo(self).offset(-40);
    }];
    //注册表格单元格
    [self.groupWineTableView registerClass:[GroupWineTableViewCell class] forCellReuseIdentifier:@"GroupWineTableViewCell"];
    [self.wineTableView registerNib:[UINib nibWithNibName:@"WineTableViewCell" bundle:nil] forCellReuseIdentifier:@"WineTableViewCell"];
    [self.wineTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    isRight = YES;
}

//初始化订单数组
- (NSMutableArray *)ordersArray{
    if (!_ordersArray) {
        _ordersArray = [NSMutableArray new];
    }
    return _ordersArray;
}

#pragma mark - 加载数据
- (void)initWithData:(NSString *)clubId{
    _groupArray = [NSMutableArray arrayWithCapacity:0];
    _wineArray = [NSMutableArray arrayWithCapacity:0];

    NSString *strUrl = [NSString stringWithFormat:@"%@Club/GetItemType?ClubID=%@",BASICURL,clubId];
    [BingNetWorkConnection connectionWithUrl:strUrl type:BINGO_REQUEST_GET params:nil FinishBlock:^(id responseObject) {
        
        if ([[NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:nil] isKindOfClass:[NSArray class]]) {
            NSArray *arr = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:nil];
            NSComparator finderSort = ^(id dic1,id dic2){
                NSString *string1 = dic1[@"Sequence"];
                NSString *string2 = dic2[@"Sequence"];
                if ([string1 integerValue] > [string2 integerValue]) {
                    return (NSComparisonResult)NSOrderedDescending;
                }else if ([string1 integerValue] < [string2 integerValue]){
                    return (NSComparisonResult)NSOrderedAscending;
                }
                else
                    return (NSComparisonResult)NSOrderedSame;
            };
            NSArray *resultArray = [arr sortedArrayUsingComparator:finderSort];
            for (NSDictionary *dic in resultArray) {
                [_groupArray addObject:dic[@"ItemType"]];
            }
        } else {
            NSLog(@"获取类型：%@",[[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding]);
        }
        [self.groupWineTableView reloadData];
        [self.wineTableView reloadData];
    } FailedBlock:^(NSError *error) {
        NSLog(@"获取类型：%@",error);
    }];
    
    NSString *strUrl2 = [NSString stringWithFormat:@"%@Club/GetItemList?ClubID=%@",BASICURL,clubId];
    [BingNetWorkConnection connectionWithUrl:strUrl2 type:BINGO_REQUEST_GET params:nil FinishBlock:^(id responseObject) {
        
        if ([[NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:nil] isKindOfClass:[NSArray class]]) {
            NSArray *arr = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:nil];
            for (NSMutableDictionary *dic in arr) {
                
                [_wineArray addObject:dic];
            }
            NSLog(@"_wineArray===%@",_wineArray);
            NSLog(@"%@",[[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding]);
        } else {
            NSLog(@"%@",[[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding]);
        }
        [self.groupWineTableView reloadData];
        [self.wineTableView reloadData];
    } FailedBlock:^(NSError *error) {
        NSLog(@"获取商品：%@",error);
    }];
    
}

#pragma UITableViewDataSource,UITableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if (self.wineTableView == tableView) {
        NSLog(@"_groupArray.count===%ld",(unsigned long)_groupArray.count);
        return _groupArray.count;
    }
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSInteger count = 0;
    if (self.groupWineTableView == tableView) {
        return _groupArray.count;
    } else  {
        for (NSMutableDictionary *product in _wineArray) {
            if (![product[@"ItemType"] isKindOfClass:[NSNull class]]) {
                if ([product[@"ItemType"] isEqualToString:[_groupArray objectAtIndex:section]]) {
                    count ++;
                }
                
            }
        }
        return count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.groupWineTableView == tableView) {
        GroupWineTableViewCell *cell;
        if (cell == nil) {
            cell = (GroupWineTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"GroupWineTableViewCell" forIndexPath:indexPath];
        }
        [cell.groupNameBtn setTitle:_groupArray[indexPath.row] forState:UIControlStateNormal];
        cell.groupNameBtn.tag = indexPath.row;
        [cell.groupNameBtn addTarget:self action:@selector(clickGroupWineName:) forControlEvents:UIControlEventTouchUpInside];
        return cell;
    } else if (self.wineTableView == tableView) {
        static NSString *identifier = @"WineTableViewCell";
        WineTableViewCell *cell;
        if (cell == nil) {
            cell = [tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
        }
        NSMutableDictionary *product = [_wineArray objectAtIndex:indexPath.row];
        if (![product[@"ItemType"] isKindOfClass:[NSNull class]]) {
            if ([product[@"ItemType"] isEqualToString:[_groupArray objectAtIndex:indexPath.section]]) {
                [cell setTableViewVell:product];
            }
            
        }
        
        [cell setTableViewVell:product];
        
        __weak __typeof(&*cell)weakCell =cell;
        __block __typeof(self)bself = self;
        
        cell.plusBlock = ^(NSInteger nCount,BOOL animated){
            
            NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithDictionary:bself.wineArray[indexPath.row]];
            [dic setObject:[NSString stringWithFormat:@"%ld",(long)nCount] forKey:@"Count"];
            
            //通过坐标转换得到抛物线的起点和终点
            CGRect parentRectA = [weakCell convertRect:weakCell.wineAddBtn.frame toView:self.superview.superview.superview];
            if (animated) {
                [self.delegate wineAddOrSubOrder:dic OrderData:self.ordersArray isAdded:YES andStartPoint:parentRectA.origin];
            } else {
                [self.delegate wineAddOrSubOrder:dic OrderData:self.ordersArray isAdded:NO andStartPoint:parentRectA.origin];
            }
        };
        return cell;
    }
    return nil;
}
//分组标题
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    if (self.wineTableView == tableView) {
        return _groupArray[section];
    }
    return nil;
}
//单元格高度
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.wineTableView == tableView) {
        return 50;
    }
    return 44;
}
#pragma mark - 单元格的点击事件
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //选中行后去掉背景颜色
//    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
//    cell.selectionStyle = UITableViewCellSelectionStyleNone;
//    //分组标题表格的点击事件
//    if (self.wineTableView == tableView) {
//        NSLog(@"section:%li=====row:%li",indexPath.section,indexPath.row);
//    } else {
//        NSLog(@"group");
//    }
    UITableView *tableView1 = (UITableView *)[self viewWithTag:100];
    UITableView *tableView2 = (UITableView *)[self viewWithTag:200];
    //如果选中左边的
    if (tableView == tableView1) {
        isRight = NO;
        //让右边相应的区头滚动到上部
        [tableView2 selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:indexPath.row] animated:YES scrollPosition:UITableViewScrollPositionTop];
        NSLog(@"11111");
    } else if (tableView == tableView2) {
        //右边的
        NSLog(@"2222222");
        if ([self.delegate respondsToSelector:@selector(view:didSelectRowAtIndexPath:Title:)]) {
            [self.delegate view:self didSelectRowAtIndexPath:indexPath Title:self.wineArray[indexPath.section][indexPath.row]];
        }
    }

}

- (void)clickGroupWineName:(UIButton *)aButton{
    NSIndexPath *index  = [NSIndexPath indexPathForRow:0 inSection:aButton.tag];
  //    [self.wineTableView 一天:index atScrollPosition:UITableViewScrollPositionTop animated:YES];
    [_wineTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:index.row] animated:YES scrollPosition:UITableViewScrollPositionTop];
}

#pragma mark - 通知更新
- (void)UpdateViewUI:(NSNotification *)notification{
    NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithDictionary:notification.userInfo];
    self.wineArray = [WineModel UpdateArray:self.wineArray atSelectDictionary:dic];
    [self.wineTableView reloadData];
}

#pragma -mark 获取 NSIndexPath
-(NSIndexPath *)indexPathForView:(UIView *)view andTabelView:(UITableView *) tabelview{
    UIView *parentView=[view superview];
    while (![parentView isKindOfClass:[UITableViewCell class]] && parentView!=nil) {
        parentView=parentView.superview;
    }
    return [tabelview indexPathForCell:(UITableViewCell *)parentView];
}

- (void)view:(WineView *)lgView didSelectRowAtIndexPath:(NSIndexPath *)indexPath Title:(NSString *)title
{
    NSLog(@"%ld, %ld, title = %@", indexPath.section, indexPath.row, title);
}
- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self forKeyPath:@"UpdateUI"];
}

@end
