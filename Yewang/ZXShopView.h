//
//  ZXShopView.h
//  Yewang
//
//  Created by everhelp on 16/11/21.
//  Copyright © 2016年 Anshare. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZXShopView : UIView
/** 订单数据 */
@property (nonatomic, strong) NSMutableArray *orderArray;

/** 订单所选总数量 */
@property (nonatomic, assign) NSInteger totalOrderCount;

/** 订单总价 */
@property (assign, nonatomic) double totalPrice;
@end
